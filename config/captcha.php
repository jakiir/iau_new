<?php

return [

    'default'   => [
        'length'    => 5,
        'width'     => 150,
        'height'    => 50,
        'quality'   => 80,
        'lines'     => 5,
        'fontColors'=> ['#FF0000','#000000','#0000FF','#808080','#0101DF','#8904B1','#FA5858','#FF0040','#5858FA'],
        'sensitive' => false,
    ],

    'flat'   => [
        'length'    => 6,
        'width'     => 160,
        'height'    => 46,
        'quality'   => 10,
        'lines'     => 0,
        'bgImage'   => false,
//         'bgColor'   => '#ecf2f4',
//         'fontColors'=> ['#FF0000', '#000033', '#660033', '#6600CC', '#6600FF', '#303f9f', '#FF6600', '#AA0000'],
        'fontColors'=> ['#FF0000','#000000','#0000FF','#00FF00'],
        'contrast'  => -5,
    ],

    'mini'   => [
        'length'    => 3,
        'width'     => 60,
        'height'    => 32,
    ],

    'inverse'   => [
        'length'    => 5,
        'width'     => 120,
        'height'    => 36,
        'quality'   => 90,
        'sensitive' => true,
        'angle'     => 12,
        'sharpen'   => 10,
        'blur'      => 2,
        'invert'    => true,
        'contrast'  => -5,
    ]

];
