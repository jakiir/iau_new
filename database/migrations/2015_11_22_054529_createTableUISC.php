<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUISC extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uisc', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100)->index();
            $table->string('code', 100);
            $table->string('location', 100);
            $table->string('phone', 40);
            $table->string('email', 40);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('uisc');
    }
}
