<?php

Route::group(array('module' => 'search', 'middleware' => ['auth','checkAdmin','XssProtection'], 'namespace' => 'App\Modules\search\Controllers'), function() {

    Route::get('search/index', 'SearchController@index');
    Route::get('search/pilgrim', 'SearchController@pilgrim');
    Route::get('search/user', 'SearchController@user');


    //****** FAQ List ****//
    Route::get('search/faq', "SearchController@faq");
    Route::get('search/create-faq', "SearchController@createFaq");
    Route::get('search/edit-faq/{id}', "SearchController@editFaq");

    Route::patch('search/store-faq', "SearchController@storeFaq");
    Route::patch('search/update-faq/{id}', "SearchController@updateFaq");

    Route::get('search/get-faq-details-data', "SearchController@getFaqDetailsData");
    Route::get('search/user-view/{id}', "SearchController@userView");
    
        Route::resource('search', 'SearchController');
    /*     * ***********************End of Group Route file***************************** */
});
