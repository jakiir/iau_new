@extends('layouts.admin')

@section('page_heading',trans('messages.search_list'))

@section('content')
<?php
$accessMode = ACL::getAccsessRight('search');
if (!ACL::isAllowed($accessMode, 'V'))
    die('no access right!');
?>
<div class="col-lg-12">

    {!! Session::has('success') ? '<div class="alert alert-success alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("success") .'</div>' : '' !!}
    {!! Session::has('error') ? '<div class="alert alert-danger alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("error") .'</div>' : '' !!}


    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class=" {!! (Request::segment(2)=='index' OR Request::segment(2)=='')?'active':'' !!}">
                <a data-toggle="tab" href="#list_1" aria-expanded="true">
                    {{trans('messages.faq')}}
                </a>
            </li>

            <li class=" {!! (Request::segment(2)=='user')?'active':'' !!}">
                <a data-toggle="tab" href="#list_3" aria-expanded="true">
                    User Search
                </a>
            </li>

        </ul>
        <div class="tab-content">
            <div id="list_1" class="tab-pane {!! (Request::segment(2)=='index' OR Request::segment(2)=='')?'active':'' !!}">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        {!! Form::open(['url' => 'search/index','method' => 'get','id' => 'faq_search'])!!}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group custom-search-form">
                                    <span class="input-group-addon">
                                        <i class="fa fa-list"></i>
                                    </span>
                                    <input name="q" type="text" class="form-control input-sm" value="{{Request::get('q')}}" placeholder="Search FAQ by Keyword">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm btn-default" type="submit">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group has-feedback {{ $errors->has('faqs_type') ? 'has-error' : ''}}">
                                    <label  class="col-md-2 text-right required-star"> ধরণ</label>
                                    <div class="col-md-8">
                                        {!! Form::select('faqs_type', $faqs_type, Request::get('faqs_type'), ['class'=>'form-control input-sm required',
                                        'id'=>"faqs_type"]) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close()!!}


                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="row">
                            <!--1x101 System Admin,, 2x202 Help Desk, 2x203 Call Center-->
                            @if(ACL::getAccsessRight('search','A'))
                            <div class="col-md-11">
                                <div class="pull-right">
                                    <a class="" href="{{ url('/search/create-faq') }}">
                                        {!! Form::button('<i class="fa fa-plus"></i> '.trans('messages.new_faq'), array('type' => 'button', 'class' => 'btn btn-sm btn-default')) !!}
                                    </a>
                                </div>
                            </div>
                            @endif
                            <div class="col-md-1"></div>

                            @if($faqs)
                            <div class="col-md-12">
                                <div class="">
                                    <?php $i = 1; ?>
                                    @foreach($faqs as $faq)
                                    <div class="form-group">
                                        <span class="col-lg-12">{{ $i++ }}. <a href='#faq_{{$faq->id}}'>{{ $faq->question}} </a>
                                            <code>{{$faq->faq_type_name}}</code>
                                        </span>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            @endif

                            <div class="col-md-12"><br/></div>

                            @if($faqs)
                            <div class="col-md-12">
                                <div class="">
                                    <?php $i = 1; ?>
                                    @foreach($faqs as $faq)
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label  class="col-lg-12"><code id='faq_{{$faq->id}}'>{{ $i++ }}. প্রশ্ন:</code> {{ $faq->question}}
                                                    <code>{{$faq->faq_type_name}}</code></label>
                                                <div class="col-lg-12"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<code>উত্তর:</code>
                                                    {!! $faq->answer !!}
                                                </div>
                                                <div class="col-md-1"></div>
                                                <div class="col-md-1"></div>
                                                <div class="col-md-10">
                                                    <div class="col-md-9">
                                                        {!! CommonFunction::showAuditLog($faq->updated_at,$faq->updated_by) !!}
                                                    </div>
                                                    <div class="col-md-3">
                                                        @if(ACL::getAccsessRight('search','E'))
                                                                @if($faq->updated_by == Auth::user()->id || Auth::user()->user_type == '1x101')
                                                                <!--1x101 System Admin,, 2x202 Help Desk-->
                                                                <a href="{{url('search/edit-faq/'.Encryption::encodeId($faq->id))}}" id="editBtn" class="btn btn-xs btn-default">
                                                                    <i class="fa fa-edit "></i> Edit</a>
                                                        @endif
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            @endif

                        </div>
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>



            <div id="list_3" class="tab-pane {!! (Request::segment(2)=='user')?'active':'' !!}">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        {!! Form::open(['url' => 'search/user','method' => 'get','id' => 'sb-search'])!!}
                        <div class="col-md-6 input-group custom-search-form">
                            <span class="input-group-addon">
                                <i class="fa fa-list"></i>
                            </span>
                            <input name="qu" type="text" class="form-control input-sm" value="{{Request::get('qu')}}" placeholder="Search User by Email...">
                            <span class="input-group-btn">
                                <button class="btn btn-sm btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close()!!}
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="list" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Type</th>
                                        <th>Location</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($user_result))
                                    @foreach($user_result as $row)
                                    <tr>
                                        <td>{!! $row->user_full_name !!}</td>
                                        <td>
                                            {!! $row->user_email !!}
                                        </td>
                                        <td>{!! $row->type_name !!}</td>
                                        <td>{!! $row->users_district !!}</td>
                                        <td>
                                            <span class="text-primary"> {{$row->user_status}}</span>
                                        </td>
                                        <td>
                                            @if(ACL::getAccsessRight('search','V'))
                                            <a href="{!! url('search/user-view/'. Encryption::encodeId($row->id)) !!}" class="btn btn-xs btn-primary">
                                                <i class="fa fa-folder-open-o"></i> Open
                                            </a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
        </div>
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->

@endsection

@section('footer-script')

@include('partials.datatable-scripts')
{{--<script src="{{ asset("assets/scripts/datatable/handlebars.js") }}" type="text/javascript"></script>--}}

<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
<script>
    $(function () {
        $('#list').DataTable();
        $('#faqs_type').change(function () {
            $('#faq_search').submit();
        });
    });
</script>
@endsection
