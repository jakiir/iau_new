@extends('layouts.admin')

@section('page_heading',trans('messages.area_list'))

@section('content')

@include('partials.messages')
<?php
$accessMode = ACL::getAccsessRight('settings');
if (!ACL::isAllowed($accessMode, 'V')) {
    die('You have no access right! Please contact with system admin for more information.');
}
?>
<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-6">
                    <i class="fa fa-list"></i> <strong>List of Areas</strong>
                </div>
                <div class="col-md-6">
                    @if(ACL::getAccsessRight('settings','A'))
                    <a class="" href="{{ url('/settings/create-area') }}">
                        {!! Form::button('<i class="fa fa-plus"></i><b> New Area</b>', array('type' => 'button',
                        'class' => 'btn btn-sm btn-default pull-right')) !!}
                    </a>
                    @endif
                </div>
            </div>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="table-responsive">
                <table id="list" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Area Name</th>
                            <th>Area Name in Bangla</th>
                            <th>Area Type</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div><!-- /.table-responsive -->
        </div><!-- /.panel-body -->
    </div><!-- /.panel -->
</div><!-- /.col-lg-12 -->

@endsection

@section('footer-script')
@include('partials.datatable-scripts')

<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
<script>
    $(function () {
        $('#list').DataTable({
            processing: true,
            serverSide: true,
            iDisplayLength:50,
            ajax: {
                url: '{{url("settings/get-area-data")}}',
                method: 'post',
                data: function (d) {
                    d._token = $('input[name="_token"]').val();
                }
            },
            columns: [
                {data: 'area_nm', name: 'area_nm'},
                {data: 'area_nm_ban', name: 'area_nm_ban'},
                {data: 'area_type', name: 'area_type'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            "aaSorting": []
        });

    });
    function ConfirmDelete() {
        var sure_del = confirm("Are you sure you want to delete this item?");
        if (sure_del)
            return true;
        else
            return false;
    }
</script>
@endsection
