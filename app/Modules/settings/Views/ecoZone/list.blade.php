@extends('layouts.admin')

@section('page_heading',trans('messages.doc_list'))

@section('content')
<?php
$accessMode = ACL::getAccsessRight('settings');
if (!ACL::isAllowed($accessMode, 'V')) {
    die('You have no access right! Please contact system admin for more information');
}
?>
<div class="col-lg-12">

    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-6">
                    <i class="fa fa-list"></i> <strong>List of Economic Zones</strong>
                </div>
                <div class="col-md-6">
                    @if(ACL::getAccsessRight('settings','A'))
                    <a class="" href="{{ url('/settings/create-eco-zone') }}">
                        {!! Form::button('<i class="fa fa-plus"></i> <b>New Economic Zone </b>', array('type' => 'button', 'class' => 'btn btn-default btn-sm pull-right')) !!}
                    </a>
                    @endif
                </div>
            </div>
        </div>

        <div class="panel-body">
            <div class="table-responsive">

                @include('partials.messages')

                <table id="list" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Upazilla</th>
                            <th>District</th>
                            <th>Area</th>
                            <th>Active Status</th>
                            <th width="9%">Action</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>

    @endsection

    @section('footer-script')
    @include('partials.datatable-scripts')
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    <script>
        $(function () {
            $('#list').DataTable({
                processing: true,
                serverSide: true,
                iDisplayLength: 50,
                ajax: {
                    url: '{{url("settings/get-eco-zone-data")}}',
                    method: 'POST',
                    data: function (d) {
                        d._token = $('input[name="_token"]').val();
                    }
                },
                columns: [
                    {data: 'name', name: 'name'},
                    {data: 'upazilla', name: 'upazilla'},
                    {data: 'district', name: 'district'},
                    {data: 'area', name: 'area'},
                    {data: 'is_active', name: 'is_active'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
        });

        function ConfirmDelete() {
            var sure_del = confirm("Are you sure you want to delete this item?");
            if (sure_del)
                return true;
            else
                return false;
        }
    </script>
    @endsection
