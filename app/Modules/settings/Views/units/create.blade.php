@extends('layouts.admin')

@section('content')
<?php
$accessMode = ACL::getAccsessRight('settings');
if (!ACL::isAllowed($accessMode, 'A')) {
    die('You have no access right! Please contact system admin for more information');
}
?>

@include('partials.messages')

<div class="col-lg-12">

    <div class="panel panel-primary">
        <div class="panel-heading">
            <b>New Unit Entry</b>
        </div>

        <div class="panel-body">
            {!! Form::open(array('url' => '/settings/store-unit','method' => 'post', 'class' => 'form-horizontal', 'id' => 'formId',
            'enctype' =>'multipart/form-data', 'files' => 'true', 'role' => 'form')) !!}
            <div class="col-md-12">
                <div class="form-group {{$errors->has('name') ? 'has-error' : ''}}">
                    {!! Form::label('name','Name of the Unit : ',['class'=>'col-md-3  required-star']) !!}
                    <div class="col-md-5">
                        {!! Form::text('name', null, ['class'=>'form-control required input-sm', 'placeholder' => 'e.g. Liter']) !!}
                    </div>
                </div>
                <div class="form-group {{$errors->has('is_active') ? 'has-error' : ''}}">
                    {!! Form::label('is_active','Active Staus',['class'=>'col-md-3 required-star']) !!}
                    <div class="col-md-5">
                        {!! Form::select('is_active', $active_status, null, ['class' => 'form-control required input-sm']) !!}
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <a href="{{ url('/settings/units') }}">
                    {!! Form::button('<i class="fa fa-times"></i> Close', array('type' => 'button', 'class' => 'btn btn-default')) !!}
                </a>
                <button type="submit" class="btn btn-success pull-right">
                    <i class="fa fa-chevron-circle-right"></i> <b>Save</b></button>
            </div>

            {!! Form::close() !!}<!-- /.form end -->

            <div class="overlay" style="display: none;">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>

</div>
@endsection

@section('footer-script')

<script>
    var _token = $('input[name="_token"]').val();

    $(document).ready(function () {
        $("#formId").validate({
            errorPlacement: function () {
                return false;
            }
        });
    });
</script>
@endsection <!--- footer script--->