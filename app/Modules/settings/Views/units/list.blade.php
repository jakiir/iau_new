@extends('layouts.admin')

@section('page_heading',trans('messages.doc_list'))

@section('content')
<?php
$accessMode = ACL::getAccsessRight('settings');
if (!ACL::isAllowed($accessMode, 'V')) {
    die('You have no access right! Please contact system admin for more information');
}
?>

@include('partials.messages')

<div class="col-lg-12">

    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-6">
                    <i class="fa fa-list"></i> <strong>List of Units</strong>
                </div>

                <div class="col-md-6">
                    @if(ACL::getAccsessRight('settings','A'))
                    <a class="" href="{{ url('/settings/create-unit') }}">
                        {!! Form::button('<i class="fa fa-plus"></i> <b>New Unit</b>', array('type' => 'button', 'class' => 'btn btn-sm btn-success pull-right')) !!}
                    </a>
                    @endif
                </div>
            </div>
        </div>

        <div class="panel-body">
            <div class="table-responsive">
                <table id="list" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Active Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        @foreach($rows as $row)
                        <tr>
                            <td>{!! $i++ !!}</td>
                            <td>{!! $row->name !!}</td>
                            <td>
                                @if($row->is_active == 1)
                                <span class="text-success"><strong>Active</strong></span>
                                @else
                                <span class="text-danger"><strong>Inactive</strong></span>
                                @endif
                            </td>
                            <td>
                                @if(ACL::getAccsessRight('settings','E'))
                                <a href="{!! url('settings/edit-unit/'. Encryption::encodeId($row->id)) !!}" class="btn btn-xs btn-primary">
                                    <i class="fa fa-folder-open-o"></i> Open
                                </a>
                                <a href="{{ url('settings/delete/Unit/' . Encryption::encodeId($row->id)) }}"
                                   class="btn btn-xs btn-danger" onclick="ConfirmDelete()">
                                    <i class="fa fa-times"></i></a>
                                @endif
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @endsection

    @section('footer-script')
    @include('partials.datatable-scripts')
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    <script>
        $(function () {
            $('#list').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": false,
                "autoWidth": false,
                "iDisplayLength": 50
            });
        });

        function ConfirmDelete() {
            var sure_del = confirm("Are you sure you want to delete this item?");
            if (sure_del)
                return true;
            else
                return false;
        }
    </script>
    @endsection
