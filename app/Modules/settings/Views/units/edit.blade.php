@extends('layouts.admin')

@section('content')
<?php
$accessMode = ACL::getAccsessRight('settings');
if (!ACL::isAllowed($accessMode, 'E')) {
    die('You have no access right! Please contact system admin for more information');
}
?>

@include('partials.messages')

<div class="col-lg-12">

    <div class="panel panel-primary">
        <div class="panel-heading">
            <b>Updating details of {!! $data->name !!}</b>
        </div>

        <div class="panel-body">

            {!! Form::open(array('url' => '/settings/update-unit/'.$id,'method' => 'patch', 'class' => 'form-horizontal', 'id' => 'info',
            'enctype' =>'multipart/form-data', 'files' => 'true', 'role' => 'form')) !!}
            <div class="col-md-12">
                 <div class="form-group {{$errors->has('name') ? 'has-error' : ''}}">
                    {!! Form::label('name','Name of the Economic Zone : ',['class'=>'col-md-3  required-star']) !!}
                    <div class="col-md-5">
                        {!! Form::text('name', $data->name, ['class'=>'form-control required input-sm']) !!}
                    </div>
                </div>
                <div class="form-group {{$errors->has('is_active') ? 'has-error' : ''}}">
                    {!! Form::label('is_active','Active Status',['class'=>'col-md-3 required-star']) !!}
                    <div class="col-md-5">
                        {!! Form::select('is_active', $active_status, $data->is_active, ['class' => 'form-control required input-sm', 'placeholder' => 'Select One']) !!}
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-2">
                    <a href="{{ url('/settings/units') }}">
                        {!! Form::button('<i class="fa fa-times"></i> Close', array('type' => 'button', 'class' => 'btn btn-default')) !!}
                    </a>
                </div>
                <div class="col-md-6 col-md-offset-1">
                    {!! CommonFunction::showAuditLog($data->updated_at, $data->updated_by) !!}
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn btn-success  pull-right">
                        <i class="fa fa-chevron-circle-right"></i> <b>Save</b></button>
                </div>
            </div><!-- /.col-md-12 -->

            {!! Form::close() !!}<!-- /.form end -->

            <div class="overlay" style="display: none;">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>

</div>
@endsection

@section('footer-script')
<script>
    var _token = $('input[name="_token"]').val();

    $(document).ready(function () {
        $("#info").validate({
            errorPlacement: function () {
                return false;
            }
        });
    });
</script>
@endsection <!--- footer script--->