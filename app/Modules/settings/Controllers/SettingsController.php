<?php

namespace App\Modules\Settings\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\LoginController;
use App\Libraries\ACL;
use App\Libraries\CommonFunction;
use App\Libraries\Encryption;
use App\Modules\apps\Models\docInfo;
use App\Modules\Dashboard\Models\Dashboard;
use App\Modules\Dashboard\Models\Services;
use App\Modules\Settings\Models\Area;
use App\Modules\Settings\Models\Bank;
use App\Modules\Settings\Models\Configuration;
use App\Modules\Settings\Models\Currencies;
use App\Modules\Settings\Models\FaqTypes;
use App\Modules\Settings\Models\HighComissions;
use App\Modules\Settings\Models\HsCodes;
use App\Modules\Settings\Models\Notice;
use App\Modules\Settings\Models\Notification;
use App\Modules\Settings\Models\Ports;
use App\Modules\Settings\Models\SecurityProfile;
use App\Modules\Settings\Models\Units;
use App\Modules\Users\Models\Countries;
use App\Modules\Users\Models\EconomicZones;
use App\Modules\Users\Models\UsersModel;
use App\Modules\Users\Models\UserTypes;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Session;
use yajra\Datatables\Datatables;

class SettingsController extends Controller {

    public function __construct() {
        if (Session::has('lang'))
            \App::setLocale(Session::get('lang'));
        ACL::db_reconnect();
    }

    public function index() {
        if (!ACL::getAccsessRight('settings', 'V')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        return view("settings::index");
    }

    /* Starting of Bank Related Functions */

    public function bank() {
        if (!ACL::getAccsessRight('settings', 'V')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $getList = Bank::where('is_archieved', 0)->get();
        return view("settings::bank.list", compact('getList'));
    }

    public function createBank() {
        if (!ACL::getAccsessRight('settings', 'A')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        return view("settings::bank.form-basic");
    }

    public function storeBank(Request $request) {
        if (!ACL::getAccsessRight('settings', 'A')) {
            die('You have no access right! Please contact system administration for more information.');
        }

        $this->validate($request, [
            'name' => 'required',
            'code' => 'required',
            'location' => 'required',
            'email' => 'required|email',
            'phone' => 'required|Max:50|regex:/[0-9+,-]$/',
            'location' => 'required',
        ]);
        try {
            $insert = Bank::create(
                            array(
                                'name' => $request->get('name'),
                                'code' => $request->get('code'),
                                'location' => $request->get('location'),
                                'email' => $request->get('email'),
                                'phone' => $request->get('phone'),
                                'location' => $request->get('location'),
                                'address' => $request->get('address'),
                                'website' => $request->get('website'),
                                'created_by' => CommonFunction::getUserId()
            ));

            Session::flash('success', 'Data is stored successfully!');
            return redirect('/settings/edit-bank/' . Encryption::encodeId($insert->id));
        } catch (\Exception $e) {
            Session::flash('error', 'Sorry! Something went wrong.');
            return Redirect::back()->withInput();
        }
    }

    public function editBank($id) {
        $bank_id = Encryption::decodeId($id);
        $data = Bank::where('id', $bank_id)->first();

        return view("settings::bank.edit", compact('data', 'id'));
    }

    public function updateBank($id, Request $request) {
        if (!ACL::getAccsessRight('settings', 'E')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $bank_id = Encryption::decodeId($id);

        $this->validate($request, [
            'name' => 'required',
            'code' => 'required',
            'email' => 'required|email',
            'phone' => 'required|Max:50|regex:/[0-9+,-]$/',
            'location' => 'required',
        ]);

        Bank::where('id', $bank_id)->update([
            'name' => $request->get('name'),
            'code' => $request->get('code'),
            'location' => $request->get('location'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'address' => $request->get('address'),
            'website' => $request->get('website'),
            'is_active' => $request->get('is_active'),
            'updated_by' => CommonFunction::getUserId()
        ]);

        Session::flash('success', 'Data has been changed successfully.');
        return redirect('/settings/edit-bank/' . $id);
    }

    public function viewBank($id) {
        $bank_id = Encryption::decodeId($id);
        $data = Bank::where('id', $bank_id)->first();

        return view("settings::bank.view", compact('data', 'id', 'bank_id', 'getList'));
    }

    /* Start of Currency related functions */

    public function currency() {
        $rows = Currencies::where('is_archieved', 0)->orderBy('code')->get();
        return view("settings::currency.list", compact('rows'));
    }

    public function createCurrency() {
        return view("settings::currency.create", compact(''));
    }

    public function storeCurrency(Request $request) {
        if (!ACL::getAccsessRight('settings', 'A')) {
            die('You have no access right! Please contact system administration for more information.');
        }

        $this->validate($request, [
            'code' => 'required',
            'name' => 'required',
            'usd_value' => '',
            'bdt_value' => '',
        ]);

        $insert = Currencies::create([
                    'code' => $request->get('code'),
                    'name' => $request->get('name'),
                    'usd_value' => $request->get('usd_value'),
                    'bdt_value' => $request->get('bdt_value'),
                    'created_by' => CommonFunction::getUserId(),
        ]);

        Session::flash('success', 'Data is stored successfully!');
        return redirect('/settings/edit-currency/' . Encryption::encodeId($insert->id));
    }

    public function editCurrency($encrypted_id) {
        $id = Encryption::decodeId($encrypted_id);
        $data = Currencies::where('id', $id)->first();

        return view("settings::currency.edit", compact('data', 'encrypted_id'));
    }

    public function updateCurrency($enc_id, Request $request) {
        if (!ACL::getAccsessRight('settings', 'E')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $id = Encryption::decodeId($enc_id);

        $this->validate($request, [
            'code' => 'required',
            'name' => 'required',
            'usd_value' => '',
            'bdt_value' => '',
        ]);

        Currencies::where('id', $id)->update([
            'code' => $request->get('code'),
            'name' => $request->get('name'),
            'usd_value' => $request->get('usd_value'),
            'bdt_value' => $request->get('bdt_value'),
            'is_active' => $request->get('is_active'),
            'updated_by' => CommonFunction::getUserId()
        ]);

        Session::flash('success', 'Data has been changed successfully.');
        return redirect('/settings/edit-currency/' . $enc_id);
    }

    /* End of Currency related functions */

    public function getPoliceStations(Request $request) {
        if ($request->get('lang') && $request->get('lang') == 'en') {
            $areaField = 'area_info.area_nm';
        } else {
            $areaField = 'area_info.area_nm_ban';
        }

        $data = ['responseCode' => 0, 'data' => ''];
        $area = Area::where($areaField, $request->get('districtId'))->where('area_type', 2)->first();
        if ($area) {
            $area_id = $area->area_id;
            $get_data = Area::where('pare_id', DB::raw($area_id))
                    ->whereNotNull($areaField)
                    ->where('area_type', 3)
                    ->select($areaField)
                    ->orderBy($areaField)
                    ->lists($areaField);

            $data = ['responseCode' => 1, 'data' => $get_data];
        }
        return response()->json($data);
    }

    public function getPoliceStationsWithId(Request $request) {

        if ($request->get('lang') && $request->get('lang') == 'en') {
            $areaField = 'area_info.area_id';
            $areaValue = 'area_info.area_nm';
            $area_select = array('area_info.area_nm', 'area_info.area_id');
        } else {
            $areaField = 'area_info.area_id';
            $areaValue = 'area_info.area_nm_ban';
            $area_select = 'area_info.area_nm_ban,area_info.area_id';
            $area_select = array('area_info.area_nm_ban', 'area_info.area_id');
        }

        $data = ['responseCode' => 0, 'data' => ''];
        $area = Area::where($areaField, $request->get('districtId'))->where('area_type', 2)->first();
        if ($area) {
            $area_id = $area->area_id;
            $get_data = Area::where('pare_id', DB::raw($area_id))
                    ->whereNotNull($areaField)
                    ->where('area_type', 3)
                    ->select($area_select)
                    ->orderBy($areaField)
                    ->lists('area_info.area_nm', 'area_info.area_id');
//                ->lists($areaField, $areaValue);

            $data = ['responseCode' => 1, 'data' => $get_data];
        }
        return response()->json($data);
    }

    public function getThana(Request $request) {

        if ($request->get('lang') && $request->get('lang') == 'en') {
            $areaField = 'area_info.area_nm';
        } else {
            $areaField = 'area_info.area_nm_ban';
        }

        $data = ['responseCode' => 0, 'data' => ''];
        $get_data = Area::where('pare_id', $request->get('districtId'))
                ->whereNotNull($areaField)
                ->where('area_type', 3)
                ->orderBy($areaField)
                ->lists($areaField, 'area_id');
        $data = ['responseCode' => 1, 'data' => $get_data];
        return response()->json($data);
    }

    public function getDistrictUser(Request $request) {
        $area_id = $request->get('districtId');
        $get_data = UsersModel::where('district', '=', $area_id)
                ->where(function ($query) {
                    return $query->where('user_type', '=', '7x713');
                })
                ->select('user_full_name', 'id')
                ->orderBy('user_full_name')
                ->lists('user_full_name', 'id');
        $data = ['responseCode' => 1, 'data' => $get_data];
        return response()->json($data);
    }

    public function areaList() {
        if (!ACL::getAccsessRight('settings', 'V'))
            die('You have no access right! Please contact system administration for more information.');
        $getList = Area::all();
        return view("settings::area.list", compact('getList'));
    }

    public function createArea() {
        if (!ACL::getAccsessRight('settings', 'A'))
            die('You have no access right! Please contact system administration for more information.');
        $divisions = ['' => 'Select one'] + Area::orderBy('area_nm')
                        ->where('pare_id', 0)->lists('area_nm', 'area_id')->all();

        return view("settings::area.form-basic", compact('divisions'));
    }

    public function storeArea(Request $request) {
        if (!ACL::getAccsessRight('settings', 'A')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $this->validate($request, [
            'area_nm' => 'required',
            'area_nm_ban' => 'required',
        ]);
        try {
            $area_type = $request->get('area_type');
            if ($area_type == 1) { //for division
                $parent_id = 0;
            } elseif ($area_type == 2) { // for district
                $parent_id = $request->get('division');
            } elseif ($area_type == 3) { //for thana
                $parent_id = $request->get('district');
            }

            $insert = Area::create([
                        'area_type' => $area_type,
                        'pare_id' => $parent_id,
                        'area_nm' => $request->get('area_nm'),
                        'area_nm_ban' => $request->get('area_nm_ban'),
            ]);

            Session::flash('success', 'Data is stored successfully!');
            return redirect('/settings/edit-area/' . Encryption::encodeId($insert->id));
        } catch (\Exception $e) {
            Session::flash('error', 'Sorry! Something went wrong.');
            return Redirect::back()->withInput();
        }
    }

    public function editArea($id) {
        if (!ACL::getAccsessRight('settings', 'E')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $area_id = Encryption::decodeId($id);
        $data = Area::leftJoin('area_info as ai', 'area_info.pare_id', '=', 'ai.area_id')
                        ->where('area_info.area_id', $area_id)
                        ->get(['area_info.*', 'ai.pare_id as division_id'])[0];

        $divisions = ['' => 'Select one'] + Area::orderBy('area_nm')
                        ->where('pare_id', 0)->lists('area_nm', 'area_id')->all();

        return view("settings::area.edit", compact('data', 'id', 'divisions'));
    }

    public function updateArea($id, Request $request) {
        if (!ACL::getAccsessRight('settings', 'E')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $area_id = Encryption::decodeId($id);

        $this->validate($request, [
            'area_nm' => 'required',
            'area_nm_ban' => 'required',
        ]);

        $area_type = $request->get('area_type');
        if ($area_type == 1) { //for division
            $parent_id = 0;
        } elseif ($area_type == 2) { // for district
            $parent_id = $request->get('division');
        } elseif ($area_type == 3) { //for thana
            $parent_id = $request->get('district');
        }

        Area::where('area_id', $area_id)->update([
            'area_type' => $area_type,
            'pare_id' => $parent_id,
            'area_nm' => $request->get('area_nm'),
            'area_nm_ban' => $request->get('area_nm_ban'),
            'is_active' => $request->get('is_active'),
            'updated_by' => Auth::user()->id,
        ]);

        Session::flash('success', 'Data has been changed successfully.');
        return redirect('/settings/edit-area/' . $id);
    }

    public function get_district_by_division_id(Request $request) {
        if (!ACL::getAccsessRight('settings', 'V')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $divisionId = $request->get('divisionId');

        $districts = Area::where('PARE_ID', $divisionId)->orderBy('AREA_NM', 'ASC')->lists('AREA_NM', 'AREA_ID');
        $data = ['responseCode' => 1, 'data' => $districts];
        return response()->json($data);
    }

    public function getAreaData() {
        $areas = Area::where('is_archieved', 0)->orderBy('area_nm', 'asc')->get(['area_id', 'area_nm', 'area_nm_ban', 'area_type']);
        $mode = ACL::getAccsessRight('settings', 'E');

        return Datatables::of($areas)
                        ->addColumn('action', function ($areas) use ($mode) {
                            if ($mode) {
                                return '<a href="' . url('settings/edit-area/' . Encryption::encodeId($areas->area_id)) .
                                        '" class="btn btn-xs btn-primary"><i class="fa fa-folder-open-o"></i> Open</a>'
                                        . ' '
                                        . '<a href="' . url('settings/delete/Area/' . Encryption::encodeId($areas->area_id)) . ' " ' .
                                        ' class="btn btn-xs btn-danger" onclick="ConfirmDelete()"><i class="fa fa-times"></i></a>';
                            } else
                                return '';
                        })
                        ->editColumn('area_type', function ($areas) {
                            if ($areas->area_type == 1) {
                                return 'Division';
                            } elseif ($areas->area_type == 2) {
                                return 'District';
                            } elseif ($areas->area_type == 3) {
                                return 'Thana';
                            }
                        })
                        ->removeColumn('area_id')
                        ->make(true);
    }

    /* Starting of User Type Related Functions */

    public function userType() {
        $getList = UserTypes::leftJoin('security_assign as sa', 'sa.id', '=', 'user_types.security_profile_id')
                ->get(['user_types.id', 'type_name', 'security_profile_id', 'auth_token_type', 'db_access_data', 'sa.caption', 'user_types.status']);
        return view("settings::user_type.list", compact('getList'));
    }

    public function editUserType($id) {
        if (!ACL::getAccsessRight('settings', 'E')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $id = Encryption::decodeId($id);
        $security_profiles = SecurityProfile::orderBy('profile_name', 'ASC')
                ->lists('profile_name', 'id');
        $data = UserTypes::where('id', $id)
                ->first(['id', 'type_name', 'security_profile_id', 'auth_token_type', 'db_access_data', 'updated_at', 'updated_by', 'status']);
        return view("settings::user_type.edit", compact('data', 'security_profiles'));
    }

    public function updateUserType($encoded_id, Request $request) {
        if (!ACL::getAccsessRight('settings', 'A')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $this->validate($request, [
            'type_name' => 'required',
            'auth_token_type' => 'required',
        ]);
//        CommonFunction::createAuditLog('userType.edit', $request);
        $id = Encryption::decodeId($encoded_id);
        $update_data = array(
            'type_name' => $request->get('type_name'),
            'security_profile_id' => $request->get('security_profile'),
            'auth_token_type' => $request->get('auth_token_type'),
            'db_access_data' => Encryption::encode($request->get('db_access_data')),
            'status' => $request->get('status'),
            'updated_by' => Auth::user()->id,
        );
        $data = UserTypes::where('id', $id)
                ->update($update_data);

        if ($request->get('status') == 'inactive') {
            $user_ids = UsersModel::where('user_type', $id)->get(['id']);
            foreach ($user_ids as $user_id) {
                LoginController::killUserSession($user_id);
            }
        }

        Session::flash('success', 'Data has been changed successfully.');
        return redirect('settings/edit-user-type/' . $encoded_id);
    }

    /* End of User Type related functions */

    /* Starting of Configuration Related Functions */

    public function configuration() {
        if (!ACL::getAccsessRight('settings', 'V')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $getList = Configuration::where('is_locked', '=', 0)->get();
        return view("settings::config.list", compact('getList'));
    }

    public function editConfiguration($id) {
        if (!ACL::getAccsessRight('settings', 'E')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $config_id = Encryption::decodeId($id);
        $data = Configuration::where('id', $config_id)->first();
        return view("settings::config.edit", compact('data', 'id'));
    }

    public function moreConfig() {
        if (Auth::user()->user_email == 'shoeb@batworld.com' || Auth::user()->user_email == 'mitul@batworld.com' || Auth::user()->user_email == 'mithu@batworld.com') {
            $getList = Configuration::where('is_locked', '=', 1)->get();
            return view("settings::config.list", compact('getList'));
        } else {
            Session::flash('error', 'Not permitted!');
            return redirect()->back();
        }
    }

    public function updateConfig($id, Request $request) {
        if (!ACL::getAccsessRight('settings', 'E')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $config_id = Encryption::decodeId($id);

        $this->validate($request, ['value' => 'required']);

        Configuration::where('id', $config_id)->update([
            'value' => $request->get('value'),
            'details' => $request->get('details'),
            'value2' => $request->get('value2'),
            'value3' => $request->get('value3'),
            'updated_by' => CommonFunction::getUserId()
        ]);

        Session::flash('success', 'Data has been changed successfully.');
        return redirect('/settings/edit-config/' . $id);
    }

    /* Starting of Notification Related Functions */

    public function notification() {
        if (!ACL::getAccsessRight('settings', 'V')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $getList = Notification::where('is_locked', 0)
                ->orderBy('id', 'desc')
                ->take(100)
                ->get();
        return view("settings::notify.list", compact('getList'));
    }

    public function viewNotify($id) {
        if (!ACL::getAccsessRight('settings', 'V')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $notify_id = Encryption::decodeId($id);
        $data = Notification::where('id', $notify_id)->first();
        return view("settings::notify.view", compact('data', 'id', '$notify_id'));
    }

    /* Start of FAQ Category related functions */

    public function faqCat() {
        return view("settings::faq_category.list");
    }

    public function createFaqCat() {
        if (!ACL::getAccsessRight('settings', 'A')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $faq_types = FaqTypes::lists('name', 'id');
        return view("settings::faq_category.create", compact('faq_types'));
    }

    public function getFaqCatDetailsData() {
        $mode = ACL::getAccsessRight('settings', 'E');
        $faq_types = FaqTypes::leftJoin('faq_multitypes', 'faq_types.id', '=', 'faq_multitypes.faq_type_id')
                ->leftJoin('faq', 'faq.id', '=', 'faq_multitypes.faq_id')
                ->groupBy('faq_types.id')
                ->get(['faq_types.id', 'faq_types.name', 'faq.status as faq_status',
            DB::raw('count(distinct faq_multitypes.faq_id) noOfItems, '
                    . 'sum(case when faq.status="unpublished" then 1 else 0 end) Unpublished,'
                    . 'sum(case when faq.status="draft" then 1 else 0 end) Draft,'
                    . 'sum(case when faq.status="private" then 1 else 0 end) Private')]);

        return Datatables::of($faq_types)
                        ->addColumn('action', function ($faq_types) use ($mode) {
                            if ($mode) {
                                return '<a href="/settings/edit-faq-cat/' . Encryption::encodeId($faq_types->id) .
                                        '" class="btn btn-xs btn-primary"><i class="fa fa-folder-open-o"></i> Open</a> '
                                        . '<a href="/search/index?q=&faqs_type=' . $faq_types->id .
                                        '" class="btn btn-xs btn-info"><i class="fa fa-folder-open-o"></i> Articles</a>';
                            } else {
                                return '';
                            }
                        })
                        ->editColumn('Draft', function ($faq_types) {
                            if ($faq_types->Draft > 0) {
                                return '<a href="/search/index?q=&faqs_type=' . $faq_types->id . "&status=draft" .
                                        '" class="">' . $faq_types->Draft . '</a>';
                            } else {
                                return $faq_types->Draft;
                            }
                        })
                        ->editColumn('Unpublished', function ($faq_types) {
                            if ($faq_types->Unpublished > 0) {
                                return '<a href="/search/index?q=&faqs_type=' . $faq_types->id . "&status=unpublished" .
                                        '" class="">' . $faq_types->Unpublished . '</a>';
                            } else {
                                return $faq_types->Unpublished;
                            }
                        })
                        ->editColumn('Private', function ($faq_types) {
                            if ($faq_types->Private > 0) {
                                return '<a href="/search/index?q=&faqs_type=' . $faq_types->id . "&status=private" .
                                        '" class="">' . $faq_types->Private . '</a>';
                            } else {
                                return $faq_types->Private;
                            }
                        })
                        ->removeColumn('id')
                        ->make(true);
    }

    public function storeFaqCat(Request $request) {
        if (!ACL::getAccsessRight('settings', 'A')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $this->validate($request, [
            'name' => 'required',
        ]);

        $insert = FaqTypes::create(
                        array(
                            'name' => $request->get('name'),
                            'created_by' => CommonFunction::getUserId()
        ));

        Session::flash('success', 'Data is stored successfully!');
        return redirect('/settings/edit-faq-cat/' . Encryption::encodeId($insert->id));
    }

    public function editFaqCat($encrypted_id) {
        $id = Encryption::decodeId($encrypted_id);
        $data = FaqTypes::where('id', $id)->first();

        return view("settings::faq_category.edit", compact('data', 'encrypted_id'));
    }

    public function updateFaqCat($id, Request $request) {
        if (!ACL::getAccsessRight('settings', 'E')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $faq_id = Encryption::decodeId($id);

        $this->validate($request, [
            'name' => 'required',
        ]);

        FaqTypes::where('id', $faq_id)->update([
            'name' => $request->get('name'),
            'updated_by' => CommonFunction::getUserId()
        ]);

        Session::flash('success', 'Data has been changed successfully.');
        return redirect('/settings/edit-faq-cat/' . $id);
    }

    /* End of FAQ Category related functions */


    /* Starting of Document Related Functions */

    public function document() {
        return view("settings::document.list");
    }

    public function getDocData() {
        $mode = ACL::getAccsessRight('settings', 'V');
        $datas = docInfo::orderBy('doc_id', 'desc')
                ->where('is_archieved', 0)
                ->get(['doc_id', 'doc_name', 'service_id', 'doc_priority', 'updated_at', 'is_active']);
        return Datatables::of($datas)
                        ->addColumn('action', function ($datas) use ($mode) {
                            if ($mode) {
                                return '<a href="/settings/edit-document/' . Encryption::encodeId($datas->doc_id) .
                                        '" class="btn btn-xs btn-primary"><i class="fa fa-folder-open-o"></i> Open</a>'
                                        . ' '
                                        . '<a href="' . url('settings/delete/Document/' . Encryption::encodeId($datas->doc_id)) . ' " ' .
                                        ' class="btn btn-xs btn-danger" onclick="ConfirmDelete()"><i class="fa fa-times"></i></a>';
                            }
                        })
                        ->editColumn('service_id', function ($datas) use ($mode) {
                            if ($mode) {
                                $service_name = Services::where('id', $datas->service_id)->pluck('name');
                                return $service_name;
                            }
                        })
                        ->editColumn('doc_priority', function ($datas) {
                            if ($datas->doc_priority == 1) {
                                return 'Mandatory';
                            } else {
                                return 'Not Mandatory';
                            }
                        })
                        ->editColumn('is_active', function ($desk) {
                            if ($desk->is_active == 1) {
                                $class = 'text-success';
                                $status = 'Active';
                            } else {
                                $class = 'text-danger';
                                $status = 'Inactive';
                            }
                            return '<span class="' . $class . '"><b>' . $status . '</b></span>';
                        })
                        ->removeColumn('doc_id')
                        ->make(true);
    }

    public function createDocument() {
        if (!ACL::getAccsessRight('settings', 'A')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $services = Services::orderby('name')->lists('name', 'id');
        return view("settings::document.create", compact('services'));
    }

    public function storeDocument(Request $request) {
        if (!ACL::getAccsessRight('settings', 'A')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $this->validate($request, [
            'doc_name' => 'required',
            'service_id' => 'required',
        ]);

        $insert = docInfo::create([
                    'doc_name' => $request->get('doc_name'),
                    'service_id' => $request->get('service_id'),
                    'doc_priority' => $request->get('doc_priority'),
                    'created_by' => CommonFunction::getUserId()
        ]);

        Session::flash('success', 'Data is stored successfully!');
        return redirect('/settings/edit-document/' . Encryption::encodeId($insert->id));
    }

    public function editDocument($id) {
        if (!ACL::getAccsessRight('settings', 'E')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $_id = Encryption::decodeId($id);
        $data = docInfo::where('doc_id', $_id)->first();
        $services = Services::orderby('name')->lists('name', 'id');
        return view("settings::document.edit", compact('data', 'id', 'services'));
    }

    public function updateDocument($id, Request $request) {
        if (!ACL::getAccsessRight('settings', 'E')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $_id = Encryption::decodeId($id);

        $this->validate($request, [
            'doc_name' => 'required',
            'service_id' => 'required',
        ]);

        docInfo::where('doc_id', $_id)->update([
            'doc_name' => $request->get('doc_name'),
            'service_id' => $request->get('service_id'),
            'doc_priority' => $request->get('doc_priority'),
            'order' => $request->get('order'),
            'additional_field' => $request->get('additional_field'),
            'is_active' => $request->get('is_active'),
            'updated_by' => CommonFunction::getUserId()
        ]);

        Session::flash('success', 'Data has been changed successfully.');
        return redirect('settings/edit-document/' . $id);
    }

    /* Starting of Economic Zone Related Functions */

    public function EcoZones() {
        return view("settings::ecoZone.list");
    }

    public function getEcoZoneData() {
        $mode = ACL::getAccsessRight('settings', 'V');
        $datas = EconomicZones::where('is_archieved', 0)->orderBy('name', 'asc')
                ->get(['id', 'name', 'upazilla', 'district', 'area', 'remarks', 'is_active']);
        return Datatables::of($datas)
                        ->addColumn('action', function ($datas) use ($mode) {
                            if ($mode) {
                                return '<a href="/settings/edit-eco-zone/' . Encryption::encodeId($datas->id) .
                                        '" class="btn btn-xs btn-success"><i class="fa fa-folder-open-o"></i> Open</a>'
                                        . ' '
                                        . '<a href="' . url('settings/delete/EcoZone/' . Encryption::encodeId($datas->id)) . ' " ' .
                                        ' class="btn btn-xs btn-danger" onclick="ConfirmDelete()"><i class="fa fa-times"></i></a>';
                            }
                        })
                        ->editColumn('is_active', function ($desk) {
                            if ($desk->is_active == 1) {
                                $class = 'text-success';
                                $status = 'Active';
                            } else {
                                $class = 'text-danger';
                                $status = 'Inactive';
                            }
                            return '<span class="' . $class . '"><b>' . $status . '</b></span>';
                        })
                        ->removeColumn('id')
                        ->make(true);
    }

    public function createEcoZone() {
        if (!ACL::getAccsessRight('settings', 'A')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $districts = Area::orderby('area_nm')->where('area_type', 2)->lists('area_nm', 'area_nm');
        return view("settings::ecoZone.create", compact('districts'));
    }

    public function storeEcoZone(Request $request) {
        if (!ACL::getAccsessRight('settings', 'A')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $this->validate($request, [
            'name' => 'required',
            'upazilla' => 'required',
            'district' => 'required',
            'area' => 'required',
        ]);

        $insert = EconomicZones::create([
                    'name' => $request->get('name'),
                    'upazilla' => $request->get('upazilla'),
                    'district' => $request->get('district'),
                    'area' => $request->get('area'),
                    'remarks' => $request->get('remarks'),
                    'created_by' => CommonFunction::getUserId()
        ]);

        Session::flash('success', 'Data is stored successfully!');
        return redirect('/settings/edit-eco-zone/' . Encryption::encodeId($insert->id));
    }

    public function editEcoZone($id) {
        if (!ACL::getAccsessRight('settings', 'E')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $_id = Encryption::decodeId($id);
        $data = EconomicZones::where('id', $_id)->first();
        $districts = Area::orderby('area_nm')->where('area_type', 2)->lists('area_nm', 'area_nm');
        return view("settings::ecoZone.edit", compact('data', 'id', 'districts'));
    }

    public function updateEcoZone($id, Request $request) {
        if (!ACL::getAccsessRight('settings', 'E')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $_id = Encryption::decodeId($id);

        $this->validate($request, [
            'name' => 'required',
            'upazilla' => 'required',
            'district' => 'required',
            'area' => 'required',
        ]);

        EconomicZones::where('id', $_id)->update([
            'name' => $request->get('name'),
            'upazilla' => $request->get('upazilla'),
            'district' => $request->get('district'),
            'area' => $request->get('area'),
            'is_active' => $request->get('is_active'),
            'remarks' => $request->get('remarks'),
            'updated_by' => CommonFunction::getUserId()
        ]);

        Session::flash('success', 'Data has been changed successfully.');
        return redirect('settings/edit-eco-zone/' . $id);
    }

    /* Start of Notice related functions */

    public function notice() {
        if (!ACL::getAccsessRight('settings', 'V')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        return view("settings::notice.list");
    }

    public function createNotice() {
        if (!ACL::getAccsessRight('settings', 'A')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        return view("settings::notice.create", compact(''));
    }

    public function getNoticeDetailsData() {
        $mode = ACL::getAccsessRight('settings', 'V');
        $notice = Notice::where('is_archieved', 0)->orderBy('notice.updated_at', 'desc')
                ->get(['notice.id', 'heading', 'details', 'importance', 'status', 'notice.updated_at as update_date', 'is_active']);

        return Datatables::of($notice)
                        ->addColumn('action', function ($notice) use ($mode) {
                            if ($mode) {
                                return '<a href="/settings/edit-notice/' . Encryption::encodeId($notice->id) .
                                        '" class="btn btn-xs btn-primary"><i class="fa fa-folder-open-o"></i> Open</a> '
                                        . ' '
                                        . '<a href="' . url('settings/delete/Notice/' . Encryption::encodeId($notice->id)) . ' " ' .
                                        ' class="btn btn-xs btn-danger" onclick="ConfirmDelete()"><i class="fa fa-times"></i></a>';
                            } else {
                                return '';
                            }
                        })
                        ->editColumn('details', function ($notice) {
                            return substr($notice->details, 0, 150).' <a href="/support/view-notice/'.Encryption::encodeId($notice->id)  . '">'
                                    . 'See more... </a>';
                        })
                        ->editColumn('update_date', function ($notice) {
                            return CommonFunction::changeDateFormat(substr($notice->update_date, 0, 10));
                        })
                        ->editColumn('status', function ($notice) {
                            return ucfirst($notice->status);
                        })
                        ->editColumn('importance', function ($notice) {
                            return ucfirst($notice->importance);
                        })
                        ->editColumn('is_active', function ($desk) {
                            if ($desk->is_active == 1) {
                                $class = 'text-success';
                                $status = 'Active';
                            } else {
                                $class = 'text-danger';
                                $status = 'Inactive';
                            }
                            return '<span class="' . $class . '"><b>' . $status . '</b></span>';
                        })
                        ->removeColumn('id')
                        ->make(true);
    }

    public function storeNotice(Request $request) {
        if (!ACL::getAccsessRight('settings', 'A')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $this->validate($request, [
            'heading' => 'required',
            'details' => 'required',
            'status' => 'required',
            'importance' => 'required',
        ]);
        try {
            $insert = Notice::create(
                            array(
                                'heading' => $request->get('heading'),
                                'details' => $request->get('details'),
                                'status' => $request->get('status'),
                                'importance' => $request->get('importance'),
                                'created_by' => CommonFunction::getUserId()
            ));

            Session::flash('success', 'Data is stored successfully!');
            return redirect('/settings/edit-notice/' . Encryption::encodeId($insert->id));
        } catch (\Exception $e) {
            Session::flash('error', 'Sorry! Something went wrong.');
            return Redirect::back()->withInput();
        }
    }

    public function editNotice($encrypted_id) {
        $id = Encryption::decodeId($encrypted_id);
        $data = Notice::where('id', $id)->first();
        return view("settings::notice.edit", compact('data', 'encrypted_id'));
    }

    public function updateNotice($id, Request $request) {
        if (!ACL::getAccsessRight('settings', 'E')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $faq_id = Encryption::decodeId($id);

        $this->validate($request, [
            'heading' => 'required',
            'details' => 'required',
            'status' => 'required',
            'importance' => 'required',
        ]);

        Notice::where('id', $faq_id)->update([
            'heading' => $request->get('heading'),
            'details' => $request->get('details'),
            'status' => $request->get('status'),
            'importance' => $request->get('importance'),
            'is_active' => $request->get('is_active'),
            'updated_by' => CommonFunction::getUserId()
        ]);

        Session::flash('success', 'Data has been changed successfully.');
        return redirect('/settings/edit-notice/' . $id);
    }

    /* End of Notice related functions */

    /* Start of High Commission related functions */

    public function highCommission() {
        return view("settings::high_commission.list");
    }

    public function createHighCommission() {
        $countries = Countries::where('country_status', 'Yes')->orderBy('nicename', 'asc')->lists('nicename', 'country_code');
        return view("settings::high_commission.create", compact('countries'));
    }

    public function getHighCommissionData() {
        $mode = ACL::getAccsessRight('settings', 'E');
        $hc = HighComissions::leftJoin('country_info', 'high_comissions.country_code', '=', 'country_info.country_code')
                ->where('is_archieved', 0)
                ->orderBy('country_info.name')
                ->get(['high_comissions.id', 'high_comissions.name', 'address', 'phone', 'email', 'country_info.name as country', 'is_active']);
        return Datatables::of($hc)
                        ->addColumn('action', function ($hc) use ($mode) {
                            if ($mode) {
                                return '<a href="/settings/edit-high-commission/' . Encryption::encodeId($hc->id) .
                                        '" class="btn btn-xs btn-primary"><i class="fa fa-folder-open-o"></i> Open</a> '
                                        . ' '
                                        . '<a href="' . url('settings/delete/HighCommissions/' . Encryption::encodeId($hc->id)) . ' " ' .
                                        ' class="btn btn-xs btn-danger" onclick="ConfirmDelete()"><i class="fa fa-times"></i></a>';
                            } else {
                                return '';
                            }
                        })
                        ->editColumn('is_active', function ($desk) {
                            if ($desk->is_active == 1) {
                                $class = 'text-success';
                                $status = 'Active';
                            } else {
                                $class = 'text-danger';
                                $status = 'Inactive';
                            }
                            return '<span class="' . $class . '"><b>' . $status . '</b></span>';
                        })
                        ->editColumn('country', function ($hc) {
                            if ($hc->country) {
                                return ucfirst(strtolower($hc->country));
                            }
                        })
                        ->removeColumn('id')
                        ->make(true);
    }

    public function storeHighCommission(Request $request) {
        if (!ACL::getAccsessRight('settings', 'A')) {
            die('You have no access right! Please contact system administration for more information.');
        }

        $this->validate($request, [
            'country_code' => 'required',
            'name' => 'required',
            'address' => 'required',
            'phone' => '',
            'email' => 'required|email',
        ]);

        $insert = HighComissions::create([
                    'country_code' => $request->get('country_code'),
                    'name' => $request->get('name'),
                    'address' => $request->get('address'),
                    'phone' => $request->get('phone'),
                    'email' => $request->get('email'),
                    'created_by' => CommonFunction::getUserId(),
        ]);

        Session::flash('success', 'Data is stored successfully!');
        return redirect('/settings/edit-high-commission/' . Encryption::encodeId($insert->id));
    }

    public function editHighCommission($encrypted_id) {
        $id = Encryption::decodeId($encrypted_id);
        $data = HighComissions::where('id', $id)->first();
        $hc_country = Countries::where('country_code', $data->country_code)->pluck('nicename');
        $countries = Countries::where('country_status', 'Yes')->orderBy('name', 'asc')->lists('nicename', 'country_code');

        return view("settings::high_commission.edit", compact('data', 'encrypted_id', 'hc_country', 'countries'));
    }

    public function updateHighCommission($enc_id, Request $request) {
        if (!ACL::getAccsessRight('settings', 'E')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $id = Encryption::decodeId($enc_id);

        $this->validate($request, [
            'country_code' => 'required',
            'name' => 'required',
            'address' => 'required',
            'phone' => '',
            'email' => 'required',
        ]);

        HighComissions::where('id', $id)->update([
            'country_code' => $request->get('country_code'),
            'name' => $request->get('name'),
            'address' => $request->get('address'),
            'phone' => $request->get('phone'),
            'email' => $request->get('email'),
            'is_active' => $request->get('is_active'),
            'updated_by' => CommonFunction::getUserId()
        ]);

        Session::flash('success', 'Data has been changed successfully.');
        return redirect('/settings/edit-high-commission/' . $enc_id);
    }

    /* End of High Commission related functions */

    /* Start of HS Code related functions */

    public function HsCodes() {
        $rows = HsCodes::orderBy('hs_code')->where('is_archieved', 0)
                ->get(['id', 'hs_code', 'product_name', 'is_active']);
        return view("settings::hs_codes.list", compact('rows'));
    }

    public function createHsCode() {
        return view("settings::hs_codes.create");
    }

    public function storeHsCode(Request $request) {
        if (!ACL::getAccsessRight('settings', 'A')) {
            die('You have no access right! Please contact system administration for more information.');
        }

        $this->validate($request, [
            'hs_code' => 'required',
            'product_name' => 'required',
        ]);

        $insert = HsCodes::create([
                    'hs_code' => $request->get('hs_code'),
                    'product_name' => $request->get('product_name'),
                    'is_active' => 1,
                    'created_by' => CommonFunction::getUserId(),
        ]);

        Session::flash('success', 'The HS Code is stored successfully!');
        return redirect('/settings/edit-hs-code/' . Encryption::encodeId($insert->id));
    }

    public function editHsCode($encrypted_id) {
        $id = Encryption::decodeId($encrypted_id);
        $data = HsCodes::where('id', $id)->first();
        return view("settings::hs_codes.edit", compact('data', 'encrypted_id'));
    }

    public function updateHsCode($enc_id, Request $request) {
        if (!ACL::getAccsessRight('settings', 'E')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $id = Encryption::decodeId($enc_id);

        $this->validate($request, [
            'hs_code' => 'required',
            'product_name' => 'required',
        ]);

        HsCodes::where('id', $id)->update([
            'hs_code' => $request->get('hs_code'),
            'product_name' => $request->get('product_name'),
            'is_active' => $request->get('is_active'),
            'updated_by' => CommonFunction::getUserId()
        ]);

        Session::flash('success', 'The HS Code  has been changed successfully.');
        return redirect('/settings/edit-hs-code/' . $enc_id);
    }

    /* End of HS Code related functions */


    /* Start of User Desk related functions */

    public function userDesk() {
        return view("settings::user_desk.list");
    }

    public function createUserDesk() {
        $desks = UserDesk::orderBy('desk_name')->lists('desk_name', 'desk_id');
        return view("settings::user_desk.create", compact('desks'));
    }

    public function getUserDeskData() {
        $mode = ACL::getAccsessRight('settings', 'E');
        $desk = UserDesk::orderBy('desk_name')
                ->get(['desk_id', 'desk_name', 'desk_status', 'delegate_to_desk']);

        return Datatables::of($desk)
                        ->addColumn('action', function ($desk) use ($mode) {
                            if ($mode) {
                                return '<a href="/settings/edit-user-desk/' . Encryption::encodeId($desk->desk_id) .
                                        '" class="btn btn-xs btn-primary"><i class="fa fa-folder-open-o"></i> Open</a> ';
                            } else {
                                return '';
                            }
                        })
                        ->editColumn('desk_status', function ($desk) {
                            if ($desk->desk_status == 1) {
                                $class = 'text-success';
                                $status = 'Active';
                            } else {
                                $class = 'text-danger';
                                $status = 'Inactive';
                            }
                            return '<span class="' . $class . '"><b>' . $status . '</b></span>';
                        })
                        ->removeColumn('id')
                        ->make(true);
    }

    public function storeUserDesk(Request $request) {
        if (!ACL::getAccsessRight('settings', 'A')) {
            die('You have no access right! Please contact system administration for more information.');
        }

        $this->validate($request, [
            'desk_name' => 'required',
            'desk_status' => 'required',
            'delegate_to_desk' => '',
        ]);

        $insert = UserDesk::create([
                    'desk_name' => $request->get('desk_name'),
                    'desk_status' => $request->get('desk_status'),
                    'delegate_to_desk' => $request->get('delegate_to_desk'),
                    'created_by' => CommonFunction::getUserId(),
        ]);

        Session::flash('success', 'Data is stored successfully!');
        return redirect('/settings/edit-user-desk/' . Encryption::encodeId($insert->id));
    }

    public function editUserDesk($encrypted_id) {
        $id = Encryption::decodeId($encrypted_id);
        $data = UserDesk::where('desk_id', $id)->first();

        $desks = UserDesk::orderBy('desk_name')->lists('desk_name', 'desk_id');

        return view("settings::user_desk.edit", compact('data', 'encrypted_id', 'desks'));
    }

    public function updateUserDesk($enc_id, Request $request) {
        if (!ACL::getAccsessRight('settings', 'E')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $id = Encryption::decodeId($enc_id);

        $this->validate($request, [
            'desk_name' => 'required',
            'desk_status' => 'required',
            'delegate_to_desk' => '',
        ]);

        UserDesk::where('desk_id', $id)->update([
            'desk_name' => $request->get('desk_name'),
            'desk_status' => $request->get('desk_status'),
            'delegate_to_desk' => $request->get('delegate_to_desk'),
            'updated_by' => CommonFunction::getUserId()
        ]);

        Session::flash('success', 'Data has been changed successfully.');
        return redirect('/settings/edit-user-desk/' . $enc_id);
    }

    /* End of User Desk related functions */

    /* Start of Security related functions */

    public function security() {
        $user_types = UserTypes::lists('type_name', 'id');
        return view("settings::security.list", compact('user_types'));
    }

    public function getSecurityData() {
        $_data = SecurityProfile::get(['id', 'profile_name', 'allowed_remote_ip', 'week_off_days', 'work_hour_start', 'work_hour_end', 'active_status']);
        return Datatables::of($_data)
                        ->addColumn('action', function ($_data) {
                            if ($_data->id != 1) {
                                return '<a href="/settings/edit-security/' . Encryption::encodeId($_data->id) .
                                        '" class="btn btn-xs btn-primary"><i class="fa fa-folder-open-o"></i> <b>Open<b/></a>';
                            }
                        })
                        ->removeColumn('id')
                        ->make(true);
    }

    public function storeSecurity(Request $request) {
        $this->validate($request, [
            'profile_name' => 'required',
            'allowed_remote_ip' => 'required',
            'week_off_days' => 'required',
        ]);
        SecurityProfile::create(
                array(
                    'profile_name' => $request->get('profile_name'),
//                    'user_type' => $request->get('user_type'),
                    'user_email' => $request->get('user_email'),
                    'allowed_remote_ip' => $request->get('allowed_remote_ip'),
                    'week_off_days' => $request->get('week_off_days'),
                    'work_hour_start' => $request->get('work_hour_start'),
                    'work_hour_end' => $request->get('work_hour_end'),
                    'active_status' => $request->get('active_status'),
                    'created_by' => CommonFunction::getUserId()
        ));

        Session::flash('success', 'Data is stored successfully!');
        return redirect('/settings/security');
    }

    public function editSecurity($_id) {
        $id = Encryption::decodeId($_id);
        $data = SecurityProfile::where('id', $id)->first();
        $user_types = UserTypes::lists('type_name', 'id');
        return view("settings::security.edit", compact('data', '_id', 'user_types'));
    }

    public function updateSecurity($id, Request $request) {
        $_id = Encryption::decodeId($id);

        $this->validate($request, [
            'profile_name' => 'required',
            'allowed_remote_ip' => 'required',
            'week_off_days' => 'required',
        ]);

        SecurityProfile::where('id', $_id)->update([
            'profile_name' => $request->get('profile_name'),
//            'user_type' => $request->get('user_type'),
            'user_email' => $request->get('user_email'),
            'allowed_remote_ip' => $request->get('allowed_remote_ip'),
            'week_off_days' => $request->get('week_off_days'),
            'work_hour_start' => $request->get('work_hour_start'),
            'work_hour_end' => $request->get('work_hour_end'),
            'active_status' => $request->get('active_status'),
            'updated_by' => CommonFunction::getUserId()
        ]);

        Session::flash('success', 'Data has been changed successfully.');
        return redirect('/settings/security');
    }

    /* End of Security related functions */

    /* Start of Server Inforelated functions */

    public function serverInfo(Request $request) {
        $getList = [
            [
                'caption' => 'Server Status',
                'function' => 'command:apache-status,whoami'
            ],
            [
                'caption' => 'Database',
                'function' => 'command:mysql_stat,show global status,show processlist,show table status,show full processlist'
            ],
            [
                'caption' => 'Process Status',
                'function' => 'command:NID process Status,PDF Gen Status'
            ],
            [
                'caption' => 'env file',
                'function' => "DB Host : " . env('DB_HOST') .
                '<br/>User : ' . env('DB_USERNAME') .
                '<br/>Database : ' . env('DB_DATABASE') .
                '<br/>' .
                '<br/>Mail Driver : ' . env('MAIL_DRIVER') .
                '<br/>Mail Host : ' . env('MAIL_HOST') .
                '<br/>Mail Port : ' . env('MAIL_PORT') .
                '<br/>' .
                '<br/>Recaptcha Public Key : ' . env('RECAPTCHA_PUBLIC_KEY') .
                '<br/>Recaptcha Private Key : ' . env('RECAPTCHA_PRIVATE_KEY')
            ],
            [
                'caption' => 'php Info',
                'function' => 'command:phpinfo'
            ]
        ];
        return view("settings::server-info.list", compact('getList'));
    }

    public function getCommandResult(Request $request) {
        $command = $request->get('command');
        $output = '';
        $dboutput = '';
        $result = null;
        echo 'Executing command ' . $command . ' at ' . date('h:i:s.u T', time()) . '<br />';
        if (Auth::user()->user_type == '1x101') {
            switch ($command) {
                case 'NID process Status':
                    $result = DB::select(
                                    DB::raw("select 'In last 60 Minutes' as Period, verification_flag status, count(id) as noOfNID from pilgrims_nid where submitted_at > date_add(now(), interval -60 minute) group by Period,verification_flag
                                                union all
                                                select 'Total' as Period, verification_flag status, count(id) as noOfNID from pilgrims_nid group by Period,verification_flag"));
                    $dboutput = count($result);
                    break;
                case 'PDF Gen Status':
                    $result = DB::select(
                                    DB::raw("select 'In last 60 Minutes' as Period, status, pdf_type, count(id) as noOfDoc from pdf_generator where created_at > date_add(now(), interval -60 minute) group by Period, status, pdf_type
                                                union all
                                                select 'Total' as Period, status, pdf_type, count(id) as noOfDoc from pdf_generator  group by Period, status, pdf_type"));
                    $dboutput = count($result);
                    break;
                case 'phpinfo':
                    phpinfo();
                    break;
                case 'apache-status':
                    $output = shell_exec('apachectl status');
                    break;
                case 'whoami':
                    $output = shell_exec('whoami');
                    $output .= '<br />' . $request->ip();
                    break;
                case 'show processlist':
                case 'show table status':
                case 'show full processlist':
                case 'show global status':
                    $result = DB::select(DB::raw($command));
                    $dboutput = count($result);
                    break;
                case 'top':
                    $output = shell_exec('top');
                    break;
                case 'dir':
                    $output = shell_exec('dir');
                    break;
                case 'mysql_stat':
                    $link = mysqli_connect(env('DB_HOST'), env('DB_USERNAME'), env('DB_PASSWORD'));
                    $status = explode('  ', mysqli_stat($link));
                    echo '<pre>';
                    print_r($status);
                    $dboutput = '-1';
                    echo '</pre>';
                    break;
                default:
                    $output = 'command : ' . $command . ' not found!';
                    break;
            }
        } else {
            $output = 'User ' . Auth::user()->user_full_name . ' has no access to execute this command!';
        }
        if ($output) {
            echo "<div><pre>$output</pre></div>";
        } elseif ($dboutput != '') {
            if ($dboutput > 0) {
                echo createHTMLTable($result);
            } elseif ($dboutput < 0) {
                //system table
            } else {
                echo "<pre><strong><em>$command</em></strong> has no result!</pre>";
            }
        } else {
            echo "<pre>command <strong><em>$command</em></strong> has no response text!</pre>";
        }
        echo '<br />Executed on ' . date('h:i:s.u T', time());
        return '';
    }

    public function sendNotificationToUserType(Request $request, $userType) {
        $userTypeDecoded = Encryption::decodeId($userType);
        try {
            if (Auth::user()->user_type == '1x101') {
                $users = User::where('user_type', $userTypeDecoded)->get(['id', 'user_type', 'user_email', 'user_phone']);
                if (isset($users) && count($users) > 0) {
                    foreach ($users as $user) {
                        $smsData['source'] = $request->get('message');
                        $smsData['destination'] = $user->user_phone;
                        $smsData['msg_type'] = 'SMS';
                        $smsData['ref_id'] = $user->id;
                        $smsData['is_sent'] = 0;
                        $smsData['priority'] = $request->get('priority');
                        Notification::create($smsData);
                    }
                }
                Session::flash("success", 'Sent notification.');
            } else {
                Session::flash("error", 'No access right');
            }
            return redirect('settings/edit-user-type/' . $userType);
        } catch (\Exception $e) {
            Session::flash('error', 'Sorry! Something went wrong.');
            return redirect('settings/edit-user-type/' . $userType);
        }
    }

    /* End of Server Inforelated functions */

    /* Start of Ports related functions */

    public function ports() {
        $rows = Ports::orderBy('ports.name')
                ->leftJoin('country_info', 'country_info.iso', '=', 'ports.country_iso')
                ->where('ports.is_archieved', 0)
                ->get(['country_info.nicename as country', 'ports.name as port_name', 'ports.id as port_id', 'ports.is_active']);
        return view("settings::ports.list", compact('rows'));
    }

    public function createPort() {
        $countries = Countries::orderBy('nicename')->where('country_status', 'Yes')->lists('nicename', 'iso');
        return view("settings::ports.create", compact('countries'));
    }

    public function storePort(Request $request) {
        if (!ACL::getAccsessRight('settings', 'A')) {
            die('You have no access right! Please contact system administration for more information.');
        }

        $this->validate($request, [
            'country_iso' => 'required',
            'name' => 'required',
        ]);

        $insert = Ports::create([
                    'country_iso' => $request->get('country_iso'),
                    'name' => $request->get('name'),
                    'is_active' => 1,
                    'created_by' => CommonFunction::getUserId(),
        ]);

        Session::flash('success', 'The port is stored successfully!');
        return redirect('/settings/edit-port/' . Encryption::encodeId($insert->id));
    }

    public function editPort($encrypted_id) {
        $id = Encryption::decodeId($encrypted_id);
        $data = Ports::where('id', $id)->first();
        $countries = Countries::orderBy('nicename')->where('country_status', 'Yes')->lists('nicename', 'iso');
        return view("settings::ports.edit", compact('data', 'countries', 'encrypted_id'));
    }

    public function updatePort($enc_id, Request $request) {
        if (!ACL::getAccsessRight('settings', 'E')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $id = Encryption::decodeId($enc_id);

        $this->validate($request, [
            'country_iso' => 'required',
            'name' => 'required',
        ]);

        Ports::where('id', $id)->update([
            'country_iso' => $request->get('country_iso'),
            'name' => $request->get('name'),
            'is_active' => $request->get('is_active'),
            'updated_by' => CommonFunction::getUserId()
        ]);

        Session::flash('success', 'The port has been changed successfully.');
        return redirect('/settings/edit-port/' . $enc_id);
    }

    /* End of Ports related functions */

    /* Start of Units related functions */

    public function Units() {
        $rows = Units::orderBy('name')->where('is_archieved', 0)->get(['id', 'name', 'is_active']);
        return view("settings::units.list", compact('rows'));
    }

    public function createUnit() {
        $active_status = ['1' => 'Active', '2' => 'Inactive'];
        return view("settings::units.create", compact('active_status'));
    }

    public function storeUnit(Request $request) {
        if (!ACL::getAccsessRight('settings', 'A')) {
            die('You have no access right! Please contact system administration for more information.');
        }

        $this->validate($request, [
            'name' => 'required',
            'is_active' => 'required',
        ]);

        $insert = Units::create([
                    'name' => $request->get('name'),
                    'is_active' => $request->get('is_active'),
                    'created_by' => CommonFunction::getUserId(),
        ]);

        Session::flash('success', 'The new unit is stored successfully!');
        return redirect('/settings/edit-unit/' . Encryption::encodeId($insert->id));
    }

    public function editUnit($id) {
        $_id = Encryption::decodeId($id);
        $data = Units::where('id', $_id)->first();
        $active_status = ['1' => 'Active', '2' => 'Inactive'];
        return view("settings::units.edit", compact('data', 'active_status', 'id'));
    }

    public function updateUnit($enc_id, Request $request) {
        if (!ACL::getAccsessRight('settings', 'E')) {
            die('You have no access right! Please contact system administration for more information.');
        }
        $id = Encryption::decodeId($enc_id);

        $this->validate($request, [
            'name' => 'required',
            'is_active' => 'required',
        ]);

        Units::where('id', $id)->update([
            'name' => $request->get('name'),
            'is_active' => $request->get('is_active'),
            'updated_by' => CommonFunction::getUserId()
        ]);

        Session::flash('success', 'The unit has been changed successfully.');
        return redirect('/settings/edit-unit/' . $enc_id);
    }

    /* End of Units related functions */

    /* Start of dashboard Object related functions */

    public function dashboardObj() {
        $getList = Dashboard::orderBy('db_obj_caption')
                ->get(['id', 'db_obj_title', 'db_obj_caption', 'db_obj_type', 'db_user_type']);
        return view("settings::dashboard_obj.list", compact('getList'));
    }

    public function createDashboardObj() {
        return view("settings::dashboard_obj.create", compact(''));
    }

    public function storeDashboardObj(Request $request) {
        $this->validate($request, [
            'db_obj_title' => 'required',
            'db_obj_caption' => 'required',
            'db_obj_type' => 'required',
            'db_obj_para1' => 'required',
            'db_user_type' => 'required',
        ]);

        $insert = Dashboard::create(
                        array(
                            'db_obj_title' => $request->get('db_obj_title'),
                            'db_obj_caption' => $request->get('db_obj_caption'),
                            'db_obj_type' => $request->get('db_obj_type'),
                            'db_obj_para1' => $request->get('db_obj_para1'),
                            'db_user_type' => $request->get('db_user_type'),
                            'updated_by' => CommonFunction::getUserId()
        ));

        Session::flash('success', 'Data is stored successfully!');
        return redirect('/settings/edit-dash-obj/' . Encryption::encodeId($insert->id));
    }

    public function editDashboardObj($_id) {
        $id = Encryption::decodeId($_id);
        $data = Dashboard::where('id', $id)->first();
        $user_types = UserTypes::lists('type_name', 'id');
        return view("settings::dashboard_obj.edit", compact('data', '_id', 'user_types'));
    }

    public function updateDashboardObj($id, Request $request) {
        $_id = Encryption::decodeId($id);
        $this->validate($request, [
            'db_obj_title' => 'required',
            'db_obj_caption' => 'required',
            'db_obj_type' => 'required',
            'db_obj_para1' => 'required',
            'db_user_type' => 'required',
        ]);

        Dashboard::where('id', $_id)->update([
            'db_obj_title' => $request->get('db_obj_title'),
            'db_obj_caption' => $request->get('db_obj_caption'),
            'db_obj_type' => $request->get('db_obj_type'),
            'db_obj_para1' => $request->get('db_obj_para1'),
            'db_user_type' => $request->get('db_user_type'),
            'updated_by' => CommonFunction::getUserId()
        ]);

        Session::flash('success', 'Data has been changed successfully.');
        return redirect('/settings/edit-dash-obj/' . $id);
    }

    function softDelete($model, $_id) {
        $id = Encryption::decodeId($_id);

        switch (true) {
            case ($model == "Area"):
                $cond = Area::where('area_id', $id);
                $list = 'area-list';
                break;
            case ($model == "Bank"):
                $cond = Bank::where('id', $id);
                $list = 'bank-list';
                break;
            case ($model == "Currency"):
                $cond = Currencies::where('id', $id);
                $list = 'currency';
                break;
            case ($model == "Document"):
                $cond = docInfo::where('doc_id', $id);
                $list = 'document';
                break;
            case ($model == "EcoZone"):
                $cond = EconomicZones::where('id', $id);
                $list = 'eco-zones';
                break;
            case ($model == "HighCommissions"):
                $cond = HighComissions::where('id', $id);
                $list = 'high-commission';
                break;
            case ($model == "hsCode"):
                $cond = HsCodes::where('id', $id);
                $list = 'hs-codes';
                break;
            case ($model == "Notice"):
                $cond = Notice::where('id', $id);
                $list = 'notice';
                break;
            case ($model == "Port"):
                $cond = Ports::where('id', $id);
                $list = 'ports';
                break;
            case ($model == "Unit"):
                $cond = Units::where('id', $id);
                $list = 'units';
                break;
            default:
                Session::flash('error', 'Invalid Model! error code (Del-' . $model . ')');
                return redirect('dashboard');
        }

        $cond->update([
            'is_archieved' => 1,
            'updated_by' => CommonFunction::getUserId()
        ]);

        Session::flash('success', 'Data has been deleted successfully.');
        return redirect('/settings/' . $list);
    }

    /* End of dashboard Object related functions */

    /*     * ****************************** End of Users Controller Class ********************************** */
}
