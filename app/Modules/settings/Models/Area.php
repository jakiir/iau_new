<?php

namespace App\Modules\Settings\Models;

use App\Libraries\CommonFunction;
use Illuminate\Database\Eloquent\Model;

class Area extends Model {

    protected $table = 'area_info';
    protected $fillable = array(
        'area_id',
        'area_nm',
        'pare_id',
        'area_type',
        'area_nm_ban',
        'is_active',
        'is_archieved',
        'updated_by',
    );
    public $timestamps = false;

    public static function boot() {
        parent::boot();

        static::creating(function($post) {
            $post->created_by = CommonFunction::getUserId();
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function($post) {
            $post->updated_by = CommonFunction::getUserId();
        });
    }

    /*     * *******************************************End of Model Class********************************************* */
}
