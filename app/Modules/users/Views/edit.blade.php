@extends('layouts.admin')
@section('page_heading',trans('messages.profile'))
@section("content")
<?php
$accessMode = ACL::getAccsessRight('user');
if (!ACL::isAllowed($accessMode, 'V')) {
    die('You have no access right! For more information please contact system admin.');
}
?>
<section class="content">

    {!! Session::has('success') ? '<div class="alert alert-success alert-dismissible">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("success") .'</div>' : '' !!}
    {!! Session::has('error') ? '<div class="alert alert-danger alert-dismissible">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("error") .'</div>' : '' !!}

    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel panel-heading">
                <b> Profile of {{ $users->user_full_name }} </b>
            </div>

            <div class="panel-body">
                <div class="col-md-8">
                    <?php
                    $user_type_explode = explode('x', $users->user_type);
                    ?>
                    {!! Form::open(array('url' => '/users/update/'.Encryption::encodeId($users->id),'method' => 'patch', 'class' => 'form-horizontal',
                    'id'=> 'user_edit_form')) !!}
                    <fieldset>
                        {!! Form::hidden('selected_file', '', array('id' => 'selected_file')) !!}
                        {!! Form::hidden('validateFieldName', '', array('id' => 'validateFieldName')) !!}
                        {!! Form::hidden('isRequired', '', array('id' => 'isRequired')) !!}
                        <?php
                        $random_number = str_random(30);
                        ?>
                        {!! Form::hidden('TOKEN_NO', $random_number) !!}

                        <div class="form-group has-feedback {{ $errors->has('user_full_name') ? 'has-error' : ''}}">
                            <label  class="col-md-5 required-star">{!! trans('messages.first_name') !!}</label>
                            <div class="col-md-7">
                                {!! Form::text('user_full_name', $users->user_full_name, $attributes = array('class'=>'form-control textOnly required',
                                'data-rule-maxlength'=>'40','id'=>"user_full_name")) !!}
                                {!! $errors->first('user_full_name','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('user_type') ? 'has-error' : ''}}">
                            <label  class="col-md-5 required-star">{!! trans('messages.specification') !!}</label>
                            <div class="col-md-7">
                                {!! Form::select('user_type', $user_types, $users->user_type, $attributes = array('class'=>'form-control required',
                                'data-rule-maxlength'=>'40', 'placeholder' => 'Select One', 'id'=>"user_type")) !!}
                                {!! $errors->first('user_type','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        @if($logged_in_user_type == '1x101') {{-- For System Admin --}}
                        <div class="form-group hidden" id="userDesk">
                            <label  class="col-md-5 required-star"> User's Desk </label>
                            <div class="col-md-7">
                                {!! Form::select('desk_id', $user_desks, $users->desk_id, $attributes = array('class'=>'form-control',
                                'data-rule-maxlength'=>'40',  'placeholder' => 'Select One','id'=>"desk_id")) !!}
                                {!! $errors->first('desk_id','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="form-group hidden" id="zone">
                            <label  class="col-md-5 required-star"> User's Zone </label>
                            <div class="col-md-7">
                                {!! Form::select('eco_zone_id', $economicZone, $users->ezid, $attributes = array('class'=>'form-control',
                                'data-rule-maxlength'=>'40', 'placeholder' => 'Select One','id'=>"eco_zone_id")) !!}
                                {!! $errors->first('eco_zone_id','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        @endif {{-- logged user is admin --}}

                        <div class="form-group has-feedback {{ $errors->has('country') ? 'has-error' : ''}}">
                            <label  class="col-md-5 required-star">Country </label>
                            <div class="col-md-7">
                                {!! Form::select('country', $countries, $users->country, $attributes = array('class'=>'form-control required',
                                'data-rule-maxlength'=>'40','placeholder' => 'Select One', 'id'=>"country")) !!}
                                {!! $errors->first('country','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('nationality') ? 'has-error' : ''}}">
                            <label  class="col-md-5 required-star"> Nationality</label>
                            <div class="col-md-7">
                                {!! Form::select('nationality', $nationalities, $users->country, $attributes = array('class'=>'form-control required',
                                'data-rule-maxlength'=>'40','placeholder' => 'Select One', 'id'=>"nationality")) !!}
                                {!! $errors->first('nationality','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('passport_no') ? 'has-error' : ''}}">
                            <label  class="col-md-5 text-left">Passport No.</label>
                            <div class="col-md-7">
                                {!! Form::text('passport_no',  $users->passport_no, $attributes = array('class'=>'form-control',  'data-rule-maxlength'=>'40',
                                'placeholder'=>'Enter your Passport No.', 'id'=>"passport_no")) !!}
                                {!! $errors->first('passport_no','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('division') ? 'has-error' : ''}}" id="division_div">
                            <label  class="col-md-5 required-star">Division </label>
                            <div class="col-md-7">
                                {!! Form::select('division', $divisions, $users->division, $attributes = array('class'=>'form-control',
                                'placeholder' => 'Select One', 'id'=>"division")) !!}
                                {!! $errors->first('division','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="form-group has-feedback hidden {{ $errors->has('state') ? 'has-error' : ''}}" id="state_div">
                            <label  class="col-md-5 required-star"> State </label>
                            <div class="col-md-7">
                                {!! Form::text('state', $users->state, $attributes = array('class'=>'form-control', 'placeholder' => 'Name of your state / division',
                                'data-rule-maxlength'=>'40', 'id'=>"state")) !!}
                                {!! $errors->first('state','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('district') ? 'has-error' : ''}}" id="district_div">
                            <label  class="col-md-5 required-star"> District</label>
                            <div class="col-md-7">
                                {!! Form::select('district', $districts, $users->district, $attributes = array('class'=>'form-control',
                                'placeholder' => 'Select Division First','data-rule-maxlength'=>'40','id'=>"district")) !!}
                                {!! $errors->first('district','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="form-group has-feedback hidden {{ $errors->has('province') ? 'has-error' : ''}}" id="province_div">
                            <label  class="col-md-5 required-star"> Province </label>
                            <div class="col-md-7">
                                {!! Form::text('province', $users->province, $attributes = array('class'=>'form-control', 'data-rule-maxlength'=>'40',
                                'placeholder' => 'Enter your Province', 'id'=>"province")) !!}
                                {!! $errors->first('province','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('road_no') ? 'has-error' : ''}}">
                            <label  class="col-md-5 required-star"> Address Line 1 </label>
                            <div class="col-md-7">
                                {!! Form::text('road_no', $users->road_no, $attributes = array('class'=>'form-control bnEng required', 'data-rule-maxlength'=>'100',
                                'placeholder' => 'Enter Road / Street Name / No.', 'id'=>"road_no")) !!}
                                {!! $errors->first('road_no','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('house_no') ? 'has-error' : ''}}">
                            <label  class="col-md-5"> Address Line 2 </label>
                            <div class="col-md-7">
                                {!! Form::text('house_no', $users->house_no, $attributes = array('class'=>'form-control bnEng',
                                'data-rule-maxlength'=>'100','placeholder' => 'Enter House / Flat / Holding No.', 'id'=>"house_no")) !!}
                                {!! $errors->first('house_no','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('post_code') ? 'has-error' : ''}}">
                            <label  class="col-md-5 "> Post Code </label>
                            <div class="col-md-7">
                                {!! Form::text('post_code', $users->post_code, $attributes = array('class'=>'form-control', 'data-rule-maxlength'=>'40',
                                'placeholder' => 'Enter your Post Code ', 'id'=>"post_code")) !!}
                                {!! $errors->first('post_code','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label  class="col-md-5">{!! trans('messages.dob') !!}</label>
                            <div class="col-md-7">
                                <?php
                                $dob = !empty($users->user_DOB) ?
                                        App\Libraries\CommonFunction::changeDateFormat($users->user_DOB) : '';
                                ?>
                                &nbsp; {{ $dob }}
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('user_phone') ? 'has-error' : ''}}">
                            <label  class="col-md-5 ">{!! trans('messages.mobile') !!}</label>
                            <div class="col-md-7">
                                {!! Form::text('user_phone', $users->user_phone, $attributes = array('class'=>'form-control phone',
                                'placeholder'=>'Enter the Mobile Number','id'=>"user_phone")) !!}
                                <span class="text-danger mobile_number_error"></span>
                                {!! $errors->first('user_phone','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('user_email') ? 'has-error' : ''}}">
                            <label  class="col-md-5 ">{!! trans('messages.email') !!}</label>
                            <div class="col-md-7">
                                &nbsp; {{ $users->user_email }}
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-2">
                                <a href="/users/lists">
                                    <button type="button" class="btn  btn-default">
                                        <i class="fa fa-times"></i><b> Close</b></button>
                                </a>
                            </div>
                            <div class="col-md-8 text-center">
                                {!! CommonFunction::showAuditLog($users->updated_at, $users->updated_by) !!}
                            </div>
                            <div class="col-md-2">
                                @if(ACL::getAccsessRight('user','E'))
                                <button type="submit" class="btn btn-primary"><b>Save</b></button>
                                @endif
                            </div>
                        </div>

                    </fieldset>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <!--</form>-->
                    {!! Form::close() !!}
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection
@section('footer-script')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $(document).ready(function () {
        $("#user_edit_form").validate({
            errorPlacement: function () {
                return false;
            }
        });
    });

    $(document).ready(function () {
        $("#division").change(function () {
            var divisionId = $('#division').val();
            $(this).after('<span class="loading_data">Loading...</span>');
            var self = $(this);
            $.ajax({
                type: "GET",
                url: "<?php echo url(); ?>/users/get-district-by-division",
                data: {
                    divisionId: divisionId
                },
                success: function (response) {
                    var option = '<option value="">Select One</option>';
                    if (response.responseCode == 1) {
                        var district_id = '<?php echo!empty($users->district) ? $users->district : 0; ?>';
                        $.each(response.data, function (id, value) {
                            var selected = (id === district_id) ? 'selected="selected"' : '';
                            option += '<option value="' + id + '" ' + selected + '>' + value + '</option>';
                        });
                    }
                    $("#district").html(option);
                    $(self).next().hide();
                }
            });
        });

        $('#user_type').change(function () {
            var type = $(this).val();
            var desktype = $(this).val();
            if (type == '4x404') { // 4x404 is Developers
                $('#userDesk').removeClass('hidden');
                $('#desk_id').addClass('required');
                if (desktype != '6') { // 6=RD4
                    $('#zone').removeClass('hidden');
                    $('#zone_id').addClass('required');
                } 
            }
            else if (type == '8x808') { // 8x808 is Zone MIS user
                $('#userDesk').addClass('hidden');
                $('#desk_id').removeClass('required');
                $('#zone').removeClass('hidden');
                $('#zone_id').addClass('required');
            }
            else if (type == '9x909' || type == '3x303') { // 3x303 = security, 9x909 = Customs
                $('#userDesk').removeClass('hidden');
                $('#desk_id').addClass('required');
                if (desktype != '6') { // 6=RD4
                    $('#zone').removeClass('hidden');
                    $('#zone_id').addClass('required');
                }
            }
            else {
                $('#userDesk').addClass('hidden');
                $('#desk_id').removeClass('required');
                $('#desk_id').prop('selectedIndex', 0);
                $('#desk_id').trigger('change');
                $('#zone').addClass('hidden');
                $('#zone_id').removeClass('required');
                $('#zone_id').prop('selectedIndex', 0);
            }
        });

        $("#user_type").change(function () {
            var typeId = $('#user_type').val();
            $(this).after('<span class="loading_data">Loading...</span>');
            var self = $(this);
            $.ajax({
                type: "GET",
                url: "<?php echo url(); ?>/users/get-userdesk-by-type",
                data: {
                    typeId: typeId
                },
                success: function (response) {
                    var option = '<option value="">Select One</option>';
                    var userDesk = '<?php echo!empty($users->desk_id) ? $users->desk_id : 0; ?>';
                    if (response.responseCode == 1) {
                        $.each(response.data, function (id, value) {
                            var selected = (id === userDesk) ? 'selected="selected"' : '';
                            option += '<option value="' + id + '" ' + selected + '>' + value + '</option>';
                        });
                    }
                    $("#desk_id").html(option);
                    $(self).next().hide();
                }
            });
        });
        $('#user_type').trigger('change');
        
        $("#country").change(function () {
            if (this.value == 'BD') { // 001 is country_code of Bangladesh
                $('#division_div').removeClass('hidden');
                $('#division').addClass('required');
                $('#district_div').removeClass('hidden');
                $('#district').addClass('required');
                $('#state_div').addClass('hidden');
                $('#state').removeClass('required');
                $('#province_div').addClass('hidden');
                $('#province').removeClass('required');
            }
            else {
                $('#state_div').removeClass('hidden');
                $('#state').addClass('required');
                $('#province_div').removeClass('hidden');
                $('#province').addClass('required');
                $('#division_div').addClass('hidden');
                $('#division').removeClass('required');
                $('#district_div').addClass('hidden');
                $('#district').removeClass('required');
            }
        });
        $('#country').trigger('change');
    });
</script>
@endsection <!--- footer-script--->
