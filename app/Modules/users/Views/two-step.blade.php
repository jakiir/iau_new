@extends('layouts.front')

@section("content")

    <div class="container" style="margin-top:30px;">
        <div class="row">
            <div class="col-md-10 col-md-offset-1" style="background: snow; opacity:0.88; border-radius:8px;">

                <div class="col-md-12 col-sm-12">
                    @include('partials.messages')
                    {!! Form::open(array('url' => 'users/check-two-step/','method' => 'patch', 'class' => 'form-horizontal', 'id' => 'packagesCreateForm',
                    'enctype' =>'multipart/form-data', 'files' => 'true')) !!}
                </div>
                <div class="col-sm-1 col-sm-offset-1"></div>
                <div class="col-sm-6">

                    <h3 class="">Two Step Verification </h3>
                    <br/><br/>
                    <div class="box-body">
                        <div class="form-group col-sm-12 {{$errors->has('steps') ? 'has-error' : ''}}">
                            <div class="col-sm-12">
                                <label>
                                    <?php $email =Auth::user()->user_email;  ?>
                                    {!! Form::radio('steps', 'email',  true, ['class' => ' required']) !!}
                                        Get code in Email
                                </label>
                                (<?php echo substr($email,0,3).'***************'.substr($email,-9); ?>)

                                <br/>
                                <label>
                                    {!! Form::radio('steps', 'mobile_no', null, ['class' => 'required']) !!}
                                        Get SMS in Mobile No&nbsp;
                                    {!! $errors->first('state','<span class="help-block">:message</span>') !!}
                                </label>
                                <?php echo substr(Auth::user()->user_phone,0,6).'**************'.substr(Auth::user()->user_phone,-2); ?>
                            </div>
                        </div>
                        <div class="email_address form-group col-sm-12 {{$errors->has('email_address') ? 'has-error' : ''}}" style="display: none;">
                            {!! Form::label('email_address','Email Address ',['class'=>'col-sm-5']) !!}
                            <div class="col-sm-7">
                                {!! Form::text('email_address','', ['class' => 'form-control','placeholder'=>'Email Address']) !!}

                                {!! $errors->first('email_address','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <a href="{{ url('logout') }}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                            <button type="submit" class="btn btn-primary pull-right">
                                <i class="fa fa-chevron-circle-right"></i>
                                Next
                            </button>
                        </div><!-- /.box-footer -->

                    </div>
                    {!! Form::close() !!}<!-- /.form end -->
                </div>
            </div>
@endsection

@section ('footer-script')
@endsection
