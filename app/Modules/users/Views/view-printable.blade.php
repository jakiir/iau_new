@extends('layouts.admin')

@section('page_heading',trans('messages.user_view'))
@section('content')

<?php
$accessMode = ACL::getAccsessRight('user');
if (!ACL::isAllowed($accessMode, 'V'))
    die('no access right!');
?>

<div class="col-lg-12">
    <section class="col-md-12" id="printDiv">
        <div class="row"><!-- Horizontal Form -->
            @include('partials.messages')
            {!! Form::open(array('url' => '/users/approve/'.Encryption::encodeId($user->id),'method' => 'post', 'class' => 'form-horizontal',   'id'=> 'user_edit_form')) !!}
            <div class="panel">

                <div class="panel-body">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Profile of : {!!$user->user_full_name!!}</h3>
                        </div> <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="col-md-3">

                                @if($user->user_pic)
                                <img src="{{ url('users/upload/'.$user->user_pic) }}" alt="Picture" class="profile-user-img img-responsive img-circle"  id="user_pic"  width="300" />
                                @else
                                    <img src="{{ url('assets/images/no_image.png') }}" alt="Picture" class="profile-user-img img-responsive img-circle"  id="user_pic"  width="300" />
                                @endif
                                <br/>
                                {{--  @if (!empty($user->authorization_file)) --}}
                                <!--                                Click to see authorization letter
                                                                <a href="<?php // echo url();           ?>/uploads/<?php // echo $user->authorization_file;           ?>" target="_blank">
                                                                    {!! '<img src="' . url() . '/assets/images/envelope.jpg'.'" class="letter-img img-responsive"  alt="Profile Picture" id="user_pic"  width="300" />' !!}
                                                                </a>-->
                                {{-- @else --}}

                                @if (!empty($user->authorization_file))
                                <a href="{{ url('uploads/'.$user->authorization_file) }}" target="_blank" class="btn btn-xs btn-primary">
                                    Authorization letter
                                </a>
                                @else
                                <span class="text-danger"><img src="{{ url('/assets/images/warning.png') }}" width="14" /> Authorization letter not found</span>
                                @endif
                                <br/><br/>

                                @if (!empty($user->passport_nid_file))
                                    <a href="{{ url('users/passportnid/'.$user->passport_nid_file) }}" target="_blank" class="btn btn-xs btn-primary">
                                        Scan copy of Passport/NID
                                    </a>
                                @else
                                    <span class="text-danger"><img src="{{ url('/assets/images/warning.png') }}" width="14" /> Passport/NID Not Found</span>
                                @endif
                            </div>
                            <div class="col-md-9">
                                <dl class="dls-horizontal">
                                    <dt>Full Name :</dt>
                                    <dd>{!!$user->user_full_name!!}&nbsp;</dd>
                                    <dt>Type :</dt>
                                    <dd>{!!$user->type_name!!}&nbsp;</dd> 

                                    @if($user->desk_id && $user->user_type == '4x404')    {{-- 4x404 is Zone Users--}}
                                    <dt>Desk :</dt>
                                    <dd>
                                        {!!$user->desk_name!!}
                                    </dd>
                                    @endif {{-- is zone user --}}

                                    @if($user->eco_zone_id && $user->user_type == '4x404')    {{-- 4x404 is Zone Users, D1 and D2 desks have zone set--}}
                                    <dt>Zone :</dt>
                                    <dd>
                                        {!!$user->ez_name!!}
                                    </dd>
                                    @endif {{-- users has zone --}}

                                    <dt>Phone :</dt>
                                    <dd>{!!$user->user_phone!!}&nbsp;</dd>
                                    <dt>Email :</dt>
                                    <dd>{!!$user->user_email!!}&nbsp;</dd>

                                    @if($user->user_DOB)
                                    <dt>Date of Birth :</dt>
                                    <dd>
                                        {!!CommonFunction::changeDateFormat($user->user_DOB)!!}
                                        <br/><small>({!!CommonFunction::age($user->user_DOB)!!} Old.)</small>&nbsp;
                                    </dd>
                                    @endif {{-- has DOB --}}

                                </dl>

                                <?php
                                $approval = '';
                                $type = explode('x', $user->user_type);
                                if (substr($type[1], 2, 2) == 0) { // first digit of user type
                                    echo Form::select('user_type', $user_types, $user->user_type, $attributes = array('class' => 'form-control required',
                                        'required' => 'required', 'placeholder' => 'Select One', 'id' => "user_type"));
                                }
                                $approval = '<button type="submit" class="btn btn-sm btn-success"> <i class="fa  fa-check "></i> Approve</button></form>';

                                $approval .= ' <a href="' . url('users/reject/' . Encryption::encodeId($user->id)) .
                                        '" class="btn btn-sm btn-danger"> <i class="fa fa-times"></i> Reject</a>';
                                ?>


                            </div>
                        </div><!-- /.box -->
                    </div>

                    <div class="col-md-12">

                        <div class="pull-left">
                            {{--<button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>--}}
                            <a href="{{ url('users/lists') }}" class="btn btn-sm btn-default"><i class="fa fa-times"></i> Close</a>
                        </div>
                        <div class="pull-right">
                            <?php
                            $activate = "";
                            $edit = '<a href="' . url('users/edit/' . Encryption::encodeId($user->id)) . '" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i> Edit</a>';
                            $reset_password = '<a href="' . url('users/reset-password/' . Encryption::encodeId($user->id)) . '" class="btn btn-sm btn-warning"'
                                    . 'onclick="return confirm(\'Are you sure?\')">'
                                    . '<i class="fa fa-refresh"></i> Reset password</a>';

                            $logged_in_user_type = Auth::user()->user_type;
                            if ($logged_in_user_type != '4x404') { // 4x404 is for Moha Executive
                                if ($user->user_status == 'inactive') {
                                    $activate = '<a href="' . url('users/activate/' . Encryption::encodeId($user->id)) . '" class="btn btn-sm btn-success"><i class="fa fa-unlock"></i>  Activate</a>';
                                } else {
                                    $activate = '<a href="' . url('users/activate/' . Encryption::encodeId($user->id)) . '" class="btn btn-sm btn-danger"'
                                            . 'onclick="return confirm(\'Are you sure?\')">'
                                            . '<i class="fa fa-unlock-alt"></i> Deactivate</a>';
                                }
                            }
                            $delegate = '<a  href="' . url('users/delegation/' . Encryption::encodeId($user->id)) . '" class="btn btn-sm btn-info add-delegation"><i class="fa fa-edit"></i> Delegate</a>';

                            if ($user->is_approved == true) {
                                if ($logged_in_user_type == '5x505') {
                                    echo '<button>Send Password reset token </button>';
                                } elseif (\App\Libraries\CommonFunction::isAdmin()) {
                                    if (ACL::getAccsessRight('user', 'E'))
                                        echo $edit;
                                    if (ACL::getAccsessRight('user', 'R'))
                                        echo '&nbsp;' . $reset_password;
                                    if (ACL::getAccsessRight('user', 'E'))
                                        echo '&nbsp;' . $activate;
                                    if (ACL::getAccsessRight('user', 'E') && ($user->desk_id == 3 || $user->desk_id == 4 || $user->desk_id == 2 || $user->desk_id == 1 || $user->desk_id == 8))
                                        echo '&nbsp;' . $delegate;
                                }
                            } else {
                                echo $approval;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </section>

    @if($user->desk_id == 0 && $user->user_type == '4x404') {{-- 4x404 is BEZA Executive --}}
    <div class="panel panel-success">
        <div class="panel-heading"> <b>Set User's Desk </b></div>
        <div class="panel-body">
            {!! Form::open(array('url' => '/users/set-desk/'.Encryption::encodeId($user->id),'method' => 'patch', 'class' => 'form-horizontal',
            'id'=> 'setDeskForm')) !!}
            <div class="col-md-12">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="form-group has-feedback {{ $errors->has('desk_id') ? 'has-error' : ''}}">
                        <label  class="col-md-5 text-left required-star"> User's Desk</label>
                        <div class="col-md-7">
                            {!! Form::select('desk_id', $user_desks, '', $attributes = array('class'=>'form-control required',  'data-rule-maxlength'=>'40',
                            'placeholder' => 'Select One', 'id'=>"desk_id")) !!}
                            {!! $errors->first('desk_id','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>                 
                </div>
                <div class="col-md-2"></div>
            </div>
            <div class="col-md-12">
                @if(ACL::getAccsessRight('user','E'))
                <button type="submit" class="btn btn-md btn-success pull-right"><b>Set Desk and Approve</b></button>
                @endif
            </div>
            {!! Form::close() !!}
        </div>
        @endif

    </div>
    @endsection <!--content section-->

    @section('footer-script')
    <script>
        $(".remove-delegation").click(function () {

            if (confirm('Are you sure you want to remove delegation ?')) {
                return true;
            } else {
                return false;
            }
        });

        $(".add-delegation").click(function () {

            if (confirm('Are you sure you want to Add delegation for this user?')) {
                return true;
            } else {
                return false;
            }
        });

        $(document).ready(function () {
            $("#setDeskForm").validate({
                errorPlacement: function () {
                    return false;
                }
            });
        });
    </script>
    @endsection 