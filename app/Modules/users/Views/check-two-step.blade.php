@extends('layouts.front')

@section("content")

<div class="container" style="margin-top:30px;">
    <div class="row">
        <div class="col-md-10 col-md-offset-1" style="background: snow; opacity:0.88; border-radius:8px;">

            {!! Form::open(array('url' => 'users/verify-two-step/','method' => 'patch', 'class' => 'form-horizontal', 'id' => 'verifyForm',
            )) !!}

            <div class="col-sm-6 col-sm-offset-3">
                <div class="box-body">
                    <h3 class="text-left">Code</h3>
                    <div class="form-group col-md-6 {{$errors->has('security_code') ? 'has-error' : ''}}">
                       {!! Form::text('security_code','', ['class' => 'form-control','placeholder'=>'Enter your security code']) !!}

                        {!! $errors->first('security_code','<span class="help-block">:message</span>') !!}

                    </div>
                    <div class="col-sm-12">
                        <a href="{{ url('logout') }}"><i class="fa fa-sign-out fa-fw"></i> Logout</a> or
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-chevron-circle-right"></i>
                            Submit
                        </button>

                    </div><!-- /.box-footer -->

                </div>
                {!! Form::close() !!}<!-- /.form end -->
            </div>
        </div>
        @endsection

        @section ('footer-script')
        @endsection
