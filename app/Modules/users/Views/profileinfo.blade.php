@extends('layouts.admin')

@section('page_heading',trans('messages.profile'))

@section('content')
@include('partials.messages')
<div class="col-md-12">
    <div class="panel panel-red">
        <div class="panel-heading"><b> Account Information of {{ $users->name }} </b></div>
        <div class="panel-body">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">Profile</a></li>
                    <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Change Password</a></li>
                    <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Access Log</a></li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="panel panel-info">
                            <div class="panel-body">
                                <div class="col-md-12 col-sm-12">
                                    {!! Form::open(array('url' => '/users/profile_update/'.$id,'method' => 'patch','id'=>'update_form', 'class' => 'form-horizontal',
                                    'enctype'=>'multipart/form-data')) !!}
                                    <div class="col-md-6 col-sm-6">
                                        <fieldset>
                                            <div class="row">
                                                <div class="progress hidden pull-right" id="upload_progress"
                                                     style="width: 50%;">
                                                    <div class="progress-bar progress-bar-striped active"
                                                         role="progressbar" aria-valuenow="100" aria-valuemin="0"
                                                         aria-valuemax="100" style="width: 100%">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group has-feedback {{ $errors->has('username') ? 'has-error' : '' }}">
                                                <label class="col-lg-4 text-left">Madrash EIIN.</label>
                                                <div class="col-lg-7">
                                                    {{ $users->username }}
                                                </div>
                                            </div>

                                            <div class="form-group has-feedback">
                                                <label class="col-lg-4 text-left">User Type</label>
                                                <div class="col-lg-7">
                                                    {{ $user_type_name}}
                                                </div>
                                            </div>
                                            @if(!empty($additional_info))
                                                @foreach($additional_info as $info)
                                                    <div class="form-group">
                                                        <label class="col-lg-4">{{$info['caption_division']}}</label>
                                                        <div class="col-lg-7">
                                                            {{$info['value_division']}}
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-lg-4">{{$info['caption']}}</label>
                                                        <div class="col-lg-7">
                                                            {{$info['value']}}
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-lg-4">{{$info['caption_thana']}}</label>
                                                        <div class="col-lg-7">
                                                            {{$info['value_thana']}}
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif

                                            <div class="form-group has-feedback">
                                                <label class="col-lg-4 text-left">Email Address</label>
                                                <div class="col-lg-7">
                                                    {{ $users->email }}
                                                </div>
                                            </div>
                                            <div class="form-group has-feedback {{ $errors->has('user_full_name') ? 'has-error' : '' }}">
                                                <label class="col-lg-4 text-left required-star">Name</label>
                                                <div class="col-lg-7">
                                                    {!! Form::text('user_full_name',$users->name, ['class'=>'form-control required','data-rule-maxlength'=>'40',
                                                    'placeholder'=>'Enter your Name','id'=>"user_full_name"]) !!}
                                                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                                    {!! $errors->first('user_full_name','<span class="help-block">:message</span>')
                                                    !!}
                                                </div>
                                            </div>
                                            <div class="form-group has-feedback {{ $errors->has('user_phone') ? 'has-error' : ''}}">
                                                <label class="col-lg-4 text-left required-star">Mobile Number </label>
                                                <div class="col-lg-7">
                                                    {!! Form::text('user_phone',$users->user_phone, ['class'=>'form-control required phone', 'data-rule-maxlength'=>'25',
                                                    'placeholder'=>'Enter your Mobile Number','id'=>"user_phone"]) !!}
                                                    <span class="text-danger mobile_number_error"></span>
                                                    <span class="glyphicon glyphicon-phone form-control-feedback"></span>
                                                    {!! $errors->first('user_phone','<span class="help-block">:message</span>') !!}
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>


                                    <div class="col-md-6 col-sm-6">
                                        <div class="well well-sm">
                                            <div class="row">
                                                <div class="col-sm-6 col-md-4">
                                                    <?php
                                                    if (!empty($users->user_pic)) {
                                                        $userPic = url() . '/users/upload/' . $users->user_pic;
                                                    } else {
                                                        $userPic = URL::to('/assets/images/avatar5.png');
                                                    }
                                                    ?>
                                                    <img src="{{ $userPic }}" class="profile-user-img img-responsive img-circle"
                                                         alt="Profile Picture" id="user_pic" width="200"/>

                                                </div>
                                                <div class="col-sm-6 col-md-8">
                                                    <h4>Profile Image</h4>
                                                    <cite title="[File Format: *.jpg / *.png, File size(3-25)KB]">
                                                        <label class="col-lg-8 text-left">
                                                            <span style="font-size: 9px; color: grey">
                                                            {!! $image_config !!}</span>
                                                        </label>
                                                    </cite>
                                                    <div class="clearfix"><br/></div>
                                                    <small>
                                                        <cite title="{!! $image_config !!}">
                                                            <label class="col-lg-8 text-left">
                                                                <span id="user_err" class="text-danger" style="font-size: 10px;"></span>
                                                            </label>
                                                        </cite>
                                                    </small>
                                                    <div class="clearfix"><br/></div>
                                                    <label class="btn btn-primary btn-file">
                                                        <i class="fa fa-picture-o" aria-hidden="true"></i>
                                                        Browse <input type="file" onchange="readURLUser(this);" name="image" data-type="user"
                                                                      data-ref="{{Encryption::encodeId(Auth::user()->id)}}" style="display: none;">
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-md-6 col-sm-6">
                                        <div class="well well-sm">
                                            <div class="row">
                                                <div class="col-sm-6 col-md-4">
                                                    <?php
                                                    if (!empty($users->signature)) {
                                                        $signature = url() . '/users/signature/' . $users->signature;
                                                    } else {
                                                        $signature = URL::to('/assets/images/no_image.png');
                                                    }
                                                    ?>
                                                    <img src="{{ $signature }}" class="signature-user-img img-responsive img-rounded user_signature"
                                                         alt="User Signature" id="user_signature" width="200"/>

                                                </div>
                                                <div class="col-sm-6 col-md-8">
                                                    <h4 class="required-star">Signature</h4>
                                                    <small>
                                                        <cite title="{!! $image_config !!}">
                                                            <label class="col-lg-8 text-left">
                                                            <span style="font-size: 9px; color: grey">
                                                            {!! $image_config !!}</span>
                                                            </label>
                                                        </cite>
                                                    </small>
                                                    <div class="clearfix"><br/></div>
                                                    <small>
                                                        <cite title="{!! $image_config !!}">
                                                            <label class="col-lg-8 text-left">
                                                            <span id="signature_error" class="text-danger" style="font-size: 10px;"></span>
                                                            </label>
                                                        </cite>
                                                    </small>
                                                    <div class="clearfix"><br/></div>
                                                    <label class=" btn btn-primary btn-file {{ $errors->has('signature')?'has-error':'' }}">
                                                        <i class="fa fa-picture-o" aria-hidden="true"> </i> Browse
                                                        <input @if(!isset($users->signature)) class="required" @endif
                                                        type="file" onchange="readURLSignature(this);" name="signature" data-type="signature"
                                                                      data-ref="{{Encryption::encodeId(Auth::user()->id)}}">
                                                        {!! $errors->first('signature','<span class="help-block">:message</span>') !!}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    {{--<div class="col-xs-12 col-md-6 col-sm-6">--}}
                                        {{--<div class="well well-sm">--}}
                                            {{--<div class="row">--}}
                                                {{--<div class="col-sm-12 col-md-12">--}}
                                                    {{--<h4 class="required-star">Scan Copy of Passport / NID</h4>--}}

                                                    {{--@if($users->passport_nid_file == !null)--}}
                                                        {{--<a href="{{ url('users/passportnid/'.$users->passport_nid_file) }}" target="_blank">Click Here to show the attached document</a><br/><br/>--}}
                                                    {{--@else--}}
                                                        {{--<a href=""></a>--}}
                                                    {{--@endif--}}


                                                    {{--<label class=" btn btn-primary btn-file {{ $errors->has('passport_nid_file')?'has-error':'' }}">--}}
                                                        {{--<i class="fa fa-picture-o" aria-hidden="true"> </i> Change--}}
                                                        {{--<input @if(!isset($users->passport_nid_file)) class="required" @endif--}}
                                                        {{--type="file" onchange="readPassportScanCopy(this);" name="passport_nid_file" data-type="passport_nid_file"--}}
                                                               {{--data-ref="{{Encryption::encodeId(Auth::user()->id)}}">--}}
                                                        {{--{!! $errors->first('passport_nid_file','<span class="help-block">:message</span>') !!}--}}
                                                    {{--</label>--}}
                                                    {{--<label class="text-left">--}}
                                                        {{--<span id="passport_nid_error" class="text-danger" style="font-size: 10px;"></span>--}}
                                                    {{--</label>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    <div class="col-md-12"><br/></div>
                                    <div class="col-md-12">
                                        <div class="col-md-2">               
                                            <a href="{{url('dashboard')}}" class="btn btn-default"> <i class="fa fa-times"></i> <b>Close</b></a>
                                        </div>
                                        <div class="col-md-8 text-center">
                                            {!! CommonFunction::showAuditLog($users->updated_at,$users->updated_by) !!}
                                        </div>
                                        <div class="col-md-2">
                                            <button type="submit" class="btn btn-success" id='update_info_btn'><i class="fa fa-paper-plane-o" aria-hidden="true"></i> <b>Save</b></button>
                                        </div>
                                    </div>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    {!! Form::close() !!}
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div><!-- /.tab-pane -->

                    <div class="tab-pane" id="tab_2">
                        <div class="panel panel-info">
                            <div class="panel-body">
                                <div class="col-md-12">
                                    {!! Form::open(array('url' => '/users/update-password-from-profile/'.$id,'method' =>
                                    'patch', 'class' => 'form-horizontal',
                                    'id'=> 'password_change_form')) !!}
                                    <fieldset>
                                        <div class="clearfix"><br/><br/></div>

                                        <div class="form-group has-feedback {{ $errors->has('user_old_password') ? 'has-error' : ''}}">
                                            <label class="col-lg-2 text-left">Old Password</label>
                                            <div class="col-lg-4">
                                                {!! Form::password('user_old_password', ['class'=>'form-control required', 'data-rule-maxlength'=>'40',
                                                'placeholder'=>'Enter your Old password','id'=>"user_old_password"]) !!}
                                                <span class="glyphicon glyphicon-check form-control-feedback"></span>
                                                {!! $errors->first('user_old_password','<span class="help-block">:message</span>') !!}
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback {{ $errors->has('user_new_password') ? 'has-error' : ''}}">
                                            <label class="col-lg-2 text-left">New Password</label>
                                            <div class="col-lg-4">
                                                {!! Form::password('user_new_password', ['class'=>'form-control required', 'minlength' => "6", 'data-rule-maxlength'=>'40',
                                                'placeholder'=>'Enter your New password','id'=>"user_new_password"]) !!}
                                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                                {!! $errors->first('user_new_password','<span  class="help-block">:message</span>') !!}
                                            </div>
                                            <div class="col-lg-4">
                                                <code>[*Minimum 6 characters with at least 1 letter and 1 digit]</code>
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback {{ $errors->has('user_confirm_password') ? 'has-error' : ''}}">
                                            <label class="col-lg-2 text-left">Confirm New Password</label>
                                            <div class="col-lg-4">
                                                {!! Form::password('user_confirm_password', ['class'=>'form-control required', 'minlength' => "6", 'data-rule-maxlength'=>'40',
                                                'placeholder'=>'Confirm your New password','id'=>"user_confirm_password"]) !!}
                                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                                {!! $errors->first('user_confirm_password','<span class="help-block">:message</span>')
                                                !!}
                                            </div>
                                        </div>

                                        <div class="form-group has-feedback">
                                            <div class="col-lg-1"></div>
                                            <div class="col-lg-4">
                                                {!! CommonFunction::showAuditLog($users->updated_at, $users->updated_by) !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-lg-8 col-lg-offset-4">
                                                <div class="clearfix"><br></div>
                                                <button type="submit" class="btn btn-primary" id="update_pass_btn">
                                                    <b>Save</b></button>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>

                    </div><!-- /.tab-pane -->

                    <div class="tab-pane table-responsive" id="tab_3">
                        <br/>
                        <table id="list" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0"
                               width="100%">

                            <thead>
                                <tr>
                                    <th>Log in time</th>
                                    <th>Log out time</th>
                                    <th>Access ip</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div><!-- /.tab-pane -->

                    @if(Auth::user()->user_type != '5x505' || Auth::user()->user_type != '2x202' || Auth::user()->user_type != '1x101')
                    @if(Auth::user()->user_type == '5x505' || Auth::user()->user_type == '2x202' || Auth::user()->user_type == '1x101')
                    {{-- 1x101 is Sysadmin --}}
                    @else
                    <div class="tab-pane table-responsive" id="tab_4">
                        <br/>
                        {!! Form::open(array('url' => '/users/process-deligation','method' =>
                        'patch','id'=>'deligation', 'class' => 'form-horizontal','enctype'
                        =>'multipart/form-data')) !!}
                        
                        <div class="form-group  col-lg-8">
                            <div class="col-lg-3">Delegated User</div>
                            <div class="col-lg-4">
                                {!! Form::select('delegated_user', [] , '', $attributes =
                                array('class'=>'form-control required',
                                'placeholder' => 'Select User', 'id'=>"delegated_user")) !!}
                            </div>
                        </div>

                        <div class="form-group  col-lg-8">
                            <div class="col-lg-3">Remarks</div>
                            <div class="col-lg-4">
                                {!! Form::text('remarks','', $attributes = array('class'=>'form-control', 'placeholder'=>'Enter your Remarks','id'=>"remarks")) !!}
                            </div>
                        </div>
                        <div class="form-group  col-lg-8">
                            <button type="submit" class="btn btn-primary" id='update_info_btn'><b>Deligate</b>
                            </button>
                        </div>
                        {!! Form::close() !!}
                    </div><!-- /.tab-pane -->
                    @endif
                    @endif
                </div><!-- /.tab-content -->
            </div><!-- nav-tabs-custom -->
        </div>            
    </div>            
</div>

<div class="clearfix"></div>

@endsection

@section('footer-script')
@include('partials.datatable-scripts')

<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>
<script src="{{ asset('assets/scripts/jquery.validate.js') }}"></script>
<script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function readURLUser(input) {
            if (input.files && input.files[0]) {
                $("#user_err").html('');
                var mime_type = input.files[0].type;
                if(!(mime_type=='image/jpeg' || mime_type=='image/jpg' || mime_type=='image/png')){
                    $("#user_err").html("Image format is not valid. Only PNG or JPEG or JPG type images are allowed.");
                    return false;
                }
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#user_pic').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        function readURLSignature(input) {
            if (input.files && input.files[0]) {
                $("#signature_error").html('');
                var mime_type = input.files[0].type;
                if(!(mime_type=='image/jpeg' || mime_type=='image/jpg' || mime_type=='image/png')){
                    $("#signature_error").html("Image format is not valid. Only PNG or JPEG or JPG type images are allowed.");
                    return false;
                }
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#user_signature').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        function readPassportScanCopy(input) {
            if (input.files && input.files[0]) {
                $("#passport_nid_error").html('');
                var mime_type = input.files[0].type;
                if(!(mime_type=='application/pdf')){
                    $("#passport_nid_error").html("File format is not valid. Only pdf type file is allowed.");
                    return false;
                }
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#user_passport_nid').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $(function () {
            $('.datepicker').datetimepicker({
                viewMode: 'years',
                format: 'DD-MMM-YYYY',
                maxDate: (new Date()),
                minDate: '01/01/1916'
            });
        });
        $(function () {

            $('a[href="' + window.location.hash + '"]').trigger('click');

            $('#list').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{url("users/get-access-log-data")}}',
                    method: 'POST',
                    data: function (d) {
                        d._token = $('input[name="_token"]').val();
                    }
                },
                columns: [
                    {data: 'login_dt', name: 'login_dt'},
                    {data: 'logout_dt', name: 'logout_dt'},
                    {data: 'ip_address', name: 'ip_address'}
                ]
            });
            $("#update_form").validate({
                errorPlacement: function () {
                    return false;
                }
            });
        });
        function getUserDeligate() {
            var _token = $('input[name="_token"]').val();
            var designation = $('#designation').val();

            $.ajax({
                url: '{{url("users/get-delegate-userinfo")}}',
                type: 'post',
                data: {
                    _token: _token,
                    designation: designation
                },
                dataType: 'json',
                success: function (response) {
                    html = '<option value="">Select One</option>';

                    $.each(response, function (index, value) {
                        html += '<option value="' + value.id + '" >' + value.user_full_name + '</option>';
                    });
                    $('#delegated_user').html(html);
                },
                beforeSend: function (xhr) {
                    console.log('before send');
                },
                complete: function () {
                    //completed
                }
            });
        }

        $('#password_change_form').validate({
            rules: {
                user_confirm_password: {
                    equalTo: "#user_new_password"
                }
            },
            errorPlacement: function () {
                return false;
            }
        });
        $('#designation').trigger('change');

</script>
@endsection
