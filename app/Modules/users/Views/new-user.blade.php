@extends('layouts.admin')

@section("content")
<?php
$accessMode = ACL::getAccsessRight('user');
if (!ACL::isAllowed($accessMode, 'V')) {
    die('You have no access right! For more information please contact system admin.');
}
?>
@include('partials.messages')
<div class="col-md-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div> <strong>{{trans('messages.new_user')}}</strong></div>
        </div>

        {!! Form::open(array('url' => '/users/store-new-user','method' => 'patch', 'class' => 'form-horizontal', 'id' => 'create_user_form',
        'enctype' =>'multipart/form-data', 'files' => 'true')) !!}

        <div class="panel-body">
            <div class="col-md-6">

                <div class="form-group has-feedback {{ $errors->has('user_full_name') ? 'has-error' : ''}}">
                    <label  class="col-md-4 required-star">Name</label>
                    <div class="col-md-7">
                        {!! Form::text('user_full_name', $value = null, $attributes = array('class'=>'form-control required',
                        'data-rule-maxlength'=>'40', 'placeholder'=>'Enter your Name','id'=>"user_full_name")) !!}
                        {!! $errors->first('user_full_name','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                @if($logged_user_type == '1x101') {{-- For System Admin --}}

                <div class="form-group has-feedback {{ $errors->has('user_type') ? 'has-error' : ''}}">
                    <label  class="col-md-4 required-star">User Type</label>
                    <div class="col-md-7">
                        {!! Form::select('user_type', $user_types, '', $attributes = array('class'=>'form-control required','data-rule-maxlength'=>'40',
                        'placeholder' => 'Select One', 'id'=>"user_type")) !!}
                        {!! $errors->first('user_type','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                @endif

                <div class="form-group has-feedback {{$errors->has('user_DOB') ? 'has-error' : ''}}">
                    {!! Form::label('user_DOB','Date of Birth',['class'=>'col-md-4 required-star']) !!}
                    <div class="col-md-7">
                        <div class="datepicker input-group date" data-date="12-03-2015" data-date-format="dd-mm-yyyy">
                            {!! Form::text('user_DOB', '', ['class'=>'form-control required', 'placeholder' => 'Pick from calender']) !!}
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                        {!! $errors->first('user_DOB','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="form-group has-feedback {{ $errors->has('user_phone') ? 'has-error' : ''}}">
                    <label  class="col-md-4 required-star">Mobile Number</label>
                    <div class="col-md-7">
                        {!! Form::text('user_phone', $value = null, $attributes = array('class'=>'form-control digits required phone', 'maxlength'=>"20", 
                        'minlength'=>"8", 'placeholder'=>'Enter your Mobile Number','id'=>"user_phone")) !!}
                        {!! $errors->first('user_phone','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="form-group has-feedback {{ $errors->has('user_email') ? 'has-error' : ''}}">
                    <label  class="col-md-4 required-star">Email Address</label>
                    <div class="col-md-7">
                        {!! Form::text('user_email', $value = null, $attributes = array('class'=>'form-control email required', 'data-rule-maxlength'=>'40',
                        'placeholder'=>'Enter your Email Address','id'=>"user_email")) !!}
                        {!! $errors->first('user_email','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="form-group has-feedback {{ $errors->has('passport_no') ? 'has-error' : ''}}">
                    <label  class="col-md-4 text-left">Passport No.</label>
                    <div class="col-md-7">
                        {!! Form::text('passport_no', null, $attributes = array('class'=>'form-control',  'data-rule-maxlength'=>'40',
                        'placeholder'=>'Enter the Passport No. (if any)', 'id'=>"passport_no")) !!}
                        {!! $errors->first('passport_no','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="form-group has-feedback {{ $errors->has('user_nid') ? 'has-error' : ''}}">
                    <label  class="col-md-4 text-left">National ID No.</label>
                    <div class="col-md-7">
                        {!! Form::text('user_nid', null, $attributes = array('class'=>'form-control',  'data-rule-maxlength'=>'40',
                        'placeholder'=>'Enter the NID (if any)', 'id'=>"user_nid")) !!}
                        {!! $errors->first('user_nid','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="form-group has-feedback {{ $errors->has('user_fax') ? 'has-error' : ''}}">
                    <label  class="col-md-4 text-left">Fax</label>
                    <div class="col-md-7">
                        {!! Form::text('user_fax', null, $attributes = array('class'=>'form-control', 'placeholder'=>'Enter your Fax (If Any)',
                        'data-rule-maxlength'=>'40','id'=>"user_fax")) !!}
                        {!! $errors->first('user_fax','<span class="help-block">:message</span>') !!}
                    </div>
                </div>


            </div><!--/col-md-6-->

            <div class="col-md-6">

                <div class="form-group has-feedback {{ $errors->has('country') ? 'has-error' : ''}}">
                    <label  class="col-md-5 required-star">Country </label>
                    <div class="col-md-7">
                        {!! Form::select('country', $countries, null, $attributes = array('class'=>'form-control required', 'data-rule-maxlength'=>'40',
                        'placeholder' => 'Select One', 'id'=>"country")) !!}
                        {!! $errors->first('country','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="form-group has-feedback {{ $errors->has('nationality') ? 'has-error' : ''}}">
                    <label  class="col-md-5 required-star"> Nationality</label>
                    <div class="col-md-7">
                        {!! Form::select('nationality', $nationalities, '', $attributes = array('class'=>'form-control required',  'data-rule-maxlength'=>'40',
                        'placeholder' => 'Select One', 'id'=>"nationality")) !!}
                        {!! $errors->first('nationality','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                @if($logged_user_type == '1x101') {{-- For System Admin --}}
                <div class="form-group hidden" id="userDesk">
                    <label  class="col-md-5 required-star text-left"> User's Desk </label>
                    <div class="col-md-7">
                        {!! Form::select('desk_id', $user_desk,'', $attributes = array('class'=>'form-control','data-rule-maxlength'=>'40',
                        'placeholder' => 'Select One','id'=>"desk_id")) !!}
                        {!! $errors->first('desk_id','<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                <div class="form-group hidden" id="zone">
                    <label  class="col-md-5 required-star"> User's Zone </label>
                    <div class="col-md-7">
                        {!! Form::select('eco_zone_id', $economicZone, null, $attributes = array('class'=>'form-control','data-rule-maxlength'=>'40',
                        'placeholder' => 'Select One','id'=>"eco_zone_id")) !!}
                        {!! $errors->first('eco_zone_id','<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                @endif {{-- logged user is system admin --}}

                <div class="form-group has-feedback {{ $errors->has('division') ? 'has-error' : ''}}" id="division_div">
                    <label  class="col-md-5 required-star">Division </label>
                    <div class="col-md-7">
                        {!! Form::select('division', $divisions, '', $attributes = array('class'=>'form-control','data-rule-maxlength'=>'40',
                        'placeholder' => 'Select One', 'id'=>"division")) !!}
                        {!! $errors->first('division','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="form-group has-feedback hidden {{ $errors->has('state') ? 'has-error' : ''}}" id="state_div">
                    <label  class="col-md-5 required-star"> State </label>
                    <div class="col-md-7">
                        {!! Form::text('state', '', $attributes = array('class'=>'form-control', 'placeholder' => 'Name of your state / division',
                        'data-rule-maxlength'=>'40', 'id'=>"state")) !!}
                        {!! $errors->first('state','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="form-group has-feedback {{ $errors->has('district') ? 'has-error' : ''}}" id="district_div">
                    <label  class="col-md-5 required-star"> District </label>
                    <div class="col-md-7">
                        {!! Form::select('district', $districts, '', $attributes = array('class'=>'form-control', 'placeholder' => 'Select Division First',
                        'data-rule-maxlength'=>'40','id'=>"district")) !!}
                        {!! $errors->first('district','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="form-group has-feedback hidden {{ $errors->has('province') ? 'has-error' : ''}}" id="province_div">
                    <label  class="col-md-5 required-star"> Province </label>
                    <div class="col-md-7">
                        {!! Form::text('province', '', $attributes = array('class'=>'form-control', 'data-rule-maxlength'=>'40',
                        'placeholder' => 'Enter your Province', 'id'=>"province")) !!}
                        {!! $errors->first('province','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="form-group has-feedback {{ $errors->has('road_no') ? 'has-error' : ''}}">
                    <label  class="col-md-5 required-star"> Address Line 1 </label>
                    <div class="col-md-7">
                        {!! Form::text('road_no', '', $attributes = array('class'=>'form-control bnEng required', 'data-rule-maxlength'=>'100',
                        'placeholder' => 'Enter Road / Street Name / No.', 'id'=>"road_no")) !!}
                        {!! $errors->first('road_no','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="form-group has-feedback {{ $errors->has('house_no') ? 'has-error' : ''}}">
                    <label  class="col-md-5"> Address Line 2</label>
                    <div class="col-md-7">
                        {!! Form::text('house_no', '', $attributes = array('class'=>'form-control bnEng', 'data-rule-maxlength'=>'100',
                        'placeholder' => 'Enter House / Flat / Holding No.', 'id'=>"house_no")) !!}
                        {!! $errors->first('house_no','<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="form-group has-feedback {{ $errors->has('post_code') ? 'has-error' : ''}}">
                    <label  class="col-md-5 text-left"> Post Code </label>
                    <div class="col-md-7">
                        {!! Form::text('post_code', '', $attributes = array('class'=>'form-control', 'data-rule-maxlength'=>'40',
                        'placeholder' => 'Enter your Post Code ', 'id'=>"post_code")) !!}
                        {!! $errors->first('post_code','<span class="help-block">:message</span>') !!}
                    </div>
                </div>
            </div>

            <div class='clearfix'></div>
            <div class="form-group col-md-12">
                <div class="col-md-2">
                    <a href="{{url('users/lists')}}">
                        <button type="button" class="btn btn-block btn-default"><i class="fa fa-times"></i> <b>Close</b></button>
                    </a>
                </div>
                <div class="col-md-2 col-md-offset-6 pull-right">
                    @if(ACL::getAccsessRight('user','A'))
                    <button type="submit" class="btn btn-block btn-primary"><b>Submit</b></button>
                    @endif
                </div>
            </div>

            <div class="clearfix"></div>
        </div> <!--/panel-body-->
        {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
        {!! Form::close() !!}
    </div>
</div>

@endsection

@section('footer-script')

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(function () {
        var _token = $('input[name="_token"]').val();
        $("#create_user_form").validate({
            errorPlacement: function () {
                return false;
            }
        });
    });

    $(document).ready(function () {
        var today = new Date();
        var yyyy = today.getFullYear();
        $('.datepicker').datetimepicker({
            viewMode: 'years',
            format: 'DD-MMM-YYYY',
            maxDate: (new Date()),
            minDate: '01/01/' + (yyyy - 60)
        });
    });

    $(document).ready(function () {
        
        $("#country").change(function () {
            if (this.value == 'BD') { // iso = BD, 001 = country_code for Bangladesh
                $('#division_div').removeClass('hidden');
                $('#division').addClass('required');
                $('#district_div').removeClass('hidden');
                $('#district').addClass('required');
                $('#state_div').addClass('hidden');
                $('#state').removeClass('required');
                $('#province_div').addClass('hidden');
                $('#province').removeClass('required');
            }
            else {
                $('#state_div').removeClass('hidden');
                $('#state').addClass('required');
                $('#province_div').removeClass('hidden');
                $('#province').addClass('required');
                $('#division_div').addClass('hidden');
                $('#division').removeClass('required');
                $('#district_div').addClass('hidden');
                $('#district').removeClass('required');
            }
        });
        $('#country').trigger('change');
                
        $("#division").change(function () {
            var divisionId = $('#division').val();
            $(this).after('<span class="loading_data">Loading...</span>');
            var self = $(this);
            $.ajax({
                type: "GET",
                url: "<?php echo url(); ?>/users/get-district-by-division",
                data: {
                    divisionId: divisionId
                },
                success: function (response) {
                    var option = '<option value="">Select One</option>';
                    if (response.responseCode == 1) {
                        $.each(response.data, function (id, value) {
                            option += '<option value="' + id + '">' + value + '</option>';
                        });
                    }
                    $("#district").html(option);
                    $(self).next().hide();
                }
            });
        });

        $("#user_type").change(function () {
            var typeId = $('#user_type').val();
            $(this).after('<span class="loading_data">Loading...</span>');
            var self = $(this);
            $.ajax({
                type: "GET",
                url: "<?php echo url(); ?>/users/get-userdesk-by-type",
                data: {
                    typeId: typeId
                },
                success: function (response) {
                    var option = '<option value="">Select One</option>';
                    if (response.responseCode == 1) {
                        $.each(response.data, function (id, value) {
                            option += '<option value="' + id + '">' + value + '</option>';
                        });
                    }
                    $("#desk_id").html(option);
                    $(self).next().hide();
                }
            });
        });

        $('#user_type').change(function () {
            var type = $(this).val();
            if (type == '3x303') { // 3x303 is Security
                $('#userDesk').removeClass('hidden');
                $('#desk_id').addClass('required');
                $('#zone').removeClass('hidden');
                $('#zone_id').addClass('required');
            } else if (type == '4x404') { // 4x404 is Developers
                $('#userDesk').removeClass('hidden');
                $('#desk_id').addClass('required');
            } else if (type == '8x808') { // 8x808 is Zone MIS user
                $('#userDesk').addClass('hidden');
                $('#desk_id').removeClass('required');
                $('#zone').removeClass('hidden');
                $('#zone_id').addClass('required');
            } else if (type == '9x909') { // 9x909 is Customs user
                $('#userDesk').removeClass('hidden');
                $('#desk_id').addClass('required');
            } else {
                $('#userDesk').addClass('hidden');
                $('#desk_id').removeClass('required');
                $('#desk_id').prop('selectedIndex', 0);
            }
        });
        $('#user_type').trigger('change');

        $('#desk_id').change(function () {
            var desktype = $(this).val();
            if (desktype == '3' || desktype == '4' || desktype == '5' || desktype == '7' || desktype == '9') {
                // 3 = RD1, 4 =RD2, 5=RD3, 7 = customs officer, 9 = security
                $('#zone').removeClass('hidden');
                $('#zone_id').addClass('required');
            } else {
                $('#zone').addClass('hidden');
                $('#zone_id').removeClass('required');
                $('#zone_id').prop('selectedIndex', 0);
            }
        });

    }); // end of $(document).ready(function)
</script>
@endsection
