<?php

namespace App\Modules\Users\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\LoginController;
use App\Http\Requests\profileEditRequest;
use App\Libraries\ACL;
use App\Libraries\CommonFunction;
use App\Libraries\Encryption;
use App\Modules\Files\Controllers\FilesController;
use App\Modules\projectClearance\Models\ProjectClearance;
use App\Modules\Users\Models\AreaInfo;
use App\Modules\Users\Models\Countries;
use App\Modules\Users\Models\Delegation;
use App\Modules\Users\Models\EconomicZones;
use App\Modules\Users\Models\Notification;
use App\Modules\Users\Models\UserLogs;
use App\Modules\Users\Models\Users;
use App\Modules\Users\Models\UsersEditable;
use App\Modules\Users\Models\UserDesk;
use App\Modules\Users\Models\UserTypes;
use Carbon\Carbon;
use DB;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Validator;
use yajra\Datatables\Datatables;

class UsersController extends Controller {

    public function __construct() {
        if (Session::has('lang'))
            \App::setLocale(Session::get('lang'));
    }

    public function index() {
        //nothing is here
    }

    public function lists() {
        if (!CommonFunction::isAdmin()) {
            Session::flash('error', 'Permission Denied');
            return redirect('dashboard');
        }

        $logged_in_user_type = Auth::user()->user_type;
        return view('users::user_list', compact('logged_in_user_type'))
                        ->with('title', 'User List');
    }

    public function getRowDetailsData(Users $user) {
        $mode = ACL::getAccsessRight('user', 'V');
        $userList = $user->getUserList();
        return Datatables::of($userList)
                        ->addColumn('action', function ($userList) use ($mode) {
                            if ($mode) {
                                return '<a href="' . url('users/view/' . Encryption::encodeId($userList->id)) . '" class="btn btn-xs btn-primary open" >'
                                        . '<i class="fa fa-folder-open-o"></i> Open</a>';
                            } else
                                return '';
                        })
                        ->editColumn('user_first_login', '{!! CommonFunction::showDate($user_first_login) !!}')
                        ->editColumn('user_status', function ($userList) {
                            if ($userList->user_status == 'inactive') {
                                $activate = 'class="text-danger" ';
                            } else {
                                $activate = 'class="text-success" ';
                            }
                            return '<span ' . $activate . '><b>' . $userList->user_status . '</b></span>';
                        })
                        ->editColumn('type_name', function ($userList) {
                            if ($userList->desk_name != null && $userList->user_type == '4x404') {
                                return $userList->desk_name . ', ' . ucfirst($userList->type_name);
                            } else {
                                return $userList->type_name;
                            }
                        })
                        ->removeColumn('id', 'is_sub_admin')
                        ->make(true);
    }

    // acts as the function of registration
    public function create($va = null) {
        if ($va == 'va') {
            $user_types = UserTypes::orderBy('type_name')->where('id', '6x606')->lists('type_name', 'id'); // 6x606 is Visa Assistance
            $countries = Countries::orderby('name')
                    ->where('iso', '!=', 'BD') // 001 is country code for Bangladesh
                    ->lists('name', 'iso');
            $nationalities = Countries::orderby('nationality')->where('nationality', '!=', '')->where('iso', '!=', 'BD')->lists('nationality', 'iso');
        } else {
            $user_types = ['' => 'Select One'] + UserTypes::orderBy('type_name', 'ASC')->whereIn('id', array('5x505', '7x707', '8x808'))
                    ->lists('type_name', 'id')->all(); // 5x505 is Unit Investor, 7x707=Super MIS user, 8x808=Zone wise Super MIS user
            $countries = Countries::orderby('name')->lists('name', 'iso');
            $nationalities = Countries::orderby('nationality')->where('nationality', '!=', '')->lists('nationality', 'iso');
        }

        //$doc_config = CommonFunction::getImageConfig('DOC_IMAGE_SIZE');

        $economicZone = EconomicZones::select('id', DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                        ->orderBy('zone')->lists('zone', 'id');

        $user_desks = UserDesk::orderBy('desk_name')->where('is_registarable', 1)->lists('desk_name', 'desk_id');

        $divisions = AreaInfo::orderby('area_nm')->where('area_type', 1)->lists('area_nm', 'area_id');
        $districts = AreaInfo::orderby('area_nm')->where('area_type', 2)->lists('area_nm', 'area_id');

        return view("users::registration", compact("user_types", "nationalities", "countries", "divisions", "districts", "economicZone", "user_desks"));
    }

    // for users who will signed up themselves through registration
    public function store(Request $request) {
        $requestEmail = $request->get('user_email');
        $reqUserType = $request->get('user_type');
        $result = Users::where('user_email', '=', $requestEmail)->first();
        if ($result) {
            if ($result->user_verification == 'no') {
                \Session::flash('verifyNo', 'You have previously received a sign up request with this email address, which did not get verified yet.'
                        . ' If you did not get any link to verify your email address, please click the "Resend Email" button and '
                        . 'follow the given instructions to complete your sign up process.');
                $ecptEmail = Encryption::encodeId($requestEmail);
                if ($reqUserType == '6x606') { // 6x606 for visa assistance
                    return redirect('users/create/va?tmp=' . $ecptEmail);
                } else {
                    return redirect('users/create?tmp=' . $ecptEmail);
                }
            } else if ($result->user_verification == 'yes') {
                \Session::flash('verifyYes', 'Sorry, You have already registered with this email address. Did you forget your password?');
                return redirect('users/create');
            }
        }

        // Visa assistance users will need approval from system admin
        if ($reqUserType == '6x606') { // for Visa Assistance
            $approve_status = 1;
            $user_status = 'active';
        } else {
            $approve_status = 0;
            $user_status = 'inactive';
        }
        $this->validate($request, [
            'user_full_name' => 'required',
            'designation'=>'required',
            'user_DOB' => 'required|date',
            'user_phone' => 'required',
            'user_email' => 'required|email|unique:users',
            'country' => 'required',
            'nationality' => 'required',
            'road_no' => 'required',
            'authorization_file' => 'required|mimes:pdf|max:10000',
            'passport_nid_file' => 'required|mimes:pdf|max:10000',
            'g-recaptcha-response' => 'required'
        ]);

        $token_no = hash('SHA256', "-" . $request->get('user_email') . "-");
        $encrypted_token = Encryption::encodeId($token_no);
        $prefix = date('Y_');
        $_file = $request->file('authorization_file');
        $_file_pass_nid = $request->file('passport_nid_file');

        $zone = 0;
        if (!empty($request->get('eco_zone_id'))) {
            $zone = $request->get('eco_zone_id');
        }

        $data = [
            'user_full_name' => $request->get('user_full_name'),
            'designation'=>$request->get('designation'),
            'user_DOB' => CommonFunction::changeDateFormat($request->get('user_DOB'), true),
            'user_phone' => $request->get('user_phone'),
            'user_email' => $request->get('user_email'),
            'user_hash' => $encrypted_token,
            'user_type' => $request->get('user_type'),
            'eco_zone_id' => $zone,
            'desk_id' => $request->get('desk_id'),
            'country' => $request->get('country'),
            'nationality' => $request->get('nationality'),
            'identity_type' => $request->get('identity_type'),
            'passport_no' => $request->get('passport_no'),
            'user_nid' => $request->get('user_nid'),
            'division' => $request->get('division'),
            'district' => $request->get('district'),
            'state' => $request->get('state'),
            'province' => $request->get('province'),
            'road_no' => $request->get('road_no'),
            'house_no' => $request->get('house_no'),
            'post_code' => $request->get('post_code'),
            'user_fax' => $request->get('user_fax'),
            'is_approved' => $approve_status,
            'user_status' => $user_status,
            'user_agreement' => 0,
            'first_login' => 0,
            'user_verification' => 'no'
        ];

        if ($request->hasFile('authorization_file')) {
            $original_file = trim(sprintf("%s", uniqid($prefix, true))) . $_file->getClientOriginalName();
            $file_type = $_file->getClientMimeType();
            if ($file_type != 'application/pdf') {
                \Session::flash('error', 'Authorization letter file  must be in PDF format');
                return redirect('users/create');
            }
            $authoFileUrl = $original_file;
            $_file->move('uploads', $authoFileUrl);
            $data['authorization_file'] = $authoFileUrl;
        }
        if ($request->hasFile('passport_nid_file')) {
            $original_file_pass = trim(sprintf("%s", uniqid($prefix, true))) . $_file_pass_nid->getClientOriginalName();
            $file_type_pass = $_file_pass_nid->getClientMimeType();
            if ($file_type_pass != 'application/pdf') {
                \Session::flash('error', 'Passport or NID file must be in JPEG format');
                return redirect('/users/create');
            }
            $pasNidFileUrl = time() . $original_file_pass;
            $_file_pass_nid->move('users/passportnid', $pasNidFileUrl);
            $data['passport_nid_file'] = $pasNidFileUrl;
        }


//        if ($request->get('employ_job_ad')) {
//            $file = $request->file('employ_job_ad');
//            $size = $file->getSize();
//            $extension = $file->getClientOriginalExtension();
//            $valid_formats = array("pdf");
//            if (in_array($extension, $valid_formats)) {
//                if ($size < (1024 * 1024 * 3)) {
//                    $original_file = $file->getClientOriginalName();
//                    $file->move('uploads/', time() . $original_file);
//                    $fileName = 'uploads/' . time() . $original_file;
//
//                    $workPermit->employ_job_ad = $fileName;
//                } else {
//                    Session::flash('error_message', "File size must be less than 3 megabyte");
//                    return redirect()->back();
//                }
//            } else {
//                Session::flash('error_message', "File format is not valid! Please upload an pdf format");
//                return redirect()->back();
//            }
//        }
        Users::create($data);

        $email = $request->get('user_email');
        $verify_link = 'users/verification/' . ($encrypted_token);

        $body_msg = "Bangladesh Economic Zones Authority (BEZA) thanks you for requesting to open an account in our system.<br/>
                              Click the following link to verify your email account.
                            <br/> <a href='" . url($verify_link) . "'>Verify the e-mail address you have provided earlier</a>";

        $email_data = array(
            'header' => 'Please verify your email address',
            'param' => $body_msg
        );

        \Mail::send('users::message', $email_data, function ($message) use ($email) {
            $message->from('no-reply@beza.gov.bd', 'Bangladesh Economic Zones Authority (BEZA) System')
                    ->to($email)
                    ->subject('Verify the e-mail address you have provided earlier');
        });

        \Session::flash('success', 'Thanks for signing up! Please check your email and follow the instruction to complete the sign up process');
        $ecptEmail = Encryption::encodeId($email);
        return redirect('users/create?tmp=' . $ecptEmail);
    }

    public function resendMailByAdmin($userEmail, Request $request) {
        try {
            $email = Encryption::decode($userEmail);
            $result = Users::where('user_email', '=', $email)->first();

            $ACTIVE_STATUS = $result->user_verification;
            $encrypted_token = Encryption::encode($result->user_hash);
            $verify_link = 'users/verification/' . ($encrypted_token);

            if ($result->is_approved == 1) {
                $verify_link = 'users/verify-created-user/' . ($encrypted_token);
            }

            if ($ACTIVE_STATUS == 'no') {
                Users::where('id', $result->id)->update(['user_hash_expire_time' => Carbon::now()->addHour(1)]);

                $body_msg = "Bangladesh Economic Zones Authority (BEZA) thanks you for showing interest in this system .
                                            <br/>Click the following link to confirm your e-mail account.
                                            <br/><br/><a href='" . url($verify_link) . "'>Click here to confirm  your provided e-mail address</a>";

                $data = array(
                    'header' => 'Verify your email address',
                    'param' => $body_msg
                );

                \Mail::send('users::message', $data, function ($message) use ($email) {
                    $message->from($this->email_sender_add, 'Registration system')
                            ->to($email)
                            ->subject('Email account verification for Bangladesh Economic Zones Authority (BEZA)');
                });

                \Session::flash("success", "Email has been sent successfully...");
            }
            return redirect::back();
        } catch (\Exception $e) {
            Session::flash('error', 'Sorry! Something went wrong. Please try again later.');
            return redirect::back()->withInput();
        }
    }

    public function resendMail() {
        try {
            $email = Encryption::decodeId(Input::get('tmp'));
            $userInfo = DB::table('users')
                            ->where('user_email', '=', $email)->first();

            $ACTIVE_STATUS = $userInfo->user_status;
            $user_type = $userInfo->user_type;
            $encrypted_token = $userInfo->user_hash;

            $currentTime = new Carbon;
            Users::where('user_email', $email)->update([
                'created_at' => $currentTime
            ]); // Setting user creation time again so that verification link does not get expired after resending email

            $verify_link = 'users/verification/' . ($encrypted_token);

            // As Visa Assistance Users will not need approval, they will be always in active state
            if ($ACTIVE_STATUS == 'inactive' || ($user_type == '6x606' && $ACTIVE_STATUS == 'active')) { // 6x606 is Visa Assistance Users
                $body_msg = "Bangladesh Economic Zones Authority (BEZA) thanks you for requesting to open an account in our system. <br/>
                              Kindly click the following link to confirm your e-mail account.
                            <br/> <a href='" . url($verify_link) . "'>Verify the e-mail address you have provided earlier</a>";

                $data = array(
                    'header' => 'Please verify your email address',
                    'param' => $body_msg
                );

                \Mail::send('users::message', $data, function ($message) use ($email) {
                    $message->from('no-reply@beza.gov.bd', 'Bangladesh Economic Zones Authority (BEZA) System')
                            ->to($email)
                            ->subject('Bangladesh Economic Zones Authority (BEZA) Email account verification system ( resend )');
                });

                \Session::flash("success", "An email has been re-sent to your address.<br/>
                            Please check the newest email and follow the instructions to complete the sign up process.<br/>
                            Thank you!<br/>");
            } elseif ($ACTIVE_STATUS == 'active' && $user_type != '6x606') {

                $body_msg = "Your account has already been activated . For more information, please check the e-mail .<br/>
                    Thanks, <br/> Bangladesh Economic Zones Authority (BEZA) System Authority";

                $data = array(
                    'header' => 'Account Activation Information',
                    'param' => $body_msg
                );

                \Mail::send('users::message', $data, function ($message) use ($email) {
                    $message->from('no-reply@beza.gov.bd', 'Bangladesh Economic Zones Authority (BEZA) System')
                            ->to($email)
                            ->subject('Bangladesh Economic Zones Authority (BEZA) Account activation system');
                });

                \Session::flash('success', 'Please check your email for new update!');
            } else {
                \Session::flash('error', 'For technical reasons we are unable to resend this particular email. Please contact with system administration!');
            }
            $ecptEmail = Encryption::encodeId($email);
            return redirect('users/create?tmp=' . $ecptEmail);
        } catch (\Exception $e) {
            Session::flash('error', 'Sorry! Something went wrong. Please try again later.');
            return redirect::back()->withInput();
        }
    }

    public function edit($id, Users $usersModel) {
        if (!ACL::getAccsessRight('user', 'E')) {
            die('You have no access right! Please contact with system admin for more information');
        }
        $user_id = Encryption::decodeId($id);
        $users = $usersModel->getUserRow($user_id);
        $logged_in_user_type = CommonFunction::getUserType();

        $user_type_part = explode('x', $logged_in_user_type);
        $edit_user_type = UserTypes::where('id', $users->user_type)->pluck('type_name');
        $user_types = [$users->user_type => $edit_user_type] + UserTypes::where('id', 'LIKE', "$user_type_part[0]x" . substr($user_type_part[1], 0, 2) . "_")
                        ->where('id', 'NOT LIKE', "$user_type_part[0]_" . substr($user_type_part[1], 0, 2) . "0")
                        ->where('id', '!=', "1x101")
                        ->orderBy('type_name')->lists('type_name', 'id')
                        ->all();

        $economicZone = EconomicZones::select('id', DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                        ->orderBy('zone')->lists('zone', 'id');
        $user_desks = UserDesk::orderBy('desk_name')->lists('desk_name', 'desk_id');

        $nationalities = Countries::orderby('nationality')
                ->where('nationality', '!=', '')
                ->lists('nationality', 'iso');

        $countries = Countries::orderby('nicename')->lists('nicename', 'iso');
        $divisions = AreaInfo::orderby('area_nm')->where('area_type', 1)->lists('area_nm', 'area_id');
        $districts = AreaInfo::orderby('area_nm')->where('area_type', 2)->lists('area_nm', 'area_id');

        return view('users::edit', compact("users", "user_types", "user_desks", 'logged_in_user_type', 'countries', 'divisions', 'districts', 'nationalities', 'bank_name', "economicZone"));
    }

    public function update($id, Request $request) {
        if (!ACL::getAccsessRight('user', 'E')) {
            die('You have no access right! Please contact with system admin for more information');
        }

        $this->validate($request, [
            'user_full_name' => 'required',
            'user_phone' => 'required',
            'country' => 'required',
            'nationality' => 'required',
//            'passport_no' => 'required',
            'road_no' => 'required',
        ]);

        $user_id = Encryption::decodeId($id);
        if (!empty($request->get('eco_zone_id'))) {
            $zone = $request->get('eco_zone_id');
        } else {
            $zone = 0;
        }

        UsersEditable::find($user_id)->update([
            'user_full_name' => $request->get('user_full_name'),
            'user_type' => $request->get('user_type'),
            'desk_id' => $request->get('desk_id'),
            'country' => $request->get('country'),
            'nationality' => $request->get('nationality'),
            'passport_no' => $request->get('passport_no'),
            'division' => $request->get('division'),
            'district' => $request->get('district'),
            'state' => $request->get('state'),
            'province' => $request->get('province'),
            'road_no' => $request->get('road_no'),
            'eco_zone_id' => $zone,
            'house_no' => $request->get('house_no'),
            'post_code' => $request->get('post_code'),
            'user_fax' => $request->get('user_fax'),
            'user_phone' => $request->get('user_phone'),
            'updated_by' => CommonFunction::getUserId(),
        ]);

        \Session::flash('success', "User's Profile has been updated Successfully!");
        return redirect('users/edit/' . $id);
    }

    public function setDesk($id, Request $request) {
        if (!ACL::getAccsessRight('user', 'E')) {
            die('You have no access right! Please contact with system admin for more information');
        }
        $this->validate($request, [
            'desk_id' => 'required',
        ]);

        $user_id = Encryption::decodeId($id);

        UsersEditable::find($user_id)->update([
            'desk_id' => $request->get('desk_id'),
            'updated_by' => CommonFunction::getUserId(),
        ]);

//      for approving user
        $user = Users::find($user_id);
        $user->user_status = 'active';
        $user->is_approved = 1;
        if ($request->get('user_type')) {
            $user->user_type = $request->get('user_type');
        }
        $user->save();
        \Session::flash('success', "User's Profile has been approved Successfully!");

        $email = Users::where('id', $user_id)->pluck('user_email');

        $body_msg = "Your account has been successfully approved.<br/> Please strictly follow the terms of usage.
                            <br/><br/>Thanks. <br/>Bangladesh Economic Zones Authority (BEZA) System Authority";

        $data = array(
            'header' => 'BEZA Account Verification',
            'param' => $body_msg
        );

        \Mail::send('users::message', $data, function ($message) use ($email) {
            $message->from('no-reply@beza.gov.bd', 'Bangladesh Economic Zones Authority (BEZA) System')
                    ->to($email)
                    ->subject('Bangladesh Economic Zones Authority (BEZA) System Account Approval');
        });

        \Session::flash('success', "Desk of this user has been successfully updated!");
        return redirect('users/view/' . $id);
    }

    public function activate($id) {
//        if(!ACL::getAccsessRight('user','E')) die ('no access right!');
        $user_id = Encryption::decodeId($id);

        $user_active_status = Users::where('id', $user_id)->pluck('user_status');

        if ($user_active_status == 'active') {
            Users::where('id', $user_id)->update(['user_status' => 'inactive']);
            LoginController::killUserSession($user_id);

            \Session::flash('error', "User's Profile has been deactivated Successfully!");
        } else {
            Users::where('id', $user_id)->update(['user_status' => 'active']);
            \Session::flash('success', "User's Profile has been activated Successfully!");

            $email = Users::where('id', $user_id)->pluck('user_email');

            $body_msg = "Your account has been successfully activated."
                    . "<br/><br/>Thanks. "
                    . "<br/>Bangladesh Economic Zones Authority (BEZA) System Authority";

            $data = array(
                'header' => 'Account Verification',
                'param' => $body_msg
            );
            \Mail::send('users::message', $data, function ($message) use ($email) {
                $message->from('no-reply@beza.gov.bd', 'Bangladesh Economic Zones Authority (BEZA) System')
                        ->to($email)
                        ->subject('Account activation of BEZA system');
            });
        }
        return redirect('users/lists/');
    }

    public function approveUser($id, Request $request) {
        if (!ACL::getAccsessRight('user', 'A')) {
            die('You have no access right! Please contact with system admin for more information');
        }
        $user_id = Encryption::decodeId($id);

        $user = Users::find($user_id);
        $user->user_status = 'active';
        $user->is_approved = 1;
        if ($request->get('user_type')) {
            $user->user_type = $request->get('user_type');
        }
        $user->save();
        \Session::flash('success', "User's Profile has been approved Successfully!");

        $email = Users::where('id', $user_id)->pluck('user_email');

        $body_msg = "Your account has been successfully approved .<br/> Please strictly follow the terms of usage .
                            <br/><br/>Thanks. <br/>Bangladesh Economic Zones Authority (BEZA) System Authority";

        $data = array(
            'header' => 'BEZA Account Verification',
            'param' => $body_msg
        );

        \Mail::send('users::message', $data, function ($message) use ($email) {
            $message->from('no-reply@beza.gov.bd', 'Bangladesh Economic Zones Authority (BEZA) System')
                    ->to($email)
                    ->subject('Bangladesh Economic Zones Authority (BEZA) System Account Approval');
        });
        return redirect('users/lists');
    }

    public function rejectUser($id) {
        $user_id = Encryption::decodeId($id);

        Users::where('id', $user_id)->update(['user_status' => 'rejected', 'is_approved' => 0]);
        \Session::flash('error', "User's Profile has been Rejected Successfully!");

        $email = Users::where('id', $user_id)->pluck('user_email');

        $body_msg = "We are sorry to inform you that your  account has been canceled due to some inevitable circumstances. 
                <br/> If have any query, please contact with the authority.
                <br/><br/><br/>Thanks.<br/>Bangladesh Economic Zones Authority (BEZA) ";

        $data = array(
            'header' => 'BEZA Account Cancellation',
            'param' => $body_msg
        );

        \Mail::send('users::message', $data, function ($message) use ($email) {
            $message->from('no-reply@beza.gov.bd', 'Bangladesh Economic Zones Authority (BEZA) System')
                    ->to($email)
                    ->subject('Bangladesh Economic Zones Authority (BEZA) In the context of the system account');
        });
        return redirect('users/lists');
    }

    public function reSendEmail() {
        return view('users::resend_email');
    }

    public function reSendEmailConfirm(Request $request) {
        $this->validate($request, [
            'user_email' => 'required|email',
        ]);
        $result = DB::table('users')
                        ->where('user_email', '=', Input::get('user_email'))->first();

        $ACTIVE_STATUS = $result->user_status;
        $user_type = $result->user_type;
        $email = $request->get('user_email');
        $encrypted_token = $result->user_hash;
        $verify_link = 'users/verification/' . ($encrypted_token);

        // As Visa Assistance Users will not need approval, they will be always in active state
        if ($ACTIVE_STATUS == 'inactive' || ($user_type == '6x606' && $ACTIVE_STATUS == 'active')) { // 6x606 is Visa Assistance Users
            $body_msg = "Bangladesh Economic Zones Authority (BEZA) thanks you for requesting to open an account in our system. <br/>
                              Kindly click the following link to confirm your e-mail account.
                            <br/> <a href='" . url($verify_link) . "'>Verify the e-mail address you have provided earlier</a>";

            $data = array(
                'header' => 'Please verify your email address',
                'param' => $body_msg
            );

            \Mail::send('users::message', $data, function ($message) use ($email) {
                $message->from('no-reply@beza.gov.bd', 'Bangladesh Economic Zones Authority (BEZA) System')
                        ->to($email)
                        ->subject('Bangladesh Economic Zones Authority (BEZA) Email account verification system ( resend )');
            });

            \Session::flash('success', 'An email has been re-send to your address.<br/>
                            Please check the newest email and follow the instructions to complete the sign up process.<br/>
                            Thank you!');
        } elseif ($ACTIVE_STATUS == 'active' && $user_type != '6x606') { // for visa assistance users
            $body_msg = "Your account has already been activated . For more information, please check the e-mail .<br/>
                    , <br/> Bangladesh Economic Zones Authority (BEZA) System Authority";

            $data = array(
                'header' => 'Account Activation Information',
                'param' => $body_msg
            );

            \Mail::send('users::message', $data, function ($message) use ($email) {
                $message->from('no-reply@beza.gov.bd', 'Bangladesh Economic Zones Authority (BEZA) System')
                        ->to($email)
                        ->subject('Bangladesh Economic Zones Authority (BEZA) Account activation system');
            });

            \Session::flash('success', 'Please check your email for new update!');
        }

        return redirect('login');
    }

    public function message() {
        return view('users::message');
    }

    public function view($id, Users $users) {
        $user_id = Encryption::decodeId($id);

        $profile_pic = CommonFunction::getPicture('user', $user_id);
        $auth_file = CommonFunction::getPicture('auth_file', $user_id);

        $economicZone = EconomicZones::select('id', DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                        ->orderBy('zone')->lists('zone', 'id');


        $user = $users->getUserRow($user_id);

        $user_type_part = explode('x', $user->user_type);
        if (count($user_type_part) > 1) {
            $user_types = UserTypes::where('id', 'LIKE', "$user_type_part[0]_" . substr($user_type_part[1], 0, 2) . "_")
                    ->where('id', 'NOT LIKE', "$user_type_part[0]_" . substr($user_type_part[1], 0, 2) . "0")
                    ->orderBy('type_name')
                    ->lists('type_name', 'id');

            $user_desks = UserDesk::orderBy('desk_name')->lists('desk_name', 'desk_id');
            return view('users::view-printable', compact("user", "economicZone", "user_types", "profile_pic", "auth_file", "user_desks"));
        } else {
            Session::flash('error', 'User Type not defined.');
            return redirect('users/lists');
        }
    }

    public function verification($confirmationCode) {
        $user = Users::where('user_hash', $confirmationCode)->first();
        if (!$user) {
            \Session::flash('error', 'Invalid Token! Please resend email verification link.');
            return redirect('login');
        }
        $currentTime = new Carbon;
        $validateTime = new Carbon($user->created_at . '+6 hours');
        if ($currentTime >= $validateTime) {
            Session::flash('error', 'Verification link is expired (validity period 6 hrs). Please sign up again!');
            return redirect('/login');
        }

        $user_type = $user->user_type;
        $districts = ['' => 'Select one'] + AreaInfo::where('area_type', 2)->orderBy('area_nm', 'ASC')->lists('area_nm', 'area_id')->all();

        if ($user->user_verification != 'yes') {
            $districts = ['' => 'Select one'] + AreaInfo::where('area_type', 2)->orderBy('area_nm', 'asc')->lists('area_nm', 'area_id')->all();
            return view('users::verification', compact('user_type', 'confirmationCode', 'districts'));
        } else {
            \Session::flash('error', 'Invalid Token! Please sign up again.');
            return redirect('users/reSendEmail');
        }
    }

    //When completing registration, to get thana after selecting district
    public function get_thana_by_district_id(Request $request) {
        $district_id = $request->get('districtId');

        $thanas = AreaInfo::where('PARE_ID', $district_id)->orderBy('AREA_NM', 'ASC')->lists('AREA_NM', 'AREA_ID');
        $data = ['responseCode' => 1, 'data' => $thanas];
        return response()->json($data);
    }

    public function getDistrictByDivision(Request $request) {
        $division_id = $request->get('divisionId');

        $districts = AreaInfo::where('PARE_ID', $division_id)->orderBy('AREA_NM', 'ASC')->lists('AREA_NM', 'AREA_ID');
        $data = ['responseCode' => 1, 'data' => $districts];
        return response()->json($data);
    }

    function verification_store($confirmationCode, Request $request, Users $usersmodel, FilesController $files) {
        $TOKEN_NO = $confirmationCode;
        $user = Users::where('user_hash', $TOKEN_NO)->first();
        $email = $user->user_email;
        $user_password = str_random(10);

        if (!$user) {
            \Session::flash('error', 'Invalid token! Please sign up again to complete the process');
            return redirect('create');
        }

        $this->validate($request, [
            'user_agreement' => 'required',
        ]);

        $data = array(
            'details' => $request->get('details'),
            'user_agreement' => $request->get('user_agreement'),
            'password' => Hash::make($user_password),
            'user_verification' => 'yes',
            'user_first_login' => Carbon::now()
        );

        $usersmodel->chekced_verified($TOKEN_NO, $data);

        $body_msg = "Your account password :<strong><code>" . $user_password . '</code></strong>';
        $body_msg .= "<br/>This is a sectret password generated by the system."
                . "But to ensure your own security and convenience, you should change the password after logging in.";
        $body_msg .= "<br/><br/><br/>Thanks, <br/> Bangladesh Economic Zones Authority (BEZA) System Authority";

        $email_data = array(
            'header' => 'Account Access Information',
            'param' => $body_msg
        );
        \Mail::send('users::message', $email_data, function ($message) use ($email) {
            $message->from('no-reply@beza.gov.bd', 'Bangladesh Economic Zones Authority (BEZA) System')
                    ->to($email)
                    ->subject('Bangladesh Economic Zones Authority (BEZA) Registration Details');
        });

        // transfer files_tmp to img_pilgrim
//        $files->transferTmpFile(array( 'type' => 'auth_file_tmp', 'ref_id' => $user->id));

        \Session::flash('success', 'Thanks for signing up! Please check your email for the  account activation message.');
        return redirect('login');
    }

    public function profileinfo(FilesController $files) {

        if(empty(Session::get('examVal')) && empty(Session::get('typeVal')) && empty(Session::get('yearVal'))){
            //return redirect('/dashboard');
        }

        $users = Users::find(Auth::user()->id);

        $profile_pic = CommonFunction::getPicture('user', Auth::user()->id);
        $auth_file = CommonFunction::getPicture('auth_file', Auth::user()->id);

        $image_config = CommonFunction::getImageConfig('IMAGE_SIZE');
        $doc_config = CommonFunction::getImageConfig('DOC_IMAGE_SIZE');

        $user_type_info = UserTypes::where('id', $users->user_type)->first();
        $user_type_name = $user_type_info->type_name;


        //user specialization or additional info like bank name, agency name
        $additional_info = Users::getUserSpecialFields($users);

        $id = Encryption::encodeId(Auth::user()->id);

        return view('users::profileinfo', compact('id','users', 'user_type_name', 'additional_info', 'profile_pic', 'auth_file', 'image_config', 'doc_config', 'user_type_info'));
    }

    public function profile_update($id, profileEditRequest $request) {
        $auth_token_allow = 0;
        if ($request->get('auth_token_allow') == '1') {
            $auth_token_allow = 1;
        }

        $session_user_id = Encryption::decodeId($id);

        $data = [
            'user_full_name' => $request->get('user_full_name'),
            'designation'=>$request->get('designation'),
            'auth_token_allow' => $auth_token_allow,
            'user_DOB' => Carbon::createFromFormat('d-M-Y', $request->get('user_DOB'))->format('Y-m-d'),
            'user_phone' => $request->get('user_phone'),
            'user_nid' => $request->get('user_nid'),
            'passport_no' => $request->get('passport_no'),
            'updated_by' => CommonFunction::getUserId()
        ];

        $prefix = date('Y_');
        $_file = $request->file('authorization_file');
        $_ifile = $request->file('signature');
        $_imfile = $request->file('image');
//        $_sfile = $request->file('passport_nid_file');

        if ($request->hasFile('authorization_file')) {
            $original_file = trim(sprintf("%s", uniqid($prefix, true))) . $_file->getClientOriginalName();
            $file_type = $_file->getClientMimeType();

            if ($file_type != 'application/pdf') {
                \Session::flash('error', 'Authorization letter file type must be PDF');
                return redirect('users/profileinfo');
            }
            $_file->move('uploads', $original_file);
            $data['authorization_file'] = $original_file;
        }

        /* if ($request->hasFile('passport_nid_file')) {
          $scan_file = $_sfile->getClientOriginalName();
          $scan_file_type = $_sfile->getClientMimeType();
          if ($scan_file_type == 'application/pdf') {
          $_sfile->move('users/passportnid', $scan_file);
          $data['passport_nid_file'] = $scan_file;
          }
          else{
          \Session::flash('error', 'Passport / NID type must be pdf format');
          return redirect('users/profileinfo');
          }
          } */


        if ($request->hasFile('signature')) {

            $s_file = trim(sprintf("%s", uniqid($prefix, true))) . $_ifile->getClientOriginalName();

            $mime_type = $_ifile->getClientMimeType();
            if ($mime_type == 'image/jpeg' || $mime_type == 'image/jpg' || $mime_type == 'image/png') {
                $_ifile->move('users/signature', $s_file);
                $data['signature'] = $s_file;
            } else {
                \Session::flash('error', 'Signature type must be png or jpg or jpeg format');
                return redirect('users/profileinfo');
            }
        }

        if ($request->hasFile('image')) {
            $img_file = trim(sprintf("%s", uniqid($prefix, true))) . $_imfile->getClientOriginalName();
            $mime_type = $_imfile->getClientMimeType();
            if ($mime_type == 'image/jpeg' || $mime_type == 'image/jpg' || $mime_type == 'image/png') {
                $_imfile->move('users/upload', $img_file);
                $data['user_pic'] = $img_file;
            } else {
                \Session::flash('error', 'Profile Picture type must be png or jpg or jpeg format');
                return redirect('users/profileinfo');
            }
        }

        UsersEditable::find($session_user_id)->update($data);
        Users::find($session_user_id)->first();
        \Session::flash('success', 'Your profile has been updated successfully.');
        return redirect('users/profileinfo');
    }

    // Reset password from user list by Admin
    public function resetPassword($id) {
        if (!ACL::getAccsessRight('user', 'R'))
            die('You have no access right! Please contact with system admin for more information');
        $user_id = Encryption::decodeId($id);
        $password = str_random(10);

        $user_active_status = DB::table('users')->where('id', $user_id)->pluck('user_status');
        $email_address = DB::table('users')->where('id', $user_id)->pluck('user_email');

        if ($user_active_status == 'active') {
            Users::where('id', $user_id)->update([
                'password' => Hash::make($password)
            ]);

            $body_msg = '<span style="color:#000;text-align:justify;"><b>';
            $body_msg .= 'Your password has been changed successfully by the system administrator.';
            $body_msg .= '<br/>New Password : <code>' . $password . '</code>';
            $body_msg .= '</span><br/><br/><br/>';
            $body_msg .= 'This is a sectret password generated by the system.'
                    . 'But to ensure your own security and convenience, you should change the password after logging in.';
            $body_msg .= '<br/><br/><br/>Thanks,<br/>';
            $body_msg .= '<b>Bangladesh Economic Zones Authority (BEZA) System Authority</b>';


            $data = array(
                'header' => 'Password Reset Information',
                'param' => $body_msg
            );

            \Mail::send('users::message', $data, function ($message) use ($email_address) {
                $message->from('no-reply@beza.gov.bd', 'Bangladesh Economic Zones Authority (BEZA) System')
                        ->to($email_address)
                        ->subject('Password reset related information from BEZA system');
            });
            \Session::flash('success', "User's Password has been reset Successfully! An email has been sent to the user!");
        } else {
            \Session::flash('error', "User profile has not been activated yet! Password can not be changed");
        }
        return redirect('users/lists');
    }

    public function updatePassFromProfile($id, Request $request) {
        $user_id = Encryption::decodeId($id);

        $dataRule = [
            'user_old_password' => 'required',
            'user_new_password' => [
                'required',
                'min:6',
                'regex:/^(?=.*[A-Za-z])(?=.*\d)[a-zA-Z0-9]{6,}$/'
            ],
            'user_confirm_password' => [
                'required',
                'same:user_new_password',
            ]
        ];

        $validator = Validator::make($request->all(), $dataRule);

        if ($validator->fails()) {
            /* $this->throwValidationException(
              $request, $validator
              ); */
            return redirect('users/profileinfo#tab_2')->withErrors($validator)->withInput();
        }


        $old_password = $request->get('user_old_password');
        $new_password = $request->get('user_new_password');
        $password_match = Users::where('id', $user_id)->pluck('password');
        $password_chk = Hash::check($old_password, $password_match);

        if ($password_chk == true) {
            if ($old_password == $new_password) {
                \Session::flash('error', 'Current password should not be set for new password');
                return Redirect('users/profileinfo#tab_2')->with('status', 'error');
            }

            Users::where('id', $user_id)
                    ->update(array('password' => Hash::make($new_password), 'first_login' => 1));

            Auth::logout();
            $loginObj = new \App\Http\Controllers\LoginController();
            $loginObj->entryAccessLogout();

            \Session::flash('success', 'Your Password has been changed Successfully! Please login with the new password.');
            return redirect('login');
        } else {
            \Session::flash('error', 'Old password do not match');
            return Redirect('users/profileinfo#tab_2')->with('status', 'error');
        }
    }

    public function forgetPassword() {
        return view('users::forgetPassword');
    }

    //For Forget Password functionality
    public function resetForgottenPass(Request $request) {
        $this->validate($request, [
            'g-recaptcha-response' => 'required',
        ]);
        $email = $request->get('user_email');
        $users = DB::table('users')
                ->where('user_email', $email)
                ->first();

        if (!empty($users)) {

            $token_no = hash('SHA256', "-" . $email . "-");
            $update_token_in_db = array(
                'user_hash' => $token_no,
            );
            DB::table('users')
                    ->where('user_email', $email)
                    ->update($update_token_in_db);

            $encrytped_token = Encryption::encode($token_no);
            $verify_link = 'users/verify-forgotten-pass/' . ($encrytped_token);

            $body_msg = "Someone recently asked for recoving password for your account through the  system of 
                                        Bangladesh Economic Zones Authority (BEZA) .
                                        If you request this, you can reset the password by clicking on the link below .
                                        <br/><br/> <a href='" . url($verify_link) . "'>Password Reset</a>";
            $body_msg .= "<br/><br/>If you did not request this , you do not have to take any action .
                                          This link will be expired within today .
                                            <br/><br/>To protect your account , please do not forward this email to anyone .
                                        If you need any assistance , please contact your system admin .
                                         <br/><br/>Thanks,<br/>Authority of Bangladesh Economic Zones Authority (BEZA) System";

            $data = array(
                'header' => 'Password recovery related information from Bangladesh Economic Zones Authority (BEZA) System',
                'param' => $body_msg
            );

            \Mail::send('users::message', $data, function ($message) use ($email) {
                $message->from('no-reply@beza.gov.bd', 'Bangladesh Economic Zones Authority (BEZA) System')
                        ->to($email)
                        ->subject('Forgotten Password Recovery');
            });

            \Session::flash('success', 'Please check your email to verify Password Change');
            return redirect('login');
        } else {
            \Session::flash('error', 'No user with this email is existed in our current database. Please sign-up first');
            return Redirect('forget-password')->with('status', 'error');
        }
    }

    // Forgotten Password reset after verification
    function verifyForgottenPass($token_no) {
        $TOKEN_NO = Encryption::decode($token_no);

        $user = Users::where('user_hash', $TOKEN_NO)->first();

        if ($user) {
            $fetched_email_address = $user->user_email;

            $user_password = str_random(10);

            DB::table('users')
                    ->where('user_hash', $TOKEN_NO)
                    ->update(array('password' => Hash::make($user_password)));

            $body_msg = "Your new password :<strong><code>" . $user_password . '</code></strong>';
            $body_msg .= "This is a sectret password generated by the system.'
                    . 'But to ensure your own security and convenience, you should change the password after logging in.
                     <br/><br/><br/>Thanks, <br/> Bangladesh Economic Zones Authority (BEZA) System Authority";

            $data = array(
                'header' => 'Your New Password Details',
                'param' => $body_msg
            );

            \Mail::send('users::message', $data, function ($message) use ($fetched_email_address) {
                $message->from('no-reply@beza.gov.bd', 'Bangladesh Economic Zones Authority (BEZA) System')
                        ->to($fetched_email_address)
                        ->subject('Bangladesh Economic Zones Authority (BEZA) Change the system password');
            });


            \Session::flash('success', 'Your password has been reset successfully! Please check your mail for access information.');
            return redirect('login');
        } else { /* If User couldn't be found */
            \Session::flash('error', 'Invalid token! No such user is found. Please sign up first.');
            return redirect('users/create');
        }
    }

    // for adding new users from Authentic Admin's end
    public function createNewUser() {
        if (!ACL::getAccsessRight('user', 'A')) {
            die('You have no access right! Please contact with system admin for more information');
        }
        $logged_user_type = Auth::user()->user_type;
        $user_type_part = explode('x', $logged_user_type);

        if ($logged_user_type == '1x101') { // 1x101 is Sys Admin
            $user_types = UserTypes::orderBy('type_name')->where('is_registarable', '!=', '-1')->lists('type_name', 'id');
        } else {
            $user_types = UserTypes::where('id', 'LIKE', "$user_type_part[0]x" . substr($user_type_part[1], 0, 2) . "_")
                            ->where('id', 'NOT LIKE', "$user_type_part[0]_" . substr($user_type_part[1], 0, 2) . "0")
                            ->orderBy('type_name')->lists('type_name', 'id');
        }

        $economicZone = EconomicZones::select('id', DB::raw('CONCAT(name, ", ", upazilla, ", ", district) AS zone'))
                        ->orderBy('zone')->lists('zone', 'id');
        $user_desk = UserDesk::orderBy('desk_name')->lists('desk_name', 'desk_id');

        $nationalities = Countries::orderby('nationality')->where('nationality', '!=', '')->lists('nationality', 'iso');
        $countries = Countries::orderby('nicename')->lists('nicename', 'iso');
        
        $divisions = AreaInfo::orderby('area_nm')->where('area_type', 1)->lists('area_nm', 'area_id');
        $districts = AreaInfo::orderby('area_nm')->where('area_type', 2)->lists('area_nm', 'area_id');

        return view("users::new-user", compact("user_types", "user_desk", "logged_user_type", "districts", "divisions", "countries", 'nationalities', "economicZone"));
    }

    // User creation by admin
    public function storeNewUser(Request $request) {
        if (!ACL::getAccsessRight('user', 'A')) {
            die('You have no access right! Please contact with system admin for more information');
        }
        $this->validate($request, [
            'user_full_name' => 'required',
            'user_DOB' => 'required|date',
            'user_phone' => 'required',
            'user_email' => 'required|email|unique:users',
            'country' => 'required',
            'nationality' => 'required',
            'passport_no' => '',
            'road_no' => 'required',
        ]);

        if ($request->get('country') == 'BD') { // 001 is country code of Bangladesh
            $this->validate($request, [
                'division' => 'required',
                'district' => 'required'
            ]);
        } else {
            $this->validate($request, [
                'state' => 'required',
                'province' => 'required'
            ]);
        }

        $token_no = hash('SHA256', "-" . $request->get('user_email') . "-");
        $encrypted_token = Encryption::encodeId($token_no);

        if (Auth::user()->user_type == '1x101') {     //System admin
            $desk_id = $request->get('desk_id');
            $user_type = $request->get('user_type');
        } else {
            $desk_id = Auth::user()->desk_id;
            $user_type = Auth::user()->user_type;
        }

        $data = array(
            'user_full_name' => $request->get('user_full_name'),
            'user_DOB' => CommonFunction::changeDateFormat($request->get('user_DOB'), true),
            'user_phone' => $request->get('user_phone'),
            'user_email' => $request->get('user_email'),
            'user_hash' => $encrypted_token,
            'user_type' => $user_type,
            'eco_zone_id' => $request->get('eco_zone_id'),
            'desk_id' => $desk_id,
            'country' => $request->get('country'),
            'nationality' => $request->get('nationality'),
            'passport_no' => $request->get('passport_no'),
            'user_nid' => $request->get('user_nid'),
            'division' => $request->get('division'),
            'district' => $request->get('district'),
            'state' => $request->get('state'),
            'province' => $request->get('province'),
            'road_no' => $request->get('road_no'),
            'house_no' => $request->get('house_no'),
            'post_code' => $request->get('post_code'),
            'user_status' => 'active',
            'is_approved' => 1,
            'user_agreement' => 0,
            'first_login' => 0,
            'user_verification' => 'no',
            'user_hash_expire_time' => new Carbon('+6 hours')
        );
        Users::create($data);

        $email = $request->get('user_email');
        $verify_link = 'users/verify-created-user/' . ($encrypted_token);

        $body_msg = "Bangladesh Economic Zones Authority (BEZA) thanks you for requesting to open an account in our system.<br/>
                              Click the following link to confirm your e-mail account.
                            <br/> <a href='" . url($verify_link) . "'>Verify the e-mail address you have provided earlier</a>";

        $data = array(
            'header' => 'Verify your email address',
            'param' => $body_msg
        );

        \Mail::send('users::message', $data, function ($message) use ($email) {
            $message->from('no-reply@beza.gov.bd', 'Bangladesh Economic Zones Authority (BEZA) System')
                    ->to($email)
                    ->subject('Bangladesh Economic Zones Authority (BEZA) Registration details');
        });

        \Session::flash('success', 'User has been created successfully! An email has been sent to the user for account activation.');
        return redirect('users/create-new-user');
    }

    // Verifying new users created by admin
    public function verifyCreatedUser($encrypted_token) {
        $user = Users::where('user_hash', $encrypted_token)->first();
        if (!$user) {
            \Session::flash('error', 'Invalid Token. Please try again...');
            return redirect('login');
        }
        $currentTime = new Carbon;

        if ($currentTime >= $user->user_hash_expire_time) {
            Session::flash('error', 'Verifying link is expired (Validity period is 1 hours). Please sign-up again to continue.');
            return redirect('\login');
        }

        if ($user->user_verification == 'no') {
            return view('users::verify-created-user', compact('encrypted_token'));
        } else {
            \Session::flash('error', 'Invalid Token! Please sign-up again to continue');
            return redirect('users/reSendEmail');
        }
    }

    function createdUserVerification($encrypted_token, Request $request, Users $usersmodel) {
        $user = Users::where('user_hash', $encrypted_token)->first();
        $email = $user->user_email;
        $user_password = str_random(10);

        if (!$user) {
            \Session::flash('error', 'Invalid token! Please sign up again to complete the process');
            return redirect('create');
        }

        $this->validate($request, [
            'user_agreement' => 'required',
        ]);

        $data = array(
            'user_agreement' => $request->get('user_agreement'),
            'password' => Hash::make($user_password),
            'user_verification' => 'yes',
            'user_first_login' => Carbon::now()
        );

        $usersmodel->chekced_verified($encrypted_token, $data);

        $body_msg = "Your password is :<strong><code>" . $user_password . '</code></strong>';
        $body_msg .= "<br/>This is a sectret password generated by the system.
                                        But to ensure your own security and convenience, you should change the password after logging in.
                                        <br/><br/><br/>Thanks, <br/> Bangladesh Economic Zones Authority (BEZA) System Authority";

        $email_data = array(
            'header' => 'Account Access Information',
            'param' => $body_msg
        );
        \Mail::send('users::message', $email_data, function ($message) use ($email) {
            $message->from('no-reply@beza.gov.bd', 'Bangladesh Economic Zones Authority (BEZA) System')
                    ->to($email)
                    ->subject('Bangladesh Economic Zones Authority (BEZA) Registration details');
        });

        \Session::flash('success', 'An account activation message has been sent to your email address. Please check your email');
        return redirect('login');
    }

    public function support() {
        return view('users::helpdesk-contact');
    }

    public function helpdeskContact() {
        return view("users::helpdesk-contact");
    }

    public function getAccessLogData() {
        $user_logs = UserLogs::where('user_id', '=', Auth::user()->id)
                ->limit(10)
                ->orderBy('user_logs.id', 'desc')
                ->get(['user_id', 'ip_address', 'login_dt', 'logout_dt']);

        return Datatables::of($user_logs)
                        ->removeColumn('user_id')
                        ->make(true);
    }

    public function twoStep() {
        return view("users::two-step");
    }

    public function checkTwoStep(Request $request) {
        $steps = $request->get('steps');
        $code = rand(1000, 9999);
        $body_msg = "Security code for 2nd step login is: <strong><code>" . $code . "</code></strong>";

        $user_email = Auth::user()->user_email;
        $user_mobile = Auth::user()->user_phone;
        $token = $code . '-' . Auth::user()->id;
        $encrypted_token = Encryption::encode($token);
        UsersEditable::where('user_email', $user_email)->update(['auth_token' => $encrypted_token]);

        if ($steps == 'email') {
            $email_data = array(
                'header' => ' Information',
                'param' => $body_msg
            );
            \Mail::send('users::message', $email_data, function ($message) use ($user_email) {
                $message->from('no-reply@beza.gov.bd', 'Bangladesh Economic Zones Authority (BEZA) System')
                        ->to($user_email)
                        ->subject('Bangladesh Economic Zones Authority (BEZA) Registration details');
            });
        } else {
            $notification = new Notification();
            $notification->sendSecondStepSMS($code);
        }

        return view("users::check-two-step", compact('steps'));
    }

    public function verifyTwoStep(Request $request) {
        $this->validate($request, [
            'security_code' => 'required',
        ]);
        $security_code = trim($request->get('security_code'));
        $user_id = Auth::user()->id;
        $token = $security_code . '-' . $user_id;
        $encrypted_token = Encryption::encode($token);
        $count = Users::where('id', $user_id)->where(['auth_token' => $encrypted_token])->count();
        Users::where('id', $user_id)->update(['auth_token' => '']);
        if ($count > 0) {
            Session::flash('success', "Security match successfully, Welcome to Bangladesh Economic Zones Authority (BEZA) platform");
            return redirect('dashboard');
        } else {
//            Auth::logout();
            Session::flash('error', "Security Code doesn't match.");
            return redirect('users/two-step');
        }
    }

    /*
     * deligated process
     * sb deligate only dgi as
     * nsi delegate only nsi director
     * other desk delegate one another
     */

    public function delegate() {
        $delegate_to_user_id = Auth::user()->delegate_to_user_id;
        $info = Users::leftJoin('user_desk as ud', 'ud.desk_id', '=', 'users.desk_id')
                ->where('id', $delegate_to_user_id)
                ->first(['user_full_name', 'user_email', 'user_phone', 'ud.desk_name']);
        return view("dashboard::delegated", compact('info'));
    }

    public function getDeligatedUserInfo(Request $request) {
        $desk_id = $request->get('designation');
        $result = Users::where('desk_id', '=', $desk_id)->get(['user_full_name', 'id']);
        echo json_encode($result);
    }

    function processDeligation(Request $request) {
        $delegate_by_user_id = Auth::user()->id;
        $delegate_to_user_id = $request->get('delegated_user');

        $udata = array(
            'delegate_to_user_id' => $delegate_to_user_id,
            'delegate_by_user_id' => $delegate_by_user_id,
        );
        $complt = Users::where('id', $delegate_by_user_id)
                ->orWhere('delegate_to_user_id', $delegate_by_user_id)
                ->update($udata);

        $type = Auth::user()->user_type;
        $user_type = explode('x', $type)[0];

        if ($user_type != 1 || $user_type != 2) {
            Session::put('sess_delegated_user_id', $delegate_by_user_id);
        }
        if ($complt) {
            Delegation::create([
                'delegate_to_user_id' => $delegate_to_user_id,
                'delegate_to_user_id' => $delegate_to_user_id,
                'remarks' => $request->get('remarks'),
                'status' => 1,
            ]);
            return redirect()
                            ->intended('/users/delegate')
                            ->with('success', 'Delegation process completed Successfully');
        } else {
            Session::flash('error', 'Delegation Not completed');
            return redirect('users/profileinfo/#deligation');
        }
    }

    public function removeDeligation() {
        $sess_user_id = Auth::user()->id;

        //USER INFO DELATION REMOVE
        Users::where('id', $sess_user_id)
                ->update(['delegate_to_user_id' => 0, 'delegate_by_user_id' => 0]);

        Users::where('delegate_by_user_id', $sess_user_id)
                ->where('id', '!=', $sess_user_id)
                ->update(['delegate_to_user_id' => $sess_user_id, 'delegate_by_user_id' => $sess_user_id]);

        //DELEGATION HISTORY UPDATE
        Delegation::where('delegate_by_user_id', $sess_user_id)
                ->where('delegate_to_user_id', Auth::user()->delegated_to_user_id)
                ->orderBy('created_at')
                ->limit(1)
                ->update(['remarks' => '', 'status' => 0]);

        //REMOVE DELEGATION HISTORY ENTRY
        Delegation::where('delegate_by_user_id', $sess_user_id)
                ->where('delegate_to_user_id', Auth::user()->delegated_to_user_id)
                ->orderBy('created_at', 'DESC')->first();

        Session::flash('success', 'Remove Delegation Successfully');
        Session::forget('sess_delegated_user_id');
        return redirect("dashboard");
    }

    public function delegation($id) {
        $delegate_to_user_id = Encryption::decodeId($id);

        $info = Users::leftJoin('user_desk as ud', 'ud.desk_id', '=', 'users.desk_id')
                ->where('id', $delegate_to_user_id)
                ->first(['id', 'user_full_name', 'users.desk_id', 'user_email', 'user_phone', 'ud.desk_name']);

        $desk_id = $info->desk_id;

        if ($desk_id == 1) {
            $designation = UserDesk::whereIn('desk_id', [2, 8])->lists('desk_name', 'desk_id');
        } elseif ($desk_id == 3) {
            $designation = UserDesk::where('desk_id', 9)->lists('desk_name', 'desk_id');
        } elseif ($desk_id == 4) {
            $designation = UserDesk::where('desk_id', 10)->lists('desk_name', 'desk_id');
        } else {
            $designation = UserDesk::lists('desk_name', 'desk_id');
        }
        return view("users::delegation", compact('info', 'designation'));
    }

    public function getDeligatedUserInfos(Request $request) {
        $desk_id = $request->get('designation');
        $result = Users::where('desk_id', '=', $desk_id)->get(['user_full_name', 'id']);
        echo json_encode($result);
    }

    function storeDelegation(Request $request) {
        $delegate_by_user_id = Auth::user()->id;
        $delegate_to_user_id = $request->get('delegated_user');
        $delegate_from_user_id = $request->get('user_id');
        $data = [
            'delegate_by_user_id' => $delegate_by_user_id,
            'delegate_to_user_id' => $delegate_to_user_id,
            'remarks' => $request->get('remarks'),
            'status' => 1
        ];
        Delegation::create($data);

        $udata = array(
            'delegate_to_user_id' => $delegate_to_user_id,
            'delegate_by_user_id' => $delegate_by_user_id
        );

        $complt = Users::where('id', $delegate_from_user_id)
                ->orWhere('delegate_to_user_id', $delegate_from_user_id)
                ->update($udata);

        if ($complt) {
            Session::flash('success', 'Delegation process completed Successfully');
            return redirect("users/lists");
        } else {
            Session::flash('error', 'Delegation Not completed');
            return redirect("users/view/" . Encryption::encodeId($delegate_from_user_id));
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserSession(Request $request) {
        if (Auth::user()) {
            $checkSession = Users::where(['id' => Auth::user()->id, 'login_token' => Encryption::encode(Session::getId())])->count();
            if ($checkSession >= 1) {
                $data = ['responseCode' => 1, 'data' => 'matched'];
            } else {
                Auth::logout();
                $data = ['responseCode' => -1, 'data' => 'not matched'];
            }
        } else {
            Auth::logout();
            $data = ['responseCode' => -1, 'data' => 'closed'];
        }

        $LgController = new LoginController;
        if (!$LgController->_checkSecurityProfile($request)) {
            Auth::logout();
            Session::flash('error', 'Security Profile does not support login from this network or time!');
            $data = ['responseCode' => -1, 'data' => 'Security Profile does not matched'];
        }

        return response()->json($data);
    }
    
        public function getUserDeskByType(Request $request) {
        $typeId = $request->get('typeId');

        $desks = UserDesk::where('user_type', $typeId)->orderBy('desk_name', 'ASC')->lists('desk_name', 'desk_id');

        $data = ['responseCode' => 1, 'data' => $desks];
        return response()->json($data);
    }

    public function checkPassportNo(Request $request) {
        $passport_no = $request->get('passport_no');
        $if_existed_pass = Users::where('passport_no', $passport_no)->count();
        return $if_existed_pass;
    }

    public function checkUniqueEmail(Request $request) {
        $email = $request->get('email');
        $if_existed = Users::where('user_email', $email)->count();
        return $if_existed;
    }

    /*     * ******************* End of Users Controller Class ********************** */
}
