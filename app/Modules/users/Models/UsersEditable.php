<?php

namespace App\Modules\Users\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class UsersEditable extends Model {

    protected $table = 'users';
    protected $fillable = array(
        'user_type',
        'name',
        'username',
        'email',
        'user_phone',
        'user_status',
        'security_profile_id',
        'submited_status_fazil3',
        'submited_status_fazil3_private',
        'submited_status_fazil3_eff',
        'submited_status_fazil3_priv_eff',
        'submited_status_fazil4',
        'submited_status_fazil4_eff',
        'final_lock_fazil3_eff',
        'final_lock_fazil3_priv_eff',
        'password',
        'remember_token',
        'login_token',
        'is_approved',
        'first_login',
        'is_archieved',
        'created_by',
        'updated_by'
    );

    public static function boot() {
        parent::boot();
        // Before update
        static::creating(function($post) {
            if (Auth::guest()) {
                $post->created_by = 0;
                $post->updated_by = 0;
            } else {
                $post->created_by = Auth::user()->id;
                $post->updated_by = Auth::user()->id;
            }
        });

        static::updating(function($post) {
            if (Auth::guest()) {
                $post->updated_by = 0;
            } else {
                $post->updated_by = Auth::user()->id;
            }
        });
    }

    /*     * ***************************** Users Model Class ends here ************************* */
}
