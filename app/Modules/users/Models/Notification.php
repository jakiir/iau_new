<?php

namespace App\Modules\Users\Models;

use App\Libraries\CommonFunction;
use App\Libraries\ReportHelper;
use App\Modules\Settings\Models\Bank;
use App\Modules\Settings\Models\Configuration;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model {

    protected $table = 'notifications';
    protected $fillable = array(
         'source',
         'ref_id',
         'destination',
         'status',
         'is_sent',
         'msg_type',
         'template_id',
         'priority',
         'created_by',
         'updated_by'
    );

    public static function boot()
    {
        parent::boot();
        // Before update
        static::creating(function($post)
        {
            $post->created_by = CommonFunction::getUserId();
            $post->updated_by = CommonFunction::getUserId();
        });

        static::updating(function($post)
        {
            $post->updated_by = Auth::user()->id;
        });

    }

    /**
     * @param $pilgrim_id
     */
    public function sendRegSMS($pilgrim_id){
        $pilgrim = Pilgrim::find($pilgrim_id);
        if($pilgrim->is_govt=='Private'){
            $template = Template::where('caption','REG_SMS_P')->first()->toArray();
        }else{
            $template = Template::where('caption','REG_SMS_G')->first()->toArray();
        }
        $data = [
            'token_no' => $pilgrim->tracking_no,
            'pilgrim' => CommonFunction::getName4SMS($pilgrim->full_name_english),
            'birth_date' => CommonFunction::changeDateFormat($pilgrim->birth_date),
            'validity' => CommonFunction::changeDateFormat(Configuration::getLastDate($pilgrim->is_govt))
        ];
        //$reportHelper = new ReportHelper();
        $smsData['source'] = CommonFunction::ConvParaEx($template['details'], $data);

        $smsData['destination'] = $pilgrim->mobile;
        $smsData['msg_type'] = 'SMS';
        $smsData['ref_id'] = $pilgrim->id;
        $smsData['is_sent'] = 0;
        $smsData['template_id'] = $template['id'];

        Notification::create($smsData);
    }


    public function sendPaidSMS($pilgrim_id,$bank_id){
        $pilgrim = Pilgrim::findOrFail($pilgrim_id);
        
        // getting payment or pilgrims info
        $bank_name = Bank::find($bank_id);
        if($pilgrim->is_govt=='Private'){
            $template = Template::where('caption','PAID_SMS_P')->first()->toArray();
        }else{
            $template = Template::where('caption','PAID_SMS_G')->first()->toArray();
        }
        if($pilgrim->is_govt == 'Government'){
            $serial_no = 'G-'.$pilgrim->serial_no;
        }elseif($pilgrim->is_govt == 'Private'){
            $serial_no = 'NG-'.$pilgrim->serial_no;
        }else{
            $serial_no = '';
        }
        $data = [
            'token_no' => $pilgrim->tracking_no,
            'bank_name' => $bank_name->name,
            'serial_no' => $serial_no,
            'pilgrim' => CommonFunction::getName4SMS($pilgrim->full_name_english),
        ];
        //$reportHelper = new ReportHelper();
        $smsData['source'] = CommonFunction::ConvParaEx($template['details'], $data);

        $smsData['destination'] = $pilgrim->mobile;
        $smsData['msg_type'] = 'SMS';
        $smsData['ref_id'] = $pilgrim->id;
        $smsData['is_sent'] = 0;
        $smsData['template_id'] = $template['id'];

        Notification::create($smsData);
    }

    public function sendRegPaidSMS($pilgrim_id,$bank_id)
    {
        $pilgrim = Pilgrim::findOrFail($pilgrim_id);
        // getting payment or pilgrims info
        $bank_name = Bank::find($bank_id);

        if($pilgrim->is_govt=='Private'){
            $template = Template::where('caption','REG_PAID_SMS_P')->first()->toArray();
        }else{
            $template = Template::where('caption','REG_PAID_SMS_G')->first()->toArray();
        }

        $data = [
            'token_no' => $pilgrim->tracking_no,
            'bank_name' => $bank_name->name,
            'pilgrim' => CommonFunction::getName4SMS($pilgrim->full_name_english),
        ];
        $reportHelper = new ReportHelper();
        $smsData['source'] = CommonFunction::ConvParaEx($template['details'], $data);

        $smsData['destination'] = $pilgrim->mobile;
        $smsData['msg_type'] = 'SMS';
        $smsData['ref_id'] = $pilgrim->id;
        $smsData['is_sent'] = 0;
        $smsData['template_id'] = $template['id'];
        Notification::create($smsData);
    }

    public function sendSecondStepSMS($code){

        $smsData['source'] = 'Your Pre-registration verification code: '.$code;

        $smsData['destination'] = Auth::user()->user_phone;
        $smsData['msg_type'] = 'SMS';
        $smsData['ref_id'] = Auth::user()->id;
        $smsData['is_sent'] = 0;
        $smsData['template_id'] = 0;
        $smsData['priority'] = 9;

        Notification::create($smsData);
    }

    /**
     * @param $pilgrim_id
     * @param string $type
     */
    public static function sendRefundSMS($pilgrim_id, $type = 'REFUNDED'){
        $pilgrim = Pilgrim::find($pilgrim_id);

        if($type == 'REFUNDED'){
            $template = Template::where('caption', 'REFUNDED')->first()->toArray();
        }else {
            $template = Template::where('caption', 'REFUND_REQ')->first()->toArray();
        }
        $data = [
            'token_no' => $pilgrim->tracking_no,
            'pilgrim' => CommonFunction::getName4SMS($pilgrim->full_name_english),
        ];
        //$reportHelper = new ReportHelper();
        $smsData['source'] = CommonFunction::ConvParaEx($template['details'], $data);

        $smsData['destination'] = $pilgrim->mobile;
        $smsData['msg_type'] = 'SMS';
        $smsData['ref_id'] = $pilgrim->id;
        $smsData['is_sent'] = 0;
        $smsData['template_id'] = $template['id'];

        Notification::create($smsData);
    }

    /*     * ******************End of Model Class***************** */
}
