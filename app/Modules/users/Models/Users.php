<?php

namespace App\Modules\Users\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Auth;

class Users extends Model {

    protected $table = 'users';
    protected $fillable = array(
        'id',
        'user_type',
        'name',
        'username',
        'email',
        'user_phone',
        'user_status',
        'security_profile_id',
        'submited_status_fazil3',
        'submited_status_fazil3_private',
        'submited_status_fazil3_eff',
        'submited_status_fazil3_priv_eff',
        'submited_status_fazil4',
        'submited_status_fazil4_eff',
        'final_lock_fazil3_eff',
        'final_lock_fazil3_priv_eff',
        'password',
        'remember_token',
        'login_token',
        'is_approved',
        'first_login',
        'is_archieved',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    );

    public static function boot() {
        parent::boot();
        // Before update
        static::creating(function($post) {
            if (Auth::guest()) {
                $post->created_by = 0;
                $post->updated_by = 0;
            } else {
                $post->created_by = Auth::user()->id;
                $post->updated_by = Auth::user()->id;
            }
        });

        static::updating(function($post) {
            if (Auth::guest()) {
                $post->updated_by = 0;
            } else {
                $post->updated_by = Auth::user()->id;
            }
        });
    }

    function chekced_verified($TOKEN_NO, $data) {
        DB::table($this->table)
                ->where('user_hash', $TOKEN_NO)
                ->update($data);
    }

    function profile_update($table, $field, $check, $value) {
        return DB::table($table)->where($field, $check)->update($value);
    }

    function getUserRow($user_id) {
        return Users::leftJoin('user_types as mty', 'mty.id', '=', 'users.user_type')
                        ->leftJoin('user_desk as ud', 'ud.desk_id', '=', 'users.desk_id')
                        ->leftJoin('economic_zones as ez', 'ez.id', '=', 'users.eco_zone_id')
                        ->where('users.id', $user_id)
                        ->first(['users.*', 'ez.id as ezid', 'ez.name as ez_name', 'ud.desk_id', 'ud.desk_name', 'mty.type_name']);
    }

    function checkEmailAndGetMemId($user_email) {
        return DB::table($this->table)
                        ->where('user_email', $user_email)
                        ->pluck('id');
    }

    public static function setLanguage($lang) {
        Users::find(Auth::user()->id)->update(['user_language' => $lang]);
    }

    /**
     * @param $users object of logged in user
     * @return array
     */
    public static function getUserSpecialFields($users) {
        $additional_info = [];
        $user_type = explode('x', $users->user_type)[0];
        $inistitute = Institutes::where('madrasah_eiin', $users->username)->first(['division','zila','thana_u_zila']);
        switch ($user_type) {
            case 2:  //Inistitute
                $additional_info = [
                    [
                        'caption_division' => 'Division',
                        'value_division' => $inistitute->division != '' ? $inistitute->division : '',
                        'caption' => 'District',
                        'value' => $inistitute->zila != '' ? $inistitute->zila : '',
                        'caption_thana' => 'Thana',
                        'value_thana' => $inistitute->thana_u_zila != '' ? $inistitute->thana_u_zila : ''
                    ]
                ];
                break;
        }
        return $additional_info;
    }

    /*     * ***************************** Users Model Class ends here ************************* */
}
