<?php

namespace App\Modules\Users\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Auth;
use App\Libraries\CommonFunction;

class UsersModel extends Model {
    protected $table = 'users';
    protected $fillable = array(
        'id',
        'user_type',
        'user_sub_type',
        'eco_zone_id',
        'desk_id',
        'user_full_name',
        'user_email',
        'password',
        'delegate_to_user_id',
        'delegate_by_user_id',
        'user_hash',
        'user_status',
        'user_verification',
        'user_pic',
        'user_nid',
        'user_DOB',
        'user_gender',
        'user_aboutme',
        'user_street_address',
        'user_country',
        'user_city',
        'user_division',
        'user_zip',
        'user_phone',
        'authorization_file',
        'passport_nid_file',
        'user_first_login',
        'user_language',
        'security_profile_id',
        'district',
        'thana',
        'details',
        'user_agreement',
        'first_login',
        'is_approved',
        'is_locked',
        'remember_token',
        'login_token',
        'auth_token_allow',
        'updated_by',
        'user_hash_expire_time',
        'created_at',
        'auth_token',
        'auth_token_allow'
    );

    protected $defaults = array(
        'authorization_file' => '',
        'is_locked' => 0,
        'district' => 0,
        'thana' => 0,
        'details' => ''
    );
    public function __construct(array $attributes = array())
    {
        $this->setRawAttributes($this->defaults, true);
        parent::__construct($attributes);
    }

    public static function boot() {
        parent::boot();
        // Before update
        static::creating(function($post) {
            if (Auth::guest()) {
                $post->created_by = 0;
                $post->updated_by = 0;
            } else {
                $post->created_by = CommonFunction::getUserId();
                $post->updated_by = CommonFunction::getUserId();
            }
        });

        static::updating(function($post) {
            if (Auth::guest()) {
                $post->updated_by = 0;
            } else {
                $post->updated_by = CommonFunction::getUserId();
            }
        });
    }

    // This function will be used to check if a value is already existed in a database or not
    function check_availability_status($field, $value, $user_social_id, $type) {
        return DB::table($this->table)
                        ->where($field, $value)
                        ->where('user_social_type', 'google')
                        ->where('user_social_id', $user_social_id)
                        ->get();
    }

    function checkUserByEmail($user_email) {
        return DB::table($this->table)
                        ->where("user_email", $user_email)
                        ->get();
    }

    function chekced_verified($TOKEN_NO, $data) {
        DB::table($this->table)
                ->where('user_hash', $TOKEN_NO)
                ->update($data);
    }

    function profile_update($table, $field, $check, $value) {
        return DB::table($table)->where($field, $check)->update($value);
    }

    function getUserList() {
        $users_type = Auth::user()->user_type;

        $type = explode('x', $users_type);

        if ($users_type == '1x101') { // 1 is for Super Admin
            return UsersModel::leftJoin('user_types as mty', 'mty.id', '=', 'users.user_type')
                            ->leftJoin('area_info', 'users.district', '=', 'area_info.area_id')
                            ->orderBy('users.created_at', 'desc')
                            ->where('users.user_agreement', '!=', 0)
                            ->where('users.user_status', '!=', 'rejected')
                            ->where('users.user_type', '!=', Auth::user()->user_type)
                            ->get(['users.user_full_name', 'users.user_sub_type', 'users.user_email', 'mty.type_name', 'users.user_status', 'users.login_token',
                                'users.created_at as user_first_login', 'users.id', 'users.is_sub_admin', 'area_info.area_nm as users_district']);
        } else if ($users_type == '3x304') { // 3x304 is for HAAB
            /*
             *  IT help desk and HAAB can approve agency Users
             */
            return UsersModel::leftJoin('user_types as mty', 'mty.id', '=', 'users.user_type')
                            ->leftJoin('area_info', 'users.district', '=', 'area_info.area_id')
                            ->orderBy('users.created_at', 'desc')
                            ->where('users.user_status', '!=', 'rejected')
                            ->where('users.id', '!=', Auth::user()->id)
                            ->where(function($query){
                                $query->where('users.user_type', '=', Auth::user()->user_type)
                                    ->orWhere('users.user_type','like', "12x43_"); //12x430 for Agency
                            })
                            ->get(['users.user_full_name', 'users.user_sub_type',  'users.user_email', 'mty.type_name', 'users.user_status', 'users.user_first_login', 'users.id', 'users.is_sub_admin', 'area_info.area_nm as users_district']);
        } else if ($users_type == '10x411') { // 10 is for DC User
            return UsersModel::leftJoin('user_types as mty', 'mty.id', '=', 'users.user_type')
                            ->leftJoin('area_info', 'users.district', '=', 'area_info.area_id')
                            ->orderBy('users.created_at', 'desc')
                            ->where('users.user_status', '!=', 'rejected')
                            ->where('users.user_agreement', '!=', 0)
                            ->where('users.user_type', 'like', "$type[0]x" . substr($type[1], 0, 2) . "_")
                            ->where('users.user_sub_type', '=', Auth::user()->user_sub_type)
                            ->where('users.id', '!=', Auth::user()->id)
                            ->where('users.district', '=', Auth::user()->district)
                            ->get(['users.user_full_name', 'users.user_sub_type',  'users.user_email', 'mty.type_name', 'users.user_status', 'users.user_first_login', 'users.id', 'users.is_sub_admin', 'area_info.area_nm as users_district']);

            } else if ($users_type == '7x712') { // 7 FOR SB Admin
                return UsersModel::leftJoin('user_types as mty', 'mty.id', '=', 'users.user_type')
                ->leftJoin('area_info', 'users.district', '=', 'area_info.area_id')
                ->orderBy('users.created_at', 'desc')
                ->where('users.user_type', 'like', "$type[0]x" . substr($type[1], 0, 2) . "_")
                ->get(['users.user_full_name', 'users.user_sub_type',  'users.user_email', 'mty.type_name', 'users.user_status', 'users.user_first_login', 'users.id', 'users.is_sub_admin', 'area_info.area_nm as users_district']);
        }
        else {
            $data = UsersModel::leftJoin('user_types as mty', 'mty.id', '=', 'users.user_type')
                    ->leftJoin('area_info', 'users.district', '=', 'area_info.area_id')
                    ->orderBy('users.created_at', 'desc')
                    ->where('users.user_agreement', '!=', 0)
                    ->where('users.user_status', '!=', 'rejected')
                    ->where('users.id', '!=', Auth::user()->id)
                    ->where('users.user_type', 'like', "$type[0]x" . substr($type[1], 0, 2) . "_")
                    ->where('users.user_sub_type', '=', Auth::user()->user_sub_type)
                    ->get(['users.user_type',  'users.user_sub_type', 'users.user_full_name', 'users.user_email', 'mty.type_name', 'users.user_status', 'users.user_first_login', 'users.id', 'is_sub_admin', 'area_info.area_nm as users_district']);
            return $data;
        }
    }

    function getUserRow($user_id) {

        return UsersModel::leftJoin('user_types as mty', 'mty.id', '=', 'users.user_type')
                        ->leftJoin('area_info as ai1', 'users.district', '=', 'ai1.area_id')
                        ->leftJoin('area_info as ai2', 'users.thana', '=', 'ai2.area_id')
                        ->where('users.id', $user_id)
                        ->first(['users.*','type_name', 'ai1.area_nm as district_name', 'ai2.area_nm as thana_name' ]);
    }

    function getListWhere($field, $value) {
        return DB::table($this->table)
                        ->where($field, $value)
                        ->get();
    }

    function getTotalUserList() {
        return DB::table($this->table)->count();
    }

    //this function is to check if the email address is associated with user table
    //and if valid return type id
    function checkEmailAndGetType($user_email) {
        return DB::table($this->table)
                        ->where('user_email', $user_email)
                        ->pluck('user_type');
    }

    function checkEmailAndGetMemId($user_email) {
        return DB::table($this->table)
                        ->where('user_email', $user_email)
                        ->pluck('id');
    }

    public static function setLanguage($lang) {
        UsersModel::find(Auth::user()->id)->update(['user_language' => $lang]);
    }

    public static function getUserSpecialFields($users) {
        try {
            $additional_info = [];
            $user_type = explode('x', $users->user_type)[0];

            switch ($user_type) {

                case 10:
                    if ($users->user_type == '10x414') {
                        //10x414 for UDC
                        $udc = $users->user_sub_type != '' ? Uisc::find($users->user_sub_type) : '';
                        $additional_info = [
                            [
                                'caption' => 'UDC Name',
                                'value' => $udc->name
                            ],
                            [
                                'caption' => 'Location',
                                'value' => $udc->location
                            ]
                        ];
                    } else if ($users->user_type == '10x413') {
                        // 10x413 = IF
                        $additional_info = [
                            [
                                'caption' => 'Branch Name',
                                'value' => ''
                            ]
                        ];
                    } else if ($users->user_type == '10x412') { //10x412=UNO office
                        $additional_info = [
                            [
                                'caption' => 'District',
                                'value' => $users->district != 0 ? AreaInfo::where('area_id', $users->district)->pluck('area_nm') : ''
                            ],
                            [
                                'caption' => 'Uzpilla',
                                'value' => $users->thana != 0 ? AreaInfo::where('area_id', $users->thana)->pluck('area_nm') : ''
                            ]
                        ];
                    } else if ($users->user_type == '10x411') {  //10x411 DC Office
                        $additional_info = [
                            [
                                'caption' => 'District',
                                'value' => $users->district != 0 ? AreaInfo::where('area_id', $users->district)->pluck('area_nm') : ''
                            ]
                        ];
                    }
                    break;

                case 11: //for bank user
                    $additional_info = [
                        [
                            'caption' => 'Bank Name',
                            'value' => $users->user_sub_type != '' ? Bank::find($users->user_sub_type)->name : ''
                        ],
                        [
                            'caption' => 'Branch Code',
                            'value' => $users->code
                        ]
                    ];
                    break;

                case 12://Agency
                    if ($users->user_sub_type) {
                        $agency = Agency::find($users->user_sub_type);
                        $additional_info = [
                            [
                                'caption' => 'Agency Name',
                                'value' => $agency->name
                            ],
                            [
                                'caption' => 'Licence',
                                'value' => $agency->license_no
                            ]
                        ];
                    }
                    break;

                case 4://Hajj office
                    $additional_info = [
                        [
                            'caption' => 'Hajj office',
                            'value' => ''
                        ]
                    ];
                    break;
                case 7://SB
                    $additional_info = [
                        [
                            'caption' => 'District',
                            'value' => $users->district != 0 ? AreaInfo::where('area_id', $users->district)->pluck('area_nm') : ''
                        ]
                    ];
                    break;
            }
        } catch (\Exception $e) {
            return $additional_info = [
                [
                    'caption' => 'Error:',
                    'value' => 'Sub type not found'
                ]
            ];
        }
        return $additional_info;
    }

    public function loginChart(){
        return DB::select(DB::raw("SELECT tab2.Agency,format(tab1.Pilgrims,0) as Pilgrims,format(Quota,0) as Quota,StartDate,EndDate,url FROM (
SELECT is_govt AS Agency, COUNT(id) AS Pilgrims FROM pilgrims WHERE is_archived=0 AND serial_no>0 GROUP BY is_govt
) tab1 RIGHT JOIN (
SELECT CASE WHEN caption='PRE_REG_GOVT_PERIOD' THEN 'Government' ELSE 'Private' END Agency,VALUE AS StartDate,value2 AS EndDate,value3 AS Quota
,CASE WHEN caption='PRE_REG_GOVT_PERIOD' THEN 'view-details/D1-305042' ELSE 'view-details/D2-972220' END url
FROM configuration WHERE caption IN('PRE_REG_GOVT_PERIOD','PRE_REG_PRIVATE_PERIOD')
) tab2 ON tab2.Agency=tab1.Agency"));
    }

    /*     * ***************************** Users Model Class ends here ************************* */
}
