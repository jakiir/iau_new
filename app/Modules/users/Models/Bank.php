<?php

namespace App\Modules\Users\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Bank extends Model {

    protected $table = 'bank';
    protected $fillable = array(
        'name',
        'code',
        'location',
        'phone',
        'email',
    );

    /************************ Users Model Class ends here ****************************/
}
