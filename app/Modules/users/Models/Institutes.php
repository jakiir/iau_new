<?php

namespace App\Modules\Users\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Institutes extends Model {

    protected $table = 'institutes';
    protected $fillable = array(
        'id',
        'institute_name',
        'eiin',
        'madrasah_code',
        'class',
        'division',
        'zilla',
        'zila_code',
        'thana_u_zilla',
        'thana_u_zilla_code',
        'center_name',
        'post_office',
        'principal_name',
        'mobile_no',
        'email'
    );

    /************************ Institutes Class ends here ****************************/
}
