<?php

// With Authorization (Login is required)

Route::group(array('module' => 'Users', 'middleware' => ['auth', 'checkAdmin'], 'namespace' => 'App\Modules\Users\Controllers'), function() {

    /* User related */
    Route::get('/users/lists', "UsersController@lists");
    Route::get('/users/delegate', "UsersController@delegate");
    Route::get('/users/delegation/{id}', "UsersController@delegation");
    Route::post('/users/get-delegate-userinfo', "UsersController@getDeligatedUserInfo");
    Route::post('/users/get-delegate-userinfos', "UsersController@getDeligatedUserInfos");
    Route::patch('/users/process-deligation', "UsersController@processDeligation");
    Route::get('/users/remove-deligation', "UsersController@removeDeligation");
    Route::patch('/users/store-delegation', "UsersController@storeDelegation");
    Route::get('users/view/{id}', "UsersController@view");
    Route::patch('/users/update/{id}', "UsersController@update");

    Route::get('/users/edit/{id}', "UsersController@edit");
    Route::get('/users/activate/{id}', "UsersController@activate");
    Route::patch('/users/set-desk/{id}', "UsersController@setDesk");

    Route::get('/users/isApproved/{id}', "UsersController@isApproved");
    /* End of User related */

    /* New User Creation by Admin */
    Route::get('users/create-new-user', "UsersController@createNewUser");
    Route::patch('/users/store-new-user', "UsersController@storeNewUser");
    /* End of New User Creation by Admin */

    Route::get('/users/logout', "UsersController@logout");

//    user approval or reject
    Route::post('/users/approve/{id}', "UsersController@approveUser");
    Route::get('/users/reject/{id}', "UsersController@rejectUser");

    Route::get('/users/two-step', 'UsersController@twoStep');
    Route::patch('/users/check-two-step', 'UsersController@checkTwoStep');
    Route::patch('/users/verify-two-step', 'UsersController@verifyTwoStep');
});


/* User profile update */
Route::group(array('module' => 'Users', 'middleware' => ['auth'], 'namespace' => 'App\Modules\Users\Controllers'), function() {
    Route::get('users/profileinfo', "UsersController@profileinfo");
    Route::patch('users/profile_update/{id}', [
        'uses' => 'UsersController@profile_update'
    ]);

    /* Reset Password from profile and Admin list */
    Route::patch('users/update-password-from-profile/{id}', "UsersController@updatePassFromProfile");
    Route::get('users/reset-password/{confirmationCode}', [
        'as' => 'confirmation_path',
        'uses' => 'UsersController@resetPassword'
    ]);

    /*
     * datatable
     */
    Route::post('users/get-row-details-data', "UsersController@getRowDetailsData");
    Route::post('users/get-access-log-data', "UsersController@getAccessLogData");
});

// Without Authorization (Login is not required)

Route::group(array('module' => 'Users', 'namespace' => 'App\Modules\Users\Controllers'), function() {

    Route::get('/users/login', function () {
        return redirect('login');
    });

    Route::get('/users/message', "UsersController@message");
    Route::get('users/message/{confirmationCode}', [
        'as' => 'confirmation_path',
        'uses' => 'UsersController@message'
    ]);

    Route::get('/users/create/{va}', 'UsersController@create');
    Route::get('/users/create/', 'UsersController@create');
    Route::patch('/users/store', "UsersController@store");

    // verification
    Route::get('/users/verify-created-user/{encrypted_token}', "UsersController@verifyCreatedUser");
    Route::patch('/users/created-user-verification/{encrypted_token}', "UsersController@createdUserVerification");

    Route::get('users/verification/{confirmationCode}', [
        'as' => 'confirmation_path',
        'uses' => 'UsersController@verification'
    ]);
    Route::patch('users/verification_store/{confirmationCode}', [
        'as' => 'confirmation_path',
        'uses' => 'UsersController@verification_store'
    ]);

    Route::get('/users/get-userdesk-by-type', 'UsersController@getUserDeskByType');
    Route::get('/users/get-district-by-division', 'UsersController@getDistrictByDivision');
    Route::get('/users/get-thana-by-district-id', 'UsersController@get_thana_by_district_id');

    //Mail Re-sending
    Route::get('users/reSendEmail', "UsersController@reSendEmail");
    Route::get('/users/resendMail', 'UsersController@resendMail');
    Route::patch('users/reSendEmailConfirm', "UsersController@reSendEmailConfirm");
    Route::get('users/resendMailByAdmin/{email}', "UsersController@resendMailByAdmin");

    Route::get('users/support', "UsersController@support");
    Route::get('users/helpdesk-contact', "UsersController@helpdeskContact");

    //Forget Password
    Route::get('forget-password', "UsersController@forgetPassword");
    Route::patch('users/reset-forgotten-password', "UsersController@resetForgottenPass");
    Route::get('users/verify-forgotten-pass/{token_no}', "UsersController@verifyForgottenPass");

    Route::get('users/get-user-session', 'UsersController@getUserSession');

// Check if valid unique passport no
    Route::get('users/check-passport_no', "UsersController@checkPassportNo");
    Route::get('users/check-email', "UsersController@checkUniqueEmail");

    /*     * ********************** End of Route Group *********************** */
});
