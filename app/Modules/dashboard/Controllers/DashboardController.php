<?php

namespace App\Modules\Dashboard\Controllers;

use App;
use App\Http\Controllers\Controller;
use App\Libraries\ACL;
use App\Libraries\CommonFunction;
use App\Modules\Apps\Models\Apps;
use App\Modules\Apps\Models\Status;
use App\Modules\Company\Models\Company;
use App\Modules\Dashboard\Models\Rules;
use App\Modules\Dashboard\Models\Services;
use App\Modules\Dashboard\Models\Widget;
use App\Modules\Users\Models\AreaInfo;
use App\Modules\Users\Models\Countries;
use App\Modules\Users\Models\Nationality;
use App\Modules\Users\Models\Users;
use App\Modules\workPermit\Models\Processlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use View;

class DashboardController extends Controller {

    public function __construct() {
        if (Session::has('lang'))
            App::setLocale(Session::get('lang'));
        ACL::db_reconnect();
    }

	public function index(){
		return view('dashboard::welcome'); 
	}

    public function stdFormAccess(Request $request)
    {
        $whereCondition = ['user_id' => Auth::user()->id , 'meta_key' => 'user_role'];
        $auth_user_role = DB::table('user_metas')->select('*')->where($whereCondition)->get();
        $user_role_id = $auth_user_role[0]->meta_value;
        $users_role = DB::table('user_roles')->select('*')->where('id', $user_role_id)->first();
        $role_name = $users_role->role_name;

        $examVal = $request->get('examVal');
        $typeVal = $request->get('typeVal');
        $yearVal = $request->get('yearVal');

        if($examVal !='' && $typeVal !='' && $yearVal){
            $getCenter = DB::table('exam_center')
                ->leftJoin('institutes as tb1', 'tb1.madrasah_eiin', '=', 'exam_center.center_eiin')
                ->groupBy('center_eiin')
                ->where('center_eiin', Auth::user()->username)
                ->first();
            $drYes='no';
            if($getCenter) $drYes='yes';

            $examTxt = $request->get('examTxt');
            $typeTxt = $request->get('typeTxt');
            //session_start();
            \Session::set('examVal',$examVal);
            \Session::set('typeVal',$typeVal);
            \Session::set('yearVal',$yearVal);
            \Session::set('examTxt',$examTxt);
            \Session::set('typeTxt',$typeTxt);
            \Session::set('yearTxt','20'.$yearVal);
            \Session::set('application_on', false);
            \Session::set('dr_yes', $drYes);

            $whereConditionAccess = ['exam' => $examVal , 'type' => $typeVal];
            $yearColumn = 'year_'.$yearVal;
            $access_profile = DB::table('access_profile')->where($whereConditionAccess)->where($yearColumn,'=','1')->first();

            if($access_profile){
                $startOffDate = $access_profile->start_date;
                $endOffDate = $access_profile->end_date;
                $typeStatus = $access_profile->type_status;
                $yearStatus = $access_profile->$yearColumn;

                if( $startOffDate !='' && $endOffDate !='' && $typeStatus==1 && $yearStatus ==1 && $role_name == 'Admin'){
                    \Session::set('application_on', $this->applicationOffTime($startOffDate,$endOffDate));
                }
                if(Session::get('application_on') == false && $typeVal == 'eff'){
                    if($role_name == 'Super Admin'){
                        \Session::set('application_on', true);
                        $result = array('success' => true,'selExam'=>Session::get('examVal'),'selType'=>Session::get('typeVal'),'selYear'=>Session::get('yearVal'),'application_on'=>Session::get('application_on'),'role_name'=>$role_name, 'msg' => 'Access permited');
                    } else {
                        $result = array('success' => true,'selExam'=>Session::get('examVal'),'selType'=>Session::get('typeVal'),'selYear'=>Session::get('yearVal'),'application_on'=>Session::get('application_on'), 'msg' => 'Access denied!','role_name'=>$role_name);
                    }
                } else {
                    $result = array('success' => true,'selExam'=>Session::get('examVal'),'selType'=>Session::get('typeVal'),'selYear'=>Session::get('yearVal'),'application_on'=>Session::get('application_on'),'role_name'=>$role_name, 'msg' => 'Access permited');
                }
            } else {
                if($role_name == 'Super Admin'){
                    //Session::get('application_on') = true;
                    \Session::set('application_on', true);
                    $result = array('success' => true,'selExam'=>Session::get('examVal'),'selType'=>Session::get('typeVal'),'selYear'=>Session::get('yearVal'),'application_on'=>Session::get('application_on'),'role_name'=>$role_name, 'msg' => 'Access permited');
                } else {
                    $result = array('success' => false, 'msg' => 'Access denied!','role_name'=>$role_name);
                }

            }
        } else {
            $result = array('success' => false, 'msg' => 'Access denied!','role_name'=>$role_name);
        }

        return response()->json($result);
    }
	
	
	public function iauDashboard(Widget $dashboard) {
        $list = [];
        $log = date('H:i:s', time());
        $dbMode = Session::get('DB_MODE');
        list($type, $code) = explode('x', Auth::user()->user_type);
        $log .= ' - ' . date('H:i:s', time());       
        $info = Users::first(['name', 'email', 'user_phone']);
        $division = AreaInfo::where('area_type', 2)->lists('area_nm', 'area_id');
        $areaList = AreaInfo::lists('area_nm', 'area_id');

        $notice = CommonFunction::getNotice(1);
        $user_type = Auth::user()->user_type;
        $deshboardObject = [];
        if ($user_type == '1x101') {
            $deshboardObject = DB::table('dashboard_object')->where('db_obj_status', 1)->get();
        }

        return view("dashboard::index", compact('deshboardObject', 'list', 'dbMode', 'log', 'info', 'division', 'areaList', 'notice'));
    }

    public function search() {
        $nationality = Countries::orderby('nationality')->where('nationality', '!=', '')->lists('nationality', 'iso');
//        $nationality = Nationality::orderby('nationality')->where('nationality', '!=', '')->lists('nationality', 'id');
        $organization = Company::orderBy('company_name', 'ASC')->lists('company_name', 'company_name')->all();
        $services = Services::where('is_active', 1)->orderBy('name', 'ASC')->lists('name', 'id')->all();
        $statusList = App\Modules\Apps\Models\Status::orderBy('status_name', 'ASC')->lists('status_name', 'status_id')->all();
        $resultList = [2 => 'No Objection', 3 => 'Objection', 4 => 'Black Listed', 5 => 'Not Applicable'];
        return view('dashboard::search-view', compact('nationality', 'organization', 'resultList', 'statusList', 'services'));
    }

    public function serviceWiseStatus(Request $request) {
        $id = $request->get('service_id');
        $service_wise_status = Status::where('process_id', '=', $id)->get();
        return $service_wise_status;
    }

    public function searchResult(Request $request) {
        $tracking_number = $request->get('tracking_number');
        $passport_number = $request->get('passport_number');
        $nationality = $request->get('nationality');
        $organization = $request->get('organization');
        $applicant_name = $request->get('applicant_name');
        $status_id = $request->get('status_id');

//        dd($request->all());

        $getList = Apps::getSearchResults($tracking_number, $passport_number, $nationality, $applicant_name, $status_id);

        $_type = Auth::user()->user_type;
        $user_type = explode('x', $_type)[0];
        $desk_id = Auth::user()->desk_id;

        $areaList = AreaInfo::lists('area_nm', 'area_id');
        $resultList = [2 => 'No Objection', 3 => 'Objection', 4 => 'Black Listed'];
        $view = View::make('dashboard::search-result', compact('getList', 'resultList', 'user_type', 'desk_id', 'areaList'));
        $contents = $view->render();
        $data = ['responseCode' => 1, 'data' => $contents];
        return response()->json($data);
    }

    public function execute() {
        $controllers = [];
        $data = [];
        $duplicate = [];
        foreach (Route::getRoutes()->getRoutes() as $route) {
            $action = $route->getAction();
            $method = $route->getMethods();

            if (array_key_exists('controller', $action)) {
                // You can also use explode('@', $action['controller']); here
                // to separate the class name from the method
                $controllers[] = $method[0] . "\\" . $action['controller'];
            }
        }
//        dd($controllers);
        $duCount = 0;
        foreach ($controllers as $value) {
            $data = explode("\\", $value);
            $method_type = $data[0];
            if (count($data) == 6) {
                $module_name = $data[3];
                $route_action = $data[5];
            } else {
                $module_name = $data[2];
                $route_action = $data[4];
            }

            $exitst = Rules::where('module_title', '=', $module_name)
                            ->where('method_type', '=', $method_type)
                            ->where('action_method', '=', $route_action)->count();

            if ($exitst == 0) {
                $rules = new Rules;
                $rules->module_title = $module_name;
                $rules->method_type = $method_type;
                $rules->action_method = $route_action;
                $rules->save();
            } else {
                $duplicate[] = $value;
            }
        }
        Session::flash('success', 'URL has been saved successfully!');
        return view('dashboard::rules', compact('duplicate'));
    }

    /*     * ***************************End of Controller Class******************************* */
}
