<?php

Route::group(array('module' => 'Dashboard', 'middleware' => 'auth', 'namespace' => 'App\Modules\Dashboard\Controllers'), function() {
	
	Route::post('/stdFormAccess', ['as'=> 'ajaxstdFormAccess', 'uses'=>'DashboardController@stdFormAccess']);
	Route::get('dashboard/iau', 'DashboardController@iauDashboard');
	
    Route::get('dashboard/search', 'DashboardController@search');
    Route::post('dashboard/search-result', 'DashboardController@searchResult');
    Route::post('dashboard/service-wise-result', 'DashboardController@serviceWiseStatus');
    Route::post('dashboard/search-view/{param}', 'DashboardController@searchView');
    Route::get('dashboard/execute', 'DashboardController@execute');
    Route::resource('dashboard', 'DashboardController');
    
    /******************************End of Route Group***********************************/
});