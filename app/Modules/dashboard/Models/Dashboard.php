<?php namespace App\Modules\Dashboard\Models;

use App\Libraries\ReportHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Dashboard extends Model {

    protected $table = 'dashboard_object';
    protected $fillable = [
        'db_obj_title','db_user_id','db_obj_type','db_obj_para1','db_obj_para2','db_obj_status','db_obj_sort','db_user_type'
    ];


    public function getWidget()
    {
        $widgetData = '';
        $data = Dashboard::where('db_obj_type','widget')
            ->where('db_user_type',Auth::user()->user_type)
            ->orWhere('db_user_type',0)
            ->orWhere('db_user_type','is null')
            ->orderBy('db_user_type','desc')
            ->first();
        if($data) {
            //user_type like {$user_type} and user_sub_type={$user_sub_type}
            $users_type = Auth::user()->user_type;
            $type = explode('x', $users_type);
            $data['user_type'] = "'"."$type[0]x" . substr($type[1], 0, 2) ."_'";
            $data['desk_id'] = Auth::user()->desk_id==''? 0 : Auth::user()->desk_id;
            $data['district'] = Auth::user()->district;
            $reportHelper = new ReportHelper();
            $sql = $reportHelper->ConvParaEx($data->db_obj_para1,$data);
            $widgetData = DB::select(DB::raw($sql));

            try
            {
                $widgetData = DB::select(DB::raw($sql));

            }catch(\Illuminate\Database\QueryException $e) {

                $widgetData = array();
//                echo "Something wrong...";
            }
        }
        return $widgetData;
    }

    /**
     * @return mixed
     */
    public function getDates()
    {
        $list = DB::select(DB::raw("SELECT tab2.Agency,format(tab1.Pilgrims,0) as Pilgrims,format(Quota,0) as Quota,StartDate,EndDate FROM (
                    SELECT is_govt AS Agency, COUNT(id) AS Pilgrims FROM pilgrims WHERE is_archived=0 AND serial_no>0 GROUP BY is_govt
                    ) tab1 RIGHT JOIN (
                    SELECT CASE WHEN caption='PRE_REG_GOVT_PERIOD' THEN 'Government' ELSE 'Private' END Agency,VALUE AS StartDate,value2 AS EndDate,value3 AS Quota
                    FROM configuration WHERE caption IN('PRE_REG_GOVT_PERIOD','PRE_REG_PRIVATE_PERIOD')
                    ) tab2 ON tab2.Agency=tab1.Agency"));
        return $list;
    }

    public function getDashboardObject()
    {
        $data = Dashboard::where('db_obj_type','list')->get();
        return $data;
    }
    
    /*********************************End of Models*******************************/
}
