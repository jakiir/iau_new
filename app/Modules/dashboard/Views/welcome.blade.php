@extends('layouts.app')
@section('content')
	<div id="content" class="site-content"><!-- Start Main Content-->
		<section class="main-content">
            <?php
				\Session::set('examVal', '');
				\Session::set('typeVal', '');
				\Session::set('yearVal', '');
				\Session::set('yearVal', '');
				\Session::set('examTxt', 'dfd');
				\Session::set('application_on', '');
            ?>
			<div class="container">
				<div class="row">

					<article class="hentry">
						<div class="col-md-12">
							<div class="content-block">
								<h4 class="text-center">Selection Page</h4>
								<div class="block-content">
									<form id="welcomform" name="welcomform" action="#" onsubmit="return stdFormAccess()" method="get">
										<div class="formAlert"></div>
										<div class="roundborder" style="background-color:#FFF; width:31%;display:inline-block;">
											<label for="exam">Select Class</label>
											<hr>
											<select name="exam" id="exam" class="rounded" size="6" style="background:#dff0d8;color:#000;width:100%;">
												<option value="fazilpass" selected="selected">Fazil Pass</option>
												<option value="fazilprivate">Fazil Private</option>
												<option value="fazilhons">Fazil Honours</option>
												<option value="kamil-1">Kamil </option>
												<!-- <option data-start-off-date="" data-end-off-date="" value="kamil-2">Kamil (2 Year)</option> -->
											</select>
										</div>

										<div class="roundborder" style="background-color:#FFF; width:31%;display:inline-block;">
											<label for="type">Select Type</label>
											<hr>
											<select name="type" id="type" class="rounded" size="6" style="background:#dff0d8;color:#000;width:100%;">
												<option data-type-status="0" value="esif" selected="selected">eSIF-Student Information Form</option>
												<option data-type-status="1" value="eff">eFF-Form Fillup</option>
												<!-- <option value="etif">eTIF-Teacher Information Form</option>
                                                <option value="ecis">eCIS-Centre Information System</option>
                                                <option value="erps">eRPS-Result Publication System</option> -->
											</select>
										</div>

										<div class="roundborder" style="background-color:#FFF; width:33%;display:inline-block;">
											<label for="year">Select Session</label>
											<hr>
											<select name="year" id="year" class="rounded" size="6" style="background:#dff0d8;color:#000;width:100%; text-align:center;">
												<option data-year-status="1" value="15" selected="selected">*** 2015-2016 ***</option>
												<!-- <option value="16">*** 2016 ***</option>
                                                <option value="17">*** 2017 ***</option> -->
											</select>
										</div>
										<div class="form-group" style="margin-top:10px;">
											<a href="{{ url('/logout') }}" class="btn-primary btn"><i class="fa fa-btn fa-hand-o-left"></i>Back</a>
											<button type="submit" name="home_step" class="btn btn-success home_step">
												<i class="fa fa-btn fa-hand-o-right"></i>Continue
											</button>
										</div>

									</form>
								</div>

								<div class="block-content kalpurush" style="color:red;font-size:20px;">
									নিয়মাবলী:
									<br>
									১। 	www.iau.edu.bd থেকে পরীক্ষার্থীর বিবরণী ফরম ডাউনলোড করে পূরণ পূর্বক শিক্ষার্থীদের সংখ্যা অনুযায়ী সাধারণ জমা রশিদের মাধ্যমে ব্যাংকে (রেজিস্ট্রার, ইসলামি আরবি বিশ্ববিদ্যালয়, ঢাকা বরাবর অগ্রণী ব্যাংক, সাতমসজিদ রোড শাখা, ঢাকার হিসাব নং-০২০০০০৯০৮২৩৪১-ইআবি পরীক্ষা ফান্ড) জমা দিতে হবে। টাকা জমার মূল রশিদসহ বিশ্ববিদ্যালয়ে প্রেরণ করতে হবে। উল্লেখ যে, উক্ত ব্যাংক হিসাব ছাড়া অন্য কোন ব্যাংক হিসাবে টাকা জমা দিলে গ্রহণ করা হবে না।
									<br>
									২। 	ব্যাংকে জমাকৃত টাকার সমান সংখ্যক শিক্ষার্থীকে অনলাইনে eFF লিস্ট থেকে সিলেক্ট করে প্রতিপাতায়  Submit This Page এবং সর্বশেষ  Final Lock করতে হবে। <span style="text-decoration:underline;">	(ফরম ফিলাপকৃত সকল শিক্ষার্থীদের Submit করার পূর্বে কোনক্রমেই  Final Lock করা যাবে না।)</span>
									<br>
									৩।	উল্লিখিত নির্দেশনা অনুযায়ী কাগজপত্র প্রেরণের অনুরোধ করা হল।
								</div>
							</div>
						</div>
					</article><!-- End Article-->
				</div>

			</div>
		</section>
	</div><!-- End Main Content-->
	<style type="text/css">
		.footer{
			bottom: 0;
			width: 100%;
		}
	</style>
	<script type="text/javascript">
        function stdFormAccess(){

            $('.formAlert').html('').removeClass('alert alert-success alert-danger');
            var exam = $('#exam');
            var type = $('#type');
            var year = $('#year');

            exam.removeClass('input-error');
            type.removeClass('input-error');
            year.removeClass('input-error');
            var examVal = exam.val();
            var typeVal = type.val();
            var yearVal = year.val();
            var examTxt = $("#exam option:selected").text();
            var typeTxt = $("#type option:selected").text();

            if (examVal == null || examVal == "") {
                alert("Select Examination must be filled out");
                exam.addClass('input-error');
                return false;
            }
            if (typeVal == null || typeVal == "") {
                alert("Dakhil roll must be filled out");
                type.addClass('input-error');
                return false;
            }

            if (yearVal == null || yearVal == "") {
                alert("Alim passing year must be filled out");
                year.addClass('input-error');
                return false;
            }
            url = '{{route("ajaxstdFormAccess")}}';
            $.ajax({
                url: url,
                type: 'POST',
                data : {
                    examVal:examVal,
                    examTxt:examTxt,
                    typeVal:typeVal,
                    typeTxt:typeTxt,
                    yearVal:yearVal,
                    "_token":"{{ csrf_token() }}"
                },
                dataType: 'JSON',
                success: function (response) {
                    if(response.success == true){
                        var submited_status_fazil3 = "{{Auth::user()->submited_status_fazil3}}";
                        var submited_status_fazil3_private = "{{Auth::user()->submited_status_fazil3_private}}";
                        var submited_status_fazil3_eff = "{{Auth::user()->submited_status_fazil3_eff}}";
                        var submited_status_fazil3_priv_eff = "{{Auth::user()->submited_status_fazil3_priv_eff}}";
                        var submited_status_fazil4 = "{{Auth::user()->submited_status_fazil4}}";
                        var submited_status_fazil4_eff = "{{Auth::user()->submited_status_fazil4_eff}}";

                        if(response.selExam != ''){
                            if(response.selExam == 'fazilpass'){
                                $('.formAlert').addClass('alert alert-success').html('<strong>Success!</strong> '+response.msg);
                                if(response.role_name != 'Super Admin'){
                                    if(response.selType == 'esif'){
//                                $('.formAlert').addClass('alert alert-danger').html('<strong>Warning!</strong> Access denied!');
//                                return false;
                                        if(submited_status_fazil3 == 'Yes' || response.application_on == false){
                                            window.location.href = "/fazil-pass/show-student-list";
                                        } else {
                                            window.location.href = "/manage-student/fazil3-esif";
                                        }
                                    }

                                    if(response.selType == 'eff'){
                                        if(submited_status_fazil3_eff == 'Yes' || response.application_on == false){
                                            window.location.href = "/manage-student/show-fazil3-eff-students";
                                        } else {
                                            window.location.href = "/manage-student/show-fazil3-eff-students";
                                        }
                                    }
                                } else {
                                    if(response.selType == 'esif'){
//                                $('.formAlert').addClass('alert alert-danger').html('<strong>Warning!</strong> Access denied!');
//                                return false;
                                        if(submited_status_fazil3 == 'Yes' || response.application_on == false){
                                            window.location.href = "/fazil-pass/show-student-list";
                                        } else {
                                            window.location.href = "/manage-student/fazil3-esif";
                                        }
                                    }

                                    if(response.selType == 'eff'){
                                        if(submited_status_fazil3_eff == 'Yes' || response.application_on == false){
                                            window.location.href = "/manage-student/show-fazil3-eff-students";
                                        } else {
                                            window.location.href = "/manage-student/show-fazil3-eff-students";
                                        }
                                    }
                                }
                            }
                            else if(response.selExam == 'fazilhons'){
                                $('.formAlert').addClass('alert alert-success').html('<strong>Success!</strong> '+response.msg);
                                if(response.role_name != 'Super Admin'){
                                    if(response.selType == 'esif'){
//                                $('.formAlert').addClass('alert alert-danger').html('<strong>Warning!</strong> Access denied!');
//                                return false;
                                        if(submited_status_fazil4 == 'Yes' || response.application_on == false){
                                            window.location.href = "/manage-student/show-fazil4-students";
                                        } else {
                                            window.location.href = "/manage-student/fazil4-esif";
                                        }
                                    }

                                    if(response.selType == 'eff'){
                                        if(submited_status_fazil4_eff == 'Yes' || response.application_on == false){
                                            window.location.href = "/manage-student/show-fazil4-eff-students";
                                        } else {
                                            window.location.href = "/manage-student/show-fazil4-eff-students";
                                        }
                                    }

                                } else {
                                    if(response.selType == 'esif'){
//                                $('.formAlert').addClass('alert alert-danger').html('<strong>Warning!</strong> Access denied!');
//                                return false;
                                        if(submited_status_fazil4 == 'Yes' || response.application_on == false){
                                            window.location.href = "/manage-student/show-fazil4-students";
                                        } else {
                                            window.location.href = "/manage-student/fazil4-esif";
                                        }
                                    }

                                    if(response.selType == 'eff'){
                                        if(submited_status_fazil4_eff == 'Yes' || response.application_on == false){
                                            window.location.href = "/manage-student/show-fazil4-eff-students";
                                        } else {
                                            window.location.href = "/manage-student/show-fazil4-eff-students";
                                        }
                                    }
                                }
                            } else if(response.selExam == 'fazilprivate'){
                                $('.formAlert').addClass('alert alert-success').html('<strong>Success!</strong> '+response.msg);

                                if(response.role_name != 'Super Admin'){
                                    if(response.selType == 'esif'){
//                                $('.formAlert').addClass('alert alert-danger').html('<strong>Warning!</strong> Access denied!');
//                                return false;
                                        if(submited_status_fazil3_private == 'Yes' || response.application_on == false){
                                            window.location.href = "/manage-student/show-fazil3-private-students";
                                        } else {
                                            window.location.href = "/manage-student/fazil3-private-esif";
                                        }
                                    }

                                    if(response.selType == 'eff'){
                                        if(submited_status_fazil3_priv_eff == 'Yes' || response.application_on == false){
                                            window.location.href = "/manage-student/show-fazil3-private-eff-students";
                                        } else {
                                            window.location.href = "/manage-student/show-fazil3-private-eff-students";
                                        }
                                    }

                                } else {
                                    if(response.selType == 'esif'){
//                                $('.formAlert').addClass('alert alert-danger').html('<strong>Warning!</strong> Access denied!');
//                                return false;
                                        if(submited_status_fazil3_private == 'Yes' || response.application_on == false){
                                            window.location.href = "/manage-student/show-fazil3-private-students";
                                        } else {
                                            window.location.href = "/manage-student/fazil3-private-esif";
                                        }
                                    }

                                    if(response.selType == 'eff'){
                                        if(submited_status_fazil3_priv_eff == 'Yes' || response.application_on == false){
                                            window.location.href = "/manage-student/show-fazil4-eff-students";
                                        } else {
                                            window.location.href = "/manage-student/show-fazil3-private-eff-students";
                                        }
                                    }
                                }
                            }
                            else if(response.selExam == 'kamil-1'){
                                $('.formAlert').addClass('alert alert-success').html('<strong>Success!</strong> '+response.msg);

                                if(response.role_name != 'Super Admin'){
                                    if(response.selType == 'esif'){
                                        if(submited_status_fazil3_private == 'Yes' || response.application_on == false){
                                            window.location.href = "/manage-student/show-kamil-esif-students";
                                        } else {

                                            window.location.href = "/manage-student/fazil3-private-esif";
                                        }
                                    }

                                    if(response.selType == 'eff'){
                                        if(submited_status_fazil3_priv_eff == 'Yes' || response.application_on == false){
                                            window.location.href = "/manage-student/show-fazil3-private-eff-students";
                                        } else {
                                            window.location.href = "/manage-student/show-kamil-esif-students";
                                        }
                                    }

                                } else {
                                    if(response.selType == 'esif'){
                                        if(submited_status_fazil3_private == 'Yes' || response.application_on == false){
                                            window.location.href = "/manage-student/show-kamil-esif-students";
                                        } else {
                                            window.location.href = "/manage-student/show-kamil-esif-students";
                                        }
                                    }

                                    if(response.selType == 'eff'){
                                        if(submited_status_fazil3_priv_eff == 'Yes' || response.application_on == false){
                                            window.location.href = "/manage-student/show-fazil4-eff-students";
                                        } else {
                                            window.location.href = "/manage-student/show-fazil3-private-eff-students";
                                        }
                                    }
                                }
                            }
                            else {
                                $('.formAlert').addClass('alert alert-danger').html('<strong>Warning!</strong> Access denied!');
                            }


                        }
                    } else {
                        $('.formAlert').addClass('alert alert-danger').html('<strong>Warning!</strong> '+response.msg);
                    }


                }
            });
            return false;
        }
	</script>

@endsection