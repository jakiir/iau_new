@extends('layouts.admin')

@section('content')

    <style>
        .service-div{
            text-align: center !important;
        }
        .service-div:hover{
            border-color:  #9acc9a;
            background: #ececf0;
            color: forestgreen;
        }
        .service-inner-div{
            padding: 15px;
            margin: 25px;
        }
        .text-large{
            font-size: 18px !important;
        }
    </style>

{{--@section ('page_heading','<i class="fa fa-home"></i> Dashboard')--}}

@include('partials.messages')

@if(Auth::user()->is_approved != 1 or Auth::user()->is_approved !=true)

    <div class="col-sm-3">
        <img width="200" src="assets/images/alarm_clock_time_bell_wait-512.png">
    </div>
    <div class="col-sm-9">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">Please see this instruction</h3>
            </div>
            <div class="panel-body">
                <strong class="text-danger">
                    Kindly contact to System Administrator or IT Help Desk officer to approve your account. <br/>
                    You will get all the available functionality once your account is approved!<br/><br/><br/>
                </strong>
                <strong>
                    Thank you!<br/>
                    ইসলামি আরবি বিশ্ববিদ্যালয়/Islamic Arabic University System
                </strong>
            </div>
        </div>
    </div>

@elseif(!empty(Auth::user()->delegate_to_user_id) && Auth::user()->delegate_to_user_id != 0)

    {{--@include('dashboard::delegated');--}}

@else
        <div class="col-sm-12 dashboardService">
            <div class="row">
                <div class="text-center">
                <?php
                   if(!empty($deshboardObject)){
                        foreach ($deshboardObject as $row) {
                            $div = 'dbobj_' . $row->db_obj_id;
                            ?>
                            <div class="col-md-4">
                                <?php
                                $para1 = DB::select(DB::raw($row->db_obj_para1));
                                switch ($row->db_obj_type) {
                                    case 'SCRIPT':
                                        ?>
                                        <div id="<?php echo $div; ?>" style="width: 380px; height: 350px; text-align:center;"><br /><br />Chart will be loading in 5 sec...</div>
                                        <?php
                                        $script = $row->db_obj_para2;
                                        $datav['charttitle'] = $row->db_obj_title;
                                        $datav['chartdata'] = json_encode($para1);
                                        $datav['baseurl'] = url();
                                        $datav['chartediv'] = $div;
                                        echo '<script type="text/javascript">' . CommonFunction::updateScriptPara($script, $datav) . '</script>';
                                        break;
                                    default:
                                        break;
                                }
                                ?>
                            </div>
                            <?php
                        }
                   }
                ?>
                </div>
            </div>


            <!--<div class="row">
                <!--Widget start-->
                {{--@if($widgetsGroup)
                    @foreach($widgetsGroup as $widget)
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-{{ !empty($widget->theme) ? $widget->theme :'default' }}">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="{!! !empty($widget->icon) ? $widget->icon : 'fa fa-th-list' !!} fa-3x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="h3">{!! $widget->value !!}</div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 text-right">
                                            <div>
                                                {!!$widget->caption!!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a href="apps/{{base64_encode($widget->url)}}" target="_blank">
                                    <div class="panel-footer">
                                        <span class="pull-left">{!!trans('messages.details')!!}</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                        </div>
                @endforeach
            @endif--}}
            <!--Widget End -->
            <!--</div>-->


            <!-- charts -->
            <div class="col-sm-1"></div>
            <div class="col-sm-12">
                @if(ACL::getAccsessRight('dashboard','N'))
                @if($notice)
                    <div class="row">
                        <?php
                        $arr = $notice;
                        echo '<table class="table basicDataTable">';
                        echo "<caption>Notice & Instructions:</caption><tbody>";
                        foreach ($arr as $value) {
                            echo "<tr><td width='150px'>$value->Date</td><td><span class='text-$value->importance'><a href='".url('support/view-notice/'.\App\Libraries\Encryption::encodeId($value->id))."'> <b>$value->heading</b></a></span></td></tr>";
                        }
                        echo '</tbody></table>';
                        ?>

                    </div>
            @endif
                @endif
            <!-- /.row -->
            </div>

            <div class="col-sm-1"></div>

            <!-- /.row -->
            <div class="row">
                <?php
                list($type, $code) = explode('x', Auth::user()->user_type);
                ?>
                {{--@include('dashboard::mis-info')--}}
            </div>
        </div>

    <div class="col-sm-1"></div>

@endif

@endsection
@section('footer-script')
    @include('partials.datatable-scripts')
    <script language="javascript">
        $(document).ready(function () {

            /*$('#_list').DataTable({
                "paging": true,
                "lengthChange": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "iDisplayLength": 50
            });*/
            $("#assign_form").validate({
                errorPlacement: function () {
                    return false;
                }
            });

            $(".send").click(function () {
                $('#division').addClass("required");
                $('#district').addClass("required");
                $('#user_id').addClass("required");
                $('#gk_status').removeClass("required");
                if (numberOfCheckedBox == 0)
                {
                    alert('Select Application');
                    return false;
                }

            });

            $(".process").click(function () {
                $('#division').removeClass("required");
                $('#district').removeClass("required");
                $('#user_id').removeClass("required");
                $('#gk_status').addClass("required");
                if (numberOfCheckedBox == 0)
                {
                    alert('Select Application');
                    return false;
                }

            });

            var _token = $('input[name="_token"]').val();
            $('#district').attr("disabled", true);

            $('#division').change(function () {
                var id = $(this).val();
                $('#district').html('<option value="">Please wait</option>');
                $.post('/apps/get-district', {id: id, _token: _token}, function (response) {

                    if (response.responseCode == 1) {
                        var option = '<option value="">Select District</option>';
                        if (response.data.length >= 1) {
                            $.each(response.data, function (id, value) {
                                option += '<option value="' + value.area_id + '">' + value.area_nm + '</option>';
                            });
                        }
                        $('#district').attr("disabled", false);
                        $('#district').html(option);
                        $('#district').trigger('change');
                        $('#user_id').attr("disabled", true);

                    } else {
                        $('#user_id').attr("disabled", true);
                        $('#district').html('<option value="">Select District</option>');
                    }
                });
            });

        });
        $('#user_id').attr("disabled", true);

        $("#district").on('change', function () {
            var _token = $('input[name="_token"]').val();
            var division = $('#division').val();
            var district = $(this).val();
            $.post('/apps/ajax/load-user-list', {district: district, division: division, _token: _token}, function (response) {

                if (response.responseCode == 1) {

                    var option = '';
                    option += '<option value="">Select One</option>';

                    $.each(response.data, function (id, value) {
                        option += '<option value="' + value.id + '">' + value.user_full_name + '</option>';
                    });
                    $('#user_id').attr("disabled", false);
                    $("#user_id").html(option);
                    $("#user_id").trigger("change");
                } else {
                    $('#user_id').attr("disabled", true);
                    $('#user_id').html('Please wait');
                }
            });

        });

        var numberOfCheckedBox = 0;

        function setCheckBox()
        {
            numberOfCheckedBox = 0;
            var flag = 1;
            var selectedWO = $("input[type=checkbox]").not(".selectall");
            selectedWO.each(function () {
                if (this.checked)
                {
                    numberOfCheckedBox++;
                }
                else
                {
                    flag = 0;
                }
            });

            if (flag == 1)
            {
                $("#chk_id").checked = true;
            }
            else {
                $("#chk_id").checked = false;
            }

        }


        function changeStatus(check)
        {
            setCheckBox();
        }

        var base_checkbox = '.selectall';
        $(base_checkbox).click(function () {
            if (this.checked) {
                // Iterate each checkbox
                $(':checkbox').each(function () {
                    this.checked = true;
                });
            } else {
                $(':checkbox').each(function () {
                    this.checked = false;
                });
            }
            setCheckBox();
            $('#status_id').html('<option selected="selected" value="">Select Below</option>');


        });
        $('input:checkbox').not(base_checkbox).click(function () {
            $(".selectall").prop("checked", false);
        });

    </script>
@endsection
