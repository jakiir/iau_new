<?php $row_sl = 0; ?>

@foreach($getList as $row)
<?php $row_sl++ ?>
<tr>
    <!--   
            @if (in_array($desk_id,array(1,2,3,4,8,9,10)))
            <td>
                {!! Form::checkbox('application[]',$row->record_id, '',['class'=>'appCheckBox', 'onChange' => 'changeStatus(this.checked)']) !!}
                {!! Form::hidden('hdn_batch[]',$row->status_id, ['class'=>'hdnStatus','id'=>"".$row->record_id."_status"]) !!}
          </td>
           @endif
    -->

    <td>
        @if(        ( ($row->status_id == 1 || $row->status_id == 13) && $desk_id == 1) ||
        ( $row->status_id == 6 && $desk_id == 2) ||
        ( ($row->status_id == 12 || $row->status_id == 2) && $desk_id == 8) ||
        ( $row->status_id == 9 && 0 == $row->sb_gk_verification_status  && ($desk_id == 3 || $desk_id == -9)) ||
        ( $row->status_id == 9 && $row->nsi_gk_verification_status == 0 && ($desk_id == 4 || $desk_id == -10))

        )
        {!! Form::checkbox('application[]',$row->record_id, '',['class'=>'appCheckBox', 'onChange' => 'changeStatus(this.checked)']) !!}
        {!! Form::hidden('hdn_batch[]',$row->status_id, ['class'=>'hdnStatus','id'=>"".$row->record_id."_status"]) !!}

        @endif
    </td>

    <td>{!! $row_sl !!}</td>
    <td>{!! $row->track_no !!}</td>
    <td>{!! $row->applicant_name !!} - {!! $row->nationality !!}</td>
    @if (!in_array($desk_id, array(3, 4, 6, 7,9,10)))
    <td>
        @if($row->desk_name == '') 
        Applicant
        @else 
        {!! $row->desk_name !!}
        @endif
    </td>
    @else
    <td>
        @if($row->desk_name == '')
            Applicant
        @else
            {!! $row->desk_name !!}
        @endif
    </td>
    @endif

    <td>
        @if(!empty($row->status_name))
            <span style="background-color:<?php echo $row->color;  ?>;color: #fff; font-weight: bold;" class="label btn-sm">
                                        {!! $row->status_name !!}
                                            </span>
        @else
            <span style="background-color:#dd4b39;color: #fff; font-weight: bold;" class="label btn-sm">
                                                   Draft
                                            </span>
        @endif
        @if(in_array($desk_id,array(3,4,9,10)) )
        {{--&& $row->user_full_name == '')--}}
        {{--{{Auth::user()->id}}--}}
        <?php
        $status_id = CommonFunction::getResultStatus($row->record_id, $desk_id);

//        $status_id = $row->result_status;
        if ($status_id > 0)
            echo "<br/><small><b>" . $resultList[$status_id] . " from Verifier</b></small>";
        ?>
        @endif
        @if((in_array(Auth::user()->desk_id, array(1,2,3,9,4,10))) || (!empty($delegated_desk) && ($desk_id == 1 || $desk_id == 2 || $desk_id == 8)))
        <?php // echo $row->sb_gk_verification_status . "sss"; ?>
        <span class="btn-toolbar">
            @if(Auth::user()->user_type != '5x505')


        </span>
        @endif

        @endif

        <!--                                    @if(Auth::user()->user_type != 5)
@if($row->status_id == 9) <br/><br/>
                                    @if($row->result_status == 2 || $row->result_status == 3)
        <span style="color: #fff; font-weight: bold;" class="label label-warning btn-sm">
        <?php $results = array('2' => 'Completed', '3' => 'Rejected'); ?>
{{  $results["$row->result_status"] }}
        </span>
        @endif
@endif
@endif-->

    </td>
    <td>{!! CommonFunction::updatedOn($row->created_at) !!}</td>
    <td>
        @if( $user_type == 5)
        @if( $row->status_id == 0)
        {{--{!! link_to('work-permit/edit/'. Encryption::encodeId($row->record_id),'Edit',['class' => 'btn btn-info btn-sm']) !!}--}}
        @endif
        {{--{!! link_to('/apps/view/'. Encryption::encodeId($row->record_id),'View',['class' => 'btn btn-xs btn-primary open']) !!}--}}
                <a href="{{url('apps/view/'.Encryption::encodeId($row->record_id))}}" class="btn btn-xs btn-primary open" ><i class="fa fa-folder-open-o"></i> View</a>
        @elseif( $user_type != 5)
        {!! link_to('apps/view/'. Encryption::encodeId($row->record_id),'Open',['class' => 'btn btn-primary btn-sm','target'=>'_blank']) !!}
        @if($row->status_id == 9 && !empty($row->certificate) && $desk_id == 1)
        {!! link_to(url().'/'. $row->certificate,'Acceptance Letter',['class' => 'btn btn-danger btn-sm','target'=>'_blank']) !!}
        @endif
        @endif

    </td>
</tr>
@endforeach
