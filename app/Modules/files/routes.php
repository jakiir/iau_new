<?php

Route::group(array('module' => 'files', 'namespace' => 'App\Modules\Files\Controllers'), function() {

    Route::post('files/store/{type}', 'FilesController@store');
    Route::get('files/get', 'FilesController@getFile');
    Route::post('files/test', 'FilesController@test');
    
});	