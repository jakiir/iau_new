<?php

namespace App\Modules\fazilPass\Controllers;

use App;
use App\Http\Controllers\Controller;
use App\Libraries\ACL;
use App\Libraries\CommonFunction;
use App\Libraries\Encryption;
use App\Modules\FazilPass\Models\FazilPass;
use App\Modules\Settings\Models\Area;
use App\Modules\FazilPass\Models\SubjectGroup;
use App\Modules\Users\Models\Users;
use App\Modules\Users\Models\UsersEditable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use View;
use yajra\Datatables\Datatables;

class FazilPassController extends Controller {

    public function __construct() {
        if (Session::has('lang'))
            App::setLocale(Session::get('lang'));
        ACL::db_reconnect();
    }
	
	public function showStudnetList(){
        $user_type = Auth::user()->user_type;
		
		if(empty(Session::get('examVal')) && empty(Session::get('typeVal')) && empty(Session::get('yearVal'))){
			return redirect('/dashboard');
		} else {
            $regi_no = Session::get('yearVal');
            $admission_year = "20" . $regi_no;
            $alimSecond = Session::get('yearVal') + 1;
            $admission_session = $admission_year . '-' . $alimSecond;
            if ($user_type == '2x202') {
                $username = Auth::user()->username;
                $whereCondition = ['tb1.madrasah_eiin' => $username, 'tb1.admission_session' => $admission_session, 'tb1.student_class' => Session::get('examVal')];
                $users = Users::join('fazilpass15 as tb1', 'tb1.madrasah_eiin', '=', 'users.username')
                    ->where($whereCondition)
                    ->orderBy('tb1.tot_serial_no', 'asc')
                    ->first(['users.id as userId']);
            }
            if ($user_type == '1x101') {
                $whereCondition = ['tb1.admission_session' => $admission_session, 'tb1.student_class' => Session::get('examVal')];
                $users = Users::leftJoin('fazilpass15 as tb1', 'tb1.madrasah_eiin', '=', 'users.username')
                    ->where($whereCondition)
                    ->orderBy('tb1.id', 'asc')
                    ->first(['users.id as userId']);
            }
            $all_institutes = ['' => 'All'] + Users::where('user_type','=','2x202')->orderBy('name', 'ASC')->lists('name', 'username')->all();

            return view('fazilPass::student-list', compact('users','all_institutes'));
        }
	}

    /**
     * @return mixed
     */
    public function showFazilPassList(){
        $user_type = Auth::user()->user_type;
        $mode = ACL::getAccsessRight('fazilPass', 'V');

        $regi_no = Session::get('yearVal');
        $admission_year = "20".$regi_no;
        $alimSecond = Session::get('yearVal') + 1;
        $admission_session = $admission_year.'-'.$alimSecond;

        if($user_type == '2x202'){
            $username = Auth::user()->username;
            $whereCondition = ['tb1.madrasah_eiin' => $username,'tb1.admission_session' => $admission_session ,'tb1.student_class' => Session::get('examVal')];
            $studentList = Users::leftJoin('fazilpass15 as tb1', 'tb1.madrasah_eiin', '=', 'users.username')
                ->where($whereCondition)
                ->get(['users.id as userId','tb1.unique_id_no','tb1.tot_serial_no','tb1.student_photo','tb1.student_name','tb1.fathers_name','tb1.mothers_name','tb1.subject_code','tb1.student_group','tb1.madrasah_eiin']);
        }
        if($user_type == '1x101'){
            $whereCondition = ['tb1.admission_session' => $admission_session,'tb1.student_class' => Session::get('examVal')];
            $studentList = Users::leftJoin('fazilpass15 as tb1', 'tb1.madrasah_eiin', '=', 'users.username')
                ->where($whereCondition)
                ->get(['users.id as userId','tb1.unique_id_no','tb1.tot_serial_no','tb1.student_photo','tb1.student_name','tb1.fathers_name','tb1.mothers_name','tb1.subject_code','tb1.student_group','tb1.madrasah_eiin']);

        }

        return Datatables::of($studentList)
            ->addColumn('checkbox', function ($studentList) use ($mode) {
                if ($mode) {
                    return '<input type="checkbox" name="student[]" value="'.$studentList->unique_id_no.'" onClick="eachSelect(this)" class="fa">';
                } else{
                    return '';
                }
            })->addColumn('student_pic', function ($studentList) use ($mode) {
                if ($mode) {
                    return '<img src="'.$studentList->student_photo.'" class="student_photo" height="32" width="32">';
                } else{
                    return '';
                }

            })->addColumn('student_uniqe_id', function ($studentList) use ($user_type) {
                if ($user_type == '1x101') {
                    return $studentList->unique_id_no;
                } else{
                    return $studentList->tot_serial_no;
                }
            })->editColumn('student_subject', function ($studentList) use ($mode) {
                if(!empty($studentList->subject_code)){
                    $subjectCode = array();
                    $subjectCodeDecode = json_decode($studentList->subject_code);
                    foreach($subjectCodeDecode as $key=>$eachYear){
                        if('3rd' == $key){
                            foreach($eachYear as $code=>$eachYearSubCode){
                                $subjectCode[] = $code;
                            }
                        }
                    }
                    $subCode = implode(",",$subjectCode);
                    return $subCode;
                } else{
                    return '';
                }
            })->addColumn('action', function ($studentList) use ($user_type) {
                if(Session::get('examVal') == 'fazilpass'){
                    $submited_status_col = 'submited_status_fazil3';
                }
                else if(Session::get('examVal') == 'fazilhons'){
                    $submited_status_col = 'submited_status_fazil4';
                } else {
                    $submited_status_col = 'submited_status_fazil3';
                }

                $output = '';
                if(Auth::user()->$submited_status_col != 'Yes') {
                    $output .='<a href="'.url('/manage-institute/edit-institute/'.Encryption::encodeId($studentList->userId)).'" class="btn btn-xs btn-primary open" ><i class="fa fa-pencil-square-o"></i> Edit</a>';
                    $output .=' <a href="'.url('/manage-institute/delete-institute/'.Encryption::encodeId($studentList->userId)).'"  onclick="return confirm(\'Are you sure you want to delete?\')" class="btn btn-xs btn-danger open" ><i class="fa fa-times"></i></a>';
                }
                if($user_type == '1x101') {
                    $output .=' <a href="'.url('/manage-institute/edit-institute/'.Encryption::encodeId($studentList->userId)).'" class="btn btn-xs btn-primary open" ><i class="fa fa-btn fa-print"></i> Print</a>';
                }
                return $output;
            })->make(true);
    }

    protected function addNewStudent(){
        $user_type = Auth::user()->user_type;

        if(Auth::user()->submited_status_fazil3 == 'Yes')
            die('no access right!');

        if(Session::get('application_on') == false && $user_type !== '1x101')
            die('You have no access right! Please contact with system admin if you have any query.');

        if (!ACL::getAccsessRight('fazilPass', 'A'))
            die('You have no access right! Please contact with system admin if you have any query.');


        $submited_status = '';
        if(empty(Session::get('examVal')) && empty(Session::get('typeVal')) && empty(Session::get('yearVal'))){
            return redirect('/dashboard');
        } else {
            $all_institutes = ['' => 'All'] + Users::where('user_type','=','2x202')->orderBy('name', 'ASC')->lists('name', 'username')->all();

            $username = Auth::user()->username;
            $lastTotSerialNo = FazilPass::where('madrasah_eiin',$username)->orderBy('tot_serial_no', 'desc')->pluck('tot_serial_no');
            if($lastTotSerialNo != null){
                $serial_nod = $lastTotSerialNo + 1;
                $esif_no = str_pad($serial_nod, 3, "0", STR_PAD_LEFT);
            } else {
                $esif_no = '001';
            }


            $allGroup = ["BTIS" => "BTIS","BA" => "BA","BSS" => "BSS","BSC" => "BSC","BBS" => "BBS"];

            return view('fazilPass::add-new-student', compact('all_institutes','allGroup','esif_no'));
        }
    }


    public function getStudentGroup(Request $request)
    {
        $studGroup = $request->get('group');
        if($studGroup ==''){
            $result = array('success' => false, 'msg' => 'This group has no subject');
        } else {

            $getGroupSubjects = SubjectGroup::leftJoin('subjects as tb1', 'tb1.subject_code', '=', 'subject_group.subject_code')
                ->where('subject_group.group',$studGroup)
                ->where('tb1.subject_year', 3)
                ->orderBy('subject_group.subject_code', 'asc')
                ->get(['subject_group.subject_code','tb1.subject_name']);

//            $subjects = array(
//                'NULL'=> array(
//                    'NULL'=>'NULL',
//                )
//            );

//            foreach($getGroupSubjects as $subject){
//                $subject_name = substr($subject->subject_name, 0, -10);
//                $subjectss .= $subject->subject_code;
//            }


            $result = array('success' => true, 'msg' => 'Get subject successfully','subjects'=>$getGroupSubjects,'ddd'=>$subjects);
        }
        return response()->json($result);
    }

    /*****************************End of Controller Class********************************/
}
