@extends('layouts.admin')

@section('content')

    <?php
    $accessMode = ACL::getAccsessRight('Institute');
    if (!ACL::isAllowed($accessMode, 'V')) {
        die('You have no access right! Please contact with system admin if you have any query.');
    }
    $memId = CommonFunction::getUserId();
    $user_type = CommonFunction::getUserType();
    $user_type_name = CommonFunction::getUserTypeName();
    ?>

	@include('partials.messages')

	<div class="col-lg-12">

		<div class="panel panel-primary">
			<div class="panel-heading">
				<i class="fa fa-paper-plane"></i>  <b>Add New Student</b>
			</div>

			<div class="panel-body">
                <?php
                if(!empty(Session::get('examVal')) && !empty(Session::get('typeVal')) && !empty(Session::get('yearVal'))) {
                if(Session::get('examVal') != 'fazilpass' || Session::get('typeVal') != 'esif' || Session::get('yearVal') != '15'){
                //return redirect('select-option'); ?>
				{{Session::get('examTxt').' '.Session::get('typeTxt').' - 20'.Session::get('yearVal')}} is not applicable. Please, select other option <a class="btn btn-warning" href="{{URL::to('/select-option')}}" alt="Back to select option" title="Back to select option">click here.</a>
                <?php
                } else {
                ?>
				{!! Form::open(array('url' => '/manage-institute/store-institute','method' => 'post', 'class' => 'form-horizontal stutentForms', 'id' => 'createInstituteForm',
                'enctype' =>'multipart/form-data', 'files' => 'true', 'role' => 'form')) !!}
				<input type="hidden" name="examVal" value="{{Session::get('examVal')}}" />
				<input type="hidden" name="typeVal" value="{{Session::get('typeVal')}}" />
				<input type="hidden" name="yearVal" value="{{Session::get('yearVal')}}" />
				<div class="studalert"></div>
				@if($user_type == '1x101')
					<fieldset class="institute">
						<legend><span> Institute Name </span></legend>
						<div class="col-md-12">
							<div class="form-group{{ $errors->has('institute_name') ? ' has-error' : '' }}">
								{!! Form::label('institute_name','Institute Name : ',['class'=>'col-md-2 required-star']) !!}
								<div class="col-md-6">
									{!! Form::select('institute_name', $all_institutes, null, ['class' => 'selectpicker form-control input-sm','data-live-search'=>true,'id'=>'all_institutes']) !!}
									@if ($errors->has('institute_name'))
										<span class="help-block">
												<strong>{{ $errors->first('institute_name') }}</strong>
											</span>
									@endif
								</div>
							</div>
						</div>
					</fieldset>
				@endif

				<fieldset class="studInfo">
					<legend><span> Dakhil - Alim Information </span></legend>
					<div class="col-md-12">
						<div class="form-group">
							<div class="col-md-6 {{ $errors->has('dakhil_passing_year_no') ? ' has-error' : '' }}">
								{!! Form::label('dakhil_passing_year_no','Dakhil passing year : ',['class'=>'col-md-4 required-star']) !!}
								<div class="col-md-6">
									<?php $dakhilPasYe = array(
										'13' => '2013',
										'12' => '2012',
										'11' => '2011',
										'10' => '2010',
										'09' => '2009',
										'08' => '2008',
										'07' => '2007',
										'06' => '2006',
										'05' => '2005',
										'04' => '2004',
										'03' => '2003',
										'02' => '2002',
										'01' => '2001',
										'00' => '2000',
										'-99' => '1999',
										'-98' => '1998',
										'-97' => '1997',
										'-96' => '1996',
										'-95' => '1995',
									);
									?>
									<select class="form-control input-sm" name="dakhil_passing_year_no" id="dakhil_passing_year_no" value="{{ old('dakhil_passing_year_no') }}">
										@foreach($dakhilPasYe as $key=>$dakhilPasY)
											<?php $selecte =  ( old('dakhil_passing_year_no') == $key) ? 'selected=selected': null  ?>
											<option {{$selecte}} value="{{$key}}">{{$dakhilPasY}}</option>
										@endforeach
									</select>

									@if ($errors->has('dakhil_passing_year_no'))
										<span class="help-block">
											<strong>{{ $errors->first('dakhil_passing_year_no') }}</strong>
										</span>
									@endif
								</div>
							</div>
							<div class="col-md-6 {{ $errors->has('dakhil_roll_no') ? ' has-error' : '' }}">
								{!! Form::label('dakhil_roll_no','Dakhil roll : ',['class'=>'col-md-4 required-star']) !!}
								<div class="col-md-6">
									<input type="text" class="form-control input-sm" name="dakhil_roll_no" id="dakhil_roll_no" value="{{ old('dakhil_roll_no') }}">

									@if ($errors->has('dakhil_roll_no'))
										<span class="help-block">
											<strong>{{ $errors->first('dakhil_roll_no') }}</strong>
										</span>
									@endif
								</div>
							</div>
						</div>
					</div>
					<input type="hidden" name="dakhil_passing_year" id="dakhil_passing_year" value="" />
					<input type="hidden" name="dakhil_roll" id="dakhil_roll" value="" />
					<input type="hidden" name="dakhil_result" id="dakhil_result" value="" />
					<input type="hidden" name="dakhil_reg_no" id="dakhil_reg_no" value="" />
					<div class="col-md-12">
						<div class="form-group">
							<div class="col-md-6 {{ $errors->has('alim_passing_year_no') ? ' has-error' : '' }}">
								{!! Form::label('alim_passing_year_no','Alim passing year : ',['class'=>'col-md-4 required-star']) !!}
								<div class="col-md-6">
									<?php
									$alimPasYe = array(
										14 => 2014,
										15 => 2015
									);
									?>
									<select class="form-control input-sm" name="alim_passing_year_no" id="alim_passing_year_no" value="{{ old('alim_passing_year_no') }}">
										@foreach($alimPasYe as $key=>$alimPasY)
											<?php $selecte =  ( old('alim_passing_year_no') == $key) ? 'selected=selected': null  ?>
											<option {{$selecte}} value="{{$key}}">{{$alimPasY}}</option>
										@endforeach
									</select>

									@if ($errors->has('alim_passing_year_no'))
										<span class="help-block">
												<strong>{{ $errors->first('alim_passing_year_no') }}</strong>
											</span>
									@endif
								</div>
							</div>
							<div class="col-md-6 {{ $errors->has('alim_roll_no') ? ' has-error' : '' }}">
								{!! Form::label('alim_roll_no','Alim roll : ',['class'=>'col-md-4 required-star']) !!}
								<div class="col-md-6">
									<input type="text" class="form-control input-sm" name="alim_roll_no" id="alim_roll_no" value="{{ old('alim_roll_no') }}">

									@if ($errors->has('alim_roll_no'))
										<span class="help-block">
												<strong>{{ $errors->first('alim_roll_no') }}</strong>
											</span>
									@endif
								</div>
							</div>
						</div>
					</div>
					<input type="hidden" name="alim_passing_year" id="alim_passing_year" value="" />
					<input type="hidden" name="alim_roll" id="alim_roll" value="" />
					<input type="hidden" name="alim_result" id="alim_result" value="" />
					<input type="hidden" name="alim_reg_no" id="alim_reg_no" value="" />

					<div class="form-group">
						<div class="col-md-8 col-md-offset-2">
							<a class="btn btn-primary" id="check_btn" style="color:#fff;" href="javascript:void(0)" onclick="check_dakhil_info()">
								<i class="fa fa-btn fa-search"></i> Check
							</a>
						</div>
					</div>
				</fieldset>


				<input type="hidden" name="addedBy" value="{{Auth::user()->id}}" />
				<!-- Ahsan End -->
				<fieldset class="first">
					<legend>Student Information</legend>

					<div class="col-md-6">
						<div class="form-group {{$errors->has('tot_serial_no') ? 'has-error' : ''}}">
							{!! Form::label('tot_serial_no','eSIF Serial No. : ',['class'=>'col-md-4 required-star']) !!}
							<div class="col-md-8">
								{!! Form::text('tot_serial_no', $esif_no, ['class'=>'form-control required input-sm', 'placeholder' => 'e.g. eSIF Serial No','readonly'=>true]) !!}
								{!! $errors->first('tot_serial_no','<span class="help-block">:message</span>') !!}
							</div>
						</div>
						<div class="form-group {{$errors->has('student_name') ? 'has-error' : ''}}">
							{!! Form::label('student_name',"Student's Name : ",['class'=>'col-md-4 required-star']) !!}
							<div class="col-md-8">
								{!! Form::text('student_name', null, ['class'=>'form-control required input-sm', 'placeholder' => "e.g. Student's Name"]) !!}
								{!! $errors->first('student_name','<span class="help-block">:message</span>') !!}
							</div>
						</div>

						<div class="form-group {{$errors->has('father_name') ? 'has-error' : ''}}">
							{!! Form::label('father_name',"Father's Name : ",['class'=>'col-md-4 required-star']) !!}
							<div class="col-md-8">
								{!! Form::text('father_name', null, ['class'=>'form-control required input-sm', 'placeholder' => "e.g. Father's Name"]) !!}
								{!! $errors->first('father_name','<span class="help-block">:message</span>') !!}
							</div>
						</div>

						<div class="form-group {{$errors->has('mother_name') ? 'has-error' : ''}}">
							{!! Form::label('mother_name',"Mother's Name : ",['class'=>'col-md-4 required-star']) !!}
							<div class="col-md-8">
								{!! Form::text('mother_name', null, ['class'=>'form-control required input-sm', 'placeholder' => "e.g. Mother's Name"]) !!}
								{!! $errors->first('mother_name','<span class="help-block">:message</span>') !!}
							</div>
						</div>

						<div class="form-group {{$errors->has('gender') ? 'has-error' : ''}}">
							{!! Form::label('gender',"Gender : ",['class'=>'col-md-4 required-star']) !!}
							<div class="col-md-8">
								{!! Form::select('gender', ['Male'=>'Male','Female'=>'Female','Both'=>'Both'], null, ['class' => 'form-control input-sm required']) !!}
								{!! $errors->first('gender','<span class="help-block">:message</span>') !!}
							</div>
						</div>

						<div class="form-group {{$errors->has('dob') ? 'has-error' : ''}}">
							{!! Form::label('dob',"Date of Birth : ",['class'=>'col-md-4 required-star']) !!}
							<div class="col-md-8">
								{!! Form::text('dob', null, ['class'=>'form-control required input-sm', 'placeholder' => 'e.g. 01/01/1900']) !!}
								{!! $errors->first('dob','<span class="help-block">:message</span>') !!}
							</div>
						</div>

						<div class="form-group {{$errors->has('student_group') ? 'has-error' : ''}}">
							{!! Form::label('student_group',"Group : ",['class'=>'col-md-4 required-star']) !!}
							<div class="col-md-8">
								{!! Form::select('group', $allGroup, null, ['class' => 'selectpicker form-control required','data-live-search'=>true,'id'=>'studGroupId','onchange'=>'studGroup(this)']) !!}
								{!! $errors->first('student_group','<span class="help-block">:message</span>') !!}
							</div>
						</div>

					</div>
					<div class="col-xs-12 col-md-6 col-sm-6">
						<div class="well well-sm">
							<div class="row">
								<div class="col-sm-6 col-md-4">
									<img src="{{URL::to('assets/images/no_image.png')}}" class="student-image img-responsive img-rounded student_image"
										 alt="Student Images" id="student_image" width="200"/>

								</div>
								<div class="col-sm-6 col-md-8">
									<h4 class="required-star">Student Images</h4>
									<small>
										<cite title="[File Format: *.jpg / *.png, File size(3-25)KB]">
											<label class="col-lg-8 text-left">
                                                <span style="font-size: 9px; color: grey">[File Format: *.jpg / *.png, File size(3-25)KB]</span>
											</label>
										</cite>
									</small>
									<div class="clearfix"><br/></div>
									<small>
										<cite title="[File Format: *.jpg / *.png, File size(3-25)KB]">
											<label class="col-lg-8 text-left">
												<span id="student_image_error" class="text-danger" style="font-size: 10px;"></span>
											</label>
										</cite>
									</small>
									<div class="clearfix"><br/></div>
									<label class=" btn btn-primary btn-file {{ $errors->has('student_image')?'has-error':'' }}">
										<i class="fa fa-picture-o" aria-hidden="true"> </i> Browse
										<input class="required" type="file" onchange="readURLStudent(this);" name="student_image" data-type="student_image"
											   data-ref="{{Encryption::encodeId(Auth::user()->id)}}">
										{!! $errors->first('student_image','<span class="help-block">:message</span>') !!}
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<fieldset>
						<legend>Registered Subjects</legend>
						<div id="subject_list">
							<div class="studGroup"></div>
							<div id="groupSubjects">
								<h2 class="groupTitle">BTIS</h2><div class="checkbox checkbox-primary checkbox-inline"><input class="subjectGroup" type="checkbox" name="subjects[]" id="inlineCheckbox0" value="407-Higher Arabic 1st Paper, 408-Higher Arabic 2nd Paper, 409-Higher Arabic 3rd Paper"><label for="inlineCheckbox0"> 407, 408, 409. Higher Arabic </label></div><div class="checkbox checkbox-primary checkbox-inline"><input class="subjectGroup" type="checkbox" name="subjects[]" id="inlineCheckbox1" value="413-Islamic History 1st Paper, 414-Islamic History 2nd Paper, 415-Islamic History 3rd Paper"><label for="inlineCheckbox1"> 413, 414, 415. Islamic History </label></div><div class="checkbox checkbox-primary checkbox-inline"><input class="subjectGroup" type="checkbox" name="subjects[]" id="inlineCheckbox2" value="416-Islamic Studies 1st Paper, 417-Islamic Studies 2nd Paper, 418-Islamic Studies 3rd Paper"><label for="inlineCheckbox2"> 416, 417, 418. Islamic Studies </label></div><div class="checkbox checkbox-primary checkbox-inline"><input class="subjectGroup" type="checkbox" name="subjects[]" id="inlineCheckbox3" value="419-Comparative Religion 1st Paper, 420-Comparative Religion 2nd Paper, 421-Comparative Religion 3rd Paper"><label for="inlineCheckbox3"> 419, 420, 421. Comparative Religion </label></div><div class="checkbox checkbox-primary checkbox-inline"><input class="subjectGroup" type="checkbox" name="subjects[]" id="inlineCheckbox4" value="422-Islamic Philosophy And Tasaud 1st Paper, 423-Islamic Philosophy And Tasaud 2nd Paper, 424-Islamic Philosophy And Tasaud 3rd Paper"><label for="inlineCheckbox4"> 422, 423, 424. Islamic Philosophy And Tasaud </label></div><div class="checkbox checkbox-primary checkbox-inline"><input class="subjectGroup" type="checkbox" name="subjects[]" id="inlineCheckbox5" value="428-Ad Dawah Al Islamia 1st Paper, 429-Ad Dawah Al Islamia 2nd Paper, 430-Ad Dawah Al Islamia 3rd Paper"><label for="inlineCheckbox5"> 428, 429, 430. Ad Dawah Al Islamia </label></div><div class="checkbox checkbox-primary checkbox-inline"><input class="subjectGroup" type="checkbox" name="subjects[]" id="inlineCheckbox6" value="437-Islamic Economics 1st Paper, 438-Islamic Economics 2nd Paper, 439-Islamic Economics 3rd Paper"><label for="inlineCheckbox6"> 437, 438, 439. Islamic Economics </label></div>
							</div>
						</div>
					</fieldset>
				</fieldset>
				<div class="col-md-12">
					<a href="{{ url('/fazil-pass/show-student-list') }}">
						{!! Form::button('<i class="fa fa-times"></i> Close', array('type' => 'button', 'class' => 'btn btn-default')) !!}
					</a>
					<button type="submit" class="btn btn-success pull-right">
						<i class="fa fa-chevron-circle-right"></i> <b>Save</b></button>
				</div>
				{!! Form::close() !!}<!-- /.form end -->
                <?php } ?>
                <?php   } else { ?>
				<script type="text/javascript"> window.location.href = "{{URL::to('/select-option')}}"; </script>
                <?php } ?>

				<div class="overlay" style="display: none;">
					<i class="fa fa-refresh fa-spin"></i>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('footer-script')
	<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>
	@include('partials.datatable-scripts')
	<script language="javascript">
        $(document).ready(function() {
            $("#createInstituteForm").validate({
                errorPlacement: function () {
                    return false;
                }
            });
        });

        function studGroup(THIS){
            $('.studGroup').html('').removeClass('alert alert-danger alert-success');
            $('#groupSubjects').html('');
            var thisId = THIS.id;
            var thisVal = THIS.value;
            $('.group_preload').html('<i style="margin-left:10px;" class="fa fa-refresh fa-spin"></i>');
            var url = '{{route("ajaxGetStudentGroup")}}';
            if(thisVal == 'BTIS' || thisVal == 'BA'){
                var inPutClass = 'subjectGroup';
            } else {
                var inPutClass = 'subjectGroup3';
            }
            $.ajax({
                url: url,
                type: 'POST',
                data : {group:thisVal, "_token":"{{ csrf_token() }}"},
                dataType: 'JSON',
                success: function (response) {
                    if(response.success == true){
                        $('#groupSubjects').append('<h2 class="groupTitle">'+thisVal+'</h2>');
                        $i=0;
                        for (var prop in response.subjects) {
                            $('#groupSubjects').append('<div class="checkbox checkbox-primary checkbox-inline"><input class="'+inPutClass+'" type="checkbox" name="subjects[]" id="inlineCheckbox'+$i+'" value="'+response.subjects[prop]['subject_code']+'"><label for="inlineCheckbox'+$i+'"> '+response.subjects[prop]['subject_name']+' </label></div>');
//                            for (var indexd in response.subjects[prop]) {
//                                $('#groupSubjects').append('<div class="checkbox checkbox-primary checkbox-inline"><input class="'+inPutClass+'" type="checkbox" name="subjects[]" id="inlineCheckbox'+$i+'" value="'+indexd+'"><label for="inlineCheckbox'+$i+'"> '+response.subjects[prop][indexd]+' </label></div>');
//                                $i++;
//                            }
                            $i++;
                        }
                    } else {
                        $('.studGroup').addClass('alert alert-danger').html('<strong>Warning!</strong> '+response.msg);
                    }
                    $('.group_preload').html('');
                }
            });
            return false;
        }


        function readURLStudent(input) {
            if (input.files && input.files[0]) {
                $("#student_image_error").html('');
                var mime_type = input.files[0].type;
                if(!(mime_type=='image/jpeg' || mime_type=='image/jpg' || mime_type=='image/png')){
                    $("#student_image_error").html("Image format is not valid. Only PNG or JPEG or JPG type images are allowed.");
                    return false;
                }
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#student_image').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

	</script>
@endsection