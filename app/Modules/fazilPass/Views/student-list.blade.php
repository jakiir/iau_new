@extends('layouts.admin')

@section('content')
    <?php
    $accessMode = ACL::getAccsessRight('Institute');
    if (!ACL::isAllowed($accessMode, 'V')) {
        die('You have no access right! Please contact with system admin if you have any query.');
    }
    $memId = CommonFunction::getUserId();
    $user_type = CommonFunction::getUserType();
    $user_type_name = CommonFunction::getUserTypeName();
    ?>

	<section class="content" xmlns="http://www.w3.org/1999/html">
		<div class="box">
			<div class="box-body">
				@include('partials.messages')
				<div class="col-lg-12">
					<div class="panel panel-primary">

						<div class="panel-heading">
							<div class="row">
								<div class="col-lg-6">
									<div class="btn" style="color:#ffffff">
										<i class="fa fa-list"></i>  <strong>
											List of students
											@if ($user_type == '2x202')
												[ {{Auth::user()->name}} ]</strong>
											@endif
									</div>
								</div>
								<div class="col-lg-6 text-right">

									<a href="{{ url('/manage-student/fazil3-esif') }}" class="btn btn-default"><i class="fa fa-plus"></i> New Student</a>
									<a href="javascript:void(0)" onClick="printStudentList()" class="btn btn-default">
										<i class="fa fa-btn fa-print"></i> Print Student List
									</a>
                                    <?php
										if(Session::get('examVal') == 'fazilpass'){
											$submited_status_col = 'submited_status_fazil3';
										}
										else if(Session::get('examVal') == 'fazilhons'){
											$submited_status_col = 'submited_status_fazil4';
										} else {
											$submited_status_col = 'submited_status_fazil3';
										}
                                    ?>
									@if($user_type == '2x202')
										<a href="javascript:void(0)" @if(Auth::user()->$submited_status_col == 'No') onClick="submitAllStudent(@if(!empty($users->userId)){{$users->userId}}@endif)" @endif class="btn btn-default submitAllStudentBtn" @if(Auth::user()->$submited_status_col == 'Yes') disabled @endif style="color:#fff;">
											<i class="fa fa-btn fa-paper-plane"></i>Submit All Students
										</a>
									@endif
								</div>
							</div>
						</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							{!! Form::open(['url' => '#', 'method' => 'get', 'class' => 'form apps_from', 'id' => 'batch_from', 'role' => 'form','enctype' =>'multipart/form-data', 'files'=>true]) !!}
							<input type="hidden" id="printLimit" value=""/>
							@if($user_type == '1x101')
								<div class="actions- bulkactions getAllInstitues" style="margin:5px 0;">
                                    <?php
                                    $selectedInis = (!empty($_GET['inis']) ? $_GET['inis'] : '');
                                    if(empty($selectedInis)){
                                        $selectedInis = (!empty($madrasahId) ? $madrasahId : '');
                                    }
                                    ?>
									{!! Form::select('all_institutes', $all_institutes, $selectedInis, ['class' => 'form-control- input-sm- required selectpicker','data-live-search'=>true,'id'=>'all_institutes']) !!}
									<a id="" href="javascript:void(0)" class="btn btn-sm btn-primary action" onClick="searchStudentByInist()">Search</a>
								</div>

								<div class="tablePrivate top">
									<div class="alignleft actions bulkactions">
										<select name="action" id="bulk-action-selector-top" class="input-sm">
											<option value="">Bulk Actions</option>
											<!-- <option value="delete">Delete</option> -->
											<option value="print_all_student">Print All Student</option>
											<option value="print_regi">Print Regi Card</option>
										</select>
										<a id="doaction" class="btn btn-sm btn-primary action" onClick="bulkAction('bulk-action-selector-top')">Apply</a>
									</div>
								</div>
							@endif

							<div class="table-responsive">
								<table id="studentList" class="table table-striped table-bordered dt-responsive " cellspacing="0" width="100%">
									<thead>
									<tr>
										<th>
											<input type="checkbox" name="selectAll" onClick="selectAllStudent(this)" class="fa">
										</th>
										<th>Image</th>
										@if($user_type == '1x101')
											<th>Unique Id</th>
										@else
											<th>Tot SN</th>
										@endif
										<th>EIIN</th>
										<th>Student Name</th>
										<th>Father</th>
										<th>Mother</th>
										<th>Subject Code</th>
										<th>Group</th>
										<th>Actions</th>
									</tr>

									</thead>
									<tbody>

									</tbody>

									<tfoot>
									<tr>
										<th>
											<input type="checkbox" name="selectAll" onClick="selectAllStudent(this)" class="fa">
										</th>
										<th>Image</th>
										@if($user_type == '1x101')
											<th>Unique Id</th>
										@else
											<th>Tot SN</th>
										@endif
										<th>EIIN</th>
										<th>Student Name</th>
										<th>Father</th>
										<th>Mother</th>
										<th>Subject Code</th>
										<th>Group</th>
										<th>Actions</th>
									</tr>
									</tfoot>
								</table>
							</div><!-- /.table-responsive -->
							{!! Form::close() !!}
						</div><!-- /.panel-body -->
					</div><!-- /.panel panel-red -->
				</div><!-- /.col-lg-12 -->
			</div><!-- /.box-body -->
		</div><!-- /.box -->
	</section>
	<style>
		@if($user_type == '1x101')
			.tablePrivate{
			vertical-align: middle;
			position: absolute;
			overflow: hidden;
			z-index: 1;
		}
		.dataTables_length{
			float:right;
		}
		@else
			.tablePrivate{
			clear: both;
			height: 30px;
			margin: 6px 0 4px;
			vertical-align: middle;
		}
		@endif
	</style>
@endsection
@section('footer-script')
	<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>
	@include('partials.datatable-scripts')
	<script type="text/javascript">
        $(function () {
//        $('#list').DataTable({
//            "paging": true,
//            "lengthChange": true,
//            "ordering": false,
//			"orderable": false,
//            "info": false,
//            "autoWidth": false,
//			"iDisplayLength": 10,
//			"fnDrawCallback": function( oSettings ) {
//				$('#printLimit').val(oSettings._iDisplayStart);
//			}
//        });


            studentList = $('#studentList').DataTable({
                processing: true,
                serverSide: true,
                "iDisplayLength": 50,
                "fnDrawCallback": function( oSettings ) {
                    $('#printLimit').val(oSettings._iDisplayStart);
                },
                ajax: {
                    url: '{{route("showFazilPassLists")}}',
                    method:'post',
                    data: function (d) {
                        d._token = $('input[name="_token"]').val();
                    }
                },
                columns: [
                    {data: 'checkbox', name: 'checkbox', orderable: false, searchable: false},
                    {data: 'student_pic', name: 'student_pic'},
                    {data: 'student_uniqe_id', name: 'student_uniqe_id'},
                    {data: 'madrasah_eiin', name: 'madrasah_eiin'},
                    {data: 'student_name', name: 'student_name'},
                    {data: 'fathers_name', name: 'fathers_name'},
                    {data: 'mothers_name', name: 'mothers_name'},
                    {data: 'student_subject', name: 'student_subject'},
                    {data: 'student_group', name: 'student_group'},
                    {data: 'action', name: 'action'}
                ],
                "aaSorting": []
            });
        });

        function searchStudentByInist() {
            var all_institutes = $('#all_institutes option:selected').val();
            studentList.column( 7 ).search( all_institutes ).draw();
        }

        function selectAllStudent(source) {
            checkboxes = document.getElementsByName('student[]');
            for(var i=0, n=checkboxes.length;i<n;i++) {
                checkboxes[i].checked = source.checked;
            }
            var selectAlld = document.getElementsByName('selectAll');
            if (selectAlld[1].checked){
                selectAlld[0].checked = source.checked;
                selectAlld[1].checked = source.checked;
            } else {
                selectAlld[0].checked = source.checked;
                selectAlld[1].checked = source.checked;
            }
        }

        function eachSelect(source){
            var selectAlld = document.getElementsByName('selectAll');
            if (selectAlld[1].checked == true || selectAlld[1].checked == true){
                selectAlld[0].checked = false;
                selectAlld[1].checked = false;
            }
            var checkboxesCount = document.getElementsByName('student[]').length;
            var checkedboxesCount = document.querySelectorAll('input[name="student[]"]:checked').length;
            if(checkboxesCount == checkedboxesCount){
                selectAlld[0].checked = true;
                selectAlld[1].checked = true;
            }
        }

	</script>
@endsection