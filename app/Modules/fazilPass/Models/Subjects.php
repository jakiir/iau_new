<?php

namespace App\Modules\FazilPass\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Auth;

class Subjects extends Model {

    protected $table = 'subjects';
    protected $fillable = array(
        'id',
        'subject_code',
        'subject_name',
        'subject_year'
    );
    /******************************* Subjects Model Class ends here ************************* */
}
