<?php

namespace App\Modules\FazilPass\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Auth;

class FazilPass extends Model {

    protected $table = 'fazilpass15';
    protected $fillable = array(
        'id',
        'unique_id_no',
        'tot_serial_no',
        'registration_no',
        'admission_session',
        'student_name',
        'fathers_name',
        'mothers_name',
        'student_class',
        'student_sex',
        'student_dob',
        'student_group',
        'subject_code',
        'student_photo',
        'dhakhil_alim_info',
        'madrasah_eiin',
        'created_by',
        'updated_by'
    );

    public static function boot() {
        parent::boot();
        // Before update
        static::creating(function($post) {
            if (Auth::guest()) {
                $post->created_by = 0;
                $post->updated_by = 0;
            } else {
                $post->created_by = Auth::user()->id;
                $post->updated_by = Auth::user()->id;
            }
        });

        static::updating(function($post) {
            if (Auth::guest()) {
                $post->updated_by = 0;
            } else {
                $post->updated_by = Auth::user()->id;
            }
        });
    }

    /*
     *
     * ALTER TABLE `fazilpass15`
ADD `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
ADD `created_by` int(11) NULL DEFAULT '0' AFTER `created_at`,
ADD `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `created_by`,
ADD `updated_by` int(11) NULL DEFAULT '0' AFTER `updated_at`;
     * */

    /*     * ***************************** FazilPass Model Class ends here ************************* */
}
