<?php

namespace App\Modules\FazilPass\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Auth;

class SubjectGroup extends Model {

    protected $table = 'subject_group';
    protected $fillable = array(
        'id',
        'subject_code',
        'group',
        'group_type'
    );
    /******************************* SubjectGroup Model Class ends here ************************* */
}
