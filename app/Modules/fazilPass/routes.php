<?php

Route::group(array('module' => 'fazilPass', 'middleware' => 'auth', 'namespace' => 'App\Modules\fazilPass\Controllers'), function() {

	Route::get('/fazil-pass/show-student-list', 'FazilPassController@showStudnetList');
    Route::post('/ajaxStudentLists', ['as'=> 'showFazilPassLists', 'uses'=>'FazilPassController@showFazilPassList']);

    //Student add
    Route::get('/fazil-pass/add-new-student', 'FazilPassController@addNewStudent');

    Route::post('/getStdgroup', ['as'=> 'ajaxGetStudentGroup', 'uses'=>'FazilPassController@getStudentGroup']);

    Route::resource('fazil-pass','FazilPassController');
    
    /******************************End of Route Group***********************************/
});