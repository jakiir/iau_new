<?php

namespace App\Modules\Institute\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Support\Facades\Auth;

class Institutes extends Model {

    protected $table = 'institutes';
    protected $fillable = array(
        'id',
        'user_id',
        'institute_name',
        'madrasah_eiin',
        'institute_code',
        'highest_class',
        'division',
        'division_code',
        'zila',
        'zila_code',
        'thana_u_zila',
        'thana_u_zila_code',
        'center_name',
        'post_office',
        'principal_name',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by'
    );

    public static function boot() {
        parent::boot();
        // Before update
        static::creating(function($post) {
            if (Auth::guest()) {
                $post->created_by = 0;
                $post->updated_by = 0;
            } else {
                $post->created_by = Auth::user()->id;
                $post->updated_by = Auth::user()->id;
            }
        });

        static::updating(function($post) {
            if (Auth::guest()) {
                $post->updated_by = 0;
            } else {
                $post->updated_by = Auth::user()->id;
            }
        });
    }

    /*     * ***************************** Institutes Model Class ends here ************************* */
}
