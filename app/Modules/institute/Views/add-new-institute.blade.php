@extends('layouts.admin')

@section('content')

    <?php
    $accessMode = ACL::getAccsessRight('Institute');
    if (!ACL::isAllowed($accessMode, 'V')) {
        die('You have no access right! Please contact with system admin if you have any query.');
    }
    $memId = CommonFunction::getUserId();
    $user_type = CommonFunction::getUserType();
    $user_type_name = CommonFunction::getUserTypeName();
    ?>

    @include('partials.messages')

    <div class="col-lg-12">

        <div class="panel panel-primary">
            <div class="panel-heading">
                <i class="fa fa-university"></i>  <b>Add New Institute</b>
            </div>

            <div class="panel-body">
                {!! Form::open(array('url' => '/manage-institute/store-institute','method' => 'post', 'class' => 'form-horizontal', 'id' => 'createInstituteForm',
                'enctype' =>'multipart/form-data', 'files' => 'true', 'role' => 'form')) !!}
                <div class="col-md-12">
                    <div class="form-group {{$errors->has('institute_name') ? 'has-error' : ''}}">
                        {!! Form::label('institute_name','Madrasah Name : ',['class'=>'col-md-3  required-star']) !!}
                        <div class="col-md-5">
                            {!! Form::text('institute_name', null, ['class'=>'form-control required input-sm', 'placeholder' => 'e.g. Institute Name']) !!}
                            {!! $errors->first('institute_name','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>

                    <div class="form-group {{$errors->has('username') ? 'has-error' : ''}}">
                        {!! Form::label('username','EIIN : ',['class'=>'col-md-3  required-star']) !!}
                        <div class="col-md-5">
                            {!! Form::text('username', null, ['class'=>'form-control required input-sm', 'placeholder' => 'e.g. 123456']) !!}
                            {!! $errors->first('username','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>

                    <div class="form-group {{$errors->has('institute_code') ? 'has-error' : ''}}">
                        {!! Form::label('institute_code','Madrasah Code : ',['class'=>'col-md-3  required-star']) !!}
                        <div class="col-md-5">
                            {!! Form::text('institute_code', null, ['class'=>'form-control required input-sm', 'placeholder' => 'e.g. 12345']) !!}
                            {!! $errors->first('institute_code','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>

                    <div class="form-group {{$errors->has('highest_class') ? 'has-error' : ''}}">
                        {!! Form::label('highest_class','Highest class : ',['class'=>'col-md-3 required-star']) !!}
                        <div class="col-md-5">
                            {!! Form::select('highest_class', $highest_class, null, ['class' => 'form-control required input-sm']) !!}
                            {!! $errors->first('highest_class','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>

                    <div class="form-group {{$errors->has('division_code') ? 'has-error' : ''}}">
                        {!! Form::label('division_code',"Institute's Division : ",['class'=>'col-md-3 required-star']) !!}
                        <div class="col-md-5">
                            {!! Form::select('division_code', $divisions, null, ['class' => 'form-control required input-sm']) !!}
                            {!! $errors->first('division_code','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>

                    {!! Form::hidden('division', null, ['id' => 'division']) !!}

                    <div class="form-group {{$errors->has('zila_code') ? 'has-error' : ''}}">
                        {!! Form::label('zila_code','Select Zilla : ',['class'=>'col-md-3  required-star']) !!}
                        <div class="col-md-5">
                            {!! Form::select('zila_code',[], null, ['class' => 'selectpicker- form-control required input-sm', 'placeholder' => 'Select Zilla','data-live-search'=>true]) !!}
                            {!! $errors->first('zila_code','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>

                    {!! Form::hidden('zila', null, ['id' => 'zila']) !!}

                    <div class="form-group {{$errors->has('thana_u_zila_code') ? 'has-error' : ''}}">
                        {!! Form::label('thana_u_zila_code','Thana / U. Zilla',['class'=>'col-md-3  required-star']) !!}
                        <div class="col-md-5">
                            {!! Form::select('thana_u_zila_code',[], null, ['class' => 'selectpicker- form-control required input-sm', 'placeholder' => 'Select Thana','data-live-search'=>true,'onchange'=>"document.getElementById('thana_u_zila').value=this.options[this.selectedIndex].text"]) !!}
                            {!! $errors->first('thana_u_zila_code','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                    {!! Form::hidden('thana_u_zila', null, ['id' => 'thana_u_zila']) !!}

                    <div class="form-group {{$errors->has('center_name') ? 'has-error' : ''}}">
                        {!! Form::label('center_name','Center Name : ',['class'=>'col-md-3']) !!}
                        <div class="col-md-5">
                            {!! Form::text('center_name', null, ['class'=>'form-control input-sm', 'placeholder' => 'e.g. dhaka']) !!}
                            {!! $errors->first('center_name','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>

                    <div class="form-group {{$errors->has('post_office') ? 'has-error' : ''}}">
                        {!! Form::label('post_office','Post Office : ',['class'=>'col-md-3 required-star']) !!}
                        <div class="col-md-5">
                            {!! Form::text('post_office', null, ['class'=>'form-control input-sm required', 'placeholder' => 'e.g. 1219']) !!}
                            {!! $errors->first('post_office','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>

                    <div class="form-group {{$errors->has('principal_name') ? 'has-error' : ''}}">
                        {!! Form::label('principal_name','Principal Name : ',['class'=>'col-md-3 required-star']) !!}
                        <div class="col-md-5">
                            {!! Form::text('principal_name', null, ['class'=>'form-control input-sm required', 'placeholder' => 'e.g. Principal Name']) !!}
                            {!! $errors->first('principal_name','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>

                    <div class="form-group {{$errors->has('user_phone') ? 'has-error' : ''}}">
                        {!! Form::label('user_phone','Mobile No : ',['class'=>'col-md-3  required-star']) !!}
                        <div class="col-md-5">
                            {!! Form::text('user_phone', null, ['class'=>'form-control input-sm required', 'placeholder' => 'e.g. 123456789']) !!}
                            {!! $errors->first('user_phone','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>

                    <div class="form-group {{$errors->has('email') ? 'has-error' : ''}}">
                        {!! Form::label('email','E-Mail Address : ',['class'=>'col-md-3  required-star']) !!}
                        <div class="col-md-5">
                            {!! Form::email('email', null, ['class'=>'form-control input-sm required', 'placeholder' => 'e.g. a@a.com']) !!}
                            {!! $errors->first('email','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>

                    <div class="form-group {{$errors->has('password') ? 'has-error' : ''}}">
                        {!! Form::label('password','Password : ',['class'=>'col-md-3  required-star']) !!}
                        <div class="col-md-5">
                            {!! Form::password('password', ['class'=>'form-control input-sm required', 'placeholder' => 'e.g. ******']) !!}
                            {!! $errors->first('password','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>

                    <div class="form-group {{$errors->has('password_confirmation') ? 'has-error' : ''}}">
                        {!! Form::label('password_confirmation','Confirm Password : ',['class'=>'col-md-3  required-star']) !!}
                        <div class="col-md-5">
                            {!! Form::password('password_confirmation', ['class'=>'form-control input-sm required', 'placeholder' => 'e.g. ******']) !!}
                            {!! $errors->first('password_confirmation','<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <a href="{{ url('/manage-institute/show-institute') }}">
                        {!! Form::button('<i class="fa fa-times"></i> Close', array('type' => 'button', 'class' => 'btn btn-default')) !!}
                    </a>
                    <button type="submit" class="btn btn-success pull-right">
                        <i class="fa fa-chevron-circle-right"></i> <b>Save</b></button>
                </div>

            {!! Form::close() !!}<!-- /.form end -->

                <div class="overlay" style="display: none;">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-script')
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>
    @include('partials.datatable-scripts')
    <script language="javascript">
        $(document).ready(function() {
            $("#createInstituteForm").validate({
                errorPlacement: function () {
                    return false;
                }
            });
        });

        $("#division_code").change(function () {

            $(this).after('<span class="loading_data"><i class="fa fa-spin fa-refresh" aria-hidden="true"></i> Loading...</span>');
            var self = $(this);
            var divisionId = $('#division_code').val();
            //$(".loading_data").after("<img style='margin-top: -15px;' src='<?php echo url(); ?>/public/assets/images/ajax-loader.gif' alt='loading' />");

            var zila_code = $('#zila_code');

            url = '<?php echo url(); ?>/manage-institute/get-selected-district';
            $.ajax({
                url: url,
                type: 'POST',
                data : {divId:divisionId, "_token":"{{ csrf_token() }}"},
                dataType: 'JSON',
                success: function (response) {

                    var option = '<option value="">Select Zilla</option>';
                    if (response.responseCode == 1) {
                        $.each(response.data, function (id, value) {
                            option += '<option value="' + id + '">' + value + '</option>';
                        });
                    }
                    zila_code.html(option);
                    self.next().remove();
                    //zila_code.selectpicker('refresh');
                    var divission_code_text = $('#division_code option:selected').text();
                    $('#division').val(divission_code_text);
                }
            });
            return false;
        });

        $("#zila_code").change(function () {

            $(this).after('<span class="loading_data"><i class="fa fa-spin fa-refresh" aria-hidden="true"></i> Loading...</span>');
            var self = $(this);
            var disId = $('#zila_code').val();

            var thana_u_zila_code = $('#thana_u_zila_code');
            url = '<?php echo url(); ?>/manage-institute/get-selected-thana';
            $.ajax({
                url: url,
                type: 'POST',
                data : {disId:disId, "_token":"{{ csrf_token() }}"},
                dataType: 'JSON',
                success: function (response) {
                    var option = '<option value="">Select Thana</option>';
                    if (response.responseCode == 1) {
                        $.each(response.data, function (id, value) {
                            option += '<option value="' + id + '">' + value + '</option>';
                        });
                    }
                    thana_u_zila_code.html(option);
                    self.next().remove();
                    var zila_code_text = $('#zila_code option:selected').text();
                    $('#zila').val(zila_code_text);
                    //thana_u_zila_code.selectpicker('refresh');
                }
            });
            return false;
        });
    </script>
@endsection