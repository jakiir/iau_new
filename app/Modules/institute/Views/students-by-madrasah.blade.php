@extends('layouts.admin')

@section('content')
    <?php
    $accessMode = ACL::getAccsessRight('Institute');
    if (!ACL::isAllowed($accessMode, 'V')) {
        die('You have no access right! Please contact with system admin if you have any query.');
    }
    $memId = CommonFunction::getUserId();
    $user_type = CommonFunction::getUserType();
    $user_type_name = CommonFunction::getUserTypeName();
    ?>

	<section class="content" xmlns="http://www.w3.org/1999/html">
		<div class="box">
			<div class="box-body">
				@include('partials.messages')
				<div class="col-lg-12">
					<div class="panel panel-primary">

						<div class="panel-heading">
							<div class="row">
								<div class="col-lg-6">
									<div class="btn" style="color:#ffffff">
										<i class="fa fa-list"></i>  <strong>List of students [ {{$users[0]->name}} ]</strong>
									</div>
								</div>
								<div class="col-lg-6 text-right">
									<a href="{{ url('/manage-institute/create') }}" class="btn btn-default"><i class="fa fa-plus"></i> New Student</a>

									<a href="javascript:void(0)" onClick="printStudentList()" class="btn btn-default">
										<i class="fa fa-btn fa-print"></i> Print Student List
									</a>
                                    <?php
										if(Session::get('examVal') == 'fazilpass'){
											$submited_status_col = 'submited_status_fazil3';
										}
										else if(Session::get('examVal') == 'fazilhons'){
											$submited_status_col = 'submited_status_fazil4';
										} else {
											$submited_status_col = 'submited_status_fazil3';
										}
                                    ?>
									<a href="javascript:void(0)" onClick="submitAllStudent(@if(!empty($users[0]->userId)){{$users[0]->userId}}@endif)" class="btn btn-default">
										<i class="fa fa-btn fa-users"></i> <span class="submitAllStudentText">@if($submited_status->$submited_status_col != 'Yes') Submit All Students @else Submit Cancel All Students @endif</span>
									</a>
								</div>
							</div>
						</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							{!! Form::open(['url' => '#', 'method' => 'get', 'class' => 'form apps_from', 'id' => 'batch_from', 'role' => 'form','enctype' =>'multipart/form-data', 'files'=>true]) !!}
							<input type="hidden" id="printLimit" value=""/>
							<div class="tablePrivate top">
								<div class="alignleft actions bulkactions">
									<select name="action" id="bulk-action-selector-top" class="input-sm">
										<option value="">Bulk Actions</option>
										<option value="delete">Delete</option>
										@if($user_type == '1x101')
											<option value="print">Print</option>
										@endif
									</select>
									<a id="doaction" class="btn btn-sm btn-primary action" onClick="bulkAction('bulk-action-selector-top')" value="Apply">Apply</a>
								</div>
							</div>

							<div class="table-responsive">
								<table id="studentList" class="table table-striped table-bordered dt-responsive " cellspacing="0" width="100%">
									<thead>
									<tr>
										<th>
											<input type="checkbox" name="selectAll" onClick="selectAllStudent(this)" class="fa">
										</th>
										<th>Image</th>
										<th>Unique Id</th>
										<th>Name</th>
										<th>Father & Mother</th>
										<th>Group</th>
										<th>Aciton</th>
									</tr>

									</thead>
									<tbody>
									@foreach($users as $key => $each_user)
										<tr id="user-{{ $each_user->id }}" class="user-item">
											<td scope="row" class="check-column">
												<input type="checkbox" name="student[]" value="{{$each_user->unique_id_no}}" onClick="eachSelect(this)" class="fa">
												<div class="locked-indicator"></div>
											</td>
											<td class="eSIF column-eSIF">
												@if(!empty($each_user->student_photo))
													<img style="clear:both;" src="{{$each_user->student_photo}}" class="student_photo" height="32" width="32">
												@else
													<img style="clear:both;" src="{{URL::to('assets/images/avatar5.png')}}" class="student_photo" height="32" width="32">
												@endif
											</td>
											<td class="eSIF column-eSIF">
												@if(!empty($each_user->unique_id_no))
													{{$each_user->unique_id_no}}
												@else
													NUll
												@endif
											</td>
											<td>
												@if(!empty($each_user->student_name))
													{{$each_user->student_name}}
												@else
													NUll
												@endif
											</td>

											<td class="father-mother column-father-mother">
												@if(!empty($each_user->fathers_name))
													{{$each_user->fathers_name}}
												@else
													NUll
												@endif
												,
												<br>
												@if(!empty($each_user->mothers_name))
													{{$each_user->mothers_name}}
												@else
													NUll
												@endif
											</td>
											<td class="email column-email">
												@if(!empty($each_user->student_group))
													{{ $each_user->student_group }}
												@endif
											</td>
											<td>
												<div class="row-actions">
													<?php
														if(Session::get('examVal') == 'fazilhons'){
															$editStdUrl = URL::to('/manage-student/edit-fazil4-students/'.$each_user->unique_id_no);
															$delStdUrl = URL::to('/manage-student/delete-fazil4-students/'.$each_user->unique_id_no);
															$printStdUrl = URL::to('/print-regi-card/', $each_user->unique_id_no);
														} else {
															$editStdUrl = URL::to('/manage-student/edit-fazil3-students/'.$each_user->unique_id_no);
															$delStdUrl = URL::to('/manage-student/delete-fazil3-students/'.$each_user->unique_id_no);
															$printStdUrl = URL::to('/print-regi-card/'.$each_user->unique_id_no);
														}
													?>
												<a href="{{$editStdUrl}}" class="btn btn-xs btn-primary open"><i class="fa fa-pencil-square-o"></i> Edit</a>
												<a href="{{$delStdUrl}}" onclick="return confirm('Are you sure you want to delete?')" class="btn btn-xs btn-danger open"><i class="fa fa-times"></i></a>
												@if($user_type == '1x101')
													<a target="_blank" href="{{$printStdUrl}}" class="btn btn-xs btn-primary" style="color:#fff;"><i class="fa fa-btn fa-print"></i> Print</a>
												@endif
											</td>
										</tr>
									@endforeach
									</tbody>

									<tfoot>
									<tr>
										<th>
											<input type="checkbox" name="selectAll" onClick="selectAllStudent(this)" class="fa">
										</th>
										<th>Image</th>
										<th>Unique Id</th>
										<th>Name</th>
										<th>Father & Mother</th>
										<th>Group</th>
										<th>Aciton</th>
									</tr>
									</tfoot>
								</table>
							</div><!-- /.table-responsive -->
							{!! Form::close() !!}
						</div><!-- /.panel-body -->
					</div><!-- /.panel panel-red -->
				</div><!-- /.col-lg-12 -->
			</div><!-- /.box-body -->
		</div><!-- /.box -->
	</section>
	<style>
		@if($user_type == '1x101')
			.tablePrivate{
					vertical-align: middle;
					position: absolute;
					overflow: hidden;
					z-index: 1;
				}
				.dataTables_length{
					float:right;
				}
				@else
			.tablePrivate{
					clear: both;
					height: 30px;
					margin: 6px 0 4px;
					vertical-align: middle;
				}
		@endif
	</style>
@endsection
@section('footer-script')
<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>
@include('partials.datatable-scripts')
<script type="text/javascript">
    $(function () {
        $('#studentList').DataTable({
            "paging": true,
            "lengthChange": true,
            "ordering": false,
            "orderable": false,
            "info": false,
            "autoWidth": false,
            "iDisplayLength": 50,
            "fnDrawCallback": function( oSettings ) {
                $('#printLimit').val(oSettings._iDisplayStart);
            }
        });
    });

    function selectAllStudent(source) {
        checkboxes = document.getElementsByName('student[]');
        for(var i=0, n=checkboxes.length;i<n;i++) {
            checkboxes[i].checked = source.checked;
        }
        var selectAlld = document.getElementsByName('selectAll');
        if (selectAlld[1].checked){
            selectAlld[0].checked = source.checked;
            selectAlld[1].checked = source.checked;
        } else {
            selectAlld[0].checked = source.checked;
            selectAlld[1].checked = source.checked;
        }
    }

    function eachSelect(source){
        var selectAlld = document.getElementsByName('selectAll');
        if (selectAlld[1].checked == true || selectAlld[1].checked == true){
            selectAlld[0].checked = false;
            selectAlld[1].checked = false;
        }
        var checkboxesCount = document.getElementsByName('student[]').length;
        var checkedboxesCount = document.querySelectorAll('input[name="student[]"]:checked').length;
        if(checkboxesCount == checkedboxesCount){
            selectAlld[0].checked = true;
            selectAlld[1].checked = true;
        }
    }

</script>
@endsection
