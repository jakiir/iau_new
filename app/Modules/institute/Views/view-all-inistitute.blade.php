@extends('layouts.admin')

@section('content')

    <?php
    $accessMode = ACL::getAccsessRight('Institute');
    if (!ACL::isAllowed($accessMode, 'V')) {
        die('You have no access right! Please contact with system admin if you have any query.');
    }
    $memId = CommonFunction::getUserId();
    $user_type = CommonFunction::getUserType();
    $user_type_name = CommonFunction::getUserTypeName();
    ?>

<section class="content" xmlns="http://www.w3.org/1999/html">
    <div class="box">
        <div class="box-body">
            @include('partials.messages')
            <div class="col-lg-12">
                <div class="panel panel-primary">

                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="btn" style="color:#ffffff">
                                    <i class="fa fa-university"></i>  <strong>List of Institutions</strong>
                                </div>
                            </div>
                            <div class="col-lg-6 text-right">
                                <a href="{{ url('/manage-institute/create-institute') }}" class="btn btn-default"><i class="fa fa-plus"></i> New Institution</a>
                                @if(Session::get('typeVal') == 'eff')
                                    <a href="/manage-institute/export-inisture/{{ Encryption::encodeId(2)}}" class="btn btn-default">
                                        <i class="fa fa-cloud-download" aria-hidden="true"></i> Export to Excel
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                                <!-- /.panel-heading -->
                    <div class="panel-body">
                            {!! Form::open(['url' => '#', 'method' => 'get', 'class' => 'form apps_from', 'id' => 'batch_from', 'role' => 'form','enctype' =>'multipart/form-data', 'files'=>true]) !!}
                            <input type="hidden" id="printLimit" value=""/>
                            <div class="tablePrivate top">
                                <div class="alignleft actions bulkactions">
                                    <select name="action" id="bulk-action-selector-top" class="input-sm">
                                        <option value="">Bulk Actions</option>
                                        <option value="delete">Delete</option>
                                        <option value="print">Print</option>
                                    </select>
                                    <a id="doaction" class="btn btn-sm btn-primary action" onClick="bulkAction('bulk-action-selector-top')" value="Apply">Apply</a>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table id="instituteList" class="table table-striped table-bordered dt-responsive " cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>
                                            <input type="checkbox" name="selectAll" onClick="selectAllInstitute(this)" class="fa">
                                        </th>
                                        <th>Madrasah Name</th>
                                        <th>EIIN</th>
                                        <th>Code</th>
                                        <th>Total</th>
                                        <th>Actions</th>
                                    </tr>

                                    </thead>
                                    <tbody>

                                    </tbody>

                                    <tfoot>
                                    <tr>
                                        <th>
                                            <input type="checkbox" name="selectAll" onClick="selectAllInstitute(this)" class="fa">
                                        </th>
                                        <th>Madrasah Name</th>
                                        <th>EIIN</th>
                                        <th>Code</th>
                                        <th>Total</th>
                                        <th>Actions</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div><!-- /.table-responsive -->
                        {!! Form::close() !!}
                    </div><!-- /.panel-body -->
                </div><!-- /.panel panel-red -->
            </div><!-- /.col-lg-12 -->
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</section>
    <style>
    @if($user_type == '1x101')
	.tablePrivate{
            vertical-align: middle;
            position: absolute;
            overflow: hidden;
            z-index: 1;
        }
        .dataTables_length{
            float:right;
        }
        @else
	.tablePrivate{
            clear: both;
            height: 30px;
            margin: 6px 0 4px;
            vertical-align: middle;
        }
        @endif
    </style>
@endsection

@section('footer-script')
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>
    @include('partials.datatable-scripts')
    <script language="javascript">
        $(function () {
            $('#instituteList').DataTable({
                processing: true,
                serverSide: true,
                "iDisplayLength": 25,
                "fnDrawCallback": function( oSettings ) {
                    $('#printLimit').val(oSettings._iDisplayStart);
                },
                ajax: {
                    url: '{{url("manage-institute/get-all-madrasah-lists")}}',
                    method:'post',
                    data: function (d) {
                        d._token = $('input[name="_token"]').val();
                    }
                },
                columns: [
                    {data: 'checkbox', name: 'checkbox', orderable: false, searchable: false},
                    {data: 'name', name: 'name'},
                    {data: 'username', name: 'username'},
                    {data: 'institute_code', name: 'institute_code'},
                    {data: 'total_students', name: 'total_students'},
                    {data: 'action', name: 'action'}
                ],
                "aaSorting": []
            });

        });

        function selectAllInstitute(source) {
            checkboxes = document.getElementsByName('madrasah[]');
            for(var i=0, n=checkboxes.length;i<n;i++) {
                checkboxes[i].checked = source.checked;
            }
            var selectAlld = document.getElementsByName('selectAll');
            if (selectAlld[1].checked){
                selectAlld[0].checked = source.checked;
                selectAlld[1].checked = source.checked;
            } else {
                selectAlld[0].checked = source.checked;
                selectAlld[1].checked = source.checked;
            }
        }

        function eachSelect(source){
            var selectAlld = document.getElementsByName('selectAll');
            if (selectAlld[1].checked == true || selectAlld[1].checked == true){
                selectAlld[0].checked = false;
                selectAlld[1].checked = false;
            }
            var checkboxesCount = document.getElementsByName('madrasah[]').length;
            var checkedboxesCount = document.querySelectorAll('input[name="madrasah[]"]:checked').length;
            if(checkboxesCount == checkedboxesCount){
                selectAlld[0].checked = true;
                selectAlld[1].checked = true;
            }
        }


        function bulkAction(event){
            var bulkActionSel = document.getElementById(event).value;
            if(bulkActionSel == 'delete'){
                var checkedboxesCount = document.querySelectorAll('input[name="madrasah[]"]:checked').length;
                if(checkedboxesCount < 1){
                    alert('Select madrasah for further action!');
                } else {
                    var checkboxesaa = document.querySelectorAll('input[name="madrasah[]"]:checked');
                    var getSelected = [];
                    for(var i=0, n=checkboxesaa.length;i<n;i++) {
                        getSelected.push(checkboxesaa[i].value);
                    }
                    console.log(getSelected);
                    url = '{{route("ajaxBulkAction")}}';
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data : {mdhIds:getSelected,actionType:'delete', "_token":"{{ csrf_token() }}"},
                        dataType: 'JSON',
                        success: function (data) {
                            if(data.success == true){
                                alert(data.msg);
                                window.location.href = "{{ url('/manage-institute/show-institute') }}";
                            } else {
                                alert(data.msg);
                            }


                        }
                    });
                    return false;

                }
            }
            if(bulkActionSel == 'print'){
                console.log(bulkActionSel);
            }

        }

        function submitAllStudent(thisItem){
            var InstId = thisItem.getAttribute("data-inisid");
            var url = '/manage-institute/submit-all-student';
            $.ajax({
                url: url,
                type: 'POST',
                data : {InstId:InstId, "_token":"{{ csrf_token() }}"},
                dataType: 'JSON',
                success: function (data) {
                    console.log();
                    if(data.success == true){
                        $('.stdSubmitStatas').remove();
                        $('.studentName').attr('href','#');
                        if(data.status == 'Yes'){
                            $(thisItem).removeClass('btn-success').addClass('btn-warning');
                        } else {
                            $(thisItem).removeClass('btn-warning').addClass('btn-success');
                        }
                        var btnText = (data.status == 'Yes' ? '<i class="fa fa-times" ></i > Submit Cancel All Students' : '<i class="fa fa-paper-plane" ></i > Submit All Students');
                        $('.submitAllStudentText-'+InstId).html(btnText);
                        //$('.submitAllStudentBtn').removeAttr('onclick');
                    } else {

                    }


                }
            });
        }

        function finalLock(institute) {
            url = '{{route("ajaxfinalLockForFazilPassEff")}}';
            $.ajax({
                url: url,
                type: 'POST',
                data : {
                    selected_institute:institute,
                    "_token":"{{ csrf_token() }}"
                },
                dataType: 'JSON',
                success: function (response) {
                    if(response.success == true){
                        alert(response.msg);
                        $('.finalLockBtn-'+institute).html(response.status);
                        //location.reload();
                    } else {
                        alert(response.msg);
                    }


                }
            });
            return false;
        }
    </script>
@endsection