<html>
	<head>
		<title>{{$pageTitle}}</title>
	</head>
<body>
<?php

$madrasahName = Auth::user()->name.' ['.Auth::user()->username.']';
?>
	<table  style="width: 100%;" border="0" cellpadding="0" cellspacing="0" class="iau-list-table widefat fixed striped users table">
		<tr>
			<td align='center' valign='top'>
				<img class="img-responsive logo" src="assets/img/iau-logo.png" width="100" alt="logo">
			</td>
		</tr>
		<tr>
			<td align='center' valign='top'>
				<h2 style="text-align: center;">ISLAMIC ARABIC UNIVERSITY</h2>
				<h3 style="text-align: center;">
					@if(!empty(Session::get('examVal')) && !empty(Session::get('typeVal')) && !empty(Session::get('yearVal')))
						<?php $yearVal = Session::get('yearVal')+1; ?>
						{{Session::get('examTxt').' '.Session::get('typeTxt').' - 20'.$yearVal}}
					@endif
				</h3>
				<div class='madrasahName' style="text-align: center;">
					<?php
                    	$username = (!empty($_GET['inis']) ? $_GET['inis'] : '');
					?>
					@if($username == '')
						{{ (!empty($users[0]->madrasha_name) ? $users[0]->madrasha_name.' ['.$users[0]->madrasha_eiin.']' : $madrasahName) }}
					@else
						{{ (!empty($users[0]->madrasha_name) ? $users[0]->madrasha_name.' ['.$users[0]->madrasha_eiin.']' : $madrasahName) }}
					@endif
				</div>
				<div class='admitTitle' style="text-align:center; margin-top:10px;">Students' List</div>
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>
	</table>
	<table style="width: 100%;" border="1" cellpadding="0" cellspacing="0" class="iau-list-table widefat fixed striped users table">
		<thead>
		<tr>
			<th scope="col" align="center" class="manage-column column-sl">SL</th>
			<th scope="col" align="center" class="manage-column column-regi_no">Regi No</th>
			<th scope="col" align="center" class="manage-column column-name">Name</th>
			<th scope="col" align="center" class="manage-column column-father-mother">Father &amp; Mother</th>
			<th scope="col" align="center" class="manage-column column-father-mother">Center Code</th>
			<th scope="col" align="center" class="manage-column column-father-mother">Center Name</th>
			<th scope="col" align="center" class="manage-column column-group">Group</th>
		</tr>
		</thead>

		<tbody id="the-list">
		<?php $i=1; ?>
		@foreach($users as $key => $each_user)
				<tr id="user-{{ $each_user->id }}" class="user-item">
					<td align="center" class="sl column-sl">{{$i}}</td>
					<td align="center">
						@if(!empty($each_user->registration_no))
							{{ $each_user->registration_no }}
						@endif
					</td>
					<td align="center" class="has-row-actions">
						@if(!empty($each_user->student_name))
							{{ $each_user->student_name }}
						@endif
					</td>

					<td align="center" class="father-mother column-father-mother">
						@if(!empty($each_user->fathers_name))
							{{$each_user->fathers_name}}
						@else
							NUll
						@endif
						,
						<br>
						@if(!empty($each_user->mothers_name))
							{{$each_user->mothers_name}}
						@else
							NUll
						@endif
					</td>
					<td align="center" class="group column-group">
						@if(!empty($each_user->center_code))
							{{ $each_user->center_code }}
						@endif
					</td>
					<td align="center" class="group column-group">
						@if(!empty($each_user->center_name))
							{{ $each_user->center_name }}
						@endif
					</td>
					<td align="center" class="group column-group">
						@if(!empty($each_user->student_group))
							{{ $each_user->student_group }}
						@endif
					</td>
				</tr>
			<?php $i++; ?>
		@endforeach
		</tbody>
	</table>
</body>
</html>