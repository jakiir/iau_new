<?php

namespace App\Modules\Institute\Controllers;

use App;
use App\Http\Controllers\Controller;
use App\Libraries\ACL;
use App\Libraries\CommonFunction;
use App\Libraries\Encryption;
use App\Modules\Institute\Models\Institutes;
use App\Modules\Settings\Models\Area;
use App\Modules\Users\Models\Users;
use App\Modules\Users\Models\UsersEditable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use View;
use yajra\Datatables\Datatables;

class InstituteController extends Controller {

    public function __construct() {
        if (Session::has('lang'))
            App::setLocale(Session::get('lang'));
        ACL::db_reconnect();
    }

    protected function showInstitute()
    {
        if(Auth::user()->user_type == '1x101'){
            $users = DB::table('users')->select('*','users.id as userId')
                ->join('institutes as tb1', 'tb1.madrasah_eiin', '=', 'users.username')
                ->orderBy('tb1.id', 'asc')
                ->get();
        }
        if(!empty(Session::get('examVal')) && !empty(Session::get('typeVal')) && !empty(Session::get('yearVal'))){
            $yearVal = Session::get('yearVal');
            $examVal = Session::get('examVal');

            if(Session::get('examVal') == 'fazilpass'){
                if(Session::get('typeVal') == 'eff'){
                    $listTable = 'fazilpasseff15';
                    $listTable2 = 'fazilpass15';
                }else{
                    $listTable = 'fazilpass15';
                    $listTable2 = '';
                }
            }
            else if(Session::get('examVal') == 'fazilhons'){
                if(Session::get('typeVal') == 'eff'){
                    $listTable = 'fazilhonseff15';
                    $listTable2 = 'fazilhons15';
                }else{
                    $listTable = 'fazilhons15';
                    $listTable2 = '';
                }
            } else if(Session::get('examVal') == 'fazilprivate'){
                if(Session::get('typeVal') == 'eff'){
                    $listTable = 'fazilpriveff15';
                    $listTable2 = 'fazilprivate15';
                }else{
                    $listTable = 'fazilprivate15';
                    $listTable2 = '';
                }
            } else {
                $listTable = '';
            }

        } else {
            $listTable = '';
        }
        $students = [];
        if($listTable !='') {
            foreach ($users as $user_each) {
                if($listTable2 !=''){
                    $students[$user_each->id]['total'] = DB::table('users')
                        ->join($listTable . ' as tb1', 'tb1.madrasah_eiin', '=', 'users.username')
                        ->join($listTable2. ' as tb2', 'tb2.id', '=', 'tb1.student_id')
                        ->where('tb1.madrasah_eiin', '=', $user_each->username)->count();
                } else {
                    $students[$user_each->id]['total'] = DB::table('users')
                        ->join($listTable . ' as tb1', 'tb1.madrasah_eiin', '=', 'users.username')
                        ->where('tb1.madrasah_eiin', '=', $user_each->username)->count();
                }
            }
        }
        return view('institute::view-all-inistitute', compact('users','students'));
    }


    public function getAllMadrasahLists(){
        $user_type = Auth::user()->user_type;
        $mode = ACL::getAccsessRight('Institute', 'V');
        $instituteList = '';
        if($user_type == '1x101'){
            $instituteList = Users::leftJoin('institutes as tb1', 'tb1.madrasah_eiin', '=', 'users.username')
                ->where('tb1.id','!=','')
                ->orderBy('tb1.id', 'asc')
                ->where('users.is_approved','=',1)
                ->where('users.is_archieved','=',0)
                ->get(['tb1.id as instituteId','users.id as userId','users.name','users.username','tb1.institute_code',
                    'users.submited_status_fazil3','users.submited_status_fazil3_private','users.final_lock_fazil3_eff',
                'users.final_lock_fazil3_priv_eff']);
        }
        return Datatables::of($instituteList)->addColumn('checkbox', function ($instituteList) use ($mode) {
            if ($mode) {
                return '<input type="checkbox" name="madrasah[]" value="'.$instituteList->username.'" onClick="eachSelect(this)" class="fa">';
            } else{
                return '';
            }
        })->addColumn('total_students', function ($instituteList) use ($mode) {
            if(!empty(Session::get('examVal')) && !empty(Session::get('typeVal')) && !empty(Session::get('yearVal'))){
                if(Session::get('examVal') == 'fazilpass'){
                    if(Session::get('typeVal') == 'eff'){
                        $listTable = 'fazilpasseff15';
                        $listTable2 = 'fazilpass15';
                    }else{
                        $listTable = 'fazilpass15';
                        $listTable2 = '';
                    }
                }
                else if(Session::get('examVal') == 'fazilhons'){
                    if(Session::get('typeVal') == 'eff'){
                        $listTable = 'fazilhonseff15';
                        $listTable2 = 'fazilhons15';
                    }else{
                        $listTable = 'fazilhons15';
                        $listTable2 = '';
                    }
                } else if(Session::get('examVal') == 'fazilprivate'){
                    if(Session::get('typeVal') == 'eff'){
                        $listTable = 'fazilpriveff15';
                        $listTable2 = 'fazilprivate15';
                    }else{
                        $listTable = 'fazilprivate15';
                        $listTable2 = '';
                    }
                } else {
                    $listTable = '';
                }

            } else {
                $listTable = '';
            }

            if($listTable !='') {
                $students = [];
                if($listTable2 !=''){
                    $students[$instituteList->instituteId]['total'] = Users::leftJoin($listTable . ' as tb1', 'tb1.madrasah_eiin', '=', 'users.username')
                        ->leftJoin($listTable2. ' as tb2', 'tb2.id', '=', 'tb1.student_id')
                        ->where('tb1.madrasah_eiin', '=', $instituteList->username)->count();
                } else {
                    $students[$instituteList->instituteId]['total'] = Users::leftJoin($listTable . ' as tb1', 'tb1.madrasah_eiin', '=', 'users.username')
                        ->where('tb1.madrasah_eiin', '=', $instituteList->username)->count();
                }
                $output = '';
                if(!empty($students[$instituteList->instituteId]['total'])){
                    $output .= '<a class="row-title" href="'.url('/manage-student/student-by-institute/'.Encryption::encodeId($instituteList->username)).'" title="View all">'.$students[$instituteList->instituteId]['total'].'</a>';
                }
                return $output;
            } else {
                return '';
            }
        })->addColumn('action', function ($instituteList) use ($user_type) {
            if ($user_type == '1x101') {

                if (Session::get('examVal') == 'fazilpass') {
                    if (Session::get('typeVal') == 'eff') {
                        $submited_status_col = '';
                        $finalLock = 'final_lock_fazil3_eff';
                        $printList = '/manage-student/print-fazil3-eff-students';
                    } else {
                        $submited_status_col = 'submited_status_fazil3';
                        $finalLock = '';
                    }
                } else if (Session::get('examVal') == 'fazilhons') {
                    if (Session::get('typeVal') == 'eff') {
                        $submited_status_col = '';
                        $finalLock = '';
                        $printList = '';
                    } else {
                        $submited_status_col = '';
                        $finalLock = '';
                    }
                } else if (Session::get('examVal') == 'fazilprivate') {
                    if (Session::get('typeVal') == 'eff') {
                        $submited_status_col = '';
                        $finalLock = 'final_lock_fazil3_priv_eff';
                        $printList = '/manage-student/print-fazil3-private-eff-students';
                    } else {
                        $submited_status_col = 'submited_status_fazil3_private';
                        $finalLock = '';
                    }
                } else {
                    $submited_status_col = 'submited_status_fazil3';
                }
                $output = '';
                $output .='<a href="'.url('/manage-institute/edit-institute/'.Encryption::encodeId($instituteList->userId)).'" class="btn btn-xs btn-primary open" ><i class="fa fa-pencil-square-o"></i> Edit</a>';
                $output .=' <a href="'.url('/manage-institute/delete-institute/'.Encryption::encodeId($instituteList->userId)).'"  onclick="return confirm(\'Are you sure you want to delete?\')" class="btn btn-xs btn-danger open" ><i class="fa fa-times"></i></a>';
                if ($submited_status_col != '') {
                    $output .= ' <a href="javascript:void(0)" data-inisId="'.Encryption::encodeId($instituteList->userId).'" onClick="submitAllStudent(this)" class="btn btn-xs '.($instituteList->$submited_status_col != 'Yes' ? "btn-success" : "btn-warning").'" style = "color:#fff;">';
                    $output .= '<span class="submitAllStudentText-'.Encryption::encodeId($instituteList->userId).'" >';
                    if($instituteList->$submited_status_col != 'Yes'){
                        $output .= '<i class="fa fa-paper-plane" ></i > Submit All Students';
                    } else {
                        $output .= '<i class="fa fa-times" ></i > Submit Cancel All Students';
                    }
                    $output .= '</span></a>';
                } else {
                    $output .= ' <a href="javascript:void(0)"';
                    if(!empty($instituteList->$finalLock) && $instituteList->$finalLock == 'Yes' && $user_type != '1x101'){
                        $output .= 'disabled = "disabled"';
                    } else {
                        $output .= 'onClick="finalLock('.$instituteList->username.')"';
                    }
                    $output .= 'class="btn btn-xs btn-primary finalLockBtn-'.$instituteList->username.'" style = "color:#fff;">';

                    if (!empty($instituteList->$finalLock) && $instituteList->$finalLock == 'Yes' && $user_type == '1x101') {
                        $output .= '<i class="fa fa-unlock" aria-hidden="true"></i> Unlock';
                    }else {
                        $output .= '<i class="fa fa-lock" aria-hidden="true"></i> Final Lock';
                    }
                    $output .= '</a >';
                    $output .= ' <a target="_blank" href= "'.$printList.'/'.Encryption::encodeId($instituteList->username).'" class="btn btn-xs btn-primary" style = "color:#fff;"><i class="fa fa-btn fa-print" ></i > Print</a>';
                }
                return $output;
            } else {
                return '';
            }
        })->make(true);
    }

    public function studentByInstitute($id){
        $eiin = Encryption::decodeId($id);
        $user_type = Auth::user()->user_type;
        //$mode = ACL::getAccsessRight('Institute', 'V');

        if(!empty(Session::get('examVal')) && !empty(Session::get('typeVal')) && !empty(Session::get('yearVal'))){
            $yearVal = Session::get('yearVal');
            $examVal = Session::get('examVal');
            if(Session::get('examVal') == 'fazilpass'){
                if(Session::get('typeVal') == 'eff'){
                    $listTable = 'fazilpasseff15';
                    $listTable2 = 'fazilpass15';
                    $path_acc = 'supperAdmin.view_all_fazil3_students_eff';
                }else{
                    $listTable = 'fazilpass15';
                }
            }
            else if(Session::get('examVal') == 'fazilhons'){
                if(Session::get('typeVal') == 'eff'){
                    $listTable = 'fazilhonseff15';
                    $listTable2 = 'fazilhons15';
                    //$path_acc = '';
                }else{
                    $listTable = 'fazilhons15';
                }
            } else if(Session::get('examVal') == 'fazilprivate'){
                if(Session::get('typeVal') == 'eff'){
                    $listTable = 'fazilpriveff15';
                    $listTable2 = 'fazilprivate15';
                    $path_acc = 'supperAdmin.view_all_fazil3_priv_students_eff';
                }else{
                    $listTable = 'fazilprivate15';
                }
            } else {
                $listTable = 'fazilpass15';
            }
        } else {
            $listTable = 'fazilpass15';
        }
        if($user_type == '1x101'){

            $regi_no = Session::get('yearVal');
            $admission_year = "20".$regi_no;
            $alimSecond = Session::get('yearVal') + 1;
            $admission_session = $admission_year.'-'.$alimSecond;
            //$username = Auth::user()->username;
            $getEffStud = [];
            $totalItem = 0;
            $authUser = [];

            if(Session::get('typeVal') == 'eff'){
                $whereCondition = ['tb1.madrasah_eiin' => $eiin,'tb1.admission_session' => $admission_session ,'tb1.student_class' => Session::get('examVal')];
                $users = Users::join($listTable2.' as tb1', 'tb1.madrasah_eiin', '=', 'users.username')
                    ->where($whereCondition)
                    ->orderBy('tb1.registration_no', 'asc')
                    ->get(['tb1.id','tb1.unique_id_no','tb1.tot_serial_no','tb1.registration_no','tb1.student_name',
                        'tb1.fathers_name','tb1.mothers_name','tb1.student_group','users.id as userId','users.name']);
                $authUser = DB::table('users')->where('username','=',$eiin)->first();
                $whereConditionEff = ['madrasah_eiin' => $eiin,'admission_session' => $admission_session];
                $getFazilPassStudData = DB::table($listTable)->where($whereConditionEff)->get(['unique_id_no']);
                foreach($getFazilPassStudData as $effStuData){
                    $getEffStud[] = $effStuData->unique_id_no;
                    $totalItem++;
                }

            } else {
                $whereCondition = ['users.username' => $eiin,'tb1.admission_session' => $admission_session,'tb1.student_class' => Session::get('examVal')];
                $users = DB::table('users')->select('*','users.id as userId')
                    ->join($listTable.' as tb1', 'tb1.madrasah_eiin', '=', 'users.username')
                    ->where($whereCondition)
                    ->orderBy('tb1.tot_serial_no', 'asc')
                    ->get();
            }
        }

        if(Session::get('examVal') == 'fazilpass'){
            $submited_status_col = 'submited_status_fazil3';
        }
        else if(Session::get('examVal') == 'fazilhons'){
            $submited_status_col = 'submited_status_fazil4';
        } else {
            $submited_status_col = 'submited_status_fazil3';
        }
        $submited_status = Users::where('username',$eiin)->first([$submited_status_col]);
        $madrasahId = $eiin;

        $all_institutes = ['' => 'All'] + Users::where('user_type','=','2x202')->orderBy('name', 'ASC')->lists('name', 'username')->all();

        return view('institute::students-by-madrasah', compact('users','madrasahId','submited_status','all_institutes','authUser','getEffStud','totalItem'));
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function createInstitute()
    {
        if (!ACL::getAccsessRight('Institute', 'A')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $highest_class = ['' => 'Select Class','fazilpass'=>'Fazil Pass','fazilprivate'=>'Fazil Private',
            'fazilhons'=>'Fazil Honours','kamil-1'=>'Kamil'];

        $divisions = ['' => 'Select Division'] + Area::orderBy('area_nm')
                ->where('pare_id', 0)->lists('area_nm', 'area_id')->all();

        return view('institute::add-new-institute', compact('divisions','highest_class'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    protected function storeInstitute(Request $request)
    {
        if (!ACL::getAccsessRight('Institute', 'A')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $this->validate($request, [
            'institute_name' => 'required|max:255',
            'username'=>'required|max:50|unique:users',
            'email'=>'required|max:50|unique:users',
            'institute_code'=>'required|max:255',
            'division_code'=>'required|max:255',
            'zila_code'=>'required|max:255',
            'thana_u_zila_code'=>'required|max:255',
            'post_office'=>'required|max:255',
            'principal_name'=>'required|max:255',
            'user_phone'=>'required|max:255',
            'password' => 'required|confirmed|min:6',
            'password_confirmation'=>'required|max:255'
        ]);
        try {
            DB::beginTransaction();
            $user = Users::create([
                'name' => $request->get('institute_name'),
                'user_type' => '2x202',
                'username' => $request->get('username'),
                'email' => $request->get('email'),
                'is_approved' => 1,
                'first_login' => 1,
                'user_status' => 'active',
                'user_phone' => $request->get('user_phone'),
                'password' => bcrypt($request->get('password')),
            ]);
            if ($user) {
                $data = [
                    'user_id' => $user->id,
                    'institute_name' => $request->get('institute_name'),
                    'madrasah_eiin' => $request->get('username'),
                    'institute_code' => $request->get('institute_code'),
                    'highest_class' => $request->get('highest_class'),
                    'division' => $request->get('division'),
                    'division_code' => $request->get('division_code'),
                    'zila' => $request->get('zila'),
                    'zila_code' => $request->get('zila_code'),
                    'thana_u_zila' => $request->get('thana_u_zila'),
                    'thana_u_zila_code' => $request->get('thana_u_zila_code'),
                    'center_name' => $request->get('center_name'),
                    'post_office' => $request->get('post_office'),
                    'principal_name' => $request->get('principal_name')
                ];
                Institutes::create($data);
                DB::commit();
                Session::flash('success', 'Institute has been saved successfully');
                return redirect('/manage-institute/show-institute');
            } else {
                Session::flash('error', 'Something went wrong! user did not created!');
                return Redirect::back()->withInput();
            }
        } catch (Exception $e) {
            DB::rollback();
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return Redirect::back()->withInput();
        }
    }
    /**
     * Edit Institute.
     *
     * @param  array  $id
     * @return User
     */
    protected function editInstitute($id){

        if (!ACL::getAccsessRight('Institute', 'E')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }

        try{
            $user_id = Encryption::decodeId($id);
            $institutes = Users::leftJoin('institutes as tb1', 'tb1.madrasah_eiin', '=', 'users.username')
                ->where('users.id',$user_id)
                ->first(['users.id as user_id','tb1.institute_name','users.username','tb1.institute_code',
                    'tb1.highest_class','tb1.division_code','tb1.division','tb1.zila_code','tb1.zila','tb1.thana_u_zila_code',
                    'tb1.thana_u_zila','tb1.center_name','tb1.post_office','tb1.principal_name','users.user_phone',
                    'users.email']);
            if($institutes){
                $highest_class = ['' => 'Select Class','fazilpass'=>'Fazil Pass','fazilprivate'=>'Fazil Private',
                    'fazilhons'=>'Fazil Honours','kamil-1'=>'Kamil'];

                $divisions = ['' => 'Select Division'] + Area::orderBy('area_nm')
                        ->where('pare_id', 0)->lists('area_nm', 'area_id')->all();
                $districts = Area::where('PARE_ID', $institutes->division_code)->orderBy('AREA_NM', 'ASC')->lists('AREA_NM', 'AREA_ID');
                $thana = Area::where('PARE_ID', $institutes->zila_code)->orderBy('AREA_NM', 'ASC')->lists('AREA_NM', 'AREA_ID');

                return view('institute::edit-institute', compact('institutes','divisions','highest_class','districts','thana'));

            } else {
                Session::flash('error', 'Something went wrong! user did not deleted!');
                return redirect('/manage-institute/show-institute');
            }
        } catch (Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return redirect('/manage-institute/show-institute');
        }
    }


    protected function updateInstitute(Request $request){
        if (!ACL::getAccsessRight('Institute', 'E')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }

        if(!empty($request->get('password'))){
            $this->validate($request, [
                'institute_name' => 'required|max:255',
                'username'=>'required|max:50',
                'email'=>'required|max:50',
                'institute_code'=>'required|max:255',
                'division_code'=>'required|max:255',
                'zila_code'=>'required|max:255',
                'thana_u_zila_code'=>'required|max:255',
                'post_office'=>'required|max:255',
                'principal_name'=>'required|max:255',
                'user_phone'=>'required|max:255',
                'password' => 'required|confirmed|min:6',
                'password_confirmation'=>'required|max:255'
            ]);
        } else {
            $this->validate($request, [
                'institute_name' => 'required|max:255',
                'username'=>'required|max:50',
                'email'=>'required|max:50',
                'institute_code'=>'required|max:255',
                'division_code'=>'required|max:255',
                'zila_code'=>'required|max:255',
                'thana_u_zila_code'=>'required|max:255',
                'post_office'=>'required|max:255',
                'principal_name'=>'required|max:255',
                'user_phone'=>'required|max:255'
            ]);
        }

        try {
            DB::beginTransaction();
            $user_id = Encryption::decodeId($request->get('user_id'));
            if(!empty($request->get('password'))) {
                $user_data = [
                    'name' => $request->get('institute_name'),
                    'username' => $request->get('username'),
                    'email' => $request->get('email'),
                    'user_phone' => $request->get('user_phone'),
                    'password' => bcrypt($request->get('password'))
                ];
            } else {
                $user_data = [
                    'name' => $request->get('institute_name'),
                    'username' => $request->get('username'),
                    'email' => $request->get('email'),
                    'user_phone' => $request->get('user_phone')
                ];
            }
            $userUpdate = UsersEditable::find($user_id)->update($user_data);

            if ($userUpdate) {
                $data = [
                    'institute_name' => $request->get('institute_name'),
                    'madrasah_eiin' => $request->get('username'),
                    'institute_code' => $request->get('institute_code'),
                    'highest_class' => $request->get('highest_class'),
                    'division' => $request->get('division'),
                    'division_code' => $request->get('division_code'),
                    'zila' => $request->get('zila'),
                    'zila_code' => $request->get('zila_code'),
                    'thana_u_zila' => $request->get('thana_u_zila'),
                    'thana_u_zila_code' => $request->get('thana_u_zila_code'),
                    'center_name' => $request->get('center_name'),
                    'post_office' => $request->get('post_office'),
                    'principal_name' => $request->get('principal_name')
                ];
                Institutes::where('user_id','=',$user_id)->update($data);

                DB::commit();
                Session::flash('success', 'Institute has been updated successfully');
                return Redirect::back();
            } else {
                Session::flash('error', 'Something went wrong! user did not updated!');
                return Redirect::back()->withInput();
            }
        } catch (Exception $e) {
            DB::rollback();
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return Redirect::back()->withInput();
        }
    }


    public function getDistrictList(Request $request)
    {
        $divisionId = $request->get('divId');
        if($divisionId !=''){
            $districts = Area::where('PARE_ID', $divisionId)->orderBy('AREA_NM', 'ASC')->lists('AREA_NM', 'AREA_ID');
            $result = ['responseCode' => 1, 'data' => $districts];
        } else {
            $result = ['responseCode' => 0, 'data' => ''];
        }
        if (!ACL::getAccsessRight('Institute', 'V')) {
            $result = ['responseCode' => 0, 'data' => ''];
        }
        return response()->json($result);
    }

    public function getThanaList(Request $request)
    {
        $districtId = $request->get('disId');
        if($districtId !=''){
            $districts = Area::where('PARE_ID', $districtId)->orderBy('AREA_NM', 'ASC')->lists('AREA_NM', 'AREA_ID');
            $result = ['responseCode' => 1, 'data' => $districts];
        } else {
            $result = ['responseCode' => 0, 'data' => ''];
        }
        if (!ACL::getAccsessRight('Institute', 'V')) {
            $result = ['responseCode' => 0, 'data' => ''];
        }
        return response()->json($result);
    }

    protected function deleteInstitute($id){
        if (!ACL::getAccsessRight('Institute', 'D')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        try{
            $user_id = Encryption::decodeId($id);
            $getUser = Users::where('id',$user_id)->first();
            if($getUser){
                //Users::where('id',$user_id)->delete();
                //Institutes::where('user_id',$user_id)->delete();
                $user_data = ['is_archieved' => 1];
                UsersEditable::where('id', $user_id)->update($user_data);
                Session::flash('success', 'Institute has been deleted successfully');
                return redirect('/manage-institute/show-institute');
            } else {
                Session::flash('error', 'Something went wrong! user did not deleted!');
                return redirect('/manage-institute/show-institute');
            }
        } catch (Exception $e) {
            Session::flash('error', CommonFunction::showErrorPublic($e->getMessage()));
            return redirect('/manage-institute/show-institute');
        }
    }

    public function ajaxBulkActionClb(Request $request)
    {
        if (!ACL::getAccsessRight('Institute', 'D')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        try{
            if($request->get('actionType')=='delete'){
                $instituteIds = $request->get('mdhIds');
                if($instituteIds){
                    $user_data = ['is_archieved' => 1];
                    UsersEditable::whereIn('username', $instituteIds)->update($user_data);
                    $result = array('success' => true, 'msg' => 'Selected student successfully deleted');
                } else {
                    $result = array('success' => false, 'msg' => 'Delete request is canceled!');
                }
            } else {
                $result = array('success' => false, 'msg' => 'Delete request is canceled!');
            }
        } catch (Exception $e) {
            $result = array('success' => false, 'msg' => CommonFunction::showErrorPublic($e->getMessage()));
        }
        return response()->json($result);
    }

    /**************
     * Print Fazil 3 Eff Students
     * function name printFazil3EffStd
     ***************/

    public function printFazil3EffStd($id){
        if (!ACL::getAccsessRight('Institute', 'V')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }

        if(empty(Session::get('examVal')) && empty(Session::get('typeVal')) && empty(Session::get('yearVal'))){
            return redirect('select-option');
        } else {
            $regi_no = Session::get('yearVal');
            $admission_year = "20" . $regi_no;
            $examVal = Session::get('examVal');
            $alimSecond = $regi_no + 1;
            $admission_session = $admission_year . '-' . $alimSecond;
            $offset = (isset($_GET['start']) ? $_GET['start'] : 0);
            if (Auth::user()->user_type == '2x202') {
                $username = Auth::user()->username;
                $users = Users::join('fazilpass15 as tb1', 'tb1.madrasah_eiin', '=', 'users.username')
                    ->join('fazilpasseff15 as tb2', 'tb2.student_id', '=', 'tb1.id')
                    ->join('exam_center as tb3', 'tb3.madrasah_eiin', '=', 'users.username')
                    ->join('users as tb4', 'tb4.username', '=', 'tb3.center_eiin')
                    ->where(function ($users) use ($username) {
                        if (!empty($username)) {
                            $users->where('tb1.madrasah_eiin', '=', $username);
                        }
                    })
                    ->where(function ($users) use ($admission_session) {
                        if (!empty($admission_session)) {
                            $users->where('tb1.admission_session', '=', $admission_session);
                        }
                    })
                    ->where(function ($users) use ($examVal) {
                        if (!empty($examVal)) {
                            $users->where('tb1.student_class', '=', $examVal);
                        }
                    })
                    ->orderBy('tb1.registration_no', 'asc')
                    ->get(['tb1.id','tb1.unique_id_no','tb1.tot_serial_no','tb1.registration_no','tb1.student_name','tb1.fathers_name',
                        'tb1.mothers_name','tb1.student_group','users.id as userId','users.name as madrasha_name',
                        'users.username as madrasha_eiin','tb3.center_code',
                        'tb3.center_eiin','tb4.name as center_name']);


            }
            if (Auth::user()->user_type == '1x101') {
                $username=Encryption::decodeId($id);
                $users = Users::join('fazilpass15 as tb1', 'tb1.madrasah_eiin', '=', 'users.username')
                    ->join('fazilpasseff15 as tb2', 'tb2.student_id', '=', 'tb1.id')
                    ->join('exam_center as tb3', 'tb3.madrasah_eiin', '=', 'users.username')
                    ->join('users as tb4', 'tb4.username', '=', 'tb3.center_eiin')
                    ->where(function ($users) use ($username) {
                        if (!empty($username)) {
                            $users->where('tb1.madrasah_eiin', '=', $username);
                        }
                    })
                    ->where(function ($users) use ($admission_session) {
                        if (!empty($admission_session)) {
                            $users->where('tb1.admission_session', '=', $admission_session);
                        }
                    })
                    ->where(function ($users) use ($examVal) {
                        if (!empty($examVal)) {
                            $users->where('tb1.student_class', '=', $examVal);
                        }
                    })
                    ->orderBy('tb1.registration_no', 'asc')
                    ->get(['tb1.id','tb1.unique_id_no','tb1.tot_serial_no','tb1.registration_no','tb1.student_name','tb1.fathers_name',
                        'tb1.mothers_name','tb1.student_group','users.id as userId','users.name as madrasha_name',
                        'users.username as madrasha_eiin','tb3.center_code',
                        'tb3.center_eiin','tb4.name as center_name']);

            }
        }
        //if(empty($users)) return redirect('select-option');
        $madrasha_eiin = (!empty($users[0]->madrasha_eiin) ? $users[0]->madrasha_eiin : '000');
        $pageNo = $offset+1;
        $fileName = $madrasha_eiin."-admit-card-".$pageNo;
        $pageTitle ='Fazil Pass Eff';
        $pdf_body = view("institute::print-fazil3-eff-html", compact('users','pageTitle'))->render();

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML($pdf_body);
        return $pdf->stream($fileName.'.pdf');
    }

    protected function submitAllStudent(Request $request)
    {
        if (!ACL::getAccsessRight('Institute', 'E')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }
        $InstId = Encryption::decodeId($request->get('InstId'));
        if($InstId !=''){
            if(Session::get('examVal') == 'fazilpass'){
                $submited_status_col = 'submited_status_fazil3';
            }
            else if(Session::get('examVal') == 'fazilhons'){
                $submited_status_col = 'submited_status_fazil4';
            } else if(Session::get('examVal') == 'fazilprivate'){
                $submited_status_col = 'submited_status_fazil3_private';
            } else {
                $submited_status_col = 'submited_status_fazil3';
            }
            $submited_status = Users::where('id',$InstId)->pluck($submited_status_col);
            if($submited_status == 'No') $subVal = 'Yes'; else $subVal = 'No';
            Users::where('id',$InstId)->update([
                $submited_status_col => $subVal
            ]);
            $result = array('success' => true, 'msg' => 'All Student Submited', 'status'=>$subVal);
        } else {
            $result = array('success' => false, 'msg' => 'Madrasah is not valid!');
        }
        return response()->json($result);
    }

    public function finalLockForFazilPassEff(Request $request){

        if (!ACL::getAccsessRight('Institute', 'E')) {
            die('You have no access right! Please contact with system admin if you have any query.');
        }

        $selected_institute = $request->get('selected_institute');
        $madId = '';
        if($selected_institute != ''){
            $madId = $selected_institute;
        } else {
            if(Auth::user()->user_type == '2x202')
                $madId = Auth::user()->username;
        }
        if($madId !=''){

            $final_lock_eff = 'Yes';
            if(Session::get('examVal') == 'fazilpass'){
                if(Session::get('typeVal') == 'eff'){
                    $finalLock = 'final_lock_fazil3_eff';
                }else{
                    $finalLock = '';
                }
            }
            else if(Session::get('examVal') == 'fazilhons'){
                if(Session::get('typeVal') == 'eff'){
                    $finalLock = '';
                }else{
                    $finalLock = '';
                }
            } else if(Session::get('examVal') == 'fazilprivate'){
                if(Session::get('typeVal') == 'eff'){
                    $finalLock = 'final_lock_fazil3_priv_eff';
                } else {
                    $finalLock = '';
                }
            } else {
                $finalLock = '';
            }
            $getLockEff = Users::where('username', $madId)->first([$finalLock]);
            if($getLockEff->$finalLock == 'Yes') $final_lock_eff = 'No';
            Users::where('username', $madId)->update(array($finalLock => $final_lock_eff));
            if($final_lock_eff == 'No'){ $msg = '<i class="fa fa-lock" aria-hidden="true"></i>  Final Lock'; } else { $msg = '<i class="fa fa-unlock" aria-hidden="true"></i>  Unlock'; }
            $result = array('success' => true, 'msg' => 'Successuflly done','status'=>$msg);
        } else {
            $result = array('success' => false, 'msg' => 'Please, select a madrasah');
        }
        return response()->json($result);
    }

    /*     * ***************************End of Controller Class******************************* */
}
