<?php

Route::group(array('module' => 'Institute', 'middleware' => 'auth', 'namespace' => 'App\Modules\Institute\Controllers'), function() {

    Route::get('/manage-institute/create-institute','InstituteController@createInstitute');
    Route::post('/manage-institute/store-institute','InstituteController@storeInstitute');
    //Student by Madrasah
    Route::get('manage-student/student-by-institute/{id}','InstituteController@studentByInstitute');

    Route::get('/manage-institute/edit-institute/{id}','InstituteController@editInstitute');
    Route::post('/manage-institute/update-institute','InstituteController@updateInstitute');
    Route::get('/manage-institute/delete-institute/{id}','InstituteController@deleteInstitute');

    Route::get('/manage-institute/show-institute','InstituteController@showInstitute');
    Route::post('/manage-institute/get-all-madrasah-lists','InstituteController@getAllMadrasahLists');
    //lock and unlock for fazil pass
    Route::post('/finalLockForFazilPassEff', ['as'=> 'ajaxfinalLockForFazilPassEff', 'uses'=>'InstituteController@finalLockForFazilPassEff']);
    // get district list
    Route::post('/manage-institute/get-selected-district','InstituteController@getDistrictList');
    // get thana list
    Route::post('/manage-institute/get-selected-thana','InstituteController@getThanaList');
    //Submit all student
    Route::post('/manage-institute/submit-all-student','InstituteController@submitAllStudent');
    //Print student by madarasah
    Route::get('manage-student/print-fazil3-eff-students/{id}','InstituteController@printFazil3EffStd');
    Route::post('/bulkAction', ['as'=> 'ajaxBulkAction', 'uses'=>'InstituteController@ajaxBulkActionClb']);

    //Route::get('/manage-institute/export-inisture/{id}','InstituteController@exportMadrasah');
    //Route::get('/manage-institute/upload-exam-center','InstituteController@uploadExamCenter');
    //Route::post('/manage-institute/upload-csv-file','InstituteController@uploadExamCenterFile');
    //Route::get('manage-institute/upload-exam-center-path/{path}', 'InstituteController@uploadExamCenterInsert');
    //Route::get('manage-institute/download-exam-center-list', 'InstituteController@downloadTotalStudentByCenter');

    Route::resource('manage-institute','InstituteController');

    //Route::post('manage-institute', 'InstituteController@store');
    //Route::post('update-institute/{id}', 'InstituteController@update');
    //Route::get('delete-institute/{id}', 'InstituteController@delete');
    //Route::get('std_by_mdh/{id}','InstituteController@student_by_madrasah');
    //Route::get('institute-details/{id}','InstituteController@view_details');

    //Route::post('/submitAllStudent', ['as'=> 'ajaxsubmitAllStudent', 'uses'=>'InstituteController@submitStudentInfo']);
    
    /******************************End of Route Group***********************************/
});