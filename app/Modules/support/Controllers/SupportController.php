<?php

namespace App\Modules\Support\Controllers;

use App\Http\Controllers\Controller;
use App\Libraries\CommonFunction;
use App\Libraries\Encryption;
use App\Modules\Settings\Models\Notice;
use App\Modules\Support\Models\Notification;
use App\Modules\Support\Models\PdfGenerator;
use App\Modules\Support\Models\Feedback;
use App\Modules\Support\Models\FeedbackTopics;
use App\Modules\Settings\Models\Faq;
use App\Modules\Settings\Models\FaqTypes;
use App\Modules\Users\Models\Users;
use App\Libraries\ACL;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use yajra\Datatables\Datatables;

class SupportController extends Controller {

    public function __construct() {
        if (Session::has('lang'))
            App::setLocale(Session::get('lang'));
    }

    public function nid_status() {

        return view("support::nid-status-list");
    }
    /* Start of Notice related functions */

    public function viewNotice($encrypted_id) {
        $id = Encryption::decodeId($encrypted_id);
        $data = Notice::where('id', $id)->first();
        $notice = CommonFunction::getNotice(1);
        return view("support::notice.view", compact('data', 'encrypted_id', 'notice'));
    }

    public function sitemap() {
        $user_type = Auth::user()->user_type;
        return view("support::help.menu",compact('user_type'));
    }

    /**
     * @param PdfGenerator $pdfGenerator
     * @return mixed
     * @internal param Notification $notificationModel
     */
    public function getNidStatusDetailsData() {
        $users_id = Auth::user()->id;
        $pilgrimNid = PilgrimNid::orderBy('created_at', 'desc')
                        ->where('created_by', '=', $users_id)
                        ->limit(50)->get(['id', 'nid', 'name', 'dob', 'verification_flag', 'submitted_at']);

        return Datatables::of($pilgrimNid)
                        ->addColumn('action', function ($pilgrimNid) {
                            if ($pilgrimNid->verification_flag == -9)
                                return '<a href="' . url('support/re_submit_nid_status/' . Encryption::encodeId($pilgrimNid->id)) . '" class="btn btn-xs btn-primary" onclick="return confirm(\'Are you sure you re-submit it ?\');">Re-submit</a> &nbsp;';
                            else
                                return '';
                        })
                        ->addColumn('dob', function ($pilgrimNid) {
                            return CommonFunction::age($pilgrimNid->dob);
                        })
                        ->addColumn('verification_flag', function ($pilgrimNid) {
                            if ($pilgrimNid->verification_flag == 0 || $pilgrimNid->verification_flag == -1) {
                                return 'In Process';
                            } elseif ($pilgrimNid->verification_flag == 1) {
                                return 'Process';
                            } elseif ($pilgrimNid->verification_flag == -9) {
                                return 'Failed';
                            } else {
                                return $pilgrimNid->verification_flag;
                            }
                        })
                        ->addColumn('submitted_at', function ($pilgrimNid) {
                            return CommonFunction::updatedOn($pilgrimNid->submitted_at);
                        })
                        ->removeColumn('id')
                        ->make(true);
    }

    public function reSubmitNidStatus($id) {
        $decoded_id = Encryption::decodeId($id);
        $reSubmitNidStatus = PilgrimNid::where('id', $decoded_id)
                ->delete();
        if ($reSubmitNidStatus) {
            Session::flash('success', 'Successfully NID re-submited.');
        } else {
            Session::flash('error', 'Sorry ! NID re-submit is failed.');
        }
        return Redirect('support/nid_status');
    }

    public function searchNid(Request $request, PilgrimNid $pilgrimNid) {
        $search_result = $pilgrimNid->getSearchList(trim($request->get('q')));
        // dd($request->all());
        return view("support::search-nid-list", compact('search_result'));
    }

    public function sms_status() {
        return view("support::sms-status-list");
    }

    /**
     * @param Notification $notificationModel
     * @return mixed
     */
    public function getNotificationDetailsData(Notification $notificationModel) {
        $notification = $notificationModel->getNotificationList();
        return Datatables::of($notification)
                        ->addColumn('action', function ($notification) {
                            if ($notification->is_sent == 1)
                                return '<a href="' . url('support/re_send_sms_status/' . Encryption::encodeId($notification->id)) . '" class="btn btn-xs btn-primary">Re-send</a> &nbsp;';
                            else
                                return '';
                        })
                        ->editColumn('source', function ($notification) {
                            return substr($notification->source, 0, 30) . '.......';
                        })
                        ->editColumn('created_at', function ($notification) {
                            return CommonFunction::updatedOn($notification->created_at);
                        })
                        ->editColumn('sent_on', function ($notification) {
                            return CommonFunction::updatedOn($notification->sent_on);
                        })
                        ->make(true);
    }

    public function reSendSMSStatus($id) {
        $decoded_id = Encryption::decodeId($id);

        $preSMS = Notification::findOrFail($decoded_id);
        $insert = Notification::create([
                    'source' => $preSMS->source,
                    'ref_id' => $preSMS->ref_id,
                    'destination' => $preSMS->destination,
                    'is_sent' => 0,
                    'sent_on' => $preSMS->sent_on,
                    'no_of_try' => $preSMS->no_of_try,
                    'msg_type' => $preSMS->msg_type,
                    'template_id' => $preSMS->template_id,
        ]);

        if ($insert) {
            Session::flash('success', 'Successfully SMS re-sent.');
        } else {
            Session::flash('error', 'Sorry ! SMS re-sent is failed.');
        }
        return Redirect('support/sms_status');
    }

    public function searchSMS(Request $request, Notification $notification) {
        $search_result = $notification->getSearchList(trim($request->get('q')));
        //dd($search_result);
        return view("support::search-sms-list", compact('search_result'));
    }

    public function pdf_status() {

        return view("support::pdf-status-list");
    }

    /**
     * @param PdfGenerator $pdfGenerator
     * @return mixed
     * @internal param Notification $notificationModel
     */
    public function getPdfStatusDetailsData(PdfGenerator $pdfGenerator) {
        $pdfGeneratorInfo = $pdfGenerator->getPDFGenerateList();
        return Datatables::of($pdfGeneratorInfo)
                        ->addColumn('action', function ($pdfGeneratorInfo) {
                            return '<a href="' . url('support/re_pdf_status/' . Encryption::encodeId($pdfGeneratorInfo->id)) . '" class="btn btn-xs btn-primary">Re-generate</a> &nbsp;';
                        })
                        ->editColumn('created_at', function ($pdfGeneratorInfo) {
                            return CommonFunction::updatedOn($pdfGeneratorInfo->created_at);
                        })
                        ->editColumn('updated_at', function ($pdfGeneratorInfo) {
                            return CommonFunction::updatedOn($pdfGeneratorInfo->updated_at);
                        })
                        ->make(true);
    }

    public function reGeneratePdfStatus($id) {
        $decoded_id = Encryption::decodeId($id);
        //echo $decoded_id;exit;
        $pdfReGenerate = PdfGenerator::where('id', $decoded_id)
                ->decrement('no_of_try', 1, ['status' => 0]);
        if ($pdfReGenerate) {
            Session::flash('success', 'PDF re-generation has been complete successfully.');
        } else {
            Session::flash('error', 'Sorry! PDF re-generation has been failed.');
        }
        return Redirect('support/pdf_status');
    }

    public function searchPdf(Request $request, PdfGenerator $pdfGenerator) {
        $search_result = $pdfGenerator->getSearchList(trim($request->get('q')));
        return view("support::search-pdf-list", compact('search_result'));
    }

    /* Start of Feedback related functions */

    public function feedback() {
        return view("support::feedback.list");
    }

    /**
     * @return \BladeView|bool|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createFeedback() {
        $topics = FeedbackTopics::lists('name', 'id');
        return view("support::feedback.create", compact('topics'));
    }

    public function getFeedbackDetailsData() {
        $feedbacks = Feedback::leftJoin('feedback_topics', 'feedback.topic_id', '=', 'feedback_topics.id')
//                ->where('status', '!=', 'closed')
                ->where('created_by', Auth::user()->id)
                ->where('parent_id', 0)
                ->get(['feedback.id as feedback_id', 'feedback_topics.name as topic_name', 'description', 'status',
            'feedback.created_by as feedbackCreator']);
        return Datatables::of($feedbacks)
                        ->addColumn('action', function ($feedbacks) {
                            if ($feedbacks->status == 'draft' AND $feedbacks->feedbackCreator == Auth::user()->id) {
                                $functionUrl = 'edit-feedback';
                            } else {
                                $functionUrl = 'view-feedback';
                            }
                            return '<a href="/support/' . $functionUrl . '/' . Encryption::encodeId($feedbacks->feedback_id) .
                                    '" class="btn btn-xs btn-primary"><i class="fa fa-folder-open-o"></i> Open</a>';
                        })
                        ->editColumn('status', function ($feedbacks) {
                            return ucfirst($feedbacks->status);
                        })
                        ->removeColumn('feedback_id')
                        ->make(true);
    }

    public function getUncategorizedFeedbackData($flag) {
        if ($flag == 'submitted_to') {
            $assigned_to = Auth::user()->id;
        } elseif ($flag == 'unassigned') {
            $assigned_to = 0;
        }
        $feedbacks = Feedback::leftJoin('feedback_topics', 'feedback.topic_id', '=', 'feedback_topics.id')
                ->where('assigned_to', $assigned_to)
                ->where('parent_id', 0)
                ->get(['feedback.id as feedback_id', 'feedback_topics.name as topic_name', 'description', 'status',
            'feedback.created_by as feedbackCreator']);
        return Datatables::of($feedbacks)
                        ->addColumn('action', function ($feedbacks) {
                            if ($feedbacks->status == 'draft' AND $feedbacks->feedbackCreator == Auth::user()->id) {
                                $functionUrl = 'edit-feedback';
                            } else {
                                $functionUrl = 'view-feedback';
                            }
                            return '<a href="/support/' . $functionUrl . '/' . Encryption::encodeId($feedbacks->feedback_id) .
                                    '" class="btn btn-xs btn-primary"><i class="fa fa-folder-open-o"></i> Open</a>';
                        })
                        ->editColumn('status', function ($feedbacks) {
                            return ucfirst($feedbacks->status);
                        })
                        ->removeColumn('feedback_id')
                        ->make(true);
    }

    public function storeFeedback(Request $request) {
        $this->validate($request, [
            'topic_id' => 'required',
            'description' => 'required'
        ]);

        if ($request->get('sent') == 'sent') {
            $status = 'submitted';
            $functionUrl = 'view-feedback';
        } elseif ($request->get('draft') == 'draft') {
            $status = 'draft';
            $functionUrl = 'edit-feedback';
        }

        $insert = Feedback::create(
                        array(
                            'topic_id' => $request->get('topic_id'),
                            'description' => $request->get('description'),
                            'priority' => $request->get('priority'),
                            'status' => $status,
                            'created_by' => CommonFunction::getUserId()
        ));

        Session::flash('success', 'Data is stored successfully!');
        return redirect('/support/' . $functionUrl . '/' . Encryption::encodeId($insert->id));
    }

    public function editFeedback($encrypted_id) {
        $id = Encryption::decodeId($encrypted_id);
        $data = Feedback::where('id', $id)->first();
        $topics = FeedbackTopics::lists('name', 'id');
        return view("support::feedback.edit", compact('data', 'encrypted_id', 'topics'));
    }

    public function updateFeedback($id, Request $request) {
        $feedback_id = Encryption::decodeId($id);

        $this->validate($request, [
            'topic_id' => 'required',
            'description' => 'required',
        ]);

        if ($request->get('sent') == 'sent') {
            $status = 'submitted';
        } elseif ($request->get('draft') == 'draft') {
            $status = 'draft';
        }

        Feedback::where('id', $feedback_id)->update([
            'topic_id' => $request->get('topic_id'),
            'description' => $request->get('description'),
            'priority' => $request->get('priority'),
            'status' => $status,
            'updated_by' => CommonFunction::getUserId()
        ]);

        Session::flash('success', 'Data has been changed successfully.');
        return redirect('/support/edit-feedback/' . $id);
    }

    public function viewFeedback($encrypted_id) {
        $id = Encryption::decodeId($encrypted_id);
        $data = Feedback::where('id', $id)->first();
        $topics = FeedbackTopics::lists('name', 'id');

        $logged_in_user_type = CommonFunction::getUserType();

        $replies = Feedback::leftJoin('users', 'feedback.created_by', '=', 'users.id')
                ->where('parent_id', $id)
                ->groupBy('feedback.id')
                ->orderBy('feedback.created_at', 'desc')
                ->get(['description', 'feedback.created_at as time',
            'users.id as replier_id', 'users.user_full_name as replied_by']);

        $call_centre_users = Users::where('user_type', '2x203') // 2x203 is for call center users
                ->lists('user_full_name', 'id');

        return view("support::feedback.view", compact('data', 'encrypted_id', 'topics', 'replies', 'call_centre_users', 'logged_in_user_type'));
    }

    public function storeFeedbackReply(Request $request) {
        $this->validate($request, [
            'topic_id' => 'required',
            'description' => 'required'
        ]);

        if ($request->get('reply') == 'reply') {
            $status = 'replied';
        }
        Feedback::create(
                array(
                    'topic_id' => $request->get('topic_id'),
                    'description' => $request->get('description'),
                    'parent_id' => Encryption::decodeId($request->get('parent_feedback')), // feedback id for which reply has been given
                    'status' => $status,
                    'created_by' => CommonFunction::getUserId()
        ));

        Session::flash('success', 'Data is stored successfully!');
        return redirect('/support/view-feedback/' . $request->get('parent_feedback'));
    }

    public function assignFeedback($id, Request $request) {
        $this->validate($request, [
            'assigned_to' => 'required',
        ]);

        $feedback_id = Encryption::decodeId($id);

        Feedback::where('id', $feedback_id)->update([
            'assigned_to' => $request->get('assigned_to'),
            'updated_by' => CommonFunction::getUserId()
        ]);

        Session::flash('success', 'User has been assigned successfully!');
        return redirect('/support/view-feedback/' . $id);
    }

    public function changeFeedbackStatus(Request $request) {
        $this->validate($request, [
            'parent_id' => 'required',
        ]);

        if ($request->get('rejected') == 'rejected') {
            $status = 'rejected';
        } else if ($request->get('resolved') == 'resolved') {
            $status = 'resolved';
        } else if ($request->get('canceled') == 'canceled') {
            $status = 'canceled';
        }

        $feedback_id = Encryption::decodeId($request->get('parent_id'));

        Feedback::where('id', $feedback_id)->update([
            'status' => $status,
            'updated_by' => CommonFunction::getUserId()
        ]);

        Session::flash('success', 'Data is stored successfully!');
        return redirect('/support/view-feedback/' . $request->get('parent_id'));
    }

    /* End of Feedback related functions */

    public function help($module = '') {
        $faqs = Faq::leftJoin('faq_multitypes', 'faq.id', '=', 'faq_multitypes.faq_id')
                ->leftJoin('faq_types', 'faq_multitypes.faq_type_id', '=', 'faq_types.id')
                ->where('status', 'public')
                ->where('faq_types.name', $module)
                ->get(['question', 'answer', 'status', 'faq_type_id as types', 'name as faq_type_name', 'faq.id as id']);

        $existedFaqType = FaqTypes::where('name', $module)->pluck('name');
        if (empty($existedFaqType)) {
            FaqTypes::create(
                    array(
                        'name' => ucfirst($module),
                        'created_by' => CommonFunction::getUserId()
            ));
        }

        if ($faqs == null) {
            Session::flash('error', 'Sorry, there is no help available for this module!');
        }

        return view("support::help.help", compact('faqs'));
    }

    /*     * *******************End of Controller******************** */
}
