@extends('layouts.admin')

@section('page_heading','<i class="fa fa-book fa-fw"></i> '.trans('messages.report_view'))

@section('content')
    <?php $accessMode=ACL::getAccsessRight('report');
    if(!ACL::isAllowed($accessMode,'V')) die('no access right!');
    ?>
    <div class="col-lg-12">

        {!! Session::has('success') ? '<div class="alert alert-success alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("success") .'</div>' : '' !!}
        {!! Session::has('error') ? '<div class="alert alert-danger alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. Session::get("error") .'</div>' : '' !!}

        <div class="panel panel-primary">
            <div class="panel-heading">
                <?php echo  $report_data->report_title . ''; ?>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                @include('reports::input-form')
            </div><!-- /.box-body -->
        </div>
    </div>


@endsection

@section('footer-script')
    <script>
        $(function () {
            // $("#report_list").DataTable();
//            $('#report_data').DataTable({
//                "paging": true,
//                "lengthChange": false,
//                "ordering": true,
//                "info": true,
//                "autoWidth": true,
//                "iDisplayLength": 20
//            });

//        $("#rpt_date").datepicker({
//            maxDate: "+20Y",
//            //showOn: "button",
//            //buttonText: "Select date",
//            buttonText: "Select date",
//            changeMonth: true,
//            changeYear: true,
//            dateFormat: 'yy-mm-dd',
//            showAnim: 'scale',
//            yearRange: "-100:+40",
//            minDate: "-200Y",
//        });

        });
    </script>
@endsection
