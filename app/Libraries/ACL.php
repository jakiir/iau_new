<?php

namespace App\Libraries;

use App\Libraries\CommonFunction;
use App\Modules\Certificate\Models\UploadedCertificates;
use App\Modules\exportPermit\Models\ExportPermit;
use App\Modules\importPermit\Models\ImportPermit;
use App\Modules\projectClearance\Models\ProjectClearance;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ACL {

    public static function db_reconnect() {
        if (Session::get('DB_MODE') == 'PRODUCTION') {
            //    DB::purge('mysql-main');
//        DB::setDefaultConnection('mysql-main');
            // DB::setDefaultConnection(Session::get('mysql_access'));
        }
    }

    public static function hasImportApplicationModificationRight($user_type, $right, $id) {
        try {
            if ($right != 'E')
                return true;
            $importInfo = ImportPermit::LeftJoin('process_list', function($join) {
                        $join->on('process_list.record_id', '=', 'import_permit.id');
                        $join->on('process_list.service_id', '=', DB::raw('6'));
                    })
                    ->where('import_permit.id', $id)
                    ->first(['process_list.status_id', 'import_permit.created_by']);
            if ($importInfo->created_by == Auth::user()->id && in_array($importInfo->status_id, [-1, 5, 21, 24]))
                return true;
            return false;
        } catch (\Exception $e) {
            dd($e->getMessage());
            return false;
        }
    }

    public static function hasProjectClearanceModificationRight($user_type, $right, $id) {
        try {
            if ($right != 'E')
                return true;
            $projectClearanceInfo = ProjectClearance::LeftJoin('process_list', function($join) {
                        $join->on('process_list.record_id', '=', 'project_clearance.id');
                        $join->on('process_list.service_id', '=', DB::raw('1'));
                    })
                    ->where('project_clearance.id', $id)
                    ->first(['process_list.status_id', 'project_clearance.created_by']);
            if ($projectClearanceInfo->created_by == Auth::user()->id && in_array($projectClearanceInfo->status_id, [-1, 5, 21, 24]))
                return true;
            return false;
        } catch (\Exception $e) {
            dd($e->getMessage());
            return false;
        }
    }

    public static function hasExportApplicationModificationRight($user_type, $right, $id) {
        try {
            if ($right != 'E')
                return true;
            $exportInfo = ExportPermit::LeftJoin('process_list', function($join) {
                        $join->on('process_list.record_id', '=', 'export_permit.id');
                        $join->on('process_list.service_id', '=', DB::raw('5'));
                    })
                    ->where('export_permit.id', $id)
                    ->first(['process_list.status_id', 'export_permit.created_by']);
            if ($exportInfo->created_by == Auth::user()->id && in_array($exportInfo->status_id, [-1, 5, 21, 24]))
                return true;
//            if ($exportInfo->created_by == Auth::user()->id && ($exportInfo->status_id == -1 || $exportInfo->status_id == 5 || $exportInfo->status_id == 21))
            return false;
        } catch (\Exception $e) {
            dd($e->getMessage());
            return false;
        }
    }

    public static function hasCertificateModificationRight($right, $id) {
        try {
            if ($right != 'E')
                return true;
            $exportInfo = UploadedCertificates::where('uploaded_certificates.doc_id', $id)
                    ->first(['created_by']);
            if ($exportInfo->created_by == Auth::user()->id) {
                return true;
            }
            return false;
        } catch (\Exception $e) {
            dd(CommonFunction::showErrorPublic($e->getMessage()));
            return false;
        }
    }

    public static function getAccsessRight($module, $right = '', $id = null) {
        $accessRight = '';
        if (Auth::user()) {
            $user_type = Auth::user()->user_type;
        } else {
            die('You are not authorized user or your session has been expired!');
        }
        switch ($module) {
            case 'settings':
                if ($user_type == '1x101') {
                    $accessRight = 'AVE';
                }
                break;
            case 'dashboard':
                if ($user_type == '1x101') {
                    $accessRight = 'AVESERN';
                } elseif ($user_type == '5x505') {
                    $accessRight = 'AVESERNH';
                }
                break;

            case 'Institute':
                if ($user_type == '1x101') {
                    $accessRight = 'AVEDSERN';
                } else {
                    $accessRight = '';
                }
            break;
            case 'fazilPass':
                if ($user_type == '1x101') {
                    $accessRight = 'AVEDSERN';
                } else {
                    $accessRight = '';
                }
                break;
            case 'report':
                if ($user_type == '1x101') {
                    $accessRight = 'AVE';
                } else if ($user_type == '5x505' || $user_type == '6x606') {
                    $accessRight = 'V';
                } else {
                    $accessRight = 'V';
                }
                break;

            case 'user':
                if ($user_type == '1x101') {
                    $accessRight = 'AVER';
                } else if ($user_type == '2x202') {
                    $accessRight = 'VER';
                } else if ($user_type == '4x404') {
                    $accessRight = 'VR';
                } else {
                    
                }
                break;

            case 'visa_assistance':
                if ($user_type == '6x606') {
                    $accessRight = 'AVE';
                } else if ($user_type == '1x101' || in_array(Auth::user()->desk_id, array(1, 2, 3, 4, 5, 6))) {
                    $accessRight = 'AVE';
                } else if ($user_type == '4x404') {
                    $accessRight = 'AVE';
                } else if ($user_type == '8x808' || $user_type == '7x707') { //7x707=Super and 8x808=Zone HMIS user
                    $accessRight = 'V';
                } else {
                    $accessRight = '';
                }
                break;
            case 'processPath':
                if ($user_type == '1x101') {
                    $accessRight = 'AVE';
                }
                break;
            case 'projectClearance':
                if ($user_type == '1x101' || $user_type == '2x202' || $user_type == '4x404' || $user_type == '7x707' || $user_type == '8x808') {//7x707=Super and 8x808=Zone HMIS user
                    $accessRight = 'V';
                } else if ($user_type == '5x505') {
                    $accessRight = 'AVE';
                    if ($id != null && !(strpos($accessRight, $right) === false)) {
                        if (ACL::hasProjectClearanceModificationRight($user_type, $right, $id) == false)
                            return false;
                    }
                }
                break;
            case 'importPermit':
                if ($user_type == '1x101' || $user_type == '2x202' || $user_type == '3x303' || $user_type == '4x404' || $user_type == '7x707' || $user_type == '8x808' || $user_type == '9x909') {
                    //7x707=Super and 8x808=Zone HMIS user, 9x909 Customs
                    $accessRight = 'V';
                } else if ($user_type == '5x505') {
                    $accessRight = 'AVE';
                    if ($id != null && !(strpos($accessRight, $right) === false)) {
                        if (ACL::hasImportApplicationModificationRight($user_type, $right, $id) == false)
                            return false;
                    }
                }
                break;
            case 'exportPermit':
                if ($user_type == '1x101' || $user_type == '2x202' || $user_type == '3x303' || $user_type == '4x404' || $user_type == '7x707' || $user_type == '8x808' || $user_type == '9x909') {
                    //7x707=Super and 8x808=Zone HMIS user, 9x909 Customs
                    $accessRight = 'V';
                } else if ($user_type == '5x505') {
                    $accessRight = 'AVE';
                    if ($id != null && !(strpos($accessRight, $right) === false)) {
                        if (ACL::hasExportApplicationModificationRight($user_type, $right, $id) == false)
                            return false;
                    }
                }
                break;
            case 'workPermit':
                if ($user_type == '1x101' || $user_type == '2x202' || $user_type == '4x404' || $user_type == '7x707' || $user_type == '8x808') {//7x707=Super and 8x808=Zone HMIS user
                    $accessRight = 'V';
                } else if ($user_type == '5x505') {
                    $accessRight = 'AVE';
                    if ($id != null && !(strpos($accessRight, $right) === false)) {
                        if (ACL::hasExportApplicationModificationRight($user_type, $right, $id) == false)
                            return false;
                    }
                }
                break;
            case 'certificate':
                if ($user_type == '4x404') {//4x404=Desk user
                    $accessRight = 'V';
                } else if ($user_type == '5x505') { // 5x505 = Unit investors
                    $accessRight = 'AVE';
                    if ($id != null && !(strpos($accessRight, $right) === false)) {
                        if (ACL::hasCertificateModificationRight($right, $id) == false)
                            return false;
                    }
                }
                break;

            default:
                $accessRight = '';
        }
        if ($right != '') {
            if (strpos($accessRight, $right) === false) {
                return false;
            } else {
                return true;
            }
        } else
            return $accessRight;
    }

    public static function isAllowed($accessMode, $right) {
        if (strpos($accessMode, $right) === false) {
            return false;
        } else {
            return true;
        }
    }

    /*     * **********************************End of Class****************************************** */
}
