<?php

namespace App\Libraries;

use App\Modules\Apps\Models\Apps;
use App\Modules\Apps\Models\processVerifylist;
use App\Modules\Files\Controllers\FilesController;
use App\Modules\projectClearance\Models\ProjectClearance;
use App\Modules\Settings\Models\Configuration;
use App\Modules\Users\Models\UserTypes;
use App\Modules\Users\Models\UserDesk;
use App\Modules\workPermit\Models\Processlist;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Session;

class CommonFunction {

    /**
     * @param Carbon|string $updated_at
     * @param string $updated_by
     * @return string
     * @internal param $Users->id /string $updated_by
     */
    public static function showAuditLog($updated_at = '', $updated_by = '') {
        $update_was = 'Unknown';
        if ($updated_at && $updated_at > '0') {
            $update_was = Carbon::createFromFormat('Y-m-d H:i:s', $updated_at)->diffForHumans();
        }

        $user_name = 'Unknown';
        if ($updated_by) {
            $name = User::where('id', $updated_by)->first();
            if ($name) {
                $user_name = $name->user_full_name;
            }
        }
        return '<span class="help-block">Last updated : <i>' . $update_was . '</i> by <b>' . $user_name . '</b></span>';
    }

    public static function updatedOn($updated_at = '') {
        $update_was = '';
        if ($updated_at && $updated_at > '0') {
            $update_was = Carbon::createFromFormat('Y-m-d H:i:s', $updated_at)->diffForHumans();
        }
        return $update_was;
    }

    public static function updatedBy($updated_by = '') {
        $user_name = 'Unknown';
        if ($updated_by) {
            $name = User::find($updated_by);
            if ($name) {
                $user_name = $name->user_full_name;
            }
        }
        return $user_name;
    }

    public static function getUserId() {

        if (Auth::user()) {
            return Auth::user()->id;
        } else {
            return 'Invalid Login Id';
        }
    }

    public static function getUserType() {

        if (Auth::user()) {
            return Auth::user()->user_type;
        } else {
            // return 1;
            dd('Invalid User Type');
        }
    }

    public static function getDeskId() {
        if (Auth::user()) {
            return Auth::user()->desk_id;
        } else {
            CommonFunction::redirectToLogin();
        }
    }

    public static function redirectToLogin() {
        echo "<script>location.replace('users/login');</script>";
    }

    public static function formateDate($date = '') {
        return date('d.m.Y', strtotime($date));
    }

    public static function getUserStatus() {

        if (Auth::user()) {
            return Auth::user()->user_status;
        } else {
            // return 1;
            dd('Invalid User Type');
        }
    }

    public static function convertUTF8($string) {
//        $string = 'u0986u09a8u09c7u09beu09dfu09beu09b0 u09b9u09c7u09beu09b8u09beu0987u09a8';
        $string = preg_replace('/u([0-9a-fA-F]+)/', '&#x$1;', $string);
        return html_entity_decode($string, ENT_COMPAT, 'UTF-8');
    }

    public static function showDate($updated_at = '') {
        if ($updated_at && $updated_at > '0') {
            $update_was = Carbon::createFromFormat('Y-m-d H:i:s', $updated_at)->diffForHumans();
        }

        return '<span class="help-block"><i>' . $update_was . '</i></span>';
    }

    public static function checkUpdate($model, $id, $updated_at) {
        if ($model::where('updated_at', $updated_at)->find($id)) {
            return true;
        } else {
            return false;
        }
    }

    /* This function determines if an user is an admin or sub-admin
     * Based On User Type
     *  */

    public static function isAdmin() {
        $user_type = Auth::user()->user_type;
        /*
         * 1_101 for System Admin
         * 5_506 for UNO Admin
         * 10x411 for DC Office Admin
         * 7x712 for SB Admin
         * Desk id 6 = executive
         */
        if ($user_type == '1x101' || $user_type == '2x202' || Auth::user()->desk_id == 6 || Auth::user()->desk_id == 4) {
            return true;
        } else {
            return false;
        }
    }

    public static function isSB() {
        $user_type = Auth::user()->user_type;
        /*
         * 7_711 for SB Admin
         * 7_712 for SB Field (SB)
         * 7_713 for SB Field Investigator (SB)
         */
        if ($user_type == '7x712' || $user_type == '7x713') {
            return true;
        } else {
            return false;
        }
    }

    public static function isBank() {
        $user_type = Auth::user()->user_type;
        if ($user_type == '11x421' || $user_type == '11x422') {
            return true;
        } else {
            return false;
        }
    }

    public static function changeDateFormat($dateicker, $mysql = false) {
        if ($mysql) {
            return Carbon::createFromFormat('d-M-Y', $dateicker)->format('Y-m-d');
        } else {
            return Carbon::createFromFormat('Y-m-d', $dateicker)->format('d-M-Y');
        }
    }

    public static function age($birthDate) {
        $year = '';
        if ($birthDate) {
            $year = Carbon::createFromFormat('Y-m-d', $birthDate)->diff(Carbon::now())->format('%y years, %m months and %d days');
        }
        return $year;
    }

    public static function getFieldName($id, $field, $search, $table) {

        if ($id == NULL || $id == '') {
            return '';
        } else {
            return DB::table($table)->where($field, $id)->pluck($search);
        }
    }

    public static function getEcoId(){
        $eco_zone_id = ProjectClearance::where('created_by', Auth::user()->id)
               ->where('status_id', 23)
            ->first(['eco_zone_id']);
          if(!empty($eco_zone_id->eco_zone_id)){
              return  $eco_zone_id->eco_zone_id;
          }
        else{
            return  0;
        }

    }



    public static function getPicture($type, $ref_id) {
        $files = new FilesController();
        $img_data = $files->getFile(['type' => $type, 'ref_id' => $ref_id]);
        $json_data = json_decode($img_data->getContent());
        if ($json_data->responseCode == 1) {
            $base64 = $json_data->data;
        } else {
            $path = 'assets/images/no_image.png';
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        }
        return $base64;
    }

    public static function convert2Bangla($eng_number) {
        $eng = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
        $ban = ['০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯'];
        return str_replace($eng, $ban, $eng_number);
    }

    public static function convert2English($ban_number) {
        $eng = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
        $ban = ['০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯'];
        return str_replace($ban, $eng, $ban_number);
    }

    public static function generateTrackingID($prefix, $id) {
        $prefix = strtoupper($prefix);
        $str = $id . date('Y') . mt_rand(0, 9);
        if ($prefix == 'M' || $prefix == 'N') {
            if (strlen($str) > 12) {
                $str = substr($str, strlen($str) - 12);
            }
        } elseif ($prefix == 'G') {
            if (strlen($str) > 10) {
                $str = substr($str, strlen($str) - 10);
            }
        } elseif ($prefix == 'T') {
            if (strlen($str) > 12) {
                $str = substr($str, strlen($str) - 12);
            }
        } else {
            if (strlen($str) > 14) {
                $str = substr($str, strlen($str) - 14);
            }
        }
        return $prefix . dechex($str);
    }

    /** Created by monir */
    public static function payMode() {
        $payMode = Configuration::where('caption', 'SINGLE_PAYMENT')->first();
        if ($payMode->value) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return bool
     */
    public static function passportMode() {
        $passportMode = Configuration::where('caption', 'PASSPORT_REQUIRED')->first();
        if ($passportMode->value == '1') {
            return true;
        } else {
            return false;
        }
    }

    public static function is_entryRestricted($management = 'Private') {
        if ($management == 'Private') {
            $mode = Configuration::where('caption', 'PRE_REG_PRIVATE_PERIOD')->first();
        } elseif ($management == 'Government') {
            $mode = Configuration::where('caption', 'PRE_REG_GOVT_PERIOD')->first();
        } else {
            return true;
        }
        $quota = Pilgrim::where(['is_govt' => $management, 'is_archived' => 0])->count();
        if ($mode) {
            if ((date('Y-m-d') >= $mode->value) && (date('Y-m-d') <= $mode->value2) && ($quota < $mode->value3)) {
                return false;
            } else {
                return true;
            }
        }
        return true;
    }

    public static function getImageConfig($type) {
        extract(CommonFunction::getImageDocConfig());
        $config = Configuration::where('caption', $type)->pluck('details');
        $reportHelper = new ReportHelper();
//        [File Format: *.jpg / *.png Dimension: {$height}x{$width}px File size($filesize)KB]
        if ($type == 'IMAGE_SIZE') {
            $data['width'] = ($IMAGE_WIDTH - ($IMAGE_WIDTH * $IMAGE_DIMENSION_PERCENT) / 100) . '-' . ($IMAGE_WIDTH + ($IMAGE_WIDTH * $IMAGE_DIMENSION_PERCENT) / 100);
            $data['height'] = ($IMAGE_HEIGHT - ($IMAGE_HEIGHT * $IMAGE_DIMENSION_PERCENT) / 100) . '-' . ($IMAGE_HEIGHT + ($IMAGE_HEIGHT * $IMAGE_DIMENSION_PERCENT) / 100);
            $data['variation'] = $IMAGE_DIMENSION_PERCENT;
            $data['filesize'] = $IMAGE_SIZE;
        } elseif ($type == 'DOC_IMAGE_SIZE') {
            $data['width'] = ($DOC_WIDTH - ($DOC_WIDTH * $IMAGE_DIMENSION_PERCENT) / 100) . '-' . ($DOC_WIDTH + ($DOC_WIDTH * $IMAGE_DIMENSION_PERCENT) / 100);
            $data['height'] = ($DOC_HEIGHT - ($DOC_HEIGHT * $IMAGE_DIMENSION_PERCENT) / 100) . '-' . ($DOC_HEIGHT + ($DOC_HEIGHT * $IMAGE_DIMENSION_PERCENT) / 100);
            $data['variation'] = $DOC_DIMENSION_PERCENT;
            $data['filesize'] = $DOC_SIZE;
        }
        $string = $reportHelper->ConvParaEx($config, $data);
        return $string;
    }

    public static function deletePilgrim($pilgrim = false) {
        $ifLeader = Group::where(array('leader_id' => $pilgrim))->first();
        if (empty($ifLeader)) {
            $pilgrimRecord = Pilgrim::where(array('id' => $pilgrim, 'created_by' => Auth::user()->id, 'payment_status' => ' <10', 'group_payment_id' => '0'))->first();
            if ($pilgrimRecord) {

                if (ACL::getAccsessRight('pilgrim', 'D'))
                    return ' <a href="' . url('pilgrim/remove-pilgrim/' . Encryption::encodeId($pilgrim)) . '" onclick="return confirm(\'Are you sure to archive this pilgrim ?\');" class="btn btn-bg btn-danger pull-right" style="margin:0px 5px;"><i class="fa fa-trash-o"></i> Delete</a>';
                else
                    return '';
            } else {
                return false;
            }
        }
    }

    public static function validateMobileNumber($mobile_no) {
        $mobile_validation_err = '';
        $first_digit = substr($mobile_no, 0, 1);
        $first_two_digit = substr($mobile_no, 0, 2);
        $first_four_digit = substr($mobile_no, 0, 5);
        // if first two digit is 01
        if (strlen($mobile_no) < 11) {
            $mobile_validation_err = 'Mobile number should be minimum 11 digit';
        } elseif ($first_two_digit == '01') {
            if (strlen($mobile_no) != 11) {
                $mobile_validation_err = 'Mobile number should be 11 digit';
            }
        }
        // if first two digit is +880
        else if ($first_four_digit == '+8801') {
            if (strlen($mobile_no) != 14) {
                $mobile_validation_err = 'Mobile number should be 14 digit';
            }
        }
        // if first digit is only
        else if ($first_digit == '+') {
            // Mobile number will be ok
        } else {
            $mobile_validation_err = 'Please enter valid Mobile number';
        }

        if (strlen($mobile_validation_err) > 0) {
            return $mobile_validation_err;
        } else {
            return 'ok';
        }
    }

    public static function getResultStatus($app_id, $desk) {
        if ($desk == 3) {
            $desk_name = 'sb';
        } elseif ($desk == 4) {
            $desk_name = 'nsi';
        }elseif ($desk == 9) {
            $desk_name = 'sb'; //sb dig
        }elseif ($desk == 10) {
            $desk_name = 'nsi'; //nsi director
        }
        $status = processVerifylist::where('app_id', $app_id)
            ->where('sb_nsi_flag', $desk_name)
            ->where('status_id', '>', 0)
            ->orderBy('created_at', 'desc')
            ->limit(1)
            ->pluck('status_id');
        if (empty($status))
            $status = 0;
        return $status;
    }

    public static function getNotice($flag = 0)
    {
        if ($flag == 1) {
            $list = DB::select(DB::raw("SELECT date_format(updated_at,'%d %M, %Y') `Date`,heading,details,importance,id, case when importance='Top' then 1 else 0 end Priority FROM notice where status='public' or status='private' order by Priority desc, updated_at desc LIMIT 10"));
        } else {
            $list = DB::select(DB::raw("SELECT date_format(updated_at,'%d %M, %Y') `Date`,heading,details,importance,id, case when importance='Top' then 1 else 0 end Priority FROM notice where status='public' order by Priority desc, updated_at desc LIMIT 10"));
        }
        return $list;
    }

    public static function getImageDocConfig() {
        $config = array();
        $config['IMAGE_DIMENSION'] = Configuration::where('caption', 'IMAGE_SIZE')->pluck('value');
        $config['IMAGE_SIZE'] = Configuration::where('caption', 'IMAGE_SIZE')->pluck('value2');

        // Image size
        $split_img_size = explode('-', $config['IMAGE_SIZE']);
        $config['IMAGE_MIN_SIZE'] = $split_img_size[0];
        $config['IMAGE_MAX_SIZE'] = $split_img_size[1];

        // image dimension
        $split_img_dimension = explode('x', $config['IMAGE_DIMENSION']);
        $split_img_variation = explode('~', $split_img_dimension[1]);
        $config['IMAGE_WIDTH'] = $split_img_dimension[0];
        $config['IMAGE_HEIGHT'] = $split_img_variation[0];
        $config['IMAGE_DIMENSION_PERCENT'] = $split_img_variation[1];

        //image max/min width and height
        $config['IMAGE_MIN_WIDTH'] = $split_img_dimension[0] - (($split_img_dimension[0] * $split_img_variation[1]) / 100);
        $config['IMAGE_MAX_WIDTH'] = $split_img_dimension[0] + (($split_img_dimension[0] * $split_img_variation[1]) / 100);

        $config['IMAGE_MIN_HEIGHT'] = $split_img_variation[0] - (($split_img_variation[0] * $split_img_variation[1]) / 100);
        $config['IMAGE_MAX_HEIGHT'] = $split_img_variation[0] + (($split_img_variation[0] * $split_img_variation[1]) / 100);

        //========================= image config end =====================
        // for doc file
        $config['DOC_DIMENSION'] = Configuration::where('caption', 'DOC_IMAGE_SIZE')->pluck('value');
        $config['DOC_SIZE'] = Configuration::where('caption', 'DOC_IMAGE_SIZE')->pluck('value2');

        // Doc size
        $split_doc_size = explode('-', $config['DOC_SIZE']);
        $config['DOC_MIN_SIZE'] = $split_doc_size[0];
        $config['DOC_MAX_SIZE'] = $split_doc_size[1];

        // doc dimension
        $split_doc_dimension = explode('x', $config['DOC_DIMENSION']);
        $split_doc_variation = explode('~', $split_doc_dimension[1]);
        $config['DOC_WIDTH'] = $split_doc_dimension[0];
        $config['DOC_HEIGHT'] = $split_doc_variation[0];
        $config['DOC_DIMENSION_PERCENT'] = $split_doc_variation[1];

        //doc max/min width and height
        $config['DOC_MIN_WIDTH'] = $split_doc_dimension[0] - (($split_doc_dimension[0] * $split_doc_variation[1]) / 100);
        $config['DOC_MAX_WIDTH'] = $split_doc_dimension[0] + (($split_doc_dimension[0] * $split_doc_variation[1]) / 100);

        $config['DOC_MIN_HEIGHT'] = $split_doc_variation[0] - (($split_doc_variation[0] * $split_doc_variation[1]) / 100);
        $config['DOC_MAX_HEIGHT'] = $split_doc_variation[0] + (($split_doc_variation[0] * $split_doc_variation[1]) / 100);

        return $config;
    }

    public static function updateScriptPara($sql, $data) {
        $start = strpos($sql, '{$');
        while ($start > 0) {
            $end = strpos($sql, '}', $start);
            if ($end > 0) {
                $filed = substr($sql, $start + 2, $end - $start - 2);
                $sql = substr($sql, 0, $start) . $data[$filed] . substr($sql, $end + 1);
            }
            $start = strpos($sql, '{$');
        }
        return $sql;
    }

    public static function getUserTypeName() {
        if (Auth::user()) {
            $user_type_id = Auth::user()->user_type;
            $user_type_name = UserTypes::where('id', $user_type_id)
                ->pluck('type_name');
            return $user_type_name;
        } else {
            CommonFunction::redirectToLogin();
        }
    }

    public static function getUserDeskName() {
        if (Auth::user()) {
            $desk_id = Auth::user()->desk_id;
            $desk_name = UserDesk::where('desk_id', $desk_id)->pluck('desk_name');
            return $desk_name;
        } else {
            return '';
        }
    }

    public static function getDeskName($desk_id) {
        if (Auth::user()) {
            $desk_name = UserDesk::where('desk_id', $desk_id)->pluck('desk_name');
            return $desk_name;
        } else {
            return '';
        }
    }

    public static function getStatusListForSBNSI() {
        $list = array('0' => 'Pending', '-1' => 'Aborted', '1' => 'Positive', '2' => 'Negative', '3' => 'Rejected');
        return $list;
    }



//    send sms or email
    public static function sendMessageFromSystem($param) {

        $mobileNo = $param[0]['mobileNo'] ==''? '0' : $param[0]['mobileNo'];
        $smsYes = $param[0]['smsYes'] ==''? '0' : $param[0]['smsYes'];
        $smsBody = $param[0]['smsBody'] ==''? '' :$param[0]['smsBody'] ;
        $emailYes = $param[0]['emailYes'] ==''? '1' :$param[0]['emailYes'] ;
        $emailBody = $param[0]['emailBody'] ==''? '' :$param[0]['emailBody'] ;
        $emailHeader = $param[0]['emailHeader'] ==''? '0' :$param[0]['emailHeader'] ;
        $emailAdd= $param[0]['emailAdd']==''? 'base@gmail.com' :$param[0]['emailAdd'];
        $template= $param[0]['emailTemplate']==''? '' :$param[0]['emailTemplate'];
        $emailSubject= $param[0]['emailSubject']==''? '' :$param[0]['emailSubject'];
        $response_mail = '{"is_success":"0","trn_id":"0","error_code":"1","message":"0"}';
        if ($emailYes == 1) {
            $email = $emailAdd;
            $data = array(
                'header' => $emailHeader,
                'param' => $emailBody
            );
            \Mail::send($template, $data, function($message) use ($email,$emailSubject) {
                $message->from('base@gmail.com', 'BCSIR');
                $message->to($email);
                $message->cc('shahin@batworld.com');
                $message->subject($emailSubject);
            });

            $response_mail = '{"is_success":"1","trn_id":"100","error_code":"0","message":"0"}';

        }

//        $smsYes = 1;
        $response_sms = '{"is_success":"0","trn_id":"0","error_code":"1","message":"0"}';
        if ($smsYes == 1) {
            $sms = $smsBody;
            $sms = str_replace(" ", "+", $sms);
            //        $sms = str_replace("<br>", "%0a", $sms);
            $mobileNo = str_replace("+88", "", "$mobileNo");
            $url = "http://202.4.119.45:777/syn_sms_gw/index.php?txtMessage=$sms&msisdn=$mobileNo&usrname=business_automation&password=bus_auto@789_admin";
//            echo $url;
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            $response = curl_exec($curl);
            curl_close($curl);

        }
        $mergeJson = '{"email":'.$response_mail.',"sms":'.$response_sms.'}';

        return $mergeJson;
    }
    
    
    public static function showErrorPublic($param, $msg = 'Sorry! Something went wrong! Please try again later..')
    {
        $j = strpos($param, '(SQL:');
        if ($j > 15) {
            $param = substr($param, 8, $j - 9);
        } else {
            //
        }
        return $msg . $param;
    }

    /*     * ****************************End of Class***************************** */
}
