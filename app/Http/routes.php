<?php
/* -------------Route for login-logout-------------- */

Route::get('/', 'LoginController@index');

Route::get('/login', 'LoginController@index');

Route::get('/google-login', function() {
    if (auth()->check()) {
        return redirect('dashboard');
    }
    return View::make('login');
});
Route::post('/login/check', 'LoginController@check');
Route::get('/logout', 'LoginController@logout');
Route::get('re-captcha', 'LoginController@reCaptcha');

//---------------end login-logout route-------------

/*
 * For language changes
 */
Route::get('language/{lan}', function ($lang) {
    App::setLocale($lang);
    Session::put('lang', $lang);
    \App\Modules\Users\Models\Users::setLanguage($lang);
    return redirect()->back();
});


