<?php

namespace App\Http\Middleware;

use App\Http\Controllers\LoginController;
use App\Libraries\Encryption;
use App\Modules\Users\Models\UsersModel;
use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use yajra\Datatables\Request;

class Authenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }
        }

        /*
         * can't proceed without two step verification
         * except 3 url: users/two-step, user/check-two-step, users/verify-two-step
         * this 3url only for two step verification after login
         */

        if ($this->auth->user()->auth_token != '' && $request->segment(2) != 'two-step' && $request->segment(2) != 'check-two-step' && $request->segment(2) != 'verify-two-step') {
            UsersModel::where('id', Auth::user()->id)->update(['auth_token' => '']);
            Auth::logout();
            Session::flash('error', "2nd Step verification not match properly. Please login again.");
            return redirect('login');
        }

        if (Auth::user()->is_approved == 1 or Auth::user()->is_approved == true) {
            $checkSession = UsersModel::where(['id' => Auth::user()->id, 'login_token' => Encryption::encode(Session::getId())])->count();
            if ($checkSession == 0 ) {
                Auth::logout();
                Session::flash('error', "Login token has been expired.");
                return redirect('login');
            }

        }


//        Config::set('database.default', 'mysql-server');
        return $next($request);
    }
}
