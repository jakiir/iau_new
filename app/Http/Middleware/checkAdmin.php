<?php

namespace App\Http\Middleware;

use App\Http\Controllers\LoginController;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class checkAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $LgController = new LoginController;
        $user_type=Auth::user()->user_type;
        $user=explode("x",$user_type); // $user[0] array index stored the users level id

        // check first login to change password
        if(Auth::user()->first_login == 0 AND (in_array($user[0], [5,6]))){
            return redirect()
                ->intended('/users/profileinfo#tab_2')
                ->with('error', 'Please change the password.');
        }
        // check the user is approved
        if(Auth::user()->is_approved == 0){
            return redirect()
                ->intended('/dashboard')
                ->with('error', 'You are not approved user ! Please contact with system admin');
        }


        if (!$LgController->_checkSecurityProfile($request)) {
            Auth::logout();
            return redirect('/login')
                ->with('error', 'Security profile does not support in this time for operation.');
        }
        $uri = $request->segment(1);
        switch (true) {
            case ($uri == 'settings' and $user[0] == '1'):
                return $next($request); // allowed system admin users to access settings module
                break;
            case ($uri == 'dashboard' and ($user[0] == '1' or $user[0] == '2' or $user[0] == '3' or $user[0] == '4' or $user[0] == '5'  or $user[0] == '6'
                    or $user[0] == '9')):
                return $next($request); // allowed dashboard  to access user module
                break;
            case ($uri == 'users' and ($user[0] == '1' || $user[0] == '2'  || $user[0] == '3'  or $user[0] == '4' or $user[0] == '5'  or $user[0] == '6')):
                return $next($request); // allowed users  to access agency module
                break;
            case ($uri == 'process-path' AND (in_array($user[0], [1]))):
                return $next($request); // allowed users  to access project clearance module
                break;
            case ($uri == 'project-clearance' AND (in_array($user[0], [1,2,4,5,7,8]))):
                return $next($request); // allowed users  to access project clearance module
                break;
            case ($uri == 'import-permit' AND (in_array($user[0], [1,2,3,4,5,7,8,9]))):
                return $next($request); // allowed users  to access import permit module
                break;
            case ($uri == 'export-permit' AND (in_array($user[0], [1,2,3,4,5,7,8,9]))):
                return $next($request); // allowed users  to access export permit module
                break;
            case ($uri == 'visa-assistance' AND (in_array($user[0], [1,2,4,6,7,8]))):
                return $next($request); // allowed users  to access visa assistance module
                break;
            case ($uri == 'visa-recommend' AND (in_array($user[0], [1,2,4,5,7,8]))):
                return $next($request); // allowed users  to access visa recommendation module
                break;
            case ($uri == 'cer-doc' AND (in_array($user[0], [5]))):
                return $next($request); // allowed users  to access visa recommendation module
                break;
            case ($uri == 'sample-doc' AND (in_array($user[0], [5]))):
                return $next($request); // allowed users  to access visa recommendation module
                break;
            default:
                Session::flash('error', 'Invalid URL ! error code('.$uri.'-'.$user[0].')    ');
                return redirect('dashboard');
        }
        return $next($request);
    }
}
