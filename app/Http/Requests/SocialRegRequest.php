<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SocialRegRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_nid' => 'digits_between:17,17|required|numeric|unique:users',
            'user_DOB' => 'required',
            'user_phone' => 'required',            
            'user_type' => 'required'            
        ];
    }
    public function messages() {
        return [
            'user_nid.required' => 'National ID No. field is required',
            'user_nid.numeric' => 'National ID No. must be numeric',
            'user_nid.digits_between' => 'National ID No. must be 17 digits',
            'user_nid.unique' => 'National ID No. must be unique',            
            'user_DOB.required' => 'Date of Birth field is required',
            'user_phone.required' => 'Mobile Number field is required',
            'user_type.required' => 'Member type field is required',
        ];
    }    
}
