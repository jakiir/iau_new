<?php

namespace App\Http\Controllers;

use App;
use App\Modules\Settings\Models\Configuration;
use App\Modules\Settings\Models\Notice;
use App\Modules\Settings\Models\SecurityProfile;
use App\Modules\Users\Models\Users;
use App\Modules\Users\Models\FailedLogin;
use App\Modules\Users\Models\UserTypes;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Mews\Captcha\Facades\Captcha;
use Session;
use App\Libraries\Encryption;
use App\Modules\Settings\Models\Security;

class LoginController extends Controller
{

    public function index()
    {

        $DeshboardObject = DB::table('dashboard_object')->where('db_obj_status', 1)->get();
        if (auth()->check()) {
            return redirect('dashboard');
        }

        $notice = Notice::
        where('status','public')
            ->orderBy('notice.updated_at', 'desc')
            ->get(['id', 'heading', 'details', 'importance', 'status', 'updated_at as update_date']);

        return view('login', compact('DeshboardObject', 'notice'));
    }

    public function check(Request $request, Users $usersModel, $rqstlth = 0)
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required|max:30',
        ]);

        if (Session::get('hit') >= 3) {
            $rules = ['captcha' => 'required|captcha'];
            $validator = \Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withInput()
                    ->with('error', 'Invalid Captcha Code');
            }
        }

        if (!$this->_checkAttack($request)) {
            return redirect()->back()->withInput();
        }
        if (Auth::attempt([
            'username' => $request->get('username'),
            'password' => $request->get('password')
        ], $request->get('remember'))
        ) {

            if (Auth::user()->is_approved == 1 && Auth::user()->user_status != 'active') {

                Auth::logout();
                return redirect()->back()
                    ->withInput()
                    ->with('error', 'The user is not active, please contact with system admin.');
            } elseif (Auth::user()->user_status == 'rejected') {
                Auth::logout();
                return redirect()->back()
                    ->withInput()
                    ->with('error', 'Your account has been rejected! Please contact with authority for more information.');
            } elseif (Auth::user()->is_approved == 1 && Auth::user()->user_status == 'active') {
                if (!$this->_checkSecurityProfile($request)) {
                    Auth::logout();
                    return redirect()->back()
                        ->withInput()
                        ->with('error', 'Security Profile does not support login from this network or time!');
                }
            }

            if (!$this->_checkSubType($request)) {
                Auth::logout();
                return redirect()->back()
                    ->withInput()
                    ->with('error', "Desk don't set properly, please contact with your system admin!");
            }
            $this->entryAccessLog();
            if ($this->_setSession() == false) {
                Auth::logout();
                return redirect('/login');
            }
            /*
             * for user caption (like: Bank name/agency name/udc name etc)
             */
            $userAdditionalInfo = $usersModel->getUserSpecialFields(Auth::user());
            $loginAccess = $this->_protectLogin(Auth::user()->user_type);
            if ($loginAccess == false) {
                return redirect('/login');
            }
            //for user default language
//            Session::put('lang', Auth::user()->user_language);
            Session::put('lang', 'en');
            App::setLocale(Session::get('lang'));
            $caption_name = '';
            if (count($userAdditionalInfo) >= 1 && $userAdditionalInfo[0]['value']) {
                $caption_name .= ' - ';
                if (Auth::user()->user_type == '4x404') {
                    $caption_name .= UserTypes::where('id', Auth::user()->user_type)->pluck('type_name') . ', ';
                }

                $caption_name .= $userAdditionalInfo[0]['value']; //$userAdditionalInfo[0]['caption'] . ': ' .
                if (strlen($caption_name) > 45) {
                    $caption_name = substr($caption_name, 0, 43) . '..';
                }
            } else {
                $caption_name .= ' - ' . Auth::user()->email;
            }
            Session::put('caption_name', $caption_name);
            Session::put('hit', 0);
            $userTypeRootStatus = $this->_checkUserTypeRootActivation(Auth::user()->user_type, Auth::user()->user_sub_type);
            if ($userTypeRootStatus == false) {
                return redirect('/login');
            }
            /* Set session id and kill any existing session */
            $this->killUserSession(Auth::user()->id);
            Users::where('id', Auth::user()->id)->update(['login_token' => Encryption::encode(Session::getId())]);

            if (Auth::user()->user_type != '1x101' || Auth::user()->user_type != '2x202') {
                if (Auth::user()->delegate_to_user_id != 0) {
                    Session::put('sess_delegated_user_id', Auth::user()->delegate_to_user_id);
                    return redirect('/users/delegate');
                }
            }

            if (Auth::user()->first_login == 0) {
//                Users::where('id', Auth::user()->id)->update(['first_login' => 1]);
                return redirect()
                    ->intended('/users/profileinfo#tab_2')
                    ->with('success', trans('messages.welcome'));
            } else {
                // a.   one function calling
                $appStatus = $this->_checkAppMode(Auth::user()->user_type);
                if ($appStatus == false) {
                    return redirect('/login');
                }

                $user_type = UserTypes::where('id', Auth::user()->user_type)->first();

                if (($user_type->auth_token_type == 'mandatory') || ($user_type->auth_token_type == 'optional' && Auth::user()->auth_token_allow == 1)) {

                    return redirect()
                        ->intended('/users/two-step')
                        ->with('success', 'Logged in successfully, Please verify the 2nd steps.');
                } else {
                    return redirect()
                        ->intended('/dashboard')
                        ->with('success', 'Logged in successfully, Welcome to Islamic Arabic University');
                }
            }
        }
        if (Session::has('hit')) {
            Session::put('hit', Session::get('hit') + 1);
        } else {
            Session::put('hit', 1);
        }
        $this->_failedLogin($request);
        return redirect()
            ->back()
            ->withInput()
            ->with('error', 'Invalid email or password');
    }

    /**
     * Logout
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout()
    {
        if (Auth::user()) {
            Users::where('id', Auth::user()->id)->update(['login_token' => '']);
        }
        $this->entryAccessLogout();
        Session::put('caption_name', '');
        Session::getHandler()->destroy(Session::getId());
        Auth::logout();
        return redirect('/login');
    }

    public static function killUserSession($user_id)
    {
        $sessionID = Users::where('id', $user_id)->pluck('login_token');
        if (!empty($sessionID)) {
            $sessionID = App\Libraries\Encryption::decode($sessionID);
            Session::getHandler()->destroy($sessionID);
        }
        Users::where('id', $user_id)->update(['login_token' => '']);
    }

    /**
     * Entry access for Login
     * Insert login info in user_logs table
     */
    public function entryAccessLog()
    {
        // access_log table.
        $str_random = str_random(10);
        $insert_id = DB::table('user_logs')->insertGetId(
            array(
                'user_id' => Auth::user()->id,
                'login_dt' => date('Y-m-d H:i:s'),
                'ip_address' => \Request::getClientIp(),
                'access_log_id' => $str_random
            )
        );

        Session::put('access_log_id', $str_random);
    }

    /**
     * Entry access for Logout
     * update logout time in user_logs table
     */
    public function entryAccessLogout()
    {
        $access_log_id = Session::get('access_log_id');
        DB::table('user_logs')->where('access_log_id', $access_log_id)->update(['logout_dt' => date('Y-m-d H:i:s')]);
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from Google.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('google')->user();

        // $user->token;
    }

    public function reCaptcha()
    {
        return Captcha::img();
    }

    /**
     * @Private method
     */
    private function _checkAppMode($userType = false)
    {
        $typeArr = explode('x', $userType);
        $userType = $typeArr[0];
        $appMode = env('APP_MODE', '10');
        $dbMode = Configuration::where('caption', 'DATABASE_MODE')->first();
        if ($dbMode->value == 'PRODUCTION') {
            if ($appMode == '90' and ($userType == '1' OR $userType == '7' OR $userType == '11')) {
                return $this->_checkHTTP('PRODUCTION');
            } else if ($appMode == '80' and ($userType == '7' OR $userType == '11')) {
                Auth::logout();
                Session::flash('error', 'Permission Deny.');
                return false;
            } else if ($appMode != '90' && ($userType != '7' && $userType != '11')) {
                return $this->_checkHTTP('PRODUCTION');
            }
        } else {
            return true;
        }
    }

    private function _checkHTTP($dbMode = false)
    {
        $protocol = stripos($_SERVER['SERVER_PROTOCOL'], 'https') === true ? 'https://' : 'http://';
        $httpType = Configuration::where('caption', 'HTTP_TYPE')->first();
        $httpType = $httpType->value;
        if ($dbMode == 'PRODUCTION') {
            if ($protocol == 'https://') {
                return true;
            } else {
                Auth::logout();
                Session::flash('error', 'insecured connection.');
                return false;
            }
        } else {
            if ($httpType) {
                if ($protocol == 'https://') {
                    return true;
                } else {
                    Auth::logout();
                    Session::flash('error', 'insecured connection.');
                    return false;
                }
            } else {
                return true;
            }
        }
    }

    private function _checkUserTypeRootActivation($userType = null, $userSubType = null)
    {
//        $typeArr = explode('x', $userType);
//        $userTypeId = $typeArr[0];
        // for checking user type status
        $userTypeInfo = UserTypes::where('id', $userType)->first();
        if ($userTypeInfo->status != "active") {
            Auth::logout();
            Session::flash('error', 'The user is not active, please contact with system admin');
            return false;
        }

        return true;
    }

    private function _failedLogin($request)
    {
        $ip_address = $request->ip();
        $emailByUsername = Users::where('username',$request->get('username'))->first(['email']);
        $user_email = $emailByUsername->email;
        FailedLogin::create(['remote_address' => $ip_address, 'user_email' => $user_email]);
    }

    private function _checkAttack($request)
    {
        $ip_address = $request->ip();
        $emailByUsername = Users::where('username',$request->get('username'))->first(['email']);
        $user_email = $emailByUsername->email;
        $count = FailedLogin::where('remote_address', "$ip_address")
            ->where('created_at', '>', DB::raw('DATE_ADD(now(),INTERVAL -20 MINUTE)'))
            ->groupBy('remote_address')
            ->count();
        if ($count > 10) {
            Session::flash('error', 'Invalid Login session. Please try after 10 minute 1001');
            return false;
        } else {
            $count = FailedLogin::where('remote_address', "$ip_address")
                ->where('created_at', '>', DB::raw('DATE_ADD(now(),INTERVAL -60 MINUTE)'))
                ->groupBy('remote_address')
                ->count();
            if ($count > 20) {
                Session::flash('error', 'Invalid Login session. Please try after 30 minute 1003');
                return false;
            } else {
                $count = FailedLogin::where('user_email', $user_email)
                    ->where('created_at', '>', DB::raw('DATE_ADD(now(),INTERVAL -10 MINUTE)'))
                    ->groupBy('remote_address')
                    ->count();
                if ($count > 6) {
                    Session::flash('error', 'Invalid Login session. Please try after 5 minute 1002');
                    return false;
                }
            }
        }
        return true;
    }

    private function _protectLogin($type = false)
    {
        if ($type == '1x1011') {
            Auth::logout();
            Session::flash('error', 'This url are not allow your access.');
            return false;
        } else {
            return true;
        }
    }

    private function _checkSubType($request)
    {
        $desk_id = Auth::user()->desk_id;
        $user_type = Auth::user()->user_type;
        $type = explode('x', $user_type);
        if ($type[0] == 4) {
            if ($desk_id > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public function _checkSecurityById($security_id, $ip_param, $request)
    {
        $security = SecurityProfile::where(['id' => $security_id, 'active_status' => 'yes'])->first();
        if ($security) {
            if ($ip_param) {
                $ip = $ip_param;
            } else {
                $ip = $request->ip();
            }
            if ($ip == '127.0.0.1' || $ip == '::1') {
                $ip = '0.0.0.0';
            }
            $net = '0.0.0.0';
            $nets = explode('.', $ip);
            $weekName = strtoupper(date('D'));
            if (count($nets) == 4) {
                $net = $nets[0] . '.' . $nets[1] . '.' . $nets[2] . '.0';
            }

            if ($security->allowed_remote_ip == '' || $security->allowed_remote_ip == '0.0.0.0' || !(strpos($security->allowed_remote_ip, $net) === false) || !(strpos($security->allowed_remote_ip, $ip) === false)) {
                if (strpos($security->week_off_days, $weekName) === false) {
                    if (time() >= strtotime($security->work_hour_start) && time() <= strtotime($security->work_hour_end)) {
                        return true;
                    }
                }
            }

        } else {
            return true;
        }
        return false;
    }

    public function _checkSecurityProfile($request, $ip_param = '')
    {

        // Individual security check
        $user_wise_passed = false;
        $security_id = Auth::user()->security_profile_id; //Individual Security profile ID
        if ($security_id >= 1) {
            $user_wise_passed = $this->_checkSecurityById($security_id, $ip_param, $request);
        } else {
            $user_wise_passed = true;
        }

        // If passed in individual Security profile ID
        if ($user_wise_passed) {

            // Checking User Type Wise
            $security_id = UserTypes::where('id', Auth::user()->user_type)->first()->security_profile_id;//Individual Security profile ID
            return $this->_checkSecurityById($security_id, $ip_param, $request);

        }
        return false;
    }

    private function _setSession()
    {
        try {
            if (Auth::user()->is_approved == 1 && Auth::user()->user_status == 'active') {
                Session::put('hit', 0);

                $user_type = Auth::user()->user_type;
                $user_type_explode = explode('x', $user_type);

                if ($user_type_explode[0] == '1') {
                    Session::put('mysql_access', 'mysql-005');
                } elseif ($user_type_explode[0] == '7') {
                    Session::put('mysql_access', 'mysql-003');
                } else {
                    Session::put('mysql_access', 'mysql-main');
                }

                //            for user report module
                Session::put('sess_user_id', Auth::user()->id);
                Session::put('sess_user_type', Auth::user()->user_type);
                Session::put('sess_district', Auth::user()->district);
                Session::put('sess_thana', Auth::user()->thana);
            }
        } catch (\Exception $e) {
            Session::flash('error', 'Invalid session ID!');
            return false;
        }
        return true;
    }

    /* End of Controller Class */
}