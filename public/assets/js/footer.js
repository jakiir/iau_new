$(document).ready(function(){
	var dateToday2 = new Date();        
	$('.datepicker').datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: '-85:' + (new Date).getFullYear(),
		dateFormat: 'dd/mm/y',
		maxDate: dateToday2
	});
	

	$(document).on('change','input.subjectGroup', function(e) {
	   if ($('input.subjectGroup:checked').length > 2) {
			$(this).prop('checked', false)
			alert("allowed only 2");
	   }
	});
	
	$(document).on('change','input.subjectGroup3', function(e) {
	   if ($('input.subjectGroup3:checked').length > 3) {
			$(this).prop('checked', false)
			alert("allowed only 3");
	   }
	});
	
});