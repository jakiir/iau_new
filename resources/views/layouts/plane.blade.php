<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8"/>
        <title>ইসলামি আরবি বিশ্ববিদ্যালয় | @if (Auth::check()) {{Auth::user()->name}} @endif</title>
		<link rel="shortcut icon" type="image/x-icon" href="{{ asset("assets/img/favicon.ico") }}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>

        <link rel="stylesheet" href="{{ asset("assets/stylesheets/styles.css") }}" />
        <!--/*comment by jakir*/<link rel="stylesheet" href="{{ asset("assets/stylesheets/formWizerd.css") }}" />
        <link rel="stylesheet" href="{{ asset("assets/css/bootstrap.min.css") }}" />-->

        <link rel="stylesheet" href="{{ asset("assets/scripts/datatable/dataTables.bootstrap.min.css") }}" />
        <link rel="stylesheet" href="{{ asset("assets/scripts/datatable/responsive.bootstrap.min.css") }}" />
        <link rel="stylesheet" href="{{ asset("assets/stylesheets/bootstrap-datetimepicker.css") }}" />
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="{{URL::to('assets/stylesheets/bootstrap-select.min.css')}}">

        <link rel="stylesheet" href="{{ asset("assets/css/jquery-ui.css") }}" />
        <link rel="stylesheet" href="{{ asset("assets/stylesheets/custom.css") }}" />

        <script src="{{ asset("assets/scripts/jquery.min.js") }}"  type="text/javascript"></script>
        <script src="{{ asset("assets/amcharts/amcharts.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/amcharts/pie.js") }}" type="text/javascript"></script>
        <script src="{{ asset("assets/amcharts/serial.js") }}" type="text/javascript"></script>
    </head>
    <body id=home>
        @yield('body')
        @include('layouts.image_config')
        <!-- jQuery -->
        <script src="{{ asset("assets/scripts/jquery.min.js") }}"  type="text/javascript"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="{{ asset("assets/scripts/bootstrap.min.js") }}" type="text/javascript"></script>
        <script src="{{URL::to('assets/scripts/bootstrap-select.min.js')}}"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="{{ asset("assets/scripts/metis-menu.min.js") }}" type="text/javascript"></script>

        <!-- Morris Charts JavaScript -->
        <script src="{{ asset("assets/scripts/raphael-min.js") }}" type="text/javascript"></script>

        <script src="{{ asset("assets/scripts/metis-menu.min.js") }}" type="text/javascript"></script>
        <!-- Custom Theme JavaScript -->
        <script src="{{ asset("assets/scripts/sb-admin-2.js") }}" type="text/javascript"></script>
        {{--<script src="{{ asset("assets/scripts/jquery-1.6.min.js") }}" type="text/javascript"></script>--}}
    <script src="{{ asset("assets/scripts/jquery-1.6.2.js") }}" type="text/javascript"></script>
    <script language="javascript"> var jq_my = jQuery.noConflict(true);</script>
    <script type="text/javascript">
        var base_url = '{{url()}}';
    </script>

    <script src="{{ asset("assets/scripts/jquery.form.js") }}" type="text/javascript"></script>
    <script src="{{ asset("assets/scripts/jquery.validate.js") }}" type="text/javascript"></script>
    <script src="{{ asset("assets/scripts/moment.js") }}" type="text/javascript"></script>
    <script src="{{ asset("assets/scripts/bootstrap-datetimepicker.js") }}" type="text/javascript"></script>
    <script src="{{ asset("assets/scripts/jquery-ui.js") }}"></script>

    {{--    <script src="{{ asset("assets/scripts/image-processing.js") }}" type="text/javascript"></script>--}}


@yield('footer-script')
@if(Auth::user())
<script type="text/javascript">
        var setSession = '';
        function getSession() {
            $.get("/users/get-user-session", function (data, status) {
                if (data.responseCode == 1) {
                    setSession = setTimeout(getSession, 20000);
                } else {
                    alert('Your session has been closed. Please login again');
                    window.location.replace('/login');
                }
            });
        }
        setSession = setTimeout(getSession, 20000);

        var setAccessSession = '';
        function getAccessSession() {
            url = '{{route("ajaxstdFormAccess")}}';
            $.ajax({
                url: url,
                type: 'POST',
                data : {
                    examVal:"{{Session::get('examVal')}}",
                    examTxt:"{{Session::get('examTxt')}}",
                    typeVal:"{{Session::get('typeVal')}}",
                    typeTxt:"{{Session::get('typeTxt')}}",
                    yearVal:"{{Session::get('yearVal')}}",
                    "_token":"{{ csrf_token() }}"
                },
                dataType: 'JSON',
                success: function (response) {

                    if (response.success == true) {
                        setAccessSession = setTimeout(getAccessSession, 15000);
                    } else {
                        alert('Time is over');
                        window.location.replace('/dashboard');
                    }
                }
            });
        }
        setAccessSession = setTimeout(getAccessSession, 15000);

</script>
@endif
<!-- Custom JS -->
<script src="{{ asset("assets/scripts/custom.js") }}"  type="text/javascript"></script>
</body>
</html>