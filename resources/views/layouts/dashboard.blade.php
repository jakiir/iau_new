@extends('layouts.plane')

@section('body')
<div id="wrapper">

    @include ('navigation.nav')

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">@yield('page_heading')</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">
            @yield('section')
            @yield('content')

        </div>
        <!-- /#page-wrapper -->
    </div>
</div>
@stop

