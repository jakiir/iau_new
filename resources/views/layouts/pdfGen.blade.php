@extends('layouts.pdfPlane')

@section('body')
    <div id="wrapper">
        <div id="page-wrapper">
            {{--<div class="row">
                <div class="col-md-10 col-lg-10">
                    <h3 class="page-header">@yield('page_heading')</h3>
                </div>
                <div class="col-md-2 col-lg-2 text-right">
                <!-- <a href="{{url('support/help/'.Request::segment(1))}}"><h5><span style="color: green">
                            Need Help <i class="fa fa-question-circle"></i> 
                        </span></h5></a> -->
                </div>
                <div class="col-md-2 col-lg-2 text-right">
                    @if(ACL::getAccsessRight('dashboard','H'))
                        <a href="{{url('support/help/'.Request::segment(1))}}"><h5><span style="color: green">
                            Need Help <i class="fa fa-question-circle"></i>
                        </span></h5>
                        </a>
                    @endif
                </div>
                <!-- /.col-lg-12 -->
            </div>--}}
            <div class="row">
                @yield('content')
            </div>
            <!-- /#page-wrapper -->
        </div>
    </div>
@stop

