<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ইসলামি আরবি বিশ্ববিদ্যালয়</title>
	<link rel="shortcut icon" type="image/x-icon" href="{{ asset("assets/img/favicon.ico") }}">
    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="{{URL::to('assets/css/bootstrap.min.css')}}" rel="stylesheet">
	<link rel="stylesheet" href="{{URL::to('assets/css/style-main.css')}}">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout">
    <div id="page" class="site"><!--Start Main Container-->
        <header id="masthead" class="site-header" role="banner"><!--Start Header-->
            <section class="header">

                <div class="container"><!--Start Website Heading-->
                    <div class="row">
                        <div class="col-sm-3">
						<a href="/"><img class="img-responsive logo" src="{{URL::to('assets/images/iau-logo.png')}}" alt="logo"></a>
                        </div>
                        <div class="col-sm-6">
                            <h1 class="title text-center kalpurush"><a href="{{URL::to('/')}}">ইসলামি আরবি বিশ্ববিদ্যালয়</a></h1>
                            <h2 class="title text-center"><a href="{{URL::to('/')}}">Islamic Arabic University</a></h2>
                        </div>
                        <div class="col-sm-3 social">

                        </div>
                    </div>
                </div><!--End Website Heading-->
            </section>
        </header><!--End Header-->

    @yield('content')

	<footer id="colophon" class="site-footer" role="contentinfo"><!-- Start Footer -->
            <section class="footer">
                <div class="footer-bottom text-center small"><!-- Start Copyright -->
                    <h4>Islamic Arabic University</h4>
                    All rights reserved. Copyright © ICT Section, <a href="http://iau.edu.bd/">Islamic Arabic University</a>
                </div><!-- End Copyright -->

            </section>
        </footer><!-- End Footer-->

    </div><!--End Main Container-->

    <!-- JavaScripts -->
	 <script src="{{URL::to('assets/js/jquery.min.js')}}"></script>
    <script src="{{URL::to('assets/js/bootstrap.min.js')}}"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
    @yield('footer-script')
    @if(Auth::user())
        <script type="text/javascript">
            var setSession = '';
            function getSession() {
                $.get("/users/get-user-session", function (data, status) {
                    if (data.responseCode == 1) {
                        setSession = setTimeout(getSession, 20000);
                    } else {
                        alert('Your session has been closed. Please login again');
                        window.location.replace('/login');
                    }
                });
            }
            setSession = setTimeout(getSession, 20000);
        </script>
    @endif
    <!-- Custom JS -->
    <script src="{{ asset("assets/scripts/custom.js") }}"  type="text/javascript"></script>
	
</body>
</html>
