<style>
    .hero-widget { text-align: center; padding-top: 20px; padding-bottom: 10px; }
    .hero-widget .icon { display: block; font-size: 30px; line-height: 20px; margin-bottom: 5px; text-align: center; }
    .hero-widget var { display: block; height: 64px; font-size: 64px; line-height: 64px; font-style: normal; }
    .hero-widget label { font-size: 17px; }
    .hero-widget .options { margin-top: 10px; }
    .panel-body .btn:not(.btn-block) { width:120px;margin-bottom:10px; }
</style>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="nav-tabs-custom">
            <a class="btn btn-default" target="_blank" href="http://www.beza.gov.bd/"><span class="fa fa-home"></span> Home</a>
            <div class="panel panel-beza">
                <div class="panel-heading">

                    <ul class="nav nav-tabs" style="border:none;">

                        <li class=" {!! (Request::segment(2)=='index' OR Request::segment(2)=='')?'active':'' !!}">
                            <a data-toggle="tab" href="#list_1" aria-expanded="true">
                                <i class="fa fa-clock-o"></i>
                                নোটিস
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div id="list_1" class="tab-pane {!! (Request::segment(2)=='index' OR Request::segment(2)=='')?'active':'' !!}">

                            <div class="panel-body">

                                <div class="row">

                                    @if($notice)
                                        <div class="col-md-12">

                                            <?php
                                            $arr = $notice;
                                            echo '<table class="table basicDataTable">';
                                            foreach ($arr as $value) {
                                                $update_date = App\Libraries\CommonFunction::changeDateFormat(substr($value->update_date, 0, 10));
                                                echo "<tr><td width='150px'>$update_date</td><td><span class='text-$value->importance'><a href='#' class='notice_heading'> <b>$value->heading</b></a></span><span class='details' style='display: none;'><br/>$value->details</span></td></tr>";
                                            }
                                            echo '</tbody></table>';
                                            ?>
                                        </div>
                                    @endif

                                    <div class="panel panel-body">

                                    </div><!-- /.panel-body -->
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

