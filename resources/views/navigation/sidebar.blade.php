<?php
$user_type = Auth::user()->user_type;
$type = explode('x', $user_type);
$addStudentUrl='';
?>
@if(Auth::user()->user_status='active' && (Auth::user()->is_approved == 1 or Auth::user()->is_approved == true))
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li class="{{ (Request::is('/dashboard') ? 'active' : '') }}">
                <a href="{{ url ('/dashboard/iau') }}"><i class="fa fa-dashboard fa-fw"></i>
                    {!!trans('messages.dashboard')!!}
                </a>
            </li>
			
			@if(Auth::user()->is_approved == 1)
				@if($type[0] == '1')
					<li class="{{ (Request::is('/manage-institute/show-institute') ? 'active' : '') }}">
						<a href="{{ url('/manage-institute/show-institute') }}">
							<i class="fa fa-file-text-o"></i>
							View all institutes
						</a>
					</li>
					<li class="{{ (Request::is('/manage-institute/create-institute') ? 'active' : '') }}">
						<a href="{{ url('/manage-institute/create-institute') }}">
							<i class="fa fa-plus-square"></i>
							Add new institute
						</a>
					</li>					
				@endif
			@endif
		<?php
		if(!empty(Session::get('examVal')) && !empty(Session::get('typeVal')) && !empty(Session::get('yearVal'))){
			if(Session::get('examVal') == 'fazilpass'){
				if(Session::get('typeVal') == 'esif'){
					$addStudentUrl = URL::to('/fazil-pass/add-new-student');
					$viewStudentUrl = URL::to('/fazil-pass/show-student-list');
				}
				if(Session::get('typeVal') == 'eff'){
					$addStudentUrl = 'javascript:void(0)';
					$viewStudentUrl = URL::to('/fazil-pass/show-student-list');
				}
			}
			else if(Session::get('examVal') == 'fazilhons'){
				$addStudentUrl = URL::to('/manage-student/fazil4-esif');
				$viewStudentUrl = URL::to('/manage-student/show-fazil4-students');
			} else if(Session::get('examVal') == 'fazilprivate'){
				if(Session::get('typeVal') == 'esif'){
					$addStudentUrl = URL::to('/manage-student/fazil3-private-esif');
					$viewStudentUrl = URL::to('/manage-student/show-fazil3-private-students');
				}
				if(Session::get('typeVal') == 'eff'){
					$addStudentUrl = 'javascript:void(0)';
					$viewStudentUrl = URL::to('/manage-student/show-fazil3-private-eff-students');
				}

			} else if(Session::get('examVal') == 'kamil-1'){
                if(Session::get('typeVal') == 'esif'){
                    $addStudentUrl = URL::to('/manage-student/add-new-kamil-esif');
                    $viewStudentUrl = URL::to('/manage-student/show-kamil-esif-students');
                }
                if(Session::get('typeVal') == 'eff'){
                    $addStudentUrl = 'javascript:void(0)';
                    $viewStudentUrl = URL::to('/manage-student/show-kamil-esif-students');
                }
            } else {
				$addStudentUrl = 'javascript:void(0)';
				$viewStudentUrl = 'javascript:void(0)';
			}
		 } else { ?>
			<script type="text/javascript"> window.location.href = "{{URL::to('/dashboard')}}"; </script>
		<?php } ?>
			
		@if(!empty($viewStudentUrl))
			<li class="{{ url($viewStudentUrl) === URL::current() ? 'active' : '' }}">
				<a href="{{ $viewStudentUrl }}">
					<i class="fa fa-file-text-o"></i>
					View all students
				</a>
			</li>
        @else
           <script type="text/javascript"> window.location.href = "{{URL::to('/dashboard')}}"; </script>
		@endif
		<?php
			if(Session::get('examVal') == 'fazilpass'){
				$submited_status_col = 'submited_status_fazil3';
			}
			else if(Session::get('examVal') == 'fazilhons'){
				$submited_status_col = 'submited_status_fazil4';
			} else {
				$submited_status_col = 'submited_status_fazil3';
			}
			$user_type = Auth::user()->user_type;
		?>
		@if(Session::get('application_on') || $user_type == '1x101')
			@if(Auth::user()->$submited_status_col == 'No')
				@if($addStudentUrl)
					<li class="addNewStudent {{ url($addStudentUrl) === URL::current() ? 'active' : '' }}">
						<a href="{{$addStudentUrl}}">
						<i class="fa fa-plus-square"></i>
						Add new student
						</a>
					</li>
				@endif
			@endif
		@endif
			
            @if(Auth::user()->is_approved == 1)
            <li>
                <a class="@if (Request::is('reports') || Request::is('reports/*')) active @endif" href="{{ url ('/reports ')}}">
                    <i class="fa fa-book fa-fw"></i> {!! trans('messages.reports') !!}
                </a>
            </li>

            @if($type[0] ==1) {{-- For System Admin --}}
            <li>
                <a class="@if (Request::is('users') || Request::is('users/create-new-user')) active @endif" href="{{ url ('/users/lists') }}">
                    <i class="fa fa-users fa-fw"></i> {!!trans('messages.users')!!}
                </a>
            </li>

            <li class="{{ (Request::is('settings/*') ? 'active' : '') }}">
                <a href="{{ url ('/settings') }}"><i class="fa fa-gear fa-fw"></i>
                    <!--Settings--> {!!trans('messages.settings')!!}
                    <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a class="@if(Request::is('settings/area-list') || Request::is('settings/create-area') || Request::is('settings/edit-area/*')) active @endif" href="{{ url ('/settings/area-list') }}">
                            <i class="fa fa-map-marker fa-fw"></i> {!!trans('messages.area')!!}
                        </a>
                    </li>
                    <li>
                        <a class="@if(Request::is('settings/bank-list') || Request::is('settings/create-bank') || Request::is('settings/view-bank/*')) active @endif" href="{{ url ('/settings/bank-list') }}">
                            <i class="fa fa-bank  fa-fw"></i> {!!trans('messages.bank')!!}
                        </a>
                    </li>
                    <li>
                        <a class="@if(Request::is('settings/currency') || Request::is('settings/create-currency') || Request::is('settings/edit-currency/*')) active @endif" href="{{ url ('/settings/currency') }}">
                            <i class="fa fa-money  fa-fw"></i> Currency
                        </a>
                    </li>
                    <li>
                        <a class="@if(Request::is('settings/document') || Request::is('settings/create-document') || Request::is('settings/edit-document/*')) active @endif"
                           href="{{ url ('/settings/document') }}">
                            <i class="fa fa-file-text fa-fw" aria-hidden="true"></i> <span>Documents</span>
                        </a>
                    </li>
                    <li>
                        <a class="@if(Request::is('settings/eco-zones') OR Request::is('settings/create-eco-zone') OR Request::is('settings/edit-eco-zone/*')) active @endif" href="{{ url ('/settings/eco-zones') }}">
                            <i class="fa fa-tree fa-fw" aria-hidden="true"></i> <span>Economic Zones</span>
                        </a>
                    </li>
                    <li>
                        <a class="@if(Request::is('settings/high-commission') || Request::is('settings/create-high-commission') ||
                           Request::is('settings/edit-high-commission/*')) active @endif" href="{{ url ('/settings/high-commission') }}">
                            <i class="fa fa-building fa-fw" aria-hidden="true"></i> <span>High Commission</span>
                        </a>
                    </li>
                    <li>
                        <a class="@if(Request::is('settings/hs-codes') || Request::is('settings/create-hs-code') ||
                           Request::is('settings/edit-hs-code/*')) active @endif" href="{{ url ('/settings/hs-codes') }}">
                            <i class="fa fa-codepen fa-fw" aria-hidden="true"></i> <span>HS Code</span>
                        </a>
                    </li>
                    <li>
                        <a class="@if(Request::is('settings/notice') || Request::is('settings/create-notice') || Request::is('settings/edit-notice/*')) active @endif" href="{{ url ('/settings/notice') }}">
                            <i class="fa fa-list-alt fa-fw" aria-hidden="true"></i> <span>Notice</span>
                        </a>
                    </li>
                    <li>
                        <a class="@if(Request::is('settings/ports') || Request::is('settings/create-port') || Request::is('settings/edit-port/*')) active @endif" href="{{ url ('/settings/ports') }}">
                            <i class="fa fa-support fa-fw" aria-hidden="true"></i> <span>Ports</span>
                        </a>
                    </li>
                    <li>
                        <a class="@if(Request::is('settings/security') || Request::is('settings/edit-security/*')) active @endif" href="{{ url ('/settings/security') }}" href="{{ url ('/settings/security') }}">
                            <i class="fa fa-key fa-fw" aria-hidden="true"></i> <span>Security Profile</span>
                        </a>
                    </li>

                    {{--<li class="{{ (Request::is('/settings/user-desk') ? 'active' : '') }}">--}}
                    {{--<a href="{{ url ('/settings/user-desk') }}">--}}
                    {{--<i class="fa fa-dashcube fa-fw"></i> User Desk--}}
                    {{--</a>--}}
                    {{--</li>--}}

                    <li>
                        <a class="@if(Request::is('settings/units') || Request::is('settings/create-unit') || Request::is('settings/edit-unit/*')) active @endif"
                           href="{{ url ('/settings/units') }}">
                            <i class="fa fa-beer fa-fw"></i> {!!trans('messages.units')!!}
                        </a>
                    </li>
                    <li>
                        <a class="@if(Request::is('settings/user-types') || Request::is('settings/edit-user-type/*')) active @endif" href="{{ url ('/settings/user-type') }}">
                            <i class="fa fa-user fa-fw"></i> {!!trans('messages.user_type')!!}
                        </a>
                    </li>
                </ul>
            </li>

            @endif {{-- For System Admin --}}
            @endif
        </ul>

    </div><!-- /.sidebar-collapse -->
</div><!-- /.navbar-static-side -->
@endif {{--  user is active --}}
