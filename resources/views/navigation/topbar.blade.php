<div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
</div>
<!-- /.navbar-header -->
<div class="navbar-header">
    <a href="{{URL::to('/')}}">
    {!!  Html::image('assets/images/iau-logo.png','Logo',['width'=>50,'style'=>'margin: 0px 0 0 10px !important;']) !!}
    </a>
</div>
<div class="navbar-header header-caption">
    <b>
        {{trans('messages.app_title')}} -
        @if(!empty(Session::get('examVal')) && !empty(Session::get('typeVal')) && !empty(Session::get('yearVal')))
            {{Session::get('examTxt').' '.Session::get('typeTxt').' - 20'.Session::get('yearVal')}},
        @endif
        {{Auth::user()->name}}
    </b>
</div>
<ul class="nav navbar-top-links navbar-right">

    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-language fa-fw"></i> {!! App::getLocale()=='bn'?'Bangla':'English'!!} <i class="fa fa-caret-down"></i>
        </a>
        <ul class="dropdown-menu dropdown-tasks language">
            <li>
                <a href="{{ url('language/bn') }}">
                    <div>
                        <i class="fa fa-bold fa-fw"></i> Bangla
                    </div>
                </a>
            </li>
            <li>
                <a href="{{ url('language/en') }}">
                    <div>
                        <i class="fa fa-tumblr fa-fw"></i> English
                    </div>
                </a>
            </li>
        </ul>
    </li>

    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">

            <i class="fa fa-user "></i>
            {!! Auth::user()->name !!}  <i class="fa fa-caret-down"></i>
        </a>
        <ul class="dropdown-menu dropdown-user" id="dropdown-responsive">
            @if(Auth::user()->user_type == '1x101' || Auth::user()->user_type == '2x202')
                <li>
                    <a href="{{ url('users/profileinfo') }}"><i class="fa fa-user fa-fw"></i> User Profile</a>
                </li>
                <li class="divider"></li>
            @endif
            <li>
                <a href="{{ url('logout') }}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
            </li>
        </ul>
    </li>
</ul>