@extends('layouts.app')

@section('content')

    <div id="content" class="site-content"><!-- Start Main Content-->
        <section class="main-content">

            <div class="container">
                <div class="row">
                    <article class="hentry"><!-- Start Article-->
                        <div class="col-md-12">
                            <div class="content-block">
                                <h4 class="text-center">Login Page</h4>
                                <div class="panel-body">

                                    @if (count($errors))
                                        <ul class="alert alert-danger">
                                            @foreach($errors->all() as $error)
                                                <li>{!!$error!!}</li>
                                            @endforeach
                                        </ul>
                                    @endif
                                    {!!session()->has('success') ? '<div class="alert alert-success alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'.session('success') .'</div>' : '' !!}
                                    {!!session()->has('error') ? '<div class="alert alert-danger alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'. session('error') .'</div>' : '' !!}
                                    {!! Form::open(array('url' => 'login/check','method' => 'post', 'class' => 'form-horizontal')) !!}

                                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                            <br><br>
                                            <label class="col-md-2 control-label col-md-offset-3">EIIN</label>

                                            <div class="col-md-2">
                                                {{--<input type="text" class="form-control" name="username" value="{{ old('username') }}" oninput="getinstituteName(this.value,'')">--}}
                                                <input class="form-control" placeholder="EIIN" required name="username" value="{{old('username')}}" type="text" autofocus>

                                                @if ($errors->has('username'))
                                                    <span class="help-block">
													<strong>{{ $errors->first('username') }}</strong>
												</span>
                                                @endif
                                            </div>
                                            <div class="col-md-5" id="insName"></div>
                                        </div>

                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <label class="col-md-2 control-label col-md-offset-3">Password</label>

                                            <div class="col-md-2">
                                                <input type="password" class="form-control" placeholder="Password" name="password">

                                                @if ($errors->has('password'))
                                                    <span class="help-block">
													<strong>{{ $errors->first('password') }}</strong>
												</span>
                                                @endif
                                            </div>
                                        </div>
                                        <?php if (Session::get('hit') >= 3) { ?>
                                        <div class="form-group">
                                            <div class="col-md-4">
                                                <span id="rowCaptcha"><?php echo Captcha::img(); ?></span> <img onclick="changeCaptcha();" src="assets/images/refresh.png" class="reload" alt="Reload" />
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-top: 15px;">
                                            <div class="col-md-4">
                                                <input class="form-control required" required placeholder="Enter captcha code" name="captcha" type="text">
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <div class="form-group">
                                            <div class="col-md-4 col-md-offset-5">
                                            <!--<a href="{{ URL('/') }}" class="btn btn-primary">Back</a>-->
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="fa fa-btn fa-sign-in"></i>Login
                                                </button>

                                            <!-- <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a> -->
                                            </div>
                                        </div>

                                        {{--<div class="form-group">--}}
                                            {{--<div class="col-md-12">--}}
                                                {{--<span class="pull-right">--}}
                                                    {{--{!! link_to('forget-password', 'Forget Password?', array("class" => "text-right")) !!}--}}
                                                {{--</span>--}}
                                                {{--<br/>--}}
                                                {{--<span class="pull-right">--}}
                                                    {{--New User? {!! link_to('users/create', 'Sign Up', array("class" => "text-right ")) !!}--}}
                                                {{--</span>--}}
                                                {{--<br/>--}}
                                                {{--<div class="pull-right">--}}
                                                    {{--{!! link_to('users/support', 'Need Help?', array("class" => "text-right")) !!}--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                {!! Form::close() !!}
                                    <h4 class="text-center kalpurush">Helpline (9:00am-5:00pm): <i class="text-danger"><a style="font-size:24px;" href="tel:+8801709-389007">01709-389007</a></i></h4>
                                </div>
                            </div>
                        </div>
                    </article><!-- End Article-->

                </div>
            </div>
        </section>
    </div><!-- End Main Content-->
    <style type="text/css">
        .footer{
            bottom: 0;
            width: 100%;
        }
    </style>
    <script type="text/javascript">
        {{--function getinstituteName(thisVal,emp){--}}
            {{--$('#insName').html('').removeClass('alert alert-danger alert-success');--}}
            {{--var url = '{{route("ajaxInstituteName")}}';--}}
            {{--$.ajax({--}}
                {{--url: url,--}}
                {{--type: 'POST',--}}
                {{--data : {eiin:thisVal, "_token":"{{ csrf_token() }}"},--}}
                {{--dataType: 'JSON',--}}
                {{--success: function (response) {--}}
                    {{--if(response.success == true){--}}
                        {{--$('#insName').addClass('alert alert-success').html(response.insInfo[0].institute_name);--}}
                    {{--} else {--}}
                        {{--$('#insName').addClass('alert alert-danger').html(response.msg);--}}
                    {{--}--}}

                {{--}--}}
            {{--});--}}
        {{--}--}}
    </script>

    <script src="//static.getclicky.com/js" type="text/javascript"></script>
    <script type="text/javascript">try{ clicky.init(100899632); }catch(e){}</script>
    <noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/100899632ns.gif" /></p></noscript>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-80931406-1', 'auto');
        ga('send', 'pageview');
    </script>
@endsection