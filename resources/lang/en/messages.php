<?php

return [
    'welcome' => 'Logged in successfully, Welcome to BEZA platform',
    'app_title' => 'Islamic Arabic University',
//  For Application Module
    'app_list' => 'Application List',
    'delegation_app_list' => 'Delegation Application List',
    'app_form' => 'Add New Application ',
    'app_form_title' => 'New Application Data Entry',

//  Settings Module
//   For Area Settings
    'document' => 'Document ',
    'doc_list' => 'Document List',
    'area' => 'Area ',
    'area_form' => 'Area ',
    'area_edit' => 'Area Edit',
    'area_list' => 'Area List',
    'new_area' => 'Add New Area',
    'district' => 'District',
    // For Configuration
    'list_config' => 'List of Configuration',
    'new_config' => 'Create New Configuration',
    'edit_config' => 'Change Configuration',
    'view_config' => 'View Configuration',
    // For Notification
    'list_notification' => 'List of Notification',
    'new_notification' => 'Create New Notification',
    'edit_notification' => 'Change Notification',
    'view_notification' => 'View Notification',
    //   For FAQ Settings
    'faq_form' => 'New FAQ ',
    'faq_edit' => 'Change FAQ',
    'faq_list' => 'FAQ List',
    'new_faq' => 'Create FAQ',
    //   For FAQ Category Settings
    'faq_cat_form' => 'New Knowledge Base',
    'faq_cat_edit' => 'Change Knowledge Base',
    'faq_cat_list' => 'Knowledge Base',
    'new_faq_cat' => 'Create Knowledge Base',
    // For settings->User Type
    'list_user_type' => 'List of User Type',
    'edit_user_type' => 'Edit User Type',
//  For Sidebar Menu
    'dashboard' => 'Dashboard',
    'search' => 'Search',
    'pilgrim' => 'Pilgrims',
    'reports' => 'Reports',
    'users' => 'Users',
    'settings' => 'Settings',
    'bank' => 'Bank',
    'units' => 'Units',
    'agency' => 'Agency',
    'uisc' => 'UDC',
    'bank_payment' => 'Bank Payment',
    'pre_registered' => 'Application',
    'demographic_group' => 'Demographic Group',
    'config' => 'Configuration',
    'notify' => 'Notification',
    'faq' => 'Knowledge Base',
    'faq_cat' => 'Knowledge Base',
    'feedback' => 'Support',
    //For User module
    'user_list' => 'User List',
    'user_view' => 'View User Details',
    'new_user' => 'Add New User',
    //For Dashboard
    'group_total' => 'Group Total',
    'registered_application' => 'Registered Application',
    'unpaid_application' => 'Unpaid Application',
    'total_registered' => 'Total Registered',
    'monthly_registered' => 'Pre-registration Status',
    'age_wise_registration' => "Age wise Pilgrim's Registration",
    'pilgrim_payment_chart' => 'Pilgrim Payment Chart',
    'pilgrim_chart' => "Pilgrim's Chart",
    //For Profile
    'profile' => 'Profile Information',
    'first_name' => 'Name',
    'specification' => 'User Specification',
    'nid' => 'National ID No.',
    'dob' => 'Date of Birth',
    'mobile' => 'Mobile Number',
    'email' => 'Email Address',
    'user_name' => 'User Name',
    'user_type' => 'User Type',
    'details' => 'View Details',
    //For Report Module
    'reports' => 'Reports',
    //For Group Voucher Module
    'voucher_list' => 'Payment Voucher',
    'new_voucher' => 'Create Voucher',
    //For SB
    'sb' => 'Pilgrim',
    'sb_report' => 'Special Branch Report',
    'new_sb_report' => 'Create Special Branch Report',
    'pilgrim_list_sb' => 'Verification of Pilgrim',
    //For Group
    'group' => 'Groups',
    'group_list' => 'Group List',
    'group_form' => 'Create Group',
    //  For Report Module
    'report_list' => 'Report List',
    'report_form' => 'Create Report ',
    'report_view' => 'Report View',
    'report_edit' => 'Report Edit',
    //For Agency Module
    // For session
    'session' => 'Session',
    'session_form' => 'Create Session',
    'session_edit' => 'Edit Session',
    'session_list' => 'Session List',
    'session_view' => 'Session View',
    // For Pilgrim search for Payment
    'new_payment_and_group_payment' => 'Voucher search for Payment',
    // For Search
    'search_list' => 'Search List',
    // For Support
    'support' => 'Support',
    'sms_status' => 'SMS Status',
    'pdf_status' => 'PDF Status',
    //For Security
    'security' => 'Security Profile',
    'security_list' => 'Security List',
    'security_form' => 'New Security',
    //   For Feedback (Support)
    'feedback_form_title' => 'Support',
    'feedback_form' => 'New Support Ticket',
    'feedback_edit' => 'Change Support Ticket',
    'feedback_view' => 'Support Ticket Details',
    'feedback_list' => 'Support Ticket',
    'feedback' => 'Support Ticket',
    'new_feedback' => 'Create Support Ticket',
];
