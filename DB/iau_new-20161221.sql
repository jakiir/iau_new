-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `access_profile`;
CREATE TABLE `access_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exam` varchar(34) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(34) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `type_status` tinyint(4) NOT NULL DEFAULT '0',
  `year_15` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `alim14`;
CREATE TABLE `alim14` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NAME` varchar(35) DEFAULT NULL,
  `FNAME` varchar(37) DEFAULT NULL,
  `MNAME` varchar(26) DEFAULT NULL,
  `ROLL_NO` int(6) DEFAULT NULL,
  `REGNO` int(6) DEFAULT NULL,
  `GPA` varchar(4) DEFAULT NULL,
  `RESULT` varchar(2) DEFAULT NULL,
  `STUD_SEX` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `alim15`;
CREATE TABLE `alim15` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NAME` varchar(34) DEFAULT NULL,
  `FNAME` varchar(32) DEFAULT NULL,
  `MNAME` varchar(24) DEFAULT NULL,
  `ROLL_NO` int(6) DEFAULT NULL,
  `REGNO` bigint(10) DEFAULT NULL,
  `GPA` varchar(4) DEFAULT NULL,
  `RESULT` varchar(2) DEFAULT NULL,
  `STUD_SEX` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `area_info`;
CREATE TABLE `area_info` (
  `area_id` int(11) NOT NULL AUTO_INCREMENT,
  `area_nm` varchar(50) DEFAULT NULL,
  `pare_id` int(11) DEFAULT NULL,
  `area_type` tinyint(4) DEFAULT NULL COMMENT '1 = Division, 2 = District, 3 = Thana',
  `area_nm_ban` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`area_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `configuration`;
CREATE TABLE `configuration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caption` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `details` varchar(255) NOT NULL,
  `value2` varchar(45) DEFAULT NULL,
  `value3` varchar(45) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL,
  `is_locked` int(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `dakhil11`;
CREATE TABLE `dakhil11` (
  `id` int(10) unsigned NOT NULL,
  `NAME` varchar(34) DEFAULT NULL,
  `FNAME` varchar(34) DEFAULT NULL,
  `MNAME` varchar(23) DEFAULT NULL,
  `ROLL_NO` int(6) DEFAULT NULL,
  `REGNO` int(6) DEFAULT NULL,
  `GPA` varchar(3) DEFAULT NULL,
  `RESULT` varchar(2) DEFAULT NULL,
  `STUD_SEX` varchar(6) DEFAULT NULL,
  `DOB` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `dakhil12`;
CREATE TABLE `dakhil12` (
  `id` int(10) unsigned NOT NULL,
  `NAME` varchar(33) DEFAULT NULL,
  `FNAME` varchar(37) DEFAULT NULL,
  `MNAME` varchar(25) DEFAULT NULL,
  `ROLL_NO` int(6) DEFAULT NULL,
  `REGNO` int(6) DEFAULT NULL,
  `GPA` varchar(3) DEFAULT NULL,
  `STUD_SEX` varchar(6) DEFAULT NULL,
  `DOB` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `dakhil13`;
CREATE TABLE `dakhil13` (
  `id` int(10) unsigned NOT NULL,
  `NAME` varchar(32) DEFAULT NULL,
  `FNAME` varchar(38) DEFAULT NULL,
  `MNAME` varchar(24) DEFAULT NULL,
  `ROLL_NO` int(6) DEFAULT NULL,
  `REGNO` bigint(10) DEFAULT NULL,
  `GPA` varchar(3) DEFAULT NULL,
  `RESULT` varchar(2) DEFAULT NULL,
  `STUD_SEX` varchar(6) DEFAULT NULL,
  `DOB` int(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `dashboard_object`;
CREATE TABLE `dashboard_object` (
  `db_obj_id` int(11) NOT NULL AUTO_INCREMENT,
  `db_obj_title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `db_obj_caption` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `db_user_id` int(11) DEFAULT NULL,
  `db_obj_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `db_obj_para1` text COLLATE utf8_unicode_ci,
  `db_obj_para2` text COLLATE utf8_unicode_ci,
  `db_obj_status` int(11) DEFAULT NULL,
  `db_obj_sort` int(11) DEFAULT NULL,
  `db_user_type` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_locked` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`db_obj_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `failed_login_history`;
CREATE TABLE `failed_login_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `remote_address` varchar(50) NOT NULL,
  `user_email` varchar(40) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `fazilhons15`;
CREATE TABLE `fazilhons15` (
  `id` bigint(20) NOT NULL,
  `unique_id_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tot_serial_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `registration_no` bigint(20) NOT NULL,
  `admission_session` varchar(34) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `student_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `fathers_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `mothers_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `student_class` varchar(34) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `student_sex` varchar(12) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `student_dob` varchar(34) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `subject_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `student_photo` varchar(512) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `dhakhil_alim_info` varchar(512) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `madrasah_eiin` varchar(34) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id_no` (`unique_id_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `fazilhonseff15`;
CREATE TABLE `fazilhonseff15` (
  `id` int(10) unsigned NOT NULL,
  `student_id` int(11) NOT NULL,
  `registration_no` bigint(20) NOT NULL,
  `madrasah_eiin` varchar(34) COLLATE utf8_unicode_ci NOT NULL,
  `admission_session` varchar(34) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `fazilpass15`;
CREATE TABLE `fazilpass15` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `unique_id_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tot_serial_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `registration_no` bigint(20) NOT NULL,
  `admission_session` varchar(34) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `student_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `fathers_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `mothers_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `student_class` varchar(34) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `student_sex` varchar(12) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `student_dob` varchar(34) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `student_group` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `subject_code` varchar(756) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `student_photo` varchar(512) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `dhakhil_alim_info` varchar(512) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `madrasah_eiin` varchar(34) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id_no` (`unique_id_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `fazilpasseff15`;
CREATE TABLE `fazilpasseff15` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `unique_id_no` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `registration_no` bigint(20) NOT NULL,
  `madrasah_eiin` varchar(34) COLLATE utf8_unicode_ci NOT NULL,
  `admission_session` varchar(34) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `fazilprivate15`;
CREATE TABLE `fazilprivate15` (
  `id` bigint(20) NOT NULL,
  `unique_id_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tot_serial_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `registration_no` bigint(20) NOT NULL,
  `admission_session` varchar(34) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `student_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `fathers_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `mothers_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `student_class` varchar(34) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `student_sex` varchar(12) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `student_dob` varchar(34) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `student_group` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `subject_code` varchar(756) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `student_photo` varchar(512) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `dhakhil_alim_info` varchar(512) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `madrasah_eiin` varchar(34) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `fazilpriveff15`;
CREATE TABLE `fazilpriveff15` (
  `id` int(10) unsigned NOT NULL,
  `student_id` int(11) NOT NULL,
  `unique_id_no` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `registration_no` bigint(20) NOT NULL,
  `madrasah_eiin` varchar(34) COLLATE utf8_unicode_ci NOT NULL,
  `admission_session` varchar(34) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `img_auth_file`;
CREATE TABLE `img_auth_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_id` int(11) DEFAULT NULL,
  `details` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `img_user_profile`;
CREATE TABLE `img_user_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_id` int(11) DEFAULT NULL,
  `details` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


SET NAMES utf8mb4;

DROP TABLE IF EXISTS `institutes`;
CREATE TABLE `institutes` (
  `id` bigint(20) NOT NULL,
  `institute_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `eiin` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `madrasah_code` varchar(34) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `class` varchar(34) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `division` varchar(18) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `zilla` varchar(34) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `zila_code` varchar(34) NOT NULL,
  `thana_u_zilla` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `thana_u_zilla_code` varchar(34) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `center_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_office` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `principal_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_no` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `kamil15`;
CREATE TABLE `kamil15` (
  `id` bigint(20) NOT NULL,
  `std_id` bigint(20) NOT NULL,
  `serial_no` bigint(20) NOT NULL,
  `registration_no` varchar(34) NOT NULL,
  `session` varchar(34) NOT NULL,
  `name` varchar(100) NOT NULL,
  `father_name` varchar(64) NOT NULL,
  `mothers_name` varchar(64) NOT NULL,
  `class` varchar(34) NOT NULL,
  `class_roll` varchar(10) NOT NULL,
  `tot_serial_no` varchar(20) NOT NULL,
  `sex` varchar(12) NOT NULL,
  `madrasah_name` varchar(200) NOT NULL,
  `madrasah_eiin` varchar(34) NOT NULL,
  `madrasah_code` varchar(34) NOT NULL,
  `zila` varchar(25) NOT NULL,
  `thana_upazilla` varchar(34) NOT NULL,
  `thana_upazilla_code` varchar(11) NOT NULL,
  `dakhil_passing_year` varchar(11) NOT NULL,
  `dakhil_roll_no` varchar(22) NOT NULL,
  `dakhil_reg_no` varchar(22) NOT NULL,
  `alim_passing_year` varchar(11) NOT NULL,
  `alim_roll_no` varchar(22) NOT NULL,
  `alim_reg_no` varchar(22) NOT NULL,
  `group` varchar(25) NOT NULL,
  `subject_code_1st_year` varchar(255) NOT NULL,
  `subject_code_2nd_year` varchar(255) NOT NULL,
  `subject_code_3rd_year` varchar(255) NOT NULL,
  `subject_code_4th_year` varchar(255) NOT NULL,
  `exam_roll_no` varchar(22) NOT NULL,
  `exam_center_name` varchar(64) NOT NULL,
  `exam_center_code` varchar(18) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `notice`;
CREATE TABLE `notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `heading` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` varchar(5000) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('public','unpublished','private','draft') COLLATE utf8_unicode_ci NOT NULL,
  `importance` enum('info','warning','danger','top') COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `reports`;
CREATE TABLE `reports` (
  `report_id` int(11) NOT NULL AUTO_INCREMENT,
  `report_title` varchar(765) COLLATE utf8_unicode_ci NOT NULL,
  `report_type` int(11) DEFAULT NULL,
  `report_para1` varchar(9000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `report_para2` varchar(5000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `is_locked` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`report_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `reports_mapping`;
CREATE TABLE `reports_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` varchar(10) NOT NULL,
  `report_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `security_profile`;
CREATE TABLE `security_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `allowed_remote_ip` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `week_off_days` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `work_hour_start` time NOT NULL,
  `work_hour_end` time NOT NULL,
  `active_status` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL,
  `user_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_phone` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_status` enum('active','inactive','rejected') COLLATE utf8_unicode_ci NOT NULL,
  `security_profile_id` int(11) NOT NULL DEFAULT '0',
  `submited_status_fazil3` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `submited_status_fazil3_private` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `submited_status_fazil3_eff` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `submited_status_fazil3_priv_eff` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `submited_status_fazil4` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `submited_status_fazil4_eff` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `final_lock_fazil3_eff` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `final_lock_fazil3_priv_eff` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_approved` tinyint(4) NOT NULL DEFAULT '0',
  `first_login` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `user_logs`;
CREATE TABLE `user_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `ip_address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `access_log_id` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `login_dt` datetime NOT NULL,
  `logout_dt` datetime NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `user_metas`;
CREATE TABLE `user_metas` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `meta_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_value` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`meta_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `id` int(10) unsigned NOT NULL,
  `role_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role_capabilities` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `access_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_roles_role_name_unique` (`role_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS `user_types`;
CREATE TABLE `user_types` (
  `id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `type_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_registarable` int(5) NOT NULL,
  `access_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `permission_json` text COLLATE utf8_unicode_ci,
  `status` enum('active','inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'inactive',
  `security_profile_id` int(11) NOT NULL DEFAULT '1',
  `auth_token_type` enum('optional','mandatory') COLLATE utf8_unicode_ci DEFAULT 'optional',
  `db_access_data` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- 2016-12-21 11:31:46
